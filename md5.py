import os
import hashlib
import sys

basePath = os.getcwd()

with open(basePath + '/addons.xml', "r", encoding="utf-8") as f:
	data = f.read()
with open(basePath + '/addons.xml.md5', "w", encoding="utf-8") as f:
	f.write(hashlib.md5(data.encode('utf-8')).hexdigest())

print ("Updated addons.xml.md5")	
sys.exit(0)


