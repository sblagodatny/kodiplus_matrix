import os
import shutil
import hashlib
import sys
from bs4 import BeautifulSoup



basePath = os.getcwd()
addonsSrcPath = basePath + '/src/'
addonsBuildPath = basePath + '/build/'


addonsXML = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' + "\n" + '<addons>' + "\n"

shutil.rmtree(addonsBuildPath)
os.mkdir(addonsBuildPath)


for addon in os.listdir(addonsSrcPath):
	
	addonSrcPath = addonsSrcPath + addon + '/'
	addonBuildPath = addonsBuildPath + addon + '/'
	
	with open (addonSrcPath + 'addon.xml', 'r', encoding="utf-8" ) as f:
		addonXML = BeautifulSoup(f.read(), "html.parser")
	
	addonVersion = str(addonXML.find('addon')['version'])
	print(addon + ' ' + addonVersion)
	
	addonsXML = addonsXML + str(addonXML.find('addon')) + "\n"
			
	buildFile = addonBuildPath + addon + '-' + addonVersion
	shutil.make_archive(buildFile, 'zip', addonsSrcPath, addon)
	try:
		shutil.copyfile(addonSrcPath + '/icon.png', addonBuildPath + '/icon.png')
		shutil.copyfile(addonSrcPath + '/fanart.jpg', addonBuildPath + '/fanart.jpg')
	except:
		None
	
addonsXML = addonsXML + '</addons>' + "\n"
with open(basePath + '/addons.xml', "w", encoding="utf-8") as f:
	f.write(addonsXML)
with open(basePath + '/addons.xml.md5', "w", encoding="utf-8") as f:
	f.write(hashlib.md5(addonsXML.encode('utf-8')).hexdigest())
		

sys.exit(0)


