import sys
import urllib.parse
import xbmcaddon
import xbmcgui
import xbmcplugin
import xbmcvfs
import util
import json
import shutil



_baseUrl = sys.argv[0]
_handleId = int(sys.argv[1])
_params = dict(urllib.parse.parse_qsl(sys.argv[2][1:]))	

if 'handler' not in _params.keys():	
	_params['handler'] = 'Root'

_addon = xbmcaddon.Addon()
	
_path = {'addon': _addon.getAddonInfo('path')}
_path['img'] = _path['addon'] + 'resources/img/'
_path['settings'] = _path['addon'].replace('addons','userdata/addon_data')
_path['folders'] = _addon.getSetting('foldersPath')
if len(_path['folders']) == 0:
	_path['folders'] = _path['settings']
	
if not xbmcvfs.exists(_path['folders']):
	xbmcvfs.mkdir(_path['settings'])


def handlerAddFolder():
	name = util.input('Folder name')	
	xbmcvfs.mkdir(_path['folders'] + name)	
	xbmc.executebuiltin('Container.Refresh()')


def handlerRemoveFolder():
	if not xbmcgui.Dialog().yesno('Folders', 'Remove folder: ' + _params['folder']):
		return
	dirs, files = xbmcvfs.listdir(_path['folders'] + _params['folder'])
	for file in files:
		xbmcvfs.delete(_path['folders'] + _params['folder'] + '/' + file)	
	xbmcvfs.rmdir(_path['folders'] + _params['folder'])					
	xbmc.executebuiltin('Container.Refresh()')
	
	
def handlerListFolder():
	dirs, files = xbmcvfs.listdir(_path['folders'] + _params['folder'])
	for file in files:
		item = util.fileToObj(_path['folders'] + _params['folder'] + '/' + file)						
		li = xbmcgui.ListItem(item['label'])		
		li.setArt(item['art'])
		if 'plot' in item.keys():			
			li.setInfo(type='video', infoLabels={'plot': item['plot']})				
		contextMenuItems=[]
		cparams = { 'handler': 'RemoveFile', 'folder': _params['folder'], 'file': file}
		contextMenuItems.append(('Remove from folder', 'RunPlugin(' + _baseUrl+'?' + urllib.parse.urlencode(cparams) + ')'))
		li.addContextMenuItems(contextMenuItems, replaceItems=True)	
		
#		xbmcplugin.addDirectoryItem(handle=_handleId, url=_baseUrl+'?'+urllib.parse.urlencode({'handler': 'Exec', 'path': item['path'], 'playable': item['playable'], 'window': item['window']}), listitem=li)

#		util.log(json.dumps(item))
		
		if item['path'].startswith ('androidapp://') or item['window']==10001:
			xbmcplugin.addDirectoryItem(handle=_handleId, url=item['path'], listitem=li, isFolder=False)	
		elif item['playable']=='true':
			li.setProperty('IsPlayable', 'true')
			xbmcplugin.addDirectoryItem(handle=_handleId, url=item['path'], listitem=li, isFolder=False)	
		else:
			xbmcplugin.addDirectoryItem(handle=_handleId, url=item['path'], isFolder=True, listitem=li)

		
	xbmcplugin.endOfDirectory(_handleId)

	
#def handlerExec():	
#	if _params['path'].startswith ('androidapp://'):
#		cmd = 'StartAndroidActivity("{0}")'.format(_params['path'].replace('androidapp://sources/apps/',''))		
#	elif 'playable' in _params.keys():
#		cmd = 'PlayMedia("{0}")'.format(_params['path'])
#	else:
#		cmd = 'ActivateWindow({0},"{1}",return)'.format(_params['window'], _params['path'])				
#
#	xbmc.executebuiltin(cmd)

	


def handlerRemoveFile():
	xbmcvfs.delete(_path['folders'] + _params['folder'] + '/' + _params['file'])	
	xbmc.executebuiltin('Container.Refresh()')
	
	
def handlerRoot():			
	dirs, files = xbmcvfs.listdir(_path['folders'])
	for dir in dirs:
		li = xbmcgui.ListItem(dir)		
		li.setArt({'thumb': _path['img'] + 'folder.png', 'poster': _path['img'] + 'folder.png'})
		cparams = { 'handler': 'RemoveFolder', 'folder': dir}
		contextMenuItems=[('Remove folder', 'RunPlugin(' + _baseUrl+'?' + urllib.parse.urlencode(cparams) + ')')]
		li.addContextMenuItems(contextMenuItems, replaceItems=True)	
		xbmcplugin.addDirectoryItem(handle=_handleId, url=_baseUrl+'?'+urllib.parse.urlencode({'handler': 'ListFolder', 'folder': dir}), isFolder=True, listitem=li)	
	li = xbmcgui.ListItem('Add folder')		
	li.setArt({'thumb': _path['img'] + 'add.png', 'fanart': _path['img'] + 'add.png', 'poster': _path['img'] + 'add.png'})
	xbmcplugin.addDirectoryItem(handle=_handleId, url=_baseUrl+'?'+urllib.parse.urlencode({'handler': 'AddFolder'}), isFolder=False, listitem=li)	
	xbmcplugin.endOfDirectory(_handleId)



globals()['handler' + _params['handler']]()
