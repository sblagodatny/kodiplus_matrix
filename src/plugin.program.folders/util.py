import xbmc
import xbmcaddon
import pickle
import string
import random
import json
import sys
import xbmcaddon
import urllib
import xbmcvfs
import shutil
import base64
import requests


_addon = xbmcaddon.Addon()
_pathSettings = _addon.getAddonInfo('path').replace('addons','userdata/addon_data')
_addonId = _addon.getAddonInfo('id')	

def log(msg, level=xbmc.LOGINFO):
	xbmc.log(xbmcaddon.Addon().getAddonInfo('name') + " : " + msg, level=level)
	
def objToFile(obj, path):
	f = xbmcvfs.File(path, 'wb')
	pickle.dump(obj, f)
	f.close()


def fileToObj(path):
	f = xbmcvfs.File(path, 'rb')
	obj = pickle.loads(f.readBytes())
	f.close()
	return obj


def randoms(length=10):
	return ( ''.join(random.choice(string.ascii_letters) for i in range(length)) )
	
	
def select(title, list, keyName, keyThumb=None):
	lov = []
	useDetails=False
	for item in list:
		li = xbmcgui.ListItem(item[keyName])
		if keyThumb is not None:
			li.setArt({'thumb': item[keyThumb]})
			useDetails=True
		lov.append(li)
	s = xbmcgui.Dialog().select(title, lov,  useDetails=useDetails)
	if s >= 0:			
		return s
	return None	
		
		
def input(title='Input'):
	kb = xbmc.Keyboard('', title, True)
	kb.setHiddenInput(False)
	kb.doModal()
	if not kb.isConfirmed():
		del kb
		return None
	text = kb.getText()
	del kb
	return text

			
def b64encode(str):
	return base64.b64encode(bytes(str,'utf-8'))
	
def b64decode(str):
	return base64.b64decode(str).decode("utf-8")		

	
#com.ghisler.android.TotalCommander		
def getGooglePlayIcon(app):
	content = requests.Session().get('https://play.google.com/store/apps/details?id=' + app).text
	return 'https://play-lh.googleusercontent.com' + content.split('https://play-lh.googleusercontent.com')[3].split('"')[0]


