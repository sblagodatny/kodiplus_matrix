import sys
import xbmcaddon
import xbmcgui
import xbmc
import xbmcvfs
import urllib.parse
import util
import json




_addon = xbmcaddon.Addon()
	
_path = {'addon': _addon.getAddonInfo('path')}
_path['img'] = _path['addon'] + 'resources/img/'
_path['settings'] = _path['addon'].replace('addons','userdata/addon_data')
_path['folders'] = _addon.getSetting('foldersPath')
if len(_path['folders']) == 0:
	_path['folders'] = _path['settings']
	
if not xbmcvfs.exists(_path['folders']):
	xbmcvfs.mkdir(_path['folders'])


def addToFolder(item):	
	folders, files = xbmcvfs.listdir(_path['folders'])
	if len(folders)==0:
		xbmcgui.Dialog().select('Folders', 'Please add folders')
		return	
	s = xbmcgui.Dialog().select('Select Folder', folders)
	if s < 0:
		return
	util.objToFile(item, _path['folders'] + '/' + folders[s] + '/' + util.randoms(15))
	xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%(folders[s], 'Added', 1, _addon.getAddonInfo('icon')))

	
def getItem():	
	li = sys.listitem
	item = {
		'window': xbmcgui.getCurrentWindowId(),
		'path': li.getPath(),
		'label': li.getLabel(),		
		'art': {'thumb': li.getArt('thumb'), 'poster': li.getArt('poster'), 'fanart': li.getArt('fanart')},
		'playable': li.getProperty('IsPlayable')
	}	
	plot = li.getVideoInfoTag().getPlot()
	if len(plot) > 0:
		item['plot'] = plot		
	if item['path'].startswith('androidapp://'):					
		try:
			app = item['path'].replace('androidapp://sources/apps/','')
			thumbUrl = util.getGooglePlayIcon(app)
			item['art'] = {'thumb': thumbUrl, 'poster': thumbUrl, 'fanart': thumbUrl}
		except:
			item['art'] = {'thumb': item['art']['thumb'], 'poster': item['art']['thumb'], 'fanart': item['art']['thumb']}			
	return item
	
	
	
addToFolder(getItem())
