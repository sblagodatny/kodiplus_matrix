import time
import xbmc
import xbmcaddon
#import SimpleHTTPServer
#import BaseHTTPServer
import http.server
import traceback
import threading
from urllib.parse import urlparse, parse_qs
import importlib
import urllib
import base64


_addon = xbmcaddon.Addon()
_path = _addon.getAddonInfo('path') + '/'
_port = int(_addon.getSetting('port'))

class MyHttpRequestHandler (http.server.SimpleHTTPRequestHandler):
	def do_GET(self):
		try:									
			params = parse_qs(urlparse(self.path).query)
			if 'handler' not in params.keys():
				raise Exception ('Invalid request')							
			if params['handler'][0] == 'dash':			
				with open(urllib.unquote(params['mpd'][0]), 'r') as f:
					manifest = f.read()
				self.send_response(200)
				self.send_header("Content-type", 'application/octet-stream')
				self.send_header("Content-Length", len(manifest))
				self.end_headers()				
				self.wfile.write(manifest)
			else:
				handler = importlib.import_module('handler' + params['handler'][0])
				url = handler.resolve(params)
				self.send_response(302)
				self.send_header('Location',url)
				self.end_headers()
		except:
			exc_type, exc_value, exc_traceback = sys.exc_info()
			log(traceback.format_exc(),xbmc.LOGERROR)		
			try:
				self.send_response(404)
				self.end_headers()
			except:
				None
		

	def log_message(self, format, *args):
		return		
    

def log(message,loglevel=xbmc.LOGINFO):
	xbmc.log('service.urlresolver' + " : " + message,level=loglevel)



server = http.server.HTTPServer(('localhost', _port) , MyHttpRequestHandler)
thread = threading.Thread(target = server.serve_forever)
thread.deamon = True
thread.start()
log('Started on port ' + str(_port))
monitor = xbmc.Monitor()
monitor.waitForAbort(0)
server.shutdown()
log('Ended')
