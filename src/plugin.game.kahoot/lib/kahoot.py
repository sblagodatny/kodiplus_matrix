_url = 'https://create.kahoot.it'


def parse(cards, account):
	done = []
	if account is not None:
		done = getDone(account)
	result = []		
	for item in cards:		
		result.append({
			'id': item['card']['uuid'],
			'title': item['card']['title'],
			'poster': item['card'].get('cover', ''),
			'total_questions': item['card']['number_of_questions'],
			'total_players': item['card']['number_of_players'],
			'description': item['card']['description'],
			'ts_updated': item['card']['modified'],
			'creator': {'id': item['card']['creator'], 'name': item['card']['creator_username']},
			'sample_questions': [q['title'].replace('<b>','').replace('</b>','').replace('<i>','').replace('</i>','').replace('&nbsp;','') for q in item['card']['sample_questions']],
			'done': item['card']['uuid'] in done
		})				
	return result
	
	
def search(criteria, account=None):
	from helpers import http
	session = http.initSession()	
	params = {
		'query': criteria['title'],
		'limit': '100',
		'orderBy': 'quality', # number_of_players, relevance
#		'cursor': '20',
		'searchCluster': '1',
#		'includeExtendedCounters': 'false',
		'inventoryItemId': 'NONE'
	}
	data = session.get(_url + '/rest/kahoots/', params=params).json()	
	return parse(data['entities'], account)
	

def getFavorites(account):
	from helpers import http		
	session = http.initSession()	
	params = {
		'limit': '50',
		'includeKahoot': 'true'
	}
	headers = {
		'Authorization': 'Bearer ' + account['access_token']
	}
	data = session.get(_url + '/rest/kahoots/browse/favourites/cards', params=params, headers=headers).json()
	return parse(data['entities'], account)
	

def getDone(account):
	from helpers import http		
	session = http.initSession()	
	params = {
		'userId': account['id'],
		'resultType': 'LIVE_GAME',
		'searchMode': 'HOST',
		'limit': '100',
		'orderBy': 'time',
		'reverse': 'true',
		'query': ''
	}
	headers = {
		'Authorization': 'Bearer ' + account['access_token']
	}
	data = session.get(_url + '/rest/results/browse', params=params, headers=headers).json()
	return [item['kahootId'] for item in data['entities']]
	
	
	

def validateAccount(account):	
	from helpers import http
	import json	
	session = http.initSession()		
	headers = {
		'X-Requested-With': 'no.mobitroll.kahoot.android',
		'User-Agent': 'Mozilla/5.0 (Linux; Android 11; sdk_gphone_x86_64 Build/RSR1.201211.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/132.0.6834.123 Mobile Safari/537.36',
		'Referer': 'https://create.kahoot.it/',		
	}
	params = {
		'scope': 'openid+device_sso',
		'state': '879278280',
		'redirect_uri': 'kahoot%3A%2F%2Fauthenticate%2Fsuccess',
		'code_challenge': '5NcsMmDF5DnJOPGkMkxfmEb4uV42hkbFKihhXH9LnWU',
		'code_challenge_method': 'S256',
		'client_id': 'pugfZFXTpaBKzXz79UsyAxbhDTKCBb6AG6k5',
		'response_type': 'code',
		'force_redirect': 'true'
	}
	data = {
		"username": account['username'],
		"password": account['password'],
		"grant_type": "password"
	}
	reply = session.post('https://create.kahoot.it/rest/authenticate', params=params,  json=data, headers=headers, allow_redirects=False)
	if 'Location' not in reply.headers.keys():
		raise Exception('Invalid credentials')		
	url = reply.headers['Location']	
	client_id = url.split('client_id=')[1]
	reply = session.get(url, headers=headers, allow_redirects=False)	
	code = reply.headers['Location'].split('?code=')[1].split('&')[0]		
	data = {
		'grant_type': 'authorization_code',
		'code': code,
		'redirect_uri': 'kahoot://authenticate/success',
		'client_id': client_id,
		'code_verifier': 'shq-_QvVBbPww2J-pKjvD4LRcVLM25iWGTfi5t.XPmN'
	}
	headers = {'User-Agent': 'Kahoot/6.2.5.2457 (no.mobitroll.kahoot.android) (Android 11)'}
	reply = session.post('https://create.kahoot.it/rest/oauth2/token', headers=headers, data=data).json()
	account['access_token'] = reply['access_token']
	account['validDays'] = 5
	account['id']='0d83b01d-0a7c-4ebe-928a-a46b94fb042f'
	account['title']='Stas Blagodatny'
	account['thumb']=''
	
	
	
	
#def get(id):
#	from helpers import http, storage
#	import json		
#	result = []		
#	session = http.initSession()		
#	data = session.get(_url + '/rest/kahoots/' + id + '/card/', params={'includeKahoot': True}).json()
#	storage.log(json.dumps(data))
#	return result


#https://create.kahoot.it/rest/channels/users/0d83b01d-0a7c-4ebe-928a-a46b94fb042f/followed?limit=20	