import sys, xbmc, xbmcgui, xbmcplugin, xbmcaddon, os, json
import urllib.parse as parse
from lib import kahoot
from helpers import string, storage


_baseUrl = sys.argv[0]
_handleId = int(sys.argv[1])
_params = dict(parse.parse_qsl(sys.argv[2][1:]))	

_addon = xbmcaddon.Addon()
_pathAddon = _addon.getAddonInfo('path')
_pathSettings = _pathAddon.replace('addons','userdata/addon_data')
_pathImg = _pathAddon + '/resources/img/'
_pathAccount = _pathSettings + 'accounts.json'


if 'handler' not in list(_params.keys()):
	_params['handler'] = 'Root'

if not os.path.exists(_pathSettings):
	os.mkdir(_pathSettings)


def buildListItem(data):	
	import xbmcgui
	li = xbmcgui.ListItem(data['label'])
	if 'thumb' in data.keys() and data['thumb'] is not None and len(data['thumb'])>0:
		li.setArt({'thumb': data['thumb']})
	if 'fanart' in data.keys() and data['fanart'] is not None and len(data['fanart'])>0:
		li.setArt({'fanart': data['fanart']})
	return li


def handlerPlay():
	from helpers import storage	
	quiz = json.loads(string.b64decode(_params['kahoot']))	
#	url = "https://create.kahoot.it/preview/{}".format(quiz['id'])
	url = 'https://play.kahoot.it/v2?quizId={}'.format(quiz['id'])
	if xbmc.getCondVisibility('system.platform.android'):	
		xbmc.executebuiltin('StartAndroidActivity(,android.intent.action.VIEW,,{})'.format(url) )
	else:
		import webbrowser		
		webbrowser.open(url, new=0, autoraise=True)
	

def listKahoots(cilist):
	from datetime import datetime
	xbmcplugin.setContent(_handleId, 'games')	
	for ci in cilist:		
		item = buildListItem({'label': ci['title'], 'thumb': ci['poster']})							
		item.setIsFolder(False)	
		params = {'handler': 'Play', 'kahoot': string.b64encode(json.dumps(ci)) }						
		info = item.getMusicInfoTag()			
		info.setComment("[B]Questions: {}, Players: {}, Date: {}[/B]\n\n{}".format(
			ci['total_questions'], 
			ci['total_players'], 
			datetime.fromtimestamp(ci['ts_updated']/1000).strftime("%d/%m/%Y"),
			"\n\n".join(ci['sample_questions']))
		)	
		if ci['done'] is True:
			item.setLabel('[COLOR green]{}[/COLOR]'.format(item.getLabel()))
		xbmcplugin.addDirectoryItem(_handleId, _baseUrl+'?' + parse.urlencode(params), item, item.isFolder())		
	xbmcplugin.endOfDirectory(_handleId)


def handlerSearch():		
	from helpers import account	
	criteria = json.loads(string.b64decode(_params['criteria']))	
	result = kahoot.search(criteria, account.get(_pathAccount, kahoot.validateAccount))
	listKahoots(result)			


def handlerSearchInput():	
	from helpers import gui, string
	import json
	text=gui.inputWithHistory('Search Kahoots', _pathSettings + 'history', 15)	
	if text is None or len(text) < 1:
		return	
	params = {
		'handler': 'Search',
		'criteria': string.b64encode(json.dumps({'title': text}))		
	}	
	xbmc.executebuiltin("Dialog.Close(all, true) ")
	cmd = 'ActivateWindow(Games, {}, return)'.format(_baseUrl+'?' + parse.urlencode(params))
	xbmc.executebuiltin(cmd)
	

def handlerFavorites():
	from helpers import account	
	kahoots = kahoot.getFavorites(account.get(_pathAccount, kahoot.validateAccount))
	listKahoots(kahoots)		
	

def handlerAccount():	
	from helpers import account	
	account.open(_pathAccount, kahoot.validateAccount, 'Kahoot Accounts', 'credentials')

	
def handlerRoot():				
	from helpers import account	
	
	item=buildListItem({'label': 'Search', 'thumb': _pathImg + 'search.png', 'fanart': _pathAddon + 'fanart.jpg'})
	xbmcplugin.addDirectoryItem(handle=_handleId, url=_baseUrl+'?'+parse.urlencode({'handler': 'SearchInput'}), isFolder=False, listitem=item)			
	if account.get(_pathAccount, kahoot.validateAccount) is not None:
		item=buildListItem({'label': 'Favorites', 'thumb': _pathImg + 'heart.png', 'fanart': _pathAddon + 'fanart.jpg'})
		xbmcplugin.addDirectoryItem(handle=_handleId, url=_baseUrl+'?'+parse.urlencode({'handler': 'Favorites'}), isFolder=True, listitem=item)			
	item=buildListItem({'label': 'Account', 'thumb': _pathImg + 'user.png', 'fanart': _pathAddon + 'fanart.jpg'})
	xbmcplugin.addDirectoryItem(handle=_handleId, url=_baseUrl+'?'+parse.urlencode({'handler': 'Account'}), isFolder=False, listitem=item)			
	xbmcplugin.endOfDirectory(_handleId)
	
	
	
if 'handler' in _params.keys():
	globals()['handler' + _params['handler']]()
else:
	handlerRoot()
