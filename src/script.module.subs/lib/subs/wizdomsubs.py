import json
from helpers import http, storage

_api = 'https://wizdom.xyz/api/'
_timeout = 2


def get(data):		
	if 'imdbId' not in data.keys():
		return []
	s = http.initSession()	
	content = s.get(_api + 'releases/' + data['imdbId'], timeout=_timeout).text
	if 'Not Found' in content:
		return []
	result = json.loads(content)
	if 'season' in data.keys() and 'episode' in data.keys():
		result =  result['subs'][str(data['season'])][str(data['episode'])]
	else:
		result = result['subs']					
	return [{'language': 'Hebrew', 'url': '{}files/sub/{}'.format(_api, sub['id']), 'label': sub['version'] + '.srt', 'zip': True, 'format': 'srt'} for sub in result]

