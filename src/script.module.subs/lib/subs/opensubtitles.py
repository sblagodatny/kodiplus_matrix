import json
from helpers import http, storage


_api = 'https://rest.opensubtitles.org'
_timeout = 2

def get(data):
	if 'imdbId' not in data.keys():
		return []
	s = http.initSession()	
	if 'season' in data.keys() and 'episode' in data.keys():
		url = '{}/search/episode-{}/imdbid-{}/season-{}'.format(_api, data['episode'], data['imdbId'].replace('tt',''), data['season'])
	else:
		url = '{}/search/imdbid-{}'.format(_api, data['imdbId'].replace('tt',''))
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded; charset=urf-8',
		'X-User-Agent': 'trailers.to-UA'
	}		
	result = s.get(url, headers = headers, timeout=_timeout).json()
	return [{'language': sub['LanguageName'], 'url': sub['ZipDownloadLink'], 'label': sub['SubFileName'], 'zip': True, 'format': 'srt'} for sub in result]
