import json
from helpers import http, storage
from bs4 import BeautifulSoup


_url = 'https://yifysubtitles.ch'
_timeout = 3
		


def get(data):		
	if 'imdbId' not in data.keys():
		return []
	if 'episode' in data.keys():
		return []
	s = http.initSession()		
	soup = BeautifulSoup(s.get(_url + '/movie-imdb/' + data['imdbId'], timeout=_timeout).text, "html.parser")			
	subs = []
	for tag in soup.find('tbody').find_all('tr'):
		labels = tag.find('a').get_text().replace('subtitle ','')
		if '\n' in labels:
			labels = labels.split('\n')
		else:
			labels = [labels]
		for label in labels:
			item = {
				'language': tag.find('span', class_="sub-lang").get_text().lstrip().rstrip(), 
				'url': _url + tag.find('a')['href'].replace('/subtitles/','/subtitle/') + '.zip', 						
				'label': label + '', 
				'zip': True, 
				'format': 'srt'
			}
			item['label']=item['label']+ '-{}.srt'.format(item['language'])
			subs.append(item)						
	return subs
	