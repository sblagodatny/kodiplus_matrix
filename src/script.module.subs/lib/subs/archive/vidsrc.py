# -*- coding: utf-8 -*-


import re
from helpers import storage, http
import json



_timeout = 2


def get(data):		
	s = http.initSession()		
	if data['type']=='movie':
		content = s.get('https://vidsrc.to/embed/movie/{}'.format(data['id']), timeout=_timeout ).text
	else:
		content = s.get('https://vidsrc.to/embed/tv/{}/{}/{}'.format(data['imdbId'], data['season'], data['episode']), timeout=_timeout ).text
	dataId = re.search(r'data-id="(.*?)"', content).group(1)	
	subs = s.get('https://vidsrc.to/ajax/embed/episode/{}/subtitles'.format(dataId), headers={'Referer': 'https://vidstream.pro/'} ).json()	
	return [{'language': sub['label'], 'url': sub['file'], 'format': 'vtt', 'name': 'VidSrc', 'zip': False} for sub in subs]
