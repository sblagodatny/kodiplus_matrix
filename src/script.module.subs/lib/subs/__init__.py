
_sources = ['wizdomsubs', 'opensubtitles','yifysubs']
_timeout = 2


def getLib(provider):
	import importlib
	return importlib.import_module('.' + provider, 'subs')


def get(data):		
	from helpers import storage		
	import concurrent.futures
	result = {}
	def task(source):
		try:
			r = getLib(source).get(data)
			for item in r:
				item['source'] = source
			result[source] = r
		except Exception as e:
			storage.log('Error getting subs from {}: {}'.format(source,str(e)))
	ex = concurrent.futures.ThreadPoolExecutor(max_workers=10)		
	for source in _sources:			
		ex.submit(task, source)		
	ex.shutdown(wait=True)
	result2 = []
	for subs in result.values():
		result2 = result2 + subs
	return result2
	

def filter(subtitles, label, languages=['Hebrew', 'English', 'Russian']):
	from helpers.fuzzywuzzy import fuzz
	from helpers import storage			
	import json	
	def cleanse(label):
		return label.lower().replace('.',' ').replace('-',' ').replace('_',' ').replace('[',' ').replace(']',' ').replace('(',' ').replace(')',' ').replace('  ',' ').replace('  ',' ')
	def priority(sub):	
		try:
			if sub['source'] == 'original':
				return 101
			return fuzz.token_set_ratio(cleanse(sub['label']), cleanse(label))		
		except Exception as e:
			storage.log(str(e))
			return 0			
	result = []
	for language in languages:
		subs = [sub for sub in subtitles if sub['language']==language]
		subs = sorted(subs, key=lambda d: priority(d) ) 	
		if len(subs)>0:
			result.append(subs[-1])	
	return result
	
		
def download(subtitles):
	import xbmcaddon, xbmcvfs
	import os, io, json
	from contextlib import closing
	from zipfile import ZipFile
	from helpers import storage, http
	from helpers.fuzzywuzzy import fuzz	
	addon = xbmcaddon.Addon('script.module.subs')
	pathTemp = addon.getAddonInfo('path').replace('addons','userdata/addon_data') + '/temp/'
	if not xbmcvfs.exists(pathTemp):
		xbmcvfs.mkdirs(pathTemp)
	for file in xbmcvfs.listdir(pathTemp)[1]:
		xbmcvfs.delete(pathTemp + file)			
	import concurrent.futures
	result = []			
	def task(sub):
		s = http.initSession()
		path = pathTemp + sub['source'] + '.' + sub['language'] + '.' + sub['format']
		if sub['zip'] is False:	
			with open(path, "wb") as f:
				f.write(s.get(sub['url'], timeout=_timeout).content)
		else:
			with closing(ZipFile(io.BytesIO(s.get(sub['url']).content))) as z:
				subf = z.namelist()
				subf = sorted(subf, key=lambda d: fuzz.token_set_ratio(d.lower(), sub['label'].lower())	 )
				subf = subf[-1]	
				with open(path, "wb") as f:
					f.write(z.read(subf))
		result.append(path)				
	ex = concurrent.futures.ThreadPoolExecutor(max_workers=10)		
	for sub in subtitles:			
		ex.submit(task, sub)		
	ex.shutdown(wait=True)	
	return result


	
	