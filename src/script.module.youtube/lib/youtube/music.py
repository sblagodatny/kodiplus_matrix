from . import common

		
def musicResponsiveListItemRenderer(data, category):
	result = []
	for item in data:										
		if 'musicResponsiveListItemRenderer' not in item.keys():														
			continue		
		item = item['musicResponsiveListItemRenderer']				
		try:			
			resultitem = {					
				'title': item['flexColumns'][0]['musicResponsiveListItemFlexColumnRenderer']['text']['runs'][0]['text'],					
				'thumb': common.posterUrl(item['thumbnail']['musicThumbnailRenderer']['thumbnail']['thumbnails'][0]['url'],540),				
#				'type': 'song' if len(item['flexColumns'])==3 else 'artist' if item['flexColumns'][1]['musicResponsiveListItemFlexColumnRenderer']['text']['runs'][0]['text']=='Artist' else 'video'
				'type': category
			}	
			if resultitem['type']=='song':
				resultitem['id'] = item['playlistItemData']['videoId']
				resultitem['duration'] = int(common.timeStrToSeconds(item['flexColumns'][1]['musicResponsiveListItemFlexColumnRenderer']['text']['runs'][-1]['text']))
				resultitem['artist'] = {'id': 'dummy', 'name': item['flexColumns'][1]['musicResponsiveListItemFlexColumnRenderer']['text']['runs'][0]['text']}												
#				resultitem['artist'] = {'id': item['flexColumns'][1]['musicResponsiveListItemFlexColumnRenderer']['text']['runs'][0]['navigationEndpoint']['browseEndpoint']['browseId'], 'name': item['flexColumns'][1]['musicResponsiveListItemFlexColumnRenderer']['text']['runs'][0]['text']}												
				if len([s for s in result if s['title']==resultitem['title'] and s['artist']==resultitem['artist']])>0:
					continue		
			elif resultitem['type']=='video':
				resultitem['id'] = item['playlistItemData']['videoId']
				resultitem['duration'] = int(common.timeStrToSeconds(item['flexColumns'][1]['musicResponsiveListItemFlexColumnRenderer']['text']['runs'][-1]['text']))
			elif resultitem['type']=='artist':
				resultitem['id'] = 'UC' + item['navigationEndpoint']['browseEndpoint']['browseId'][2:]
#				resultitem['channelId']=item['menu']['menuRenderer']['items'][-2]['toggleMenuServiceItemRenderer']['toggledServiceEndpoint']['subscribeEndpoint']['channelIds'][0]									
			elif resultitem['type']=='playlist':										
				resultitem['id'] = item['navigationEndpoint']['browseEndpoint']['browseId'][2:]
				resultitem['channel'] = {'id': item['flexColumns'][1]['musicResponsiveListItemFlexColumnRenderer']['text']['runs'][0]['navigationEndpoint']['browseEndpoint']['browseId'], 'title': item['flexColumns'][1]['musicResponsiveListItemFlexColumnRenderer']['text']['runs'][0]['text']}
			result.append(resultitem)	
		except Exception as e:			
			continue								
	return result


def musicTwoRowItemRenderer(data):
	result = []
	for item in data:										
		if 'musicTwoRowItemRenderer' not in item.keys():														
			continue		
		item = item['musicTwoRowItemRenderer']				
		try:			
			resultitem = {					
				'title': item['title']['runs'][0]['text'],					
				'thumb': common.posterUrl(item['thumbnailRenderer']['musicThumbnailRenderer']['thumbnail']['thumbnails'][0]['url'],540),				
				'id': item['navigationEndpoint']['browseEndpoint']['browseId'][2:]
			}														
			resultitem['title'] = resultitem['title'].replace(" [Audio]","")
			if resultitem['id'].startswith("LM") or resultitem['id'].startswith("SE") or resultitem['title'].endswith(" [Video]"):
				continue			
			result.append(resultitem)	
		except Exception as e:			
			None
	return result


def playlistPanelVideoRenderer(data):
	result = []
	for item in data:										
		if 'playlistPanelVideoRenderer' not in item.keys():														
			continue		
		item = item['playlistPanelVideoRenderer']	
		try:			
			resultitem = {					
				'id': item['videoId'],
				'title': item['title']['runs'][0]['text'],					
				'thumb': common.posterUrl(item['thumbnail']['thumbnails'][0]['url'],720),
				'duration': int(common.timeStrToSeconds(item['lengthText']['runs'][0]['text'])),
				'type': 'video'
			}
			try:
				year = int(item['longBylineText']['runs'][-1]['text'])
				resultitem['type'] = 'song'
			except:
				None
			if resultitem['type']=='song':
				resultitem['artist'] = {'id': item['longBylineText']['runs'][0]['navigationEndpoint']['browseEndpoint']['browseId'], 'name': item['longBylineText']['runs'][0]['text']}																	
				resultitem['year'] = item['longBylineText']['runs'][-1]['text']
			result.append(resultitem)	
		except Exception as e:			
			continue								
	return result	
	

	
def search(text, category='auto', language='auto', region='IL', continuation=None, resultsCountLimit=45, resultsCountCurrent=0):				
	categories = {
		'song': {'params': 'EgWKAQIIAWoQEAMQBBAJEAoQBRAREBAQFQ%3D%3D'},	
		'video': {'params': 'EgWKAQIQAWoQEAMQBBAJEAoQBRAREBAQFQ%3D%3D'},
		'playlist': {'params': 'EgeKAQQoAEABahAQAxAEEAkQChAFEBEQEBAV'}		
	}
	data = {"query": text}
	if continuation is not None:
		data['continuation'] = continuation
	if category != 'auto':
		data['params'] = categories[category]['params']		
	data = common.post(action='search', data=data, region=region, language=common.getLanguage(text) if language=='auto' else language, clientId=100)		
	if category != 'auto':					
		if continuation is None:		
			data = data['contents']['tabbedSearchResultsRenderer']['tabs'][0]['tabRenderer']['content']['sectionListRenderer']['contents'][0]			
			if 'musicShelfRenderer' not in data.keys():
				return []
			data = data['musicShelfRenderer']
		else:
			data = data['continuationContents']['musicShelfContinuation']						
		result = musicResponsiveListItemRenderer(data['contents'], category)				
		if 'continuations' not in data.keys() or resultsCountCurrent + len(result) >= resultsCountLimit:
			return result
		return result + search(text, category, language, region, continuation=data['continuations'][0]['nextContinuationData']['continuation'], resultsCountCurrent=resultsCountCurrent+len(result) )			
	else: # best match		
		data = data['contents']['tabbedSearchResultsRenderer']['tabs'][0]['tabRenderer']['content']['sectionListRenderer']['contents'][0]['musicCardShelfRenderer']['title']['runs'][0]		
		title = data['text']
		if 'watchEndpoint' in data['navigationEndpoint'].keys():		
			return {'id': data['navigationEndpoint']['watchEndpoint']['videoId'], 'title': title} 
		elif 'browseEndpoint' in data['navigationEndpoint'].keys():								
			data = post('browse', {"browseId": data['navigationEndpoint']['browseEndpoint']['browseId']}) 
			id = data['contents']["singleColumnBrowseResultsRenderer"]["tabs"][0]["tabRenderer"]['content']["sectionListRenderer"]['contents'][0]["musicShelfRenderer"]['contents'][0]["musicResponsiveListItemRenderer"]["flexColumns"][0]["musicResponsiveListItemFlexColumnRenderer"]['text']['runs'][0]['navigationEndpoint']['watchEndpoint']['videoId']			
			return {'id': id, 'title': title} 
	

def getAccountPlaylists(account):
	data = common.post(action='browse', data={'browseId': 'FEmusic_liked_playlists'}, account=account, clientId=100)				
	data = data["contents"]["singleColumnBrowseResultsRenderer"]["tabs"][0]["tabRenderer"]["content"]["sectionListRenderer"]["contents"][0]["gridRenderer"]["items"]
	return musicTwoRowItemRenderer(data)	
		

def getItemPlaylists(account, trackId):
	from . import web
	playlistsItemExists = web.getPlaylistsItemExists(account, trackId)
	result = getAccountPlaylists(account)
	for pl in result:
		pl['itemExists'] = pl['id'] in playlistsItemExists
	return result



def getPlaylistItems(playlistId, continuation=None, resultsCountLimit=45, resultsCountCurrent=0): 	
	data = {"playlistId": playlistId, "params": '8gEAmgMDCNgE'}
	if continuation is not None:
		data['continuation'] = continuation	
	data = common.post(action='next', data=data, clientId=100)		
	if continuation is None:
		data = data['contents']['singleColumnMusicWatchNextResultsRenderer']['tabbedRenderer']['watchNextTabbedResultsRenderer']['tabs'][0]['tabRenderer']['content']['musicQueueRenderer']
		if 'content' not in data.keys():
			return []			
		data = data['content']['playlistPanelRenderer']
	else:
		data = data['continuationContents']['playlistPanelContinuation']		
	result = playlistPanelVideoRenderer(data['contents'])	
	if 'continuations' not in data.keys() or resultsCountCurrent + len(result) >= resultsCountLimit:
		return result
	else:				
		if 'nextContinuationData' in data['continuations'][0].keys():
			continuation = data['continuations'][0]['nextContinuationData']['continuation']
		elif 'nextRadioContinuationData' in data['continuations'][0].keys():
			continuation = data['continuations'][0]['nextRadioContinuationData']['continuation']
		else:
			return result
		return result + getPlaylistItems(playlistId, continuation=continuation, resultsCountCurrent=resultsCountCurrent+len(result) )
	

def getNextPlayItems(track, region='IL'):	
	data = common.post(action='next', data={'videoId': track['id']}, region=region, clientId=100)			
	playlistId = data['contents']['singleColumnMusicWatchNextResultsRenderer']['tabbedRenderer']['watchNextTabbedResultsRenderer']['tabs'][0]['tabRenderer']['content']['musicQueueRenderer']['content']['playlistPanelRenderer']['contents'][1]['automixPreviewVideoRenderer']['content']['automixPlaylistVideoRenderer']['navigationEndpoint']['watchPlaylistEndpoint']['playlistId']
	return getPlaylistItems(playlistId)
	
	
def getLyrics(track):	
	data = common.post(action='next', data={'videoId': track['id']}, clientId=100)				
	browseId = data["contents"]["singleColumnMusicWatchNextResultsRenderer"]["tabbedRenderer"]["watchNextTabbedResultsRenderer"]["tabs"][1]["tabRenderer"]["endpoint"]["browseEndpoint"]['browseId']	
	data = common.post(action='browse', data={"browseId": browseId}, clientId=100)				
	try:
		result =  data['contents']['sectionListRenderer']['contents'][0]['musicDescriptionShelfRenderer']['description']['runs'][0]['text']		
	except:
		return None
	

	
	
	