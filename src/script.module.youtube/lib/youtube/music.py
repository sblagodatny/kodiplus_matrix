from . import common

_url = 'https://music.youtube.com'
_context = {"client": {"clientName": "WEB_REMIX", "clientVersion": "1.20240918.01.00", "gl": "IL", "hl": "en"}}	
		
		
def musicResponsiveListItemRenderer(data, category):
	result = []
	for item in data:										
		if 'musicResponsiveListItemRenderer' not in item.keys():														
			continue		
		item = item['musicResponsiveListItemRenderer']				
		try:			
			resultitem = {					
				'title': item['flexColumns'][0]['musicResponsiveListItemFlexColumnRenderer']['text']['runs'][0]['text'],					
				'thumb': common.posterUrl(item['thumbnail']['musicThumbnailRenderer']['thumbnail']['thumbnails'][0]['url'],540),				
#				'type': 'song' if len(item['flexColumns'])==3 else 'artist' if item['flexColumns'][1]['musicResponsiveListItemFlexColumnRenderer']['text']['runs'][0]['text']=='Artist' else 'video'
				'type': category
			}	
			if resultitem['type']=='song':
				resultitem['id'] = item['playlistItemData']['videoId']
				resultitem['duration'] = int(common.timeStrToSeconds(item['flexColumns'][1]['musicResponsiveListItemFlexColumnRenderer']['text']['runs'][-1]['text']))
				resultitem['artist'] = {'id': 'dummy', 'name': item['flexColumns'][1]['musicResponsiveListItemFlexColumnRenderer']['text']['runs'][0]['text']}												
#				resultitem['artist'] = {'id': item['flexColumns'][1]['musicResponsiveListItemFlexColumnRenderer']['text']['runs'][0]['navigationEndpoint']['browseEndpoint']['browseId'], 'name': item['flexColumns'][1]['musicResponsiveListItemFlexColumnRenderer']['text']['runs'][0]['text']}												
				if len([s for s in result if s['title']==resultitem['title'] and s['artist']==resultitem['artist']])>0:
					continue		
			elif resultitem['type']=='video':
				resultitem['id'] = item['playlistItemData']['videoId']
				resultitem['duration'] = int(common.timeStrToSeconds(item['flexColumns'][1]['musicResponsiveListItemFlexColumnRenderer']['text']['runs'][-1]['text']))
			elif resultitem['type']=='artist':
				resultitem['id'] = 'UC' + item['navigationEndpoint']['browseEndpoint']['browseId'][2:]
#				resultitem['channelId']=item['menu']['menuRenderer']['items'][-2]['toggleMenuServiceItemRenderer']['toggledServiceEndpoint']['subscribeEndpoint']['channelIds'][0]									
			elif resultitem['type']=='playlist':										
				resultitem['id'] = item['navigationEndpoint']['browseEndpoint']['browseId'][2:]
				resultitem['channel'] = {'id': item['flexColumns'][1]['musicResponsiveListItemFlexColumnRenderer']['text']['runs'][0]['navigationEndpoint']['browseEndpoint']['browseId'], 'title': item['flexColumns'][1]['musicResponsiveListItemFlexColumnRenderer']['text']['runs'][0]['text']}
			result.append(resultitem)	
		except Exception as e:			
			continue								
	return result


def playlistPanelVideoRenderer(data):
	result = []
	for item in data:										
		if 'playlistPanelVideoRenderer' not in item.keys():														
			continue		
		item = item['playlistPanelVideoRenderer']	
		try:			
			resultitem = {					
				'id': item['videoId'],
				'title': item['title']['runs'][0]['text'],					
				'thumb': common.posterUrl(item['thumbnail']['thumbnails'][0]['url'],720),
				'duration': int(common.timeStrToSeconds(item['lengthText']['runs'][0]['text'])),
				'type': 'video'
			}
			try:
				year = int(item['longBylineText']['runs'][-1]['text'])
				resultitem['type'] = 'song'
			except:
				None
			if resultitem['type']=='song':
				resultitem['artist'] = {'id': item['longBylineText']['runs'][0]['navigationEndpoint']['browseEndpoint']['browseId'], 'name': item['longBylineText']['runs'][0]['text']}																	
				resultitem['year'] = item['longBylineText']['runs'][-1]['text']
			result.append(resultitem)	
		except Exception as e:			
			continue								
	return result	
	

def post(action, data, region='IL', language='en'):
	s = common.initSession()		
	headers = {'X-Goog-Visitor-Id': '', 'Referer': _url, 'Origin': _url}	
	context = _context
	context['client']['gl'] = region
	context['client']['hl'] = language	
	data['context']=context
	return s.post(url=_url+'/youtubei/v1/{}'.format(action), params = {'prettyPrint': False}, json=data, headers=headers).json()

	
def search(text, category='auto', language='auto', region='IL', continuation=None, resultsCountLimit=45, resultsCountCurrent=0):				
	categories = {
		'song': {'params': 'EgWKAQIIAWoQEAMQBBAJEAoQBRAREBAQFQ%3D%3D'},	
		'video': {'params': 'EgWKAQIQAWoQEAMQBBAJEAoQBRAREBAQFQ%3D%3D'},
		'playlist': {'params': 'EgeKAQQoAEABahAQAxAEEAkQChAFEBEQEBAV'}		
	}
	data = {"query": text}
	if continuation is not None:
		data['continuation'] = continuation
	if category != 'auto':
		data['params'] = categories[category]['params']		
	data = post('search', data, region=region, language=common.getLanguage(text) if language=='auto' else language)	
	if category != 'auto':					
		if continuation is None:		
			data = data['contents']['tabbedSearchResultsRenderer']['tabs'][0]['tabRenderer']['content']['sectionListRenderer']['contents'][0]			
			if 'musicShelfRenderer' not in data.keys():
				return []
			data = data['musicShelfRenderer']
		else:
			data = data['continuationContents']['musicShelfContinuation']						
		result = musicResponsiveListItemRenderer(data['contents'], category)				
		if 'continuations' not in data.keys() or resultsCountCurrent + len(result) >= resultsCountLimit:
			return result
		return result + search(text, category, language, region, continuation=data['continuations'][0]['nextContinuationData']['continuation'], resultsCountCurrent=resultsCountCurrent+len(result) )			
	else: # best match		
		data = data['contents']['tabbedSearchResultsRenderer']['tabs'][0]['tabRenderer']['content']['sectionListRenderer']['contents'][0]['musicCardShelfRenderer']['title']['runs'][0]		
		title = data['text']
		if 'watchEndpoint' in data['navigationEndpoint'].keys():		
			return {'id': data['navigationEndpoint']['watchEndpoint']['videoId'], 'title': title} 
		elif 'browseEndpoint' in data['navigationEndpoint'].keys():								
			data = post('browse', {"browseId": data['navigationEndpoint']['browseEndpoint']['browseId']}) 
			id = data['contents']["singleColumnBrowseResultsRenderer"]["tabs"][0]["tabRenderer"]['content']["sectionListRenderer"]['contents'][0]["musicShelfRenderer"]['contents'][0]["musicResponsiveListItemRenderer"]["flexColumns"][0]["musicResponsiveListItemFlexColumnRenderer"]['text']['runs'][0]['navigationEndpoint']['watchEndpoint']['videoId']			
			return {'id': id, 'title': title} 
	

def getPlaylistItems(playlistId, continuation=None, resultsCountLimit=45, resultsCountCurrent=0): 	
	data = {"playlistId": playlistId, "params": '8gEAmgMDCNgE'}
	if continuation is not None:
		data['continuation'] = continuation
	data=post('next', data)
	if continuation is None:
		data = data['contents']['singleColumnMusicWatchNextResultsRenderer']['tabbedRenderer']['watchNextTabbedResultsRenderer']['tabs'][0]['tabRenderer']['content']['musicQueueRenderer']
		if 'content' not in data.keys():
			return []			
		data = data['content']['playlistPanelRenderer']
	else:
		data = data['continuationContents']['playlistPanelContinuation']		
	result = playlistPanelVideoRenderer(data['contents'])	
	if 'continuations' not in data.keys() or resultsCountCurrent + len(result) >= resultsCountLimit:
		return result
	else:				
		if 'nextContinuationData' in data['continuations'][0].keys():
			continuation = data['continuations'][0]['nextContinuationData']['continuation']
		elif 'nextRadioContinuationData' in data['continuations'][0].keys():
			continuation = data['continuations'][0]['nextRadioContinuationData']['continuation']
		else:
			return result
		return result + getPlaylistItems(playlistId, continuation=continuation, resultsCountCurrent=resultsCountCurrent+len(result) )
	

def getNextPlayItems(track, region='IL'):	
	data = post('next',{'videoId': track['id']}, region)		
	playlistId = data['contents']['singleColumnMusicWatchNextResultsRenderer']['tabbedRenderer']['watchNextTabbedResultsRenderer']['tabs'][0]['tabRenderer']['content']['musicQueueRenderer']['content']['playlistPanelRenderer']['contents'][1]['automixPreviewVideoRenderer']['content']['automixPlaylistVideoRenderer']['navigationEndpoint']['watchPlaylistEndpoint']['playlistId']
	return getPlaylistItems(playlistId)
	
	
def getLyrics(track):
	data = post('next', {"videoId": track['id']})
	browseId = data["contents"]["singleColumnMusicWatchNextResultsRenderer"]["tabbedRenderer"]["watchNextTabbedResultsRenderer"]["tabs"][1]["tabRenderer"]["endpoint"]["browseEndpoint"]['browseId']
	data = post('browse', {"browseId": browseId})	
	try:
		result =  data['contents']['sectionListRenderer']['contents'][0]['musicDescriptionShelfRenderer']['description']['runs'][0]['text']		
	except:
		return None
	


	


	