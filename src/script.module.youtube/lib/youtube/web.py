from . import common


	
def getPlaylistsItemExists(account, id):
	result = []
	data = common.post('playlist/get_add_to_playlist', {"videoIds": id, 'fetchLiveState': True}, account, clientId=1)
	for item in data['contents'][0]['addToPlaylistRenderer']['playlists']:
#		if item['playlistAddToOptionRenderer']['playlistId'] == "WL":
#			continue
		if not item['playlistAddToOptionRenderer']["containsSelectedVideos"] == "NONE":
			result.append(item['playlistAddToOptionRenderer']['playlistId'])	
	return result


def getItemPlaylists(account, trackId):
	playlistsItemExists = getPlaylistsItemExists(account, trackId)
	result = getLibrary(account,'playlist')
	for pl in result:
		pl['itemExists'] = pl['id'] in playlistsItemExists
	return result
	
	
def setItemPlaylist(account, itemId, playlistId, action):
	if action=='add':
		action = {"action": "ACTION_ADD_VIDEO", "addedVideoId": itemId}
	elif action=='remove':
		action = {"action": "ACTION_REMOVE_VIDEO_BY_VIDEO_ID", "removedVideoId": itemId}
	else:
		raise Exception('Invalid action')
	common.post('browse/edit_playlist', {"playlistId": playlistId, "actions": [action]}, account, clientId=1)


def playlistRemove(account, playlistId):	
	common.post('playlist/delete', {"playlistId": playlistId}, account, clientId=1)


def playlistAdd(account, title):
	common.post('playlist/create', {"title": title, "privacyStatus": "PUBLIC"}, account, clientId=1)
	


def videoRenderer(item):									
	
	r={
		'id': item['videoId'],
		'title': item['title']['runs'][0]['text'],
		'thumb': item['thumbnail']['thumbnails'][-1]['url']	
	}		
	try:						
		r['duration'] = common.timeStrToSeconds(item['lengthText']['simpleText'])
	except:		
		None
	try:
		r['channel'] = {
			'id': item['ownerText']['runs'][0]['navigationEndpoint']['browseEndpoint']['browseId'],
			'title': item['ownerText']['runs'][0]['text']
		}
	except:
		None
	try:
		r['age'] = item['publishedTimeText']['simpleText'].replace(' ago','')
	except:
		None
	try:
		r['views'] = item['viewCountText']['simpleText'].replace(' views','')
	except:
		None				
	return r

def playlistVideoRenderer(item):											
	r={
		'id': item['videoId'],
		'title': item['title']['runs'][0]['text'],
		'thumb': item['thumbnail']['thumbnails'][-1]['url'],	
	}
	try:
		r['duration'] = common.timeStrToSeconds(item['lengthText']['simpleText'])
	except:
		None		
	try:
		r['channel'] = {
			'id': item['shortBylineText']['runs'][0]['navigationEndpoint']['browseEndpoint']['browseId'],
			'title': item['shortBylineText']['runs'][0]['text']
		}
	except:
		None			
	try:
		data = item['title']['accessibility']['accessibilityData']['label'].split(' by ' + r['channel']['title'] + ' ')[1].split(' ago')[0]
		if 'views' in data:
			r['views'] = data.split('views')[0].rstrip()
			r['age'] = data.split('views')[1].lstrip()
		else:
			r['age'] = data
	except:
		None
	return r
	
	
def playlistRenderer(item):			
	r={
		'id': item['playlistId'], 			
		'title': item['title']['simpleText'],		
		'thumb': item['thumbnails'][0]['thumbnails'][-1]['url']		
	}		
	channel = item['shortBylineText']['runs'][0]
	if channel['text'] not in ['Public','Private','Unlisted']:
		r['channel'] = {'id': channel['navigationEndpoint']['browseEndpoint']['browseId'], 'title': channel['text']}
	else:
		r['visibility'] = channel['text']
	return r


def gridPlaylistRenderer(item, channel=None):
	r = {
		'id': item['playlistId'], 			
		'title': item['title']['runs'][0]['text'] if 'runs' in item['title'].keys() else item['title']['simpleText'],		
		'thumb': item['thumbnail']['thumbnails'][-1]['url']
	}	
	if 'shortBylineText' in item.keys():
		channel = item['shortBylineText']['runs'][0]
		if channel['text'] not in ['Public','Private','Unlisted']:
			r['channel'] = {'id': channel['navigationEndpoint']['browseEndpoint']['browseId'], 'title': channel['text']}
		else:
			r['visibility'] = channel['text']
	return r		


def lockupPlaylistRenderer(item):			
	r={
		'id': item['contentId'], 			
		'title': item['metadata']['lockupMetadataViewModel']['title']['content'],		
		'thumb': item['contentImage']['collectionThumbnailViewModel']['primaryThumbnail']['thumbnailViewModel']['image']['sources'][-1]['url']		
	}					
	metadata = item['metadata']['lockupMetadataViewModel']['metadata']['contentMetadataViewModel']['metadataRows'][0]['metadataParts']
	if metadata[0]['text']['content'] in ['Public','Private','Unlisted']:
		r['visibility'] = metadata[0]['text']['content']
	else:
		r['channel'] = {'id': '' if 'commandRuns' not in metadata[0].keys() else metadata[0]['commandRuns'][0]['onTap']['innertubeCommand']['browseEndpoint']['browseId'], 'title': metadata[0]['text']['content']}

	return r


def channelRenderer(item):	
	r = {
		'id': item['channelId'], 			
		'title': item['title']['simpleText'],		
		'thumb': common.posterUrl('https:' + item['thumbnail']['thumbnails'][0]['url'], 720),
	}	
	return r

	

def search(text, category, continuation=None, resultsCountLimit=45, resultsCountCurrent=0):	
	categories = {			
		'channel': {'params': 'EgIQAg%3D%3D', 'renderer': channelRenderer, 'rendererKey': 'channelRenderer'},
		'video': {'params': 'EgIQAQ%3D%3D', 'renderer': videoRenderer, 'rendererKey': 'videoRenderer'},
		'playlist': {'params': 'EgIQAw%3D%3D', 'renderer': lockupPlaylistRenderer, 'rendererKey': 'lockupViewModel'}		
	}
	data = {"query": text}
	if continuation is not None:
		data['continuation'] = continuation			
	data['params'] = categories[category]['params']		
	data = common.post('search', data, region='IL', language=common.getLanguage(text), clientId=1)					
	if continuation is None:
		data = data['contents']['twoColumnSearchResultsRenderer']['primaryContents']
		if 'sectionListRenderer' not in data.keys():
			return []
		data = data['sectionListRenderer']['contents']
	else:
		data = data['onResponseReceivedCommands'][0]['appendContinuationItemsAction']['continuationItems']		
	if 'continuationItemRenderer' in data[-1].keys():
		result = [categories[category]['renderer'](item[categories[category]['rendererKey']]) for item in data[-2]['itemSectionRenderer']['contents'] if categories[category]['rendererKey'] in item.keys()]			
		continuation=data[-1]['continuationItemRenderer']['continuationEndpoint']['continuationCommand']['token']
		if resultsCountCurrent + len(result) >= resultsCountLimit:
			return result
		else:
			return result + search(text, category, continuation=continuation, resultsCountCurrent=resultsCountCurrent+len(result) )			
	else:
		result = [categories[category]['renderer'](item[categories[category]['rendererKey']]) for item in data[-1]['itemSectionRenderer']['contents'] if categories[category]['rendererKey'] in item.keys()]			
		return result
	
	

def getLibrary(account, category):					
	if category=='playlist':	
		data = common.post('browse', {"browseId": "FElibrary", "fetchLiveState": True}, account, clientId=1)			
		data = data['contents']['twoColumnBrowseResultsRenderer']['tabs'][0]['tabRenderer']['content']['sectionListRenderer']['contents']		
		data_ = {}
		for g in data:
			data_[g['itemSectionRenderer']['contents'][0]['shelfRenderer']['title']['runs'][0]['text']] = g['itemSectionRenderer']['contents'][0]['shelfRenderer'].get('content',None)										
		if 'Playlists' not in data_.keys() or data_['Playlists'] is None:
			return[]		
		result = [lockupPlaylistRenderer(item['lockupViewModel']) for item in data_['Playlists']['horizontalListRenderer']['items'] if 'lockupViewModel' in item.keys()]	
		result = [pl for pl in result if not pl['title'].endswith(' [Audio]') and pl['id'] != 'RDurDPzpquTuA' and pl['title'] != 'Liked videos' and pl['title'] != 'Watch later']
		for pl in result:
			pl['title'] = pl['title'].replace(' [Video]','')
		return result
	elif category=='channel':
		data = common.post('browse', {"browseId": "FEchannels"}, account, clientId=1)		
		data = data['contents']['twoColumnBrowseResultsRenderer']['tabs'][0]['tabRenderer']['content']['sectionListRenderer']['contents'][0]['itemSectionRenderer']['contents'][0]['shelfRenderer']
		if 'content' not in data.keys():
			return []		
		return [channelRenderer(item['channelRenderer']) for item in data['content']['expandedShelfContentsRenderer']['items'] if 'channelRenderer' in item.keys()]			
	elif category=='video':
		import datetime
		payload = {
			"order":"VIDEO_ORDER_DISPLAY_TIME_DESC",
			"pageSize":100,
			"mask":{"metrics": {"all": True}, "privacy": True, "videoId":True, "lengthSeconds":True, "thumbnailDetails":{"all":True}, "title":True, "timeCreatedSeconds":True, "description":True}				
		}
		data = common.post('creator/list_creator_videos', payload, account, clientId=62)		
		if 'videos' not in data.keys():
			return[]			
		result = []
		now = datetime.datetime.now()
		for item in data['videos']:			
			resultitem={
				'id': item['videoId'],
				'title': item['title'],
				'thumb': item['thumbnailDetails']['thumbnails'][-1]['url'],			
				'duration': int(item['lengthSeconds']),
				'age':  str(now - datetime.datetime.fromtimestamp(int(item['timeCreatedSeconds'])) ),
				'visibility': item['privacy'].replace('VIDEO_PRIVACY_','').lower().capitalize(),
				'views': str(item['metrics']['viewCount'])
			}
			if ',' in resultitem['age']:
				resultitem['age']=resultitem['age'].split(',')[0]
			else:
				resultitem['age']='0 days'
			result.append(resultitem)
		return result
		

def getFeeds(account):		
	data = common.post('browse', {"browseId":"FEsubscriptions"}, account, clientId=1)
	data = data['contents']['twoColumnBrowseResultsRenderer']['tabs'][0]['tabRenderer']['content']['richGridRenderer']['contents']
	return [videoRenderer(item['richItemRenderer']['content']['videoRenderer']) for item in data if 'richItemRenderer' in item.keys() and 'videoRenderer' in item['richItemRenderer']['content'].keys()]


def getSuggestions(account):	
	data = common.post('browse', {"browseId":"FEwhat_to_watch"}, account, clientId=1)
	data = data['contents']['twoColumnBrowseResultsRenderer']['tabs'][0]['tabRenderer']['content']['richGridRenderer']['contents']
	return [videoRenderer(item['richItemRenderer']['content']['videoRenderer']) for item in data if 'richItemRenderer' in item.keys() and 'videoRenderer' in item['richItemRenderer']['content'].keys()]



def getPlaylistItems(playlistId): 	
	data = {"browseId": 'VL' + playlistId}	
	data=common.post('browse', data, clientId=1)	
	data = data['contents']['twoColumnBrowseResultsRenderer']['tabs'][0]['tabRenderer']['content']
	if 'sectionListRenderer' not in data.keys():
		return []			
	data = data['sectionListRenderer']['contents']
	return [playlistVideoRenderer(item['playlistVideoRenderer']) for item in data[0]['itemSectionRenderer']['contents'][0]['playlistVideoListRenderer']['contents'] if 'playlistVideoRenderer' in item.keys()]			



def validateAccount(account):
	session = common.initSession()
	session.headers.update({'Authorization': common.getAuthHash(account['cookies'], 'https://www.youtube.com')})
	reply = session.get('https://www.youtube.com/', cookies = account['cookies'])
	cookies = reply.cookies.get_dict()
	import re	
	visitorData = re.search(r'"visitorData":"(.*?)"', reply.text).group(1)		
	account['cookies'].update(cookies)
	account['validDays'] = 7
	account['visitorData'] = visitorData
	if 'title' not in account.keys():		
		data = common.post('account/account_menu', account=account, clientId=1)	
		account.update({
			'title': data['actions'][0]['openPopupAction']['popup']['multiPageMenuRenderer']['header']['activeAccountHeaderRenderer']['accountName']['simpleText'],
			'thumb': data['actions'][0]['openPopupAction']['popup']['multiPageMenuRenderer']['header']['activeAccountHeaderRenderer']['accountPhoto']['thumbnails'][0]['url'].replace('=s108','=s720'),
			'channel': None,			
		})
	
	
def setSubscription(account, channelId, action):
	if action=='subscribe':
		common.post('subscription/subscribe', {"channelIds": [channelId]}, account, clientId=1)
	elif action=='unsubscribe':
		common.post('subscription/unsubscribe', {"channelIds": [channelId]}, account, clientId=1)
		
		
def getChannel(account, channelId, category=None, continuation=None, resultsCountLimit=45, resultsCountCurrent=0):
	categories = {		
		'video': {'params': 'EgZ2aWRlb3PyBgQKAjoA', 'tab': 'Videos'},
		'playlist': {'params': 'EglwbGF5bGlzdHMYAyABcAA%3D', 'tab': 'Playlists'}
	}	
	data={"browseId": channelId}	
	if continuation is not None:
		data['continuation'] = continuation
	if category is not None:
		data['params'] = categories[category]['params']		
	data = common.post('browse', data, account, clientId=1)
	if category is not None:					
		if continuation is None:			
			data = [tab for tab in data['contents']['twoColumnBrowseResultsRenderer']['tabs'] if 'tabRenderer' in tab.keys() and tab['tabRenderer']['title']==categories[category]['tab']]
			if len(data)==0:
				return []				
			if category=='video':
				data = data[0]['tabRenderer']['content']['richGridRenderer']['contents']
			elif category=='playlist':
				data = data[0]['tabRenderer']['content']['sectionListRenderer']['contents'][0]['itemSectionRenderer']['contents'][0]['gridRenderer']['items']
		else:
			data = data['onResponseReceivedActions'][0]['appendContinuationItemsAction']['continuationItems']								
		if category=='video':
			result = [videoRenderer(item['richItemRenderer']['content']['videoRenderer']) for item in data if 'richItemRenderer' in item.keys()] 
		elif category=='playlist':		
			result = [lockupPlaylistRenderer(item['lockupViewModel']) for item in data if 'lockupViewModel' in item.keys()] 
		if 'continuationItemRenderer' not in data[-1].keys() or resultsCountCurrent + len(result) >= resultsCountLimit:
			return result
		return result + getChannel(account, channelId, category, continuation=data[-1]['continuationItemRenderer']['continuationEndpoint']['continuationCommand']['token'], resultsCountCurrent=resultsCountCurrent+len(result) )					
	else: 	
		result = {
			'id': channelId,
			'title': data['metadata']['channelMetadataRenderer']['title'],
			'thumb': data['metadata']['channelMetadataRenderer']['avatar']['thumbnails'][0]['url'],
			'details': ', '.join([d['text']['content'] for d in data['header']['pageHeaderRenderer']['content']['pageHeaderViewModel']['metadata']['contentMetadataViewModel']['metadataRows'][1]['metadataParts'] ])
		}
		if account is not None:			
			result['subscribed'] = data['frameworkUpdates']['entityBatchUpdate']['mutations'][-2]['payload']['subscriptionStateEntity']['subscribed']
			
		return result
	


#https://www.youtube.com/youtubei/v1/like/removelike?prettyPrint=false
#payload: {"context":{"client":{"hl":"en","gl":"IL","remoteHost":"193.43.244.201","deviceMake":"","deviceModel":"","visitorData":"CgtMMW93b1dpWW1YZyjmmci5BjIKCgJJTBIEGgAgYg%3D%3D","userAgent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/130.0.0.0 Safari/537.36,gzip(gfe)","clientName":"WEB","clientVersion":"2.20241107.11.00","osName":"Windows","osVersion":"10.0","originalUrl":"https://www.youtube.com/app_shell","platform":"DESKTOP","clientFormFactor":"UNKNOWN_FORM_FACTOR","configInfo":{"appInstallData":"COaZyLkGENqUzhwQj9exBRDgjf8SEKPN_xIQwc2xBRCkmc4cEJqBuCIQ0NH_EhDJ968FEIzUsQUQxNixBRCNlLEFEKGjzhwQhaexBRCQzLEFEIjjrwUQ26-vBRDGv7EFEJCjzhwQgdaxBRDT4a8FEIPDsQUQ6-j-EhDnpc4cEInorgUQ9KfOHBCq2LAFEJrOsQUQjdSxBRCU_rAFEIXDsQUQwdr_EhC9tq4FEOrDrwUQytSxBRCJp7EFEParsAUQ0I2wBRDh7LAFEKaasAUQppOxBRCWlbAFEJ7QsAUQotSxBRDlubEFEL2ZsAUQnaawBRDK2LEFEKKdsQUQ4tSuBRDJ5rAFENGqzhwQzN-uBRCKobEFEN2dzhwQ6pDOHBCHw7EFEJmYsQUQ8ZbOHBDmz7EFEI3MsAUQppKxBRDD2_8SEKmmzhwQ2arOHBConc4cENbdsAUQ1daxBRDGpLEFEL6KsAUQg53OHBCIh7AFEPClzhwQyNixBRCrqs4cEPirsQUQhKfOHBDN0bEFEKiasAUQjNCxBRDrmbEFEIHDsQUQkbexBRC1qs4cELfq_hIQmY2xBRC3768FEODNsQUQ18GxBRCO0LEFEJLLsQUQy9GxBRDvzbAFENGUzhwQ3q2xBRDtubEFEI_DsQUQy6rOHBDu3f8SEM_VsQUQ85vOHCosQ0FNU0hCVWZvTDJ3RE5Ia0J1MklvQUNPM01nQ2pfUU9xQXpoY3RaOEhRYz0%3D"},"userInterfaceTheme":"USER_INTERFACE_THEME_LIGHT","timeZone":"Asia/Jerusalem","browserName":"Chrome","browserVersion":"130.0.0.0","acceptHeader":"*/*","deviceExperimentId":"ChxOelF6TmpBeU1ERXhOekE0T1RFME5EVTVNZz09EOaZyLkGGOaZyLkG","screenWidthPoints":1530,"screenHeightPoints":911,"screenPixelDensity":1,"screenDensityFloat":1,"utcOffsetMinutes":120,"connectionType":"CONN_CELLULAR_4G","memoryTotalKbytes":"8000000","mainAppWebInfo":{"graftUrl":"https://www.youtube.com/watch?v=ReWDun-ihxo&list=RDCLAK5uy_neriXH6JbZPr7Pf4LOi5bGQP-_lWRZXs4&start_radio=1","pwaInstallabilityStatus":"PWA_INSTALLABILITY_STATUS_UNKNOWN","webDisplayMode":"WEB_DISPLAY_MODE_BROWSER","isWebNativeShareAvailable":true}},"user":{"lockedSafetyMode":false},"request":{"useSsl":true,"internalExperimentFlags":[],"consistencyTokenJars":[]},"clickTracking":{"clickTrackingParams":"CFkQ-0sYHiITCNHx__W31IkDFTlETwQdYAUW3Q=="},"adSignalsInfo":{"params":[{"key":"dt","value":"1731333465686"},{"key":"flash","value":"0"},{"key":"frm","value":"0"},{"key":"u_tz","value":"120"},{"key":"u_his","value":"5"},{"key":"u_h","value":"1080"},{"key":"u_w","value":"1920"},{"key":"u_ah","value":"1032"},{"key":"u_aw","value":"1920"},{"key":"u_cd","value":"24"},{"key":"bc","value":"31"},{"key":"bih","value":"911"},{"key":"biw","value":"1513"},{"key":"brdim","value":"0,0,0,0,1920,0,1920,1032,1530,911"},{"key":"vis","value":"1"},{"key":"wgl","value":"true"},{"key":"ca_type","value":"image"}]}},"target":{"playlistId":"RDCLAK5uy_neriXH6JbZPr7Pf4LOi5bGQP-_lWRZXs4"}}

#https://www.youtube.com/youtubei/v1/like/like?prettyPrint=false
#{"context":{"client":{"hl":"en","gl":"IL","remoteHost":"193.43.244.201","deviceMake":"","deviceModel":"","visitorData":"CgtMMW93b1dpWW1YZyi-nMi5BjIKCgJJTBIEGgAgYg%3D%3D","userAgent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/130.0.0.0 Safari/537.36,gzip(gfe)","clientName":"WEB","clientVersion":"2.20241107.11.00","osName":"Windows","osVersion":"10.0","originalUrl":"https://www.youtube.com/feed/playlists","platform":"DESKTOP","clientFormFactor":"UNKNOWN_FORM_FACTOR","configInfo":{"appInstallData":"CL6cyLkGEIzUsQUQppKxBRDGpLEFENfBsQUQoaPOHBDN0bEFEKidzhwQppqwBRCO0LEFEIiHsAUQ6sOvBRDE2LEFENbdsAUQieiuBRC3768FEJDMsQUQ2arOHBDJ968FENPhrwUQiqGxBRCFw7EFEKSZzhwQ0I2wBRDBzbEFEOHssAUQ0ZTOHBDRqs4cEOW5sQUQvbauBRCSy7EFEODNsQUQo83_EhCHw7EFEJqBuCIQzN-uBRC-irAFEI3MsAUQ65mxBRCEp84cELWqzhwQjZSxBRCPw7EFEI_XsQUQyNixBRD0p84cEMrYsQUQ1daxBRDalM4cEJmNsQUQwdr_EhCFp7EFEOLUrgUQms6xBRDxls4cEJG3sQUQvZmwBRD2q7AFEMnmsAUQjNCxBRCBw7EFEIPDsQUQqJqwBRDvzbAFEO25sQUQlP6wBRCmk7EFEN6tsQUQqabOHBDgjf8SEPGlzhwQg53OHBCB1rEFENuvrwUQ56XOHBCinbEFEOqQzhwQ3Z3OHBCq2LAFEJ7QsAUQ6-j-EhD4q7EFEJaVsAUQmZixBRCN1LEFEIjjrwUQ5s-xBRCdprAFEMPb_xIQt-r-EhCQo84cEKLUsQUQytSxBRDGv7EFEImnsQUQ0NH_EhCrqs4cEMvRsQUQy6rOHBDP1bEFEO7d_xIQ85vOHCosQ0FNU0hCVWZvTDJ3RE5Ia0J1MklvQUNPM01nQ2pfUU9xQXpoY3RaOEhRYz0%3D"},"userInterfaceTheme":"USER_INTERFACE_THEME_LIGHT","timeZone":"Asia/Jerusalem","browserName":"Chrome","browserVersion":"130.0.0.0","acceptHeader":"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7","deviceExperimentId":"ChxOelF6TmpBeU1UVTVOVEk0T1RnM09EYzRNZz09EL6cyLkGGL6cyLkG","screenWidthPoints":1046,"screenHeightPoints":911,"screenPixelDensity":1,"screenDensityFloat":1,"utcOffsetMinutes":120,"connectionType":"CONN_CELLULAR_4G","memoryTotalKbytes":"8000000","mainAppWebInfo":{"graftUrl":"https://www.youtube.com/watch?v=V58pulQZFts&list=PLtBqc4qrCw3HYBQfUcYuGa3q8bM_Zzz-z","pwaInstallabilityStatus":"PWA_INSTALLABILITY_STATUS_CAN_BE_INSTALLED","webDisplayMode":"WEB_DISPLAY_MODE_BROWSER","isWebNativeShareAvailable":true}},"user":{"lockedSafetyMode":false},"request":{"useSsl":true,"consistencyTokenJars":[{"encryptedTokenJarContents":"AKreu9tlunQ1tT-pfX1fJ-zPyaaUYtLv-GtAKfMyXixnHDvMefaVcvwa7Ec6fl8jp-ca_ZumJmDUn5rOrdAnZCtp0BSqajhcDTmrRXAp8MkiP57qBpdSLxZykLA"}],"internalExperimentFlags":[]},"clickTracking":{"clickTrackingParams":"CGEQ-0sYyAEiEwi-gYCgudSJAxUa6EkHHe2AErs="},"adSignalsInfo":{"params":[{"key":"dt","value":"1731333694943"},{"key":"flash","value":"0"},{"key":"frm","value":"0"},{"key":"u_tz","value":"120"},{"key":"u_his","value":"7"},{"key":"u_h","value":"1080"},{"key":"u_w","value":"1920"},{"key":"u_ah","value":"1032"},{"key":"u_aw","value":"1920"},{"key":"u_cd","value":"24"},{"key":"bc","value":"31"},{"key":"bih","value":"911"},{"key":"biw","value":"1029"},{"key":"brdim","value":"0,0,0,0,1920,0,1920,1032,1046,911"},{"key":"vis","value":"1"},{"key":"wgl","value":"true"},{"key":"ca_type","value":"image"}],"bid":"ANyPxKoKa6PH6yYEEaNX1E0ymhyHlB2w_7QCiQzUvmu70ZdzYfLawvsTnNdDpQ9iaDWdVsQl9-QwWDDI8Sw6iM1oHKYLa-5SbQ"}},"target":{"playlistId":"PLtBqc4qrCw3HYBQfUcYuGa3q8bM_Zzz-z"}}



def getVideo(id, account=None):	
	video = common.post(action='player', data={"videoId": id}, account=account, clientId=5)	
	if 'isLive' not in video['videoDetails'].keys():
		video['videoDetails']['isLive'] = False				
	return video