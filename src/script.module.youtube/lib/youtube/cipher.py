# -*- coding: utf-8 -*-


import re
import requests
import os



class JsonScriptEngine(object):
    def __init__(self, json_script):
        self._json_script = json_script

    def execute(self, signature):
        _signature = signature

        _actions = self._json_script['actions']
        for action in _actions:
            func = ''.join(['_', action['func']])
            params = action['params']

            if func == '_return':
                break

            for i in range(len(params)):
                param = params[i]
                if param == '%SIG%':
                    param = _signature
                    params[i] = param
                    break

            method = getattr(self, func)
            if method:
                _signature = method(*params)
            else:
                raise Exception("Unknown method '%s'" % func)

        return _signature

    @staticmethod
    def _join(signature):
        return ''.join(signature)

    @staticmethod
    def _list(signature):
        return list(signature)

    @staticmethod
    def _slice(signature, b):
        del signature[b:]
        return signature

    @staticmethod
    def _splice(signature, a, b):
        del signature[a:b]
        return signature

    @staticmethod
    def _reverse(signature):
        return signature[::-1]

    @staticmethod
    def _swap(signature, b):
        c = signature[0]
        signature[0] = signature[b % len(signature)]
        signature[b] = c
        return signature


class Cipher(object):
	
	def __init__(self):
		self._object_cache = {}
	

	def _load_javascript(self, javascript):
		function_name = self._find_signature_function_name(javascript)
		if not function_name:
			raise Exception('Signature function not found')
		_function = self._find_function_body(function_name, javascript)
		function_parameter = _function[0].replace('\n', '').split(',')
		function_body = _function[1].replace('\n', '').split(';')
		json_script = {'actions': []}
		for line in function_body:
			# list of characters
			split_match = re.match(r'%s\s?=\s?%s.split\(""\)' % (function_parameter[0], function_parameter[0]), line)
			if split_match:
				json_script['actions'].append({'func': 'list', 'params': ['%SIG%']})

			# return
			return_match = re.match(r'return\s+%s.join\(""\)' % function_parameter[0], line)
			if return_match:
				json_script['actions'].append({'func': 'join', 'params': ['%SIG%']})

			# real object functions
			cipher_match = re.match(r'(?P<object_name>[$a-zA-Z0-9]+)\.?\[?"?(?P<function_name>[$a-zA-Z0-9]+)"?\]?\((?P<parameter>[^)]+)\)',	line)
			if cipher_match:
				object_name = cipher_match.group('object_name')
				function_name = cipher_match.group('function_name')
				parameter = cipher_match.group('parameter').split(',')
				for i in range(len(parameter)):
					param = parameter[i].strip()
					if i == 0:
						param = '%SIG%'
					else:
						param = int(param)
					parameter[i] = param

				# get function from object
				_function = self._get_object_function(object_name, function_name, javascript)

				# try to find known functions and convert them to our json_script
				slice_match = re.match(r'[a-zA-Z]+.slice\((?P<a>\d+),[a-zA-Z]+\)', _function['body'][0])
				if slice_match:
					a = int(slice_match.group('a'))
					params = ['%SIG%', a, parameter[1]]
					json_script['actions'].append({'func': 'slice', 'params': params})

				splice_match = re.match(r'[a-zA-Z]+.splice\((?P<a>\d+),[a-zA-Z]+\)', _function['body'][0])
				if splice_match:
					a = int(splice_match.group('a'))
					params = ['%SIG%', a, parameter[1]]
					json_script['actions'].append({'func': 'splice', 'params': params})

				swap_match = re.match(r'var\s?[a-zA-Z]+=\s?[a-zA-Z]+\[0\]', _function['body'][0])
				if swap_match:
					params = ['%SIG%', parameter[1]]
					json_script['actions'].append({'func': 'swap', 'params': params})

				reverse_match = re.match(r'[a-zA-Z].reverse\(\)', _function['body'][0])
				if reverse_match:
					params = ['%SIG%']
					json_script['actions'].append({'func': 'reverse', 'params': params})

		return json_script

	@staticmethod
	def _find_signature_function_name(javascript):
		match_patterns = [
            r'\b[cs]\s*&&\s*[adf]\.set\([^,]+\s*,\s*encodeURIComponent\s*\(\s*(?P<name>[a-zA-Z0-9$]+)\(',
            r'\b[a-zA-Z0-9]+\s*&&\s*[a-zA-Z0-9]+\.set\([^,]+\s*,\s*encodeURIComponent\s*\(\s*(?P<name>[a-zA-Z0-9$]+)\(',
            r'(?:\b|[^a-zA-Z0-9$])(?P<name>[a-zA-Z0-9$]{2})\s*=\s*function\(\s*a\s*\)\s*{\s*a\s*=\s*a\.split\(\s*""\s*\)',
            r'(?P<name>[a-zA-Z0-9$]+)\s*=\s*function\(\s*a\s*\)\s*{\s*a\s*=\s*a\.split\(\s*""\s*\)',
            r'(["\'])signature\1\s*,\s*(?P<name>[a-zA-Z0-9$]+)\(',
            r'\.sig\|\|(?P<name>[a-zA-Z0-9$]+)\(',
            r'yt\.akamaized\.net/\)\s*\|\|\s*.*?\s*[cs]\s*&&\s*[adf]\.set\([^,]+\s*,\s*(?:encodeURIComponent\s*\()?\s*'
            r'(?P<name>[a-zA-Z0-9$]+)\(',
            r'\b[cs]\s*&&\s*[adf]\.set\([^,]+\s*,\s*(?P<name>[a-zA-Z0-9$]+)\(',
            r'\b[a-zA-Z0-9]+\s*&&\s*[a-zA-Z0-9]+\.set\([^,]+\s*,\s*(?P<name>[a-zA-Z0-9$]+)\(',
            r'\bc\s*&&\s*a\.set\([^,]+\s*,\s*\([^)]*\)\s*\(\s*(?P<name>[a-zA-Z0-9$]+)\(',
            r'\bc\s*&&\s*[a-zA-Z0-9]+\.set\([^,]+\s*,\s*\([^)]*\)\s*\(\s*(?P<name>[a-zA-Z0-9$]+)\(',
            r'\bc\s*&&\s*[a-zA-Z0-9]+\.set\([^,]+\s*,\s*\([^)]*\)\s*\(\s*(?P<name>[a-zA-Z0-9$]+)\('
        ]
		for pattern in match_patterns:
			match = re.search(pattern, javascript)
			if match:
				return match.group('name')
		return ''

	@staticmethod
	def _find_function_body(function_name, javascript):
		# normalize function name
		function_name = function_name.replace('$', '\\$')
		match = re.search(r'\s?%s=function\((?P<parameter>[^)]+)\)\s?{\s?(?P<body>[^}]+)\s?\}' % function_name, javascript)
		if match:
			return match.group('parameter'), match.group('body')
		return '', ''

	@staticmethod
	def _find_object_body(object_name, javascript):
		object_name = object_name.replace('$', '\\$')
		match = re.search(r'var %s={(?P<object_body>.*?})};' % object_name, javascript, re.S)
		if match:
			return match.group('object_body')
		return ''

	def _get_object_function(self, object_name, function_name, javascript):
		if object_name not in self._object_cache:
			self._object_cache[object_name] = {}
		else:
			if function_name in self._object_cache[object_name]:
				return self._object_cache[object_name][function_name]
		_object_body = self._find_object_body(object_name, javascript)
		_object_body = _object_body.split('},')
		for _function in _object_body:
			if not _function.endswith('}'):
				_function = ''.join([_function, '}'])
			_function = _function.strip()
			match = re.match(r'(?P<name>[^:]*):function\((?P<parameter>[^)]*)\){(?P<body>[^}]+)}', _function)
			if match:
				name = match.group('name').replace('"', '')
				parameter = match.group('parameter')
				body = match.group('body').split(';')
				self._object_cache[object_name][name] = {'name': name, 'body': body, 'params': parameter}
		return self._object_cache[object_name][function_name]



#def decipher(js, sig):
#	cipher = Cipher()
#	jsonscript = cipher._load_javascript(js)
#	json_script_engine = JsonScriptEngine(jsonscript)	
#	return json_script_engine.execute(sig)




def getVideoCipher(id, cipherCachePath):
	from helpers import http, storage
	_url = 'https://www.youtube.com'
	session = http.initSession()	
	content =  session.get(_url + '/watch', params={'v': id}).text
#	jsUrl = _url + content.split('"PLAYER_JS_URL":"')[1].split('"')[0].replace('\/','/')	
	jsUrl = _url + re.compile('"PLAYER_JS_URL":"(.*?)\"', re.S).findall(content)[0]	
	jsName = jsUrl.split('player/')[1].split('/')[0]
	cipherCacheFile = cipherCachePath + '/cipherCache_' + jsName
	if os.path.isfile(cipherCacheFile):
		return storage.fileToObj(cipherCacheFile)
	else:
		js = session.get(jsUrl).text
		c = Cipher()
		cipherJson = JsonScriptEngine(c._load_javascript(js))
		storage.objToFile(cipherJson, cipherCacheFile)
		return cipherJson