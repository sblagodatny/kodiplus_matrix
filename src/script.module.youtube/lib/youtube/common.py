_url = 'https://www.youtube.com'
_urlApi = _url + '/youtubei/v1'


_clients = {
	1: {
		'client': {
			'clientName': 'WEB', 
			'clientVersion': '2.20220918'
		}		
	},


	5: {
		'client': {
			'clientName': 'IOS',
			'clientVersion': '19.45.4'
		}		
	},	
	7: {		
		"client": {
			"clientName": "TVHTML5", 
			"clientVersion": "7.20241201.18.00"
		}		
	},
	62: {		
		'client': {
			'clientName': 'WEB_CREATOR', 
			'clientVersion': '1.20220918'
		}
	},
	100: {
		'client': {
			'clientName': 'WEB_REMIX', 
			'clientVersion': '1.20240918.01.00'
		}		
	},	
	
	
}


def post(action, data={}, account=None, clientId=1, language='en', region='US'):	
	session = initSession()		
	headers = {'X-Goog-Visitor-Id': 'Cgt5eTVsSzFYVDlHYyio4c28BjIKCgJJTBIEGgAgHA%3D%3D' }
	cookies = None
	if account is not None:						
		headers['Origin'] = _url
		headers['Authorization'] = getAuthHash(account['cookies'], headers['Origin'])
		headers['X-Goog-Visitor-Id'] = account['visitorData']
		cookies = account['cookies']	
	data['context'] = {'client': _clients[clientId]['client']}
	data['context']['client'].update({'hl': language, 'gl': region})
	return session.post(_urlApi + '/{}'.format(action),  json=data, cookies=cookies, headers=headers).json()		


def getAuthHash(cookies, origin):
	from hashlib import sha1
	import time		
	auth = cookies['__Secure-3PAPISID'] + ' ' + origin	
	sha_1 = sha1()
	unix_timestamp = str(int(time.time()))
	sha_1.update((unix_timestamp + ' ' + auth).encode('utf-8'))
	return "SAPISIDHASH " + unix_timestamp + "_" + sha_1.hexdigest()	
	

def log(msg, level=None):
	import xbmc
	if level is None:
		level = xbmc.LOGINFO
	xbmc.log(msg, level=level)


def initSession():
	import requests
	s = requests.Session()
	s.verify = False
	s.headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'
	return s


def getLanguage(text):
	languages = [
		{'chars': 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя', "hl":"ru"},
		{'chars': 'אבגדהוזחטיכלמנסעפצקרשת', "hl":"iw"}
	]
	for language in languages:
		for i in range(len(language['chars'])):
			if language['chars'][i] in text:
				return language['hl']
	return 'en'
	

def timeStrToSeconds (str):
	import time, datetime
	if str is None:
		return None
	format = '%H:%M:%S'
	if len(str.split(':')) == 2:
		format = '%M:%S'	
	x = time.strptime(str,format)
	return int(datetime.timedelta(hours=x.tm_hour,minutes=x.tm_min,seconds=x.tm_sec).total_seconds())	
	
	
	

def posterUrl(thumbUrl, size='540'):
	if '=w60-h60' in thumbUrl:
		return thumbUrl.replace('=w60-h60','=w{}-h{}'.format(size,size))
	elif thumbUrl.endswith('=s192'):
		return thumbUrl.replace('=s192','=s{}'.format(size))
	elif '=s88-c-k' in thumbUrl:	
		return thumbUrl.replace('=s88-c-k','=s{}-c-k'.format(size))
	elif '=s48-c-k' in thumbUrl:	
		return thumbUrl.replace('=s48-c-k','=s{}-c-k'.format(size))
#	elif 'hqdefault.jpg' in thumbUrl or 'sddefault.jpg' in thumbUrl:
#		if '?' in thumbUrl:
#			thumbUrl = thumbUrl.split('?')[0]
#		result = thumbUrl.replace('hqdefault.jpg','maxresdefault.jpg').replace('sddefault.jpg','maxresdefault.jpg')
	else:
		return thumbUrl
		
		



def getAudioStream(data):
	streams = [stream for stream in data['streamingData']['adaptiveFormats'] if stream['mimeType'].startswith('audio/mp4')]
	streams = sorted(streams, key=lambda x: x['itag'])
	return streams[-1]['url']



def getDashManifest(video, cipher, vp9=True):			
	import urllib
	if not video['videoDetails']['isLive']:
		manifest = '<?xml version="1.0" encoding="UTF-8"?><MPD xmlns="urn:mpeg:dash:schema:mpd:2011" type="static" mediaPresentationDuration="PT{}S"><Period>'.format(video['videoDetails']['lengthSeconds'])
	else:
		manifest = '<?xml version="1.0" encoding="UTF-8"?><MPD xmlns="urn:mpeg:dash:schema:mpd:2011" type="dynamic" ><Period>'
		from bs4 import BeautifulSoup
		session = initSession()
		video['streamingData']['dashSegmentListStartNumber'] = BeautifulSoup(session.get(video['streamingData']['dashManifestUrl']).text, features="html.parser").find('segmentlist')['startnumber']
	repSets = {}	
	if vp9 is True:
		streams = forceVP9(video['streamingData']['adaptiveFormats'])
	else:
		streams = video['streamingData']['adaptiveFormats']
	for stream in streams:	
		mimeType = urllib.parse.unquote(stream['mimeType'])
		codecs = mimeType.split('; codecs=')[1].replace('"','')
		mimeType = mimeType.split('; codecs=')[0]					
		rep = '<Representation codecs="{}" bandwidth="{}" mimeType="{}"'.format(codecs, stream['bitrate'], mimeType)
		if mimeType.startswith('video'):
			rep += ' width="{}" height="{}">'.format(stream['width'], stream['height'])
		else:
			rep += '><AudioChannelConfiguration schemeIdUri="urn:mpeg:dash:23003:3:audio_channel_configuration:2011" value="2"/>'
		rep += '<BaseURL>{}</BaseURL>'.format(escape(getStreamUrl(stream, cipher)))
		if video['videoDetails']['isLive']:
			rep += '<SegmentTemplate startNumber="{}" media="{}"><SegmentTimeline><S t="0" r="1" d="5005"/></SegmentTimeline></SegmentTemplate>'.format(video['streamingData']['dashSegmentListStartNumber'], escape('&sq=$Number$'))
		else:
			rep += '<SegmentBase indexRange="{}-{}"><Initialization range="{}-{}" /></SegmentBase>'.format(stream['indexRange']['start'], stream['indexRange']['end'], stream['initRange']['start'], stream['initRange']['end'])			
		rep += '</Representation>'		
		if mimeType not in repSets.keys():
			repSets[mimeType] = []
		repSets[mimeType].append(rep)
	for mimeType, sets in repSets.items():
		manifest += '<AdaptationSet mimeType="{}">{}</AdaptationSet>'.format(mimeType, ''.join(sets))	
	manifest += '</Period></MPD>'
	return manifest		


def forceVP9(streams):
	import urllib	
	exists = False
	for stream in streams:
		mimeType = urllib.parse.unquote(stream['mimeType'])
		codecs = mimeType.split('; codecs=')[1].replace('"','')
		if codecs=='vp9':
			exists=True
			break
	if exists is False:
		return streams	
	result = []
	for stream in streams:
		mimeType = urllib.parse.unquote(stream['mimeType'])
		codecs = mimeType.split('; codecs=')[1].replace('"','')
		if mimeType.startswith('audio') or codecs=='vp9':
			result.append(stream)
	return result
	
		
		
	
def getStreamUrl(stream, cipherJson):	
	import urllib
	if 'url' in stream.keys():
		return stream['url']
	data = dict(urllib.parse.parse_qsl(stream['signatureCipher']))	
	url = unquote(data['url'])
	if 'sig' in data:
		url += '&{}={}'.format(data['sp'], urllib.parse.unquote(data['sig']))
#	else:
#		c = copy.deepcopy(cipherJson) 
#		url += '&{}={}'.format(data['sp'], c.execute(urllib.parse.unquote(data['s'])))
	return url


def escape(str):
	return str.replace("&", "&amp;").replace('"', "&quot;").replace("<", "&lt;").replace(">", "&gt;")


def unquote(str):
	import urllib
	return urllib.parse.unquote(str).replace('%2C',',').replace('%2F','/').replace('%3D','=')


def startMPDProxyServer(manifest, port):
	import http.server as http_server
	import threading
	class mpdProxyHandler (http_server.SimpleHTTPRequestHandler):
		def do_GET(self):
			try:			
				self.send_response(200)
				self.send_header("Content-type", 'application/octet-stream')
				self.send_header("Content-Length", len(self.server.manifest))
				self.end_headers()				
				self.wfile.write(self.server.manifest.encode())			
			except:				
				try:
					self.send_response(404)
					self.end_headers()
				except:
					None				
		def log_message(self, format, *args):
			return			
	server = http_server.HTTPServer(('localhost', port) , mpdProxyHandler)											
	server.manifest = manifest				
	thread = threading.Thread(target = server.serve_forever)
	thread.deamon = True
	thread.start()	
	return server


def streamLabel(stream):
	ext = stream['mimeType'].split(';')[0].split('/')[1]
	codecs = stream['mimeType'].split('codecs="')[1].split('"')[0]
	if stream['mimeType'].startswith('audio'):
		return 'ITag {}: Audio [{} ; {} ; {} kbps]'.format(stream['itag'], ext, codecs, int(stream['bitrate']/1024))
	elif stream['mimeType'].startswith('video'):
		if ',' in codecs:
			return 'ITag {}: Video [{} ; {} ; {}]  Audio [{} ; {} ; {} kbps]'.format(stream['itag'],ext, codecs.split(',')[0], stream['qualityLabel'], ext, codecs.split(',')[1], int(stream['bitrate']/1024))
		else:
			return 'ITag {}: Video [{} ; {} ; {}]'.format(stream['itag'],ext, codecs, stream['qualityLabel'])
			
			
			
			