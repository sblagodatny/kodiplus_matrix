def log(msg, level=None):
	import xbmc
	if level is None:
		level = xbmc.LOGINFO
	xbmc.log(msg, level=level)
	
def initSession():
	import requests
	s = requests.Session()
	s.verify = False
	s.headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'
	return s

def validateAccount(account):
	from . import api, web
	api.validateAccount(account)
	

# portrait: 600x900	landscape: 1066x600
def imageUrl(size):
	if size=='original':
		return 'https://image.tmdb.org/t/p/original'
	else:
		return 'https://image.tmdb.org/t/p/w{}_and_h{}_bestv2'.format(size.split('x')[0],size.split('x')[1])
