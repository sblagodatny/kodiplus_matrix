from . import common

_timeout=2
_url = 'https://api.themoviedb.org/3'
_key = '0b09b57f878cd1d504010c1aad40e753'
#_key = '8d6d91941230817f7807d643736e8a49' # found in web



def validateAccount(account):	
	s = common.initSession()
	token = s.get(_url+'/authentication/token/new', params={'api_key': _key}, timeout=_timeout).json()['request_token']	
	data = s.post(_url+'/authentication/token/validate_with_login', params={'api_key': _key}, data={'username': account['username'], 'password': account['password'], 'request_token': token}, timeout=_timeout).json()
	if 'status_message' in list(data.keys()):
		raise Exception(data['status_message'])
	account['api_session_id'] = s.post(_url+'/authentication/session/new', params={'api_key': _key}, data={'request_token': token}, timeout=_timeout).json()['session_id']
	data = s.get(_url+'/account', params={'api_key': _key, 'session_id': account['api_session_id']}, timeout=_timeout).json()		
	account['id']=data['id']
	account['title']=account['username']
	account['thumb']=''


def delEmptyKeys(d):
	remove = []
	for key, value in d.items():
		if value is None or (isinstance(value, str) and len(value.lstrip())==0):		
			remove.append(key)
	for key in remove:
		del d[key]


def getContentDetails(account, content, language='ru-RU'):
	s = common.initSession()	
	params = {'api_key': _key,	'language': language, 'session_id': None if account is None else account['api_session_id'], 'append_to_response': 'external_ids,account_states,credits'}	
	data = s.get('{}/{}/{}'.format(_url, content['type'], content['id']), params=params, timeout=_timeout).json()			
	content.update({
		'id': data['id'],
		'title': data.get('title', data.get('name','')),
		'titleOriginal': data.get('original_title', data.get('original_name','')),						
		'poster': '' if data.get('poster_path','')=='' else '' if data['poster_path'] is None else common.imageUrl('600x900') + data['poster_path'],
		'fanart': '' if data.get('backdrop_path','')=='' else '' if data['backdrop_path'] is None else common.imageUrl('original') + data['backdrop_path'],		
		'plot': data.get('overview',''),		
		'releaseDate': data.get('release_date', data.get('first_air_date','')),
		'rating': data.get('vote_average', ''),
		'imdbId':  data.get('external_ids',{}).get('imdb_id',''),
		'studio': [c['name'] for c in data.get('production_companies',[])],
		'status': data.get('status',''),
		'genre': [c['name'] for c in data.get('genres',[])],
		'language': [c['name'] for c in data.get('spoken_languages',[])],
		'country': [c['name'] for c in data.get('production_countries',[])],		
		'cast': [{'name': c['name'], 'thumbnail': common.imageUrl('300x450') + c['profile_path'], 'role': c['character']} for c in data.get('credits',{}).get('cast',[]) if c['profile_path'] is not None ],		
	})
	if account is not None:
		content.update({
			'myrating':  '' if data['account_states']['rated'] is False else data['account_states']['rated']['value'],
			'favorite': data['account_states']['favorite'],
			'watchlist': data['account_states']['watchlist']				
		})
	delEmptyKeys(content)
	if content['type']=='tv':
		content['seasons']=[]
		for sdata in data.get('seasons',[]):
			if sdata['season_number']<1:
				continue
			item = {
				'id': sdata['season_number'],
				'title': sdata['name'],
				'poster': '' if sdata.get('poster_path','')=='' else '' if sdata['poster_path'] is None else common.imageUrl('600x900') + sdata['poster_path'],
				'plot': sdata.get('overview',''),
				'releaseDate': sdata.get('air_date',''),
				'rating': sdata.get('vote_average',''),
				'episodes': sdata.get('episode_count',0)
			}
			delEmptyKeys(item)
			content['seasons'].append(item)						
			

def getContentDetailsList(account, contentList, language='ru-RU'):
	import concurrent.futures
	def task(content):		
		getContentDetails(account, content, language)		
	ex = concurrent.futures.ThreadPoolExecutor(max_workers=20)
	for content in contentList:
		ex.submit(task, content)
	ex.shutdown(wait=True)
		

def getAccountList(account, listId, language='ru-RU', details=True):
	params = {'language': language, 'session_id': account['api_session_id'], 'api_key': _key, 'sort_by': 'created_at.desc'}
	if listId not in ['watchlist', 'rated', 'favorite']:
		s = common.initSession()
		result = s.get('{}/list/{}'.format(_url, listId), params=params, timeout=_timeout).json().get('items',[])	
	else:
		import concurrent.futures
		result = {'tv': [],'movie': []}
		def task(type):
			s = common.initSession()
			result[type] = [{'id': item['id'], 'type': type} for item in s.get('{}/account/{}/{}/{}'.format(_url, account['id'], listId, type.replace('movie','movies')), params=params, timeout=_timeout).json().get('results',[])]
		ex = concurrent.futures.ThreadPoolExecutor(max_workers=10)
		for type in result.keys():
			ex.submit(task, type)
		ex.shutdown(wait=True)
		if listId == 'rated':
			result = result['tv'][:10] + result['movie'][:10]
		else:
			result = result['tv'] + result['movie']	
	if details is True:
		getContentDetailsList(account, result, language)	
	return result
	

def getAccountContentState(account, content, season=None):
	s = common.initSession()	
	params = {'api_key': _key, 'session_id': account['api_session_id']}
	if season is None:
		data = s.get('{}/{}/{}/account_states'.format(_url, content['type'], content['id']), params=params, timeout=_timeout).json()
		if data['rated'] is not False:
			content['myrating']=data['rated']['value']
		content['favorite'] = data['favorite']
		content['watchlist'] = data['watchlist']	
	else:
		data = s.get('{}/tv/{}/season/{}/account_states'.format(_url, content['id'], season['id']), params=params, timeout=_timeout).json()		
		season['episodesWatched'] = len([i for i in data.get('results',[]) if i['rated'] is not False])
		

def getAccountContentStateSeasons(account, content):
	import concurrent.futures
	def task(season):		
		getAccountContentState(account, content, season)		
	ex = concurrent.futures.ThreadPoolExecutor(max_workers=10)
	for season in content['seasons']:
		ex.submit(task, season)
	ex.shutdown(wait=True)
	
	
def getSeason(account, contentId, seasonId, language='ru-RU'):
	s = common.initSession()
	params = {'api_key': _key, 'language': language, 'append_to_response': 'account_states', 'session_id': None if account is None else account['api_session_id']}
	data = s.get('{}/tv/{}/season/{}'.format(_url, contentId, seasonId), params=params, timeout=_timeout).json()
	season = {
		'id': data['season_number'],
		'title': data['name'],
		'poster': '' if data.get('poster_path','')=='' else '' if data['poster_path'] is None else common.imageUrl('600x900') + data['poster_path'],
		'plot': data.get('overview',''),
		'releaseDate': data.get('air_date',''),
		'rating': data.get('vote_average',''),				
		'episodes': []
	}
	delEmptyKeys(season)
	for sdata in data.get('episodes',[]):
		if sdata['episode_number'] < 1:
			continue
		episode = {
			'id': sdata['episode_number'],
			'title': sdata['name'],
			'poster': '' if sdata.get('still_path','')=='' else '' if sdata['still_path'] is None else common.imageUrl('1066x600') + sdata['still_path'],
			'plot': sdata.get('overview',''),
			'releaseDate': sdata.get('air_date',''),
			'rating': sdata.get('vote_average',''),						
			'duration': '' if sdata.get('runtime',0)==0 else '' if sdata['runtime'] is None else sdata['runtime']*60
		}		
		if account is not None:
			myrating = [ e for e in data['account_states']['results'] if e['id']==sdata['id'] ][0]['rated']
			if myrating is not False:
				episode['myrating']=myrating['value']
		delEmptyKeys(episode)
		season['episodes'].append(episode)
	return season
	

def search(account, criteria, language='ru-RU', page=1):
	s = common.initSession()
	params = {'api_key': _key, 'language': language, 'sort_by': 'popularity.desc', 'query': criteria['title'], 'page': page}
	data = s.get('{}/search/{}'.format(_url, criteria['type']), params=params, timeout=_timeout).json()
	result = [{'id': item['id'], 'type': criteria['type']} for item in data.get('results',[])]
	getContentDetailsList(account, result, language)	
	return result
	
	
	
def discover(account, criteria, language='ru-RU', page=1):
	import datetime
	s = common.initSession()	
	params = {'api_key': _key, 'session_id': None if account is None else account['api_session_id']}
	
	
	params.update( {
		'air_date.gte': '', 'air_date.lte': '', 'primary_release_date.gte': '', 'primary_release_date.lte': '', 'first_air_date.gte': '', 'first_air_date.lte': '', 'release_date.gte': '', 'release_date.lte': '',
		'watch_region': 'IL', 'certification': '', 'certification_country': '', 'debug': '', 'region': '', 'vote_average.gte': '0', 'vote_average.lte': '10', 'vote_count.gte': '0',
		'with_keywords': '', 'with_origin_country': '', 'with_watch_monetization_types': '', 'with_watch_providers': '', 'with_release_type': '', 'with_runtime.gte': '0', 'with_runtime.lte': '400',			
		'page': page,								
		'show_me': 'unwatched',
		'sort_by': 'popularity.desc' if criteria['sort_by']=='popularity' else 'vote_average.desc',				
		'with_genres': criteria.get('genre',''),
		'with_networks': criteria.get('studio',''),		
		'with_original_language': criteria.get('language','ru|en|he'),		
	})
	gte = str(datetime.datetime.now().year - 5) + '-01-01'		
	if criteria['type'] == 'tv':
		params['first_air_date.gte'] = gte
	else:
		params['release_date.gte'] = gte
		params['with_release_type'] = '1'			
	data = s.get('{}/discover/{}'.format(_url, criteria['type']), params=params, timeout=_timeout).json()	
	result = [{'id': item['id'], 'type': criteria['type']} for item in data.get('results',[])]
	getContentDetailsList(account, result, language)	
	return result	




def discoverSpecial(account, criteria, language='ru-RU', pageSize = 40):
	result = []
	for page in range(10):
		result = result + [item for item in discover(account, criteria, language, page) if 'myrating' not in item.keys() ]
		if len(result) >= pageSize:
			return result
	return result


	
#def getExternalIds(id, type):
#	s = http.initSession()	
#	params = {'api_key': _key}
#	return s.get(url='{}/{}/{}/external_ids'.format(_api, type, str(id)), params=params).json()





# rate=None: delete
def setContentRate(account, content, rate):	
	s = common.initSession()
	params = {'api_key': _key, 'session_id': account['api_session_id']}
	if rate is not None:
		s.post('{}/{}/{}/rating'.format(_url, content['type'], content['id']), params=params, json={'value': float(rate)}, timeout=_timeout)
	else:
		s.delete('{}/{}/{}/rating'.format(_url, content['type'], content['id']), params=params, timeout=_timeout)
		
	
# listId: favorite, watchlist, state: boolean	
def setContentListState(account, content, listId, state):
	s = common.initSession()
	params = {'api_key': _key, 'session_id': account['api_session_id']}
	if listId in ['favorite','watchlist']:
		data = {'media_type': content['type'], 'media_id': content['id'], listId: state}	
		s.post('{}/account/{}/{}'.format(_url, account['id'], listId), params=params, json=data)

def setEpisodeRate(account, contentId, seasonId, episodeId, rate):
	s = common.initSession()
	params = {'api_key': _key, 'session_id': account['api_session_id']}	
	if rate is not None:
		s.post('{}/tv/{}/season/{}/episode/{}/rating'.format(_url, contentId, seasonId, episodeId), params=params, json={'value': float(rate)}, timeout=_timeout)
	else:
		s.delete('{}/tv/{}/season/{}/episode/{}/rating'.format(_url, contentId, seasonId, episodeId), params=params, timeout=_timeout)



#def getAccountLists(language='ru-RU'):	
#	account = getAccount()
#	s = http.initSession()			
#	params = {'language': language, 'session_id': account['session'], 'api_key': _key}
#	data = s.get(url=_api + '/account/' + account['id'] + '/lists', params=params).json()	
#	result = []
#	for item in  data['results']:
#		result.append({'id': str(item['id']), 'title': item['name']})	
#	return result


#def trending(type, language='ru-RU', maxresults=40, filter=None):
#	params = {
#		'language': language
#	}
#	url = _api + '/trending/' + type + '/week'
#	return buildResult(url,params,maxresults,filter)

