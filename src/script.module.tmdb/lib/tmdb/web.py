from . import common

_timeout=4
_url = 'https://www.themoviedb.org'

_studios = {	
	'Netflix': 213,
	'Amazon': 1024,
	'SyFy': 77,
	'ТНТ': 1191,
	'START': 2493
}

_languages = {
	'Russian': 'ru',
	'Hebrew': 'he',
	'English': 'en'			
}

_genres = {
	'Comedy': '35',
	'Action': '28|10759|53',
	'Detective': '9648',
	'Drama': '18',
	'Science Fiction': '878|10765',
	'Kids': '10751'
}



def validateAccount(account):
	from bs4 import BeautifulSoup
	s = common.initSession()
	content = BeautifulSoup(s.get(_url+'/login', timeout=_timeout).text, "html.parser")
	token = content.find('input', {'name': 'authenticity_token'})['value']		
	headers = {
		'Referer': 'https://www.themoviedb.org/login',
		'Content-Type': 'application/x-www-form-urlencoded',
		'Origin': 'https://www.themoviedb.org',
		'Cookie': '_ga=GA1.1.916854544.1734262470; preferences=%7B%22adult%22%3Atrue%2C%22i18n_fallback_language%22%3A%22en-US%22%2C%22locale%22%3A%22ru-RU%22%2C%22country_code%22%3A%22IL%22%2C%22timezone%22%3A%22Asia%2FJerusalem%22%7D; OptanonAlertBoxClosed=2024-12-15T11:45:35.009Z; _ga_HCD46FTYN6=GS1.1.1734268687.2.0.1734268694.53.0.0; session=eyJhbGciOiJIUzI1NiJ9.eyJzZXNzaW9uX2lkIjoiYTk0MTc2YzUyMTBkMzVmNDA2NTY0OTc5ZDQ5NzI4YmVmMDY1MDJkNTNjNTY2NDI5ZDcyZWRiZGM3OTQ2OGFkNiIsIm5iZiI6MTczNDI3Mjk2MS4yODEwMzU0LCJ2ZXJzaW9uIjoxLCJleHAiOjE3MzY4NjQ5NDksInByaXZpbGVnZSI6MCwiX2NzcmZfdG9rZW4iOiJiXzM0R0FHaXpFZUtIMTVhNU51eWVQY1kxQUZnakJPdUJPOVprTldDLVY4PSJ9.FYa83fyMYLkrjEeat4i-t-PMc9dSqhJWYzChgp5Vx-k; OptanonConsent=isGpcEnabled=0&datestamp=Sun+Dec+15+2024+16%3A29%3A21+GMT%2B0200+(Israel+Standard+Time)&version=202310.2.0&browserGpcFlag=0&isIABGlobal=false&hosts=&landingPath=NotLandingPage&groups=C0001%3A1%2CC0002%3A1%2CC0003%3A1%2CC0004%3A1&AwaitingReconsent=false&geolocation=IL%3BM; aws-waf-token=b769fe4d-790d-4ba8-970a-fdc83a84ab87:EQoAt4xlf542AAAA:SQroR+sSSgGKWjAxehgC816Jf6GJmbCs2Oa87UuxvyWpxHqnOxKczHo+XPZO1IYA9RQLNdOhqvmhkBsV2+VF5ektLwgtquLQEWsOwN2sojkUhsXGY2MT0t3rBdVXogi0LHh4Jj0qOoWqRbb2CQeALuDGZHkx/aCA+wuR+IK31OZjGSomvzPEXfj4sEGCXGviQt57OM/NdrrsDyuNGlLdV5gmSR4pm29iA1oEIoEy+hDxpEbrDS8I6RrZ28Y=; _ga_4257LNMWD9=GS1.1.1734272949.3.1.1734272969.40.0.0',
		'Sec-Fetch-Site': 'same-origin',
		'Sec-Fetch-Mode': 'navigate',
		'Sec-Fetch-User': '?1',
		'Sec-Fetch-Dest': 'document'
	}	
	reply = s.post(_url+'/login', data={'authenticity_token': token, 'username': account['username'], 'password': account['password']}, headers=headers, timeout=_timeout)	
	content = reply.text					
	content = BeautifulSoup(content, "html.parser")		
	try:	
#		account['title']=content.find('div', class_="about").find('h2').get_text().lstrip().rstrip()	
		account['title'] = account['username']
	except Exception as e:		
		common.log(str(e))
		raise Exception('Invalid Credentials')
	try:	
		account['thumb']=content.find('div',class_='avatar').find('img')['src'].replace('s=32','s=540')
	except:
		account['thumb']=''			
	account['cookies']=s.cookies.get_dict()
	account['validDays'] = 7
	

def parseCards(content):	
	result = []
	for tag in content.find_all('div', class_='card'):		
		try:
			result.append({
				'id': tag.find('h2').find('a')['href'].split('/')[-1],
				'type': 'tv' if '/tv/' in tag.find('h2').find('a')['href'] else 'movie'
			})		
		except:
			None
	return result


def search(account, criteria, language='ru-RU', page=1):	
	from . import api		
	if 'title' in criteria.keys():
		return api.search(account, criteria, language, page)		
	from bs4 import BeautifulSoup	
	import datetime
	s = common.initSession()		
	data = {
		'air_date.gte': '', 'air_date.lte': '', 'primary_release_date.gte': '', 'primary_release_date.lte': '', 'first_air_date.gte': '', 'first_air_date.lte': '', 'release_date.gte': '', 'release_date.lte': '',
		'watch_region': 'IL', 'certification': '', 'certification_country': '', 'debug': '', 'region': '', 'vote_average.gte': '0', 'vote_average.lte': '10', 'vote_count.gte': '0',
		'with_keywords': '', 'with_origin_country': '', 'with_watch_monetization_types': '', 'with_watch_providers': '', 'with_release_type': '', 'with_runtime.gte': '0', 'with_runtime.lte': '400',			
		'page': page,								
		'show_me': 'unwatched',
		'sort_by': 'popularity.desc' if criteria['sort_by']=='popularity' else 'vote_average.desc',				
		'with_genres': criteria.get('genre',''),
		'with_networks': criteria.get('studio',''),		
		'with_original_language': criteria.get('language','ru|en|he'),		
	}
	gte = str(datetime.datetime.now().year - 5) + '-01-01'		
	if criteria['type'] == 'tv':
		data['first_air_date.gte'] = gte
	else:
		data['release_date.gte'] = gte
		data['with_release_type'] = '1'			
	
	
#	headers = {
#		'X-Requested-With': 'XMLHttpRequest',
#		'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/5',
#		'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
#	}
#	cookies = {
#		'_ga':'GA1.1.916854544.1734262470',
#		'aws-waf-token':'b769fe4d-790d-4ba8-970a-fdc83a84ab87:EQoAhqxR8J8uAAAA:5DiQ6CnjprmFVn1oOqWEgcIEOWqlDQp9k5kYI6ikVRE6fyZ7BzAv3Ld3+9ArtNHEYTV+Ke+UFVWuauNvs9mJg5egvTx93tU02w2+dDD6TAEf/aMu/mUeo4nlYrPp6wf7x4IRyhMd+xk6nNG40uM41yselpI6VF8Gx20ObpnLUIoj5rxjeeXDIu200pAcrxuyejYTcGz/meHNH4kV1Eop3fnJevm/0iqHp/jxdY0SiffTO8BycTDjbYQVkJo=',
#		'preferences':'%7B%22adult%22%3Atrue%2C%22i18n_fallback_language%22%3A%22en-US%22%2C%22locale%22%3A%22ru-RU%22%2C%22country_code%22%3A%22IL%22%2C%22timezone%22%3A%22Asia%2FJerusalem%22%7D',
#		'OptanonAlertBoxClosed':'2024-12-15T11:45:35.009Z',
#		'_ga_HCD46FTYN6':'GS1.1.1734268687.2.0.1734268694.53.0.0',
#		'_ga_4257LNMWD9':'GS1.1.1734270126.2.1.1734270130.56.0.0',
#		'OptanonConsent':'isGpcEnabled=0&datestamp=Sun+Dec+15+2024+15%3A42%3A11+GMT%2B0200+(Israel+Standard+Time)&version=202310.2.0&browserGpcFlag=0&isIABGlobal=false&hosts=&landingPath=NotLandingPage&groups=C0001%3A1%2CC0002%3A1%2CC0003%3A1%2CC0004%3A1&AwaitingReconsent=false&geolocation=IL%3BM',
#		'session':'eyJhbGciOiJIUzI1NiJ9.eyJzZXNzaW9uX2lkIjoiYTk0MTc2YzUyMTBkMzVmNDA2NTY0OTc5ZDQ5NzI4YmVmMDY1MDJkNTNjNTY2NDI5ZDcyZWRiZGM3OTQ2OGFkNiIsImV4cCI6MTczNjg1NDcxOCwicHJpdmlsZWdlIjoxLCJfY3NyZl90b2tlbiI6IkFNX1ZKR0VpNkxIODkzclFIY2QxU0hud2lRNjQxODdlTkVETHBFX3RXbjA9IiwibmJmIjoxNzM0MjcwMTMxLjczOTc5NCwidmVyc2lvbiI6MSwiYWNjb3VudF9pZCI6IjU3ZTkzNjhjOTI1MTQxMjk4NzAxZTdiZCIsImRiX2lkIjoiYTk0MTc2YzUyMTBkMzVmNDA2NTY0OTc5ZDQ5NzI4YmVmMDY1MDJkNTNjNTY2NDI5ZDcyZWRiZGM3OTQ2OGFkNiIsInN0YWZmIjpmYWxzZSwidXBkYXRlZF9hdCI6MTczNDI3MDEyNX0.0wQ6wtzXRGoScSupEIHd1G2rM9NfYPo50v6QsvAnL3E'
#	}
#	
#	import json
#	common.log(json.dumps(account['cookies']))
	
	content = BeautifulSoup(s.post('{}/discover/{}'.format(_url,  criteria['type']), data=data, cookies=account['cookies'], timeout=_timeout).text, "html.parser")
#	content = BeautifulSoup(s.post('{}/discover/{}'.format(_url,  criteria['type']), data=data, cookies=account['cookies'], headers=headers, timeout=_timeout).text, "html.parser")

	
	result = parseCards(content)	
	api.getContentDetailsList(account, result, language)
	return result
#	return []











#def getSeasons(id, language='ru-RU'):	
#	s = http.initSession()
#	account = tmdb.getAccount()
#	setLanguage(account['cookies'], language)	
#	soup = BeautifulSoup(s.get(url='{}/tv/{}/seasons'.format(_url,id), cookies=account['cookies']).content, "html.parser")
#	result = []
#	for tag in soup.find_all('div', class_="season"):
#		if tag.find('a')['href'].split('/')[-1] == '0':
#			continue
#		season = {
#			'id': tag.find('a')['href'].split('/')[-1],
#			'title': tag.find('h2').get_text(),
#			'poster': '' if tag.find('img') is None else _imgPoster + '/' + tag.find('img')['src'].split('/')[-1],
#			'year': tag.find('h4').get_text().split('|')[0].rstrip().lstrip(), ##
#			'episodes': int(tag.find('h4').get_text().split(' ')[-2]),
#			'description': tag.find('div', class_="season_overview").get_text()
#		}
#		try:
#			season['year'] = int(season['year'])
#		except:
#			del season['year']
#		result.append(season)
#	return result
	

#def getSeason(id, season, language='en-US'):	
#	s = http.initSession()
#	soup = BeautifulSoup(s.get(url='{}/tv/{}/season/{}'.format(_url,id,season), params={'language': language}).content, "html.parser")
#	result = {'name': soup.find('meta', {'property': "og:title"})['content']}
#	result['episodes'] = []
#	for tag in soup.find_all('div', class_="episode"):		
#		episode = {			
#			"episode_number": int(tag.find('span', class_="episode_number").get_text()),
#			"id": tag['id'].replace('episode_',''),
#			"name": tag.find('a', class_="open")['title'].split('Episode')[1],
#			"overview": tag.find('div', class_="overview").find('p').get_text(),
#			"still_path": 'https://image.tmdb.org' + tag.find('img')['src']
#		}			
#		try:			
#			episode['air_date'] = parseDateStr(tag.find('div', class_="date").find('span').get_text(), language)		
#			episode['air_date'] = episode['air_date'].strftime('%Y-%m-%d')		
#		except:
#			None		
#		if episode["still_path"].endswith('.svg'):
#			episode["still_path"] = ''
#		if episode['overview'].startswith('Нет обзора') or episode['overview'].startswith('אין באתר תקציר') or episode['overview'].startswith("We don't have an overview"):
#			episode['overview'] = ''
#		episode['name'] = episode['name'].replace( episode['name'].split('-')[0] + '-' , '').lstrip().rstrip()
#		result['episodes'].append(episode)
#	return result



#def setLanguage(cookies, language='en-US'):
#	import urllib, json
#	prefs = json.loads(urllib.parse.unquote(cookies['tmdb.prefs']))
#	prefs['locale'] = language
#	cookies['tmdb.prefs']=json.dumps(prefs)


#def setItemListState(id, type, list, state):
#	account = tmdb.getAccount()
#	s = http.initSession()
#	data = {"items":[{"media_type": type, "media_id": int(id)}]}
#	headers = {
#		'X-Requested-With': 'XMLHttpRequest',		
#		'Referer': _url + '/' + type + '/' + id				
#	}
#	if state is True:
#		reply=s.post('{}/list/{}/items'.format(_url, list), data={'data': json.dumps(data)}, headers=headers, cookies=account['cookies']).json()
#	else:
#		reply=s.delete('{}/list/{}/items'.format(_url, list), data={'data': json.dumps(data)}, headers=headers, cookies=account['cookies']).json()		
#	if 'errors' in reply.keys():
#		raise Exception(reply['errors'][0])	


#def parseDateStr(s, language):	
#	months = {
#		'ru-RU': ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'],
#		'he-IL': ['ינואר','פברואר','מרץ','אפריל','מאי','יוני','יולי','אוגוסט','ספטמבר','אוקטובר','נובמבר','דצמבר'],
#		'en-US': ['january','february','march','april','may','june','july','august','september','october','november','december']
#	}
#	s = s.lower().lstrip().rstrip().split(' ')
#	year = int(s[2])
#	if language in ['he-IL','en-US']:
#		month = s[0]
#		day = int(s[1].replace(',',''))
#	elif language == 'ru-RU':
#		month = s[1]
#		day = int(s[0])
#	month = months[language].index(month)
#	return datetime.datetime(year, month, day)





#### Data Managament ####


#def addSeason(id, season, language='ru-RU'):
#	account = tmdb.getAccount()
#	s = http.initSession()
#	data = {
##		'id': None,
##		'season_number_form': season['id'],
#		'season_number': season['id'],
##		'name': season['title'],
##		'overview': season['description'],
##		'air_date': None, #'Mon Sep 20 2021 15:43:42 GMT+0300 (Israel Daylight Time)',
#		'locked': False,
##		'locked_fields': ''
#	}	
#	headers = {
#		'Accept': 'application/json, text/javascript, */*; q=0.01',
#		'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
#		'X-Requested-With': 'XMLHttpRequest',
#		'Referer': '{}/tv/{}/edit?active_nav_item=seasons'.format(_url, id, season)
#	}
#	params = {
#		'timezone': 'Asia/Jerusalem',
#		'translate': 'false',
#		'language': language
#	}
#	try:
#		reply=s.post('{}/tv/{}/remote/seasons'.format(_url, id), params=params, data=data, headers=headers, cookies=account['cookies']).json()
#	except:
#		raise Exception('Invalid input')
#	if 'failure' in reply.keys():
#		raise Exception(reply['failure']['errors'][0])
		
	
#def addEpisode(contentId, seasonId, data, language='ru-RU'):
#	account = tmdb.getAccount()
#	s = http.initSession()
#	params = {
#		'timezone': 'Asia/Jerusalem',
#		'translate': False,
#		'language': language
#	}
#	headers = {
#		'Accept': 'application/json, text/javascript, */*; q=0.01',
#		'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
#		'X-Requested-With': 'XMLHttpRequest',
#		'Referer': '{}/tv/{}/season/{}/edit?active_nav_item=episodes'.format(_url, contentId, seasonId)
#	}
#	try:
#		reply=s.post('{}/tv/{}/season/{}/remote/episodes'.format(_url, contentId, seasonId), params=params, data={'data': json.dumps(data)}, headers=headers, cookies=account['cookies']).json()
#	except:
#		raise Exception('Invalid input')
#	if 'failure' in reply.keys():
#		raise Exception(reply['failure']['errors'][0])


#def updateEpisode(contentId, seasonId, data, language='ru-RU'): 
#	account = tmdb.getAccount()	
#	s = http.initSession()
#	d = s.get('{}/tv/{}/season/{}/episode/{}/images/backdrops'.format(_url, contentId, seasonId, data['episode_number']), cookies=account['cookies'] ).text	
#	mediaId = re.search(r"media_id: '(.*?)'", d).group(1)			
#	params = {
#		'series_id': contentId,
#		'timezone': 'Asia/Jerusalem',
#		'translate': False,
#		'language': language
#	}	
#	headers = {
#		'Accept': 'application/json, text/javascript, */*; q=0.01',
#		'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
#		'X-Requested-With': 'XMLHttpRequest',
#		'Referer': '{}/tv/{}/season/{}/edit?active_nav_item=episodes'.format(_url, contentId, seasonId)
#	}	
#	try:
#		reply = s.post('{}/tv/episode/{}/remote/primary_facts'.format(_url, mediaId), params=params, data={'data': json.dumps(data)}, headers=headers, cookies=account['cookies']).json()
#	except:
#		raise Exception('Invalid input')
#	if 'failure' in reply.keys():
#		raise Exception(reply['failure']['errors'][0])
		
		
#def uploadEpisodeThumb(contentId, seasonId, episodeId, thumbPath):	
#	account = tmdb.getAccount()	
#	s = http.initSession()	
#	d = s.get('{}/tv/{}/season/{}/episode/{}/images/backdrops'.format(_url, contentId, seasonId, episodeId), cookies=account['cookies'] ).text	
#	mediaId = re.search(r"media_id: '(.*?)'", d).group(1)		
#	multipart_form_data = {
#		'media_id': (None, mediaId),
#		'media_type': (None, 'tv_episode'),
#		'type': (None, 'backdrop'),
#		'translate': (None, 'false'),				
#		'upload_files': (os.path.basename(thumbPath), open(thumbPath, 'rb'), 'image/jpeg'),			
#	}	
#	headers = {
#		'Referer': '{}/tv/{}/season/{}/episode/{}/images/backdrops'.format(_url, contentId, seasonId, episodeId)
#	}	
#	try:
#		reply = s.post(_url + '/image', files=multipart_form_data, headers=headers, cookies=account['cookies']).json()	
#	except:
#		raise Exception('Invalid data')
#	if 'failure' in reply.keys():
#		raise Exception(reply['failure']['errors'][0])		


#def syncSourceSeasons(content, source):	
#	import vod
#	try:				
#		source = dict(urllib.parse.parse_qsl(source['url'].split('?')[1]))	
#		sourceData = json.loads(string.b64decode(source['content']))
#		sourceContent = json.loads(string.b64decode(sourceData['content']))	
#		seasonsData = vod.getLib(sourceData['source']).seasons(sourceData)		
#		for sourceSeason in seasonsData:
#			exists = [season for season in content['seasons'] if str(season['id'])==str(sourceSeason['id']) ]
#			if len(exists)==0:
#				if xbmcgui.Dialog().yesno('TMDB', 'Add season {} for {}'.format(sourceSeason['id'], content['title']) ) is True:
#					season={'id': str(sourceSeason['id']), 'title': sourceSeason['title'], 'description': sourceSeason['description']}		
#					try:
#						tmdbWeb.addSeason(content['id'], season, language=vod._sources[sourceContent['source']]['language'])		
#						xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%(content['title'], 'Season {} added'.format(sourceSeason['id']), 1, _addon.getAddonInfo('icon')))
#					except Exception as e:
#						xbmcgui.Dialog().ok('TMDB', 'Unable to add season {} for {}'.format(sourceSeason['id'], content['title']) + "\n" + str(e))
#	except Exception as e:
#		xbmcgui.Dialog().ok('TMDB', str(e))
#		import traceback
#		storage.log(traceback.format_exc())
#		return None
		
		
#def syncTMDBEpisodes(content, source, season):	
#	import vod	
#	import shutil	
#	import xbmcvfs
#	import requests
#	from PIL import Image
#	sourceData = dict(urllib.parse.parse_qsl(source['url'].split('?')[1]))			
#	sourceContent = json.loads(string.b64decode(sourceData['content']))	
#	sourceEpisodes = json.loads(string.b64decode(sourceData['episodes']))				
#	tmdbEpisodes = tmdbWeb.getSeason(content['id'], season, language=vod._sources[sourceContent['source']]['language'])['episodes']		
#	progressDialog = xbmcgui.DialogProgressBG()
#	progressDialog.create(content['title'], 'Syncing episodes for season {}'.format(season))
#	storage.log(content['title'] + "\n" + 'Syncing episodes for season {}'.format(season))
#	errors = False
#	updates = False
#	
#	for epSource in sourceEpisodes:
#		i = sourceEpisodes.index(epSource)
#		epTMDB=[ep for ep in tmdbEpisodes if ep['episode_number']==int(epSource['id'])]
#		if len(epTMDB)==0:
#				
#			## Add episode ##
#			
##			if 'air_date' not in epSource.keys():
##				if i > 0:
##					dt = datetime.datetime(*(time.strptime(sourceEpisodes[i-1]['air_date'], '%Y-%m-%d')[0:6]))
##					dt = dt + datetime.timedelta(days=7)
##					epSource['air_date'] = dt.strftime('%Y-%m-%d')									
##				else:
##					dstr = xbmcgui.Dialog().numeric(1, 'Enter date for episode {} of season {}:'.format(epSource['id'], season), None)
##					if dstr is None:
##						storage.log('Unable to proceed, date missing for episode {} of season {}'.format(epSource['id'], season))
##						errors = True
##						break
##					dt = datetime.datetime(*(time.strptime(dstr, '%d/%m/%Y')[0:6]))	
##					epSource['air_date'] = dt.strftime('%Y-%m-%d')	
#			data = {
#				"episode_number": epSource['id'],
#				"name": epSource['title'],
#				"overview": epSource.get('description',''),
#				"air_date": epSource.get('air_date','')
#			}								
#			try:				
#				tmdbWeb.addEpisode(content['id'], season, data, language=vod._sources[sourceContent['source']]['language'])		
#				updates = True
#				storage.log('Episode {} added for season {}'.format(epSource['id'], season))
#			except Exception as e:
#				storage.log('Unable to add episode {} for season {}'.format(epSource['id'], season) + "\n" + str(e))						
#				errors = True
#				continue
#			if 'thumb' in epSource.keys() and len(epSource['thumb'])>0:				
#				try:
#					thumbPath = xbmcvfs.translatePath('special://temp') + 'thumb.jpg'				
#					with open(thumbPath, 'wb') as f:
#						shutil.copyfileobj(requests.get(epSource['thumb'], stream=True).raw, f)
#					image = Image.open(thumbPath)		
#					image = image.resize((400, 225), Image.ANTIALIAS)
#					image.save(thumbPath, "JPEG")
#					tmdbWeb.uploadEpisodeThumb(content['id'], season, epSource['id'], thumbPath)		
#					storage.log('Thumb uploaded for episode {} of season {}'.format(epSource['id'], season))				
#				except Exception as e:
#					storage.log('Unable to upload thumb for episode {} of season {}'.format(epSource['id'], season) + "\n" + str(e))						
#					errors = True
#					continue	
#	
#		else:
#						
#			epTMDB=epTMDB[0]
#			
#			## Update episode ##						
#			data = {"episode_number": epSource['id'],}
#			proceed = False
#			if 'air_date' in epTMDB.keys():				
#				data['air_date'] = epTMDB['air_date']	
#			elif 'air_date' in epSource.keys():
#				data['air_date'] = epSource['air_date']					
#				proceed = True									
#			if len(epSource.get('description','')) <= len(epTMDB.get('overview','')):
#				data['overview'] = epTMDB.get('overview','')
#			else:
#				data['overview'] = epSource['description']				
#				proceed = True
#										
##			if len(epSource['title']) > len(epTMDB['name']):
##				data['name'] = epSource['title']				
##				storage.log('title update')				
##				proceed = True
##			else:			
#			data['name'] = epTMDB['name']
#				
#					
#			if proceed is True:																															
#				try:				
#					tmdbWeb.updateEpisode(content['id'], season, data, language=vod._sources[sourceContent['source']]['language'])		
#					updates = True
#					storage.log('Episode {} updated for season {}'.format(epSource['id'], season))
#				except Exception as e:
#					storage.log('Unable to update episode {} for season {}'.format(epSource['id'], season) + "\n" + str(e))						
#					errors = True
#					continue								
#			if 'thumb' in epSource.keys() and len(epSource['thumb'])>0 and len(epTMDB['still_path'])==0:				
#				try:
#					thumbPath = xbmcvfs.translatePath('special://temp') + 'thumb.jpg'				
#					with open(thumbPath, 'wb') as f:
#						shutil.copyfileobj(requests.get(epSource['thumb'], stream=True).raw, f)
#					image = Image.open(thumbPath)		
#					image = image.resize((400, 225), Image.ANTIALIAS)
#					image.save(thumbPath, "JPEG")
#					tmdbWeb.uploadEpisodeThumb(content['id'], season, epSource['id'], thumbPath)		
#					updates = True
#					storage.log('Thumb uploaded for episode {} of season {}'.format(epSource['id'], season))				
#				except Exception as e:
#					storage.log('Unable to upload thumb for episode {} of season {}'.format(epSource['id'], season) + "\n" + str(e))						
#					errors = True
#					continue	
#
#		progressDialog.update(percent=int((i+1)/len(sourceEpisodes)*100))
#		
#	
#	progressDialog.close()
#	del progressDialog
#	storage.log(content['title'] + "\n" + 'Completed syncing episodes for season {}'.format(season))
#	if errors is True:
#		msg = 'Episodes of season {} added and/or updated with errors, check the log for details'.format(season) 
#	elif updates is True:
#		msg = 'Episodes of season {} added and/or updated sucessfully'.format(season) 
#	else:
#		msg = 'TMDB data for season {} is up to date'.format(season)
#	xbmcgui.Dialog().ok(content['title'], msg)
