import sys, xbmcaddon, xbmcvfs
import urllib.parse as parse
import xbmc


_baseUrl = sys.argv[0]
_handleId = int(sys.argv[1])
_params = dict(parse.parse_qsl(sys.argv[2][1:]))	

if 'handler' not in _params.keys():	
	_params['handler'] = 'Root'

_addon = xbmcaddon.Addon()
	
_path = {'addon': _addon.getAddonInfo('path')}
_path['img'] = _path['addon'] + 'resources/img/'
_path['settings'] = _path['addon'].replace('addons','userdata/addon_data')
if xbmc.getCondVisibility('system.platform.android'):
	_path['download'] = '/storage/emulated/0/download/' + _addon.getAddonInfo('id') + '/'
else:
	_path['download'] = xbmcvfs.translatePath('special://home').split('AppData')[0] + 'Downloads/' + _addon.getAddonInfo('id') + '/'

_path['favorites'] = _addon.getSetting('favoritesPath')
if len(_path['favorites']) == 0:
	_path['favorites'] = _path['settings']
_path['favorites'] = _path['favorites'] + '/favorites/'


if not xbmcvfs.exists(_path['settings']):
	xbmcvfs.mkdir(_path['settings'])
if not xbmcvfs.exists(_path['favorites']):
	xbmcvfs.mkdir(_path['favorites'])

_title = 'Android Apps'

sys.path.insert(1, _path['addon'] + '/lib')



def manageFavorites(app, action):
	import json	
	from contextlib import closing
	path = _path['favorites'] + app['package'] + '.json'
	if action=='remove':
		xbmcvfs.delete(path)
		xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%(_title, 'Removed from Favorites', 1, _addon.getAddonInfo('icon')))
	elif action=='add':		
		with closing(xbmcvfs.File(path, 'w')) as f:						
			json.dump(app, f, indent=4)	
		xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%(_title, 'Added to Favorites', 1, _addon.getAddonInfo('icon')))


def downloadApp(app):
	from helpers import string, gui
	import json, xbmcgui
	from lib import repos				
	versions = repos.getLib(app['repo']).versions(app)
	s = xbmcgui.Dialog().select('{} ({})'.format(app['title'],app.get('repo','')), ['{} | {} | {} | {}'.format(v['date'], v['id'], v['size'], v['description']) for v in versions])	
	if s==-1:
		return
	version = versions[s]
	if not xbmcvfs.exists(_path['download']):
		xbmcvfs.mkdirs(_path['download'])
	url = repos.getLib(app['repo']).download(app, version)
	path = _path['download'] + '{}_{}.apk'.format(app['package'], version['id'])	
	status = gui.download(url, path, app['title'], 'Downloading from ' + app['repo'])
	if status is True:
		xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%(_title, 'Downloaded from {}'.format(app['repo']), 1, _addon.getAddonInfo('icon')))
#		xbmc.executebuiltin('StartAndroidActivity("", "android.intent.action.VIEW","vnd.android.package-archive","file:///sdcard/Movies/stas.apk")')


def handlerApp(app=None):
	from helpers import string, storage
	import json, xbmcgui
	from lib import repos
	if app is None:
		app = json.loads(string.b64decode(_params['app']))	
	repos.get(app)	
	pathFavorites = _path['favorites'] + app['package'] + '.json'
	storage.log(json.dumps(app))	
	while True:
		items = [{'label': 'Details', 'thumb': _path['img'] + '/file.png'}]
		if 'repo' in app.keys():
			items.append({'label': 'Download', 'thumb': _path['img'] + '/arrow-down.png'})
		if xbmc.getCondVisibility('system.platform.android'):		
			if app['package'] in xbmcvfs.listdir('androidapp://sources/apps/')[1]:
				items.append({'label': 'Settings', 'thumb': _path['img'] + '/android.png', 'action': 'StartAndroidActivity("", "android.settings.APPLICATION_DETAILS_SETTINGS",,"package:{}")'.format(app['package'])})				
# 				reference: https://developer.android.com/reference/android/provider/Settings
				items.append({'label': 'Launch', 'thumb': _path['img'] + '/rocket.png', 'action': 'StartAndroidActivity("{}")'.format(app['package'])})				
		if app.get('google',False) is True:		
			if xbmc.getCondVisibility('system.platform.android'):	
				items.append({'label': 'Open Google Store', 'thumb': _path['img'] + '/shapes.png', 'action': 'StartAndroidActivity(,android.intent.action.VIEW,,market://details?id={})'.format(app['package'])})		
			else:			
				items.append({'label': 'Open Google Store', 'thumb': _path['img'] + '/shapes.png', 'action': 'shell|start chrome "https://play.google.com/store/apps/details?id={}"'.format(app['package'])})						
		if xbmcvfs.exists(pathFavorites):
			items.append({'label': 'Remove from Favorites', 'thumb': _path['img'] + '/minus.png' })		
		else:
			items.append({'label': 'Add to Favorites', 'thumb': _path['img'] + '/plus.png' })				
		s = xbmcgui.Dialog().select('{} ({})'.format(app['title'],app.get('repo','')), [buildListItem(item) for item in items], useDetails=True )
		if s == -1:
			break
		item = items[s]
		if 'action' in item.keys():
			if item['action'].startswith('shell'):
				import os
				os.system(item['action'].split('|')[1])
			else:
				xbmc.executebuiltin(item['action'])
		elif item['label']=='Download':
			downloadApp(app)
		elif item['label']=='Details':
			description = '[B]Author:[/B] {}\n\n{}'.format(app.get('author',''), app.get('description',''))
			xbmcgui.Dialog().textviewer(app['title'], description)
		elif item['label']=='Add to Favorites':
			manageFavorites(app,'add')
		elif item['label']=='Remove from Favorites':
			manageFavorites(app,'remove')


def listApps(apps):	
	from helpers import string
	import xbmcplugin, json
	items=[]		
	for app in apps:						
		item=buildListItem({'label':app['title'], 'thumb': app['thumb']})			
		item.setIsFolder(False)	
		item.setPath(_baseUrl+'?' + parse.urlencode({'handler': 'App', 'app': string.b64encode(json.dumps(app)) }))				
		info = item.getMusicInfoTag()
		info.setComment("[B]Repo: [/B]{}\n[B]Package: [/B]{}\n[B]Author: [/B]{}\n{}".format(app.get('repo',''),app.get('package',''), app.get('author',''), app.get('description','')))						
		items.append((item.getPath(), item, item.isFolder(),))		
	xbmcplugin.addDirectoryItems(handle=_handleId, items=items, totalItems=len(items))	
	xbmcplugin.endOfDirectory(_handleId)


def handlerFavorites():
	from contextlib import closing
	import json
	apps = []
	for file in xbmcvfs.listdir(_path['favorites'])[1]:
		with closing(xbmcvfs.File(_path['favorites'] + file, 'r')) as f:				
			apps.append(json.load(f))		
	listApps(apps)
	

def handlerInstalledApps():
	import xbmcgui	
	path = xbmcgui.Dialog().browse(1, 'Choose App', shares='files',defaultt='androidapp://sources/apps/')					
	if path=='androidapp://sources/apps/':
		return
	app = {
		'title':  path.split('/')[-1].split('.')[-1],
		'package': path.split('/')[-1],
		'author': '',
		'updateDate': '',
		'thumb': 'androidapp://sources/apps/{}.png'.format(path.split('/')[-1]),
		'description': ''
	}
	handlerApp(app)

			
def handlerSearch():
	from lib import repos
	apps = repos.search(_params['text'])
	listApps(apps)
	

def handlerSearchInput():
	import xbmc, xbmcgui
	from helpers import gui
	text=gui.inputWithHistory('Search Apps', _path['settings'] + 'history_search')	
	if text is None:
		return	
	params = {
		'handler': 'Search',				
		'text': text		
	}	
	xbmc.executebuiltin("Dialog.Close(all, true) ")
	cmd = 'ActivateWindow(Programs, {}, return)'.format(_baseUrl+'?' + parse.urlencode(params))
	xbmc.executebuiltin(cmd)


def buildListItem(data):	
	import xbmcgui
	li = xbmcgui.ListItem(data['label'])
	li.setArt({'thumb': data['thumb']})
	if 'fanart' in data.keys():
		li.setArt({'fanart': data['fanart']})
	return li
	

def handlerDownloads():
	if xbmc.getCondVisibility('system.platform.android'):	
		from contextlib import closing
		import json
		pathConfig = _path['settings'] + 'config.json'
		if xbmcvfs.exists(pathConfig):
			with closing(xbmcvfs.File(pathConfig, 'r')) as f:				
				config = json.load(f)
		else:
			import xbmcgui	
			path = xbmcgui.Dialog().browse(1, 'Choose File Manager App', shares='files',defaultt='androidapp://sources/apps/')					
			if path=='androidapp://sources/apps/':
				return
			config = {'appFileManager': path.split('/')[-1]}
			with closing(xbmcvfs.File(pathConfig, 'w')) as f:						
				json.dump(config, f, indent=4)
		xbmc.executebuiltin('StartAndroidActivity({})'.format(config['appFileManager']))
	else:
		import os
		os.system('explorer {}'.format(_path['download'].replace('/','\\')))
	
	
def handlerRoot():	
	import xbmcplugin
	items=[		
		{'label': 'Search', 'path': _baseUrl +'?' + parse.urlencode({'handler': 'SearchInput'}), 'thumb': _path['img'] + '/search.png', 'fanart': _path['addon'] + '/fanart.jpg', 'folder': False},
		{'label': 'Favorites', 'path': _baseUrl +'?' + parse.urlencode({'handler': 'Favorites'}), 'thumb': _path['img'] + '/bars.png', 'fanart': _path['addon'] + '/fanart.jpg', 'folder': True},
		{'label': 'Open Downloads', 'path': _baseUrl +'?' + parse.urlencode({'handler': 'Downloads'}), 'thumb': _path['img'] + '/folder.png', 'fanart': _path['addon'] + '/fanart.jpg', 'folder': False}
	]
	if xbmc.getCondVisibility('system.platform.android'):
		items.append({'label': 'Installed Apps', 'path': _baseUrl +'?' + parse.urlencode({'handler': 'InstalledApps'}), 'thumb': _path['img'] + '/android.png', 'fanart': _path['addon'] + '/fanart.jpg', 'folder': False})
	for item in items:
		xbmcplugin.addDirectoryItem(_handleId, item['path'], buildListItem(item), item['folder'])
	xbmcplugin.endOfDirectory(_handleId)

globals()['handler' + _params['handler']]()
