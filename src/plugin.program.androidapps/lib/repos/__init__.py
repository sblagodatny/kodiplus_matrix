


_repos = {
	'fdroid': {},
#	'apkmonk': {}, 
	'apkcombo': {}
}


def getLib(lib):
	import importlib	
	return importlib.import_module('.' + lib, 'repos')



def get(app):
	from helpers import storage
	repos = ['google']
	if 'repo' in app.keys():
		repos.append(app['repo'])
	else:
		repos = repos + list(_repos.keys())
	import concurrent.futures
	results = {}
	def task(repo):														
		try:			
			data = getLib(repo).get(app)			
			if data is not None:				
				results[repo] = data			
		except Exception as e:
			storage.log('Get from repo ' + repo + ': ' + str(e))
	ex = concurrent.futures.ThreadPoolExecutor(max_workers=10)		
	for repo in repos:		
		ex.submit(task, repo)		
	ex.shutdown(wait=True)		
	if 'google' in results.keys():
		app.update(results['google'])
		app['google']=True
	for repo, data in results.items():
		if repo!='google':
			app.update(data)
			app['repo']=repo
			return	
#	raise Exception('Package {} not found in repos {}'.format(app['package'],str(_repos.keys())) )
	
	
def search(text):
	from helpers import storage
	from helpers.fuzzywuzzy import fuzz
	import concurrent.futures
	results = {}
	def task(repo):														
		try:			
			data = getLib(repo).search(text)
			for item in data:
				item['repo']=repo
			results[repo] = data					
		except Exception as e:
			storage.log('Search repo ' + repo + ': ' + str(e))
	ex = concurrent.futures.ThreadPoolExecutor(max_workers=10)		
	for repo in _repos.keys():		
		ex.submit(task, repo)		
	ex.shutdown(wait=True)
	reply = []
	for data in results.values():
		for item in data:
			exists = [a for a in reply if a['package']==item['package']]
			if len(exists)==0:
				match = fuzz.token_set_ratio(item['title'].lower(), text.lower())
				if match > 70:
					item['match']=match
					reply.append(item)											
	reply = sorted(reply, key=lambda k: k['match'], reverse=True)
	for item in reply:
		del item['match']
	return reply	
