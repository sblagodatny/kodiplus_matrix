from bs4 import BeautifulSoup

_url = 'https://play.google.com'
_timeout = 4

def search(text):
	result = []
	from helpers import http, storage
	import re
	session = http.initSession()
	sections = BeautifulSoup(session.get(_url + '/store/search', params={'q': text.replace(' ','+')}, timeout=_timeout).text, "html.parser").find_all('section')		
	section = sections[0]		
	try:
		return [{									
			'package': section.find('a')['href'].split('=')[1],		
			'title': section.find('img', {'alt': 'Icon image'}).find('div').find_all('div')[0].get_text().lstrip().rstrip(),
			'screenshot': section.find('img', {'alt': 'Screenshot image'})['src'],
			'thumb': section.find('img', {'alt': 'Icon image'})['src'].replace('=s64','=s540'),			
			'author': section.find('img', {'alt': 'Icon image'}).find('div').find_all('div')[1].get_text().lstrip().rstrip(),
			'rating': section.find('div', {'itemprop': 'starRating'}).find('div').get_text().replace('star','').lstrip().rstrip(),
			'description': section.find('img', {'alt': 'Icon image'}).parent.nextSibling.find('div').get_text().lstrip().rstrip()			
		}]
	except:
		None			
	for tag in section.find_all('div', {'role':'listitem'}):		
		try:						
			item = {								
				'package': tag.find('a')['href'].split('=')[1],
				'title': tag.find('img', {'alt': 'Thumbnail image'}).nextSibling.find_all('div')[0].get_text().lstrip().rstrip(),
				'screenshot': tag.find('img', {'alt': 'Screenshot image'})['src'],
				'thumb': tag.find('img', {'alt': 'Thumbnail image'})['src'].replace('=s64','=s540'),			
				'author': tag.find('img', {'alt': 'Thumbnail image'}).nextSibling.find_all('div')[1].get_text().lstrip().rstrip(),				
			}
		except:
			continue
		try:
			item['rating'] = tag.find('div', {'aria-label': re.compile("Rated*")}).get_text().replace('star','').lstrip().rstrip()											
		except:
			None	
		result.append(item)				
	return result
	

def get(app):
	from helpers import http, storage
	session = http.initSession()
	text = session.get(_url + '/store/apps/details', params={'id': app['package']}, timeout=_timeout).text
	content = BeautifulSoup(text, "html.parser")
	try:
		return {
#			'date': content.find('div', text='Updated on').nextSibling.get_text(),
			'thumb': content.find('img', {'alt': 'Icon image'})['srcset'],			
			'screenshot': content.find('img', {'alt': 'Screenshot image'})['src'],
			'title': content.find('h1', {'itemprop': 'name'}).get_text(),
			'author': content.find('h1', {'itemprop': 'name'}).nextSibling.find('span').get_text(),
			'description': content.find('div', {'data-g-id': 'description'}).get_text()
		}		
	except Exception as e:		
		return None

	
