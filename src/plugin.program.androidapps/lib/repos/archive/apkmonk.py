from bs4 import BeautifulSoup

_url = 'https://www.apkmonk.com'
_timeout = 4

def search(text):
	result = []
	from helpers import http
	session = http.initSession()
	content = BeautifulSoup(session.get(_url + '/ssearch?q={}'.format(text.replace(' ','+')), timeout=_timeout).text, "html.parser")	
	for tag in content.find_all('a', {'title': ' apk'}):
		item = {						
			'package': tag['href'].split('/')[-2],
			'title': tag.find('span', class_='af-title').get_text(),
			'thumb': tag.find('img')['data-src'].replace('_80x80','_150x150'),
			'screenshot': _url.replace('www','cdn') + '/images/' + tag['href'].split('/')[-2] + '.png'
		}
		result.append(item)
	return result
	
	
def get(app):
	from helpers import http
	session = http.initSession()
	content = BeautifulSoup(session.get(_url + '/app/{}/'.format(app['package']), timeout=_timeout).text, "html.parser")	
	try:
		info = content.find('table', class_='striped').find('tbody', class_='info').find_all('tr')
	except:
		return []
	return [{
		'source': 'apkmonk',
		'version': info[1].find('span').get_text(),
		'date': info[2].find('span').get_text(),		
		'url': content.find('a', {'id': 'download_button'})['href']
	}]
	
def download(app, source, path):
	import re, json
	from helpers import http, storage
	session = http.initSession()	
	data = session.get(source['url'], timeout=_timeout).text
	data = json.loads(re.search(r"get\('/down_file/',(.*?)\).done", data).group(1))
	url = _url + '/down_file/?pkg={}&key={}'.format(data['pkg'],data['key'])	
	headers = {'Referer': source['url'], 'X-Requested-With': 'XMLHttpRequest'}	
	data = session.get(url, headers=headers, timeout=_timeout).json()			
	return data['url']
	
	