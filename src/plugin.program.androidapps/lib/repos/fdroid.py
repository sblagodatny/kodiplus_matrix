from bs4 import BeautifulSoup

_url = 'https://f-droid.org'
_timeout = 4

def search(text):
	result = []
	from helpers import http, storage	
	session = http.initSession()
	content = BeautifulSoup(session.get(_url.replace('https://','https://search.'), params={'q': text.replace(' ','+')}, timeout=_timeout).text, "html.parser")					
	for tag in content.find_all('a', class_='package-header'):		
		result.append({																		
			'package': tag['href'].split('/')[-1],
			'title': tag.find('h4', class_='package-name').get_text().lstrip().rstrip(),
			'thumb': tag.find('img', class_='package-icon')['src'].replace('/icons/','/icons-640/'),						
			'description': tag.find('span', class_='package-summary').get_text().lstrip().rstrip()
		})		
	return result
	

def get_text(tag):
	for line_break in tag.findAll('br'): 
		line_break.replaceWith('###') 
	return tag.get_text().replace('###',"\n").lstrip().rstrip()	

def get(app):
	from helpers import http, storage
	session = http.initSession()
	text = session.get(_url + '/en/packages/{}/'.format(app['package']), timeout=_timeout).text
	content = BeautifulSoup(text, "html.parser")
	version = content.find('ul', class_="package-versions-list").find_all('li')[0]
	try:
		item = {
			'title': content.find('h3', class_="package-name").get_text().lstrip().rstrip(),
			'author': content.find('ul', class_="package-links").find_all('li')[0].find('a').get_text().lstrip().rstrip(),
			'thumb': content.find('img', {'alt': "package icon"})['src'].replace('/icons/','/icons-640/'),							
			'description': get_text(content.find('div', class_="package-description"))	
		}	
		try:
			item['screenshot'] = content.find('img', {'alt': "app screenshot"})['src']
		except:
			None
		return item
	except Exception as e:
		return None

	
def versions(app):
	from helpers import http, storage
	session = http.initSession()
	text = session.get(_url + '/en/packages/{}/'.format(app['package']), timeout=_timeout).text
	content = BeautifulSoup(text, "html.parser")
	result = []
	for tag in content.find('ul', class_="package-versions-list").find_all('li'):
		try:
			result.append({		
				'id': tag.find('div', class_="package-version-header").find('a')['name'].replace('.F-Droid',''),                                                          
				'date': tag.find('div', class_="package-version-header").get_text().split('Added on ')[1].lstrip().rstrip(),	
				'size': tag.find('p', class_="package-version-download").get_text().split('Download APK')[1].lstrip().rstrip().split("\n")[0].rstrip().replace('MiB','Mb').replace('GiB','Gb').replace('KiB','Kb'),			
				'description': tag.find('p', class_="package-version-requirement").get_text().lstrip().rstrip().replace('This version requires ','').replace(' or newer.','+'),
				'url': tag.find('p', class_="package-version-download").find('a')['href']
			})
		except:
			None
	return result
	
	
def download(app, version):	
	return version['url']