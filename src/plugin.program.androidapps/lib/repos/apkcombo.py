from bs4 import BeautifulSoup

_url = 'https://apkcombo.com'
_timeout = 4


def search(text):
	result = []
	from helpers import http, storage
	session = http.initSession()
	content = BeautifulSoup(session.get(_url + '/search/{}'.format(text.replace(' ','-')), cookies={'__apkcombo_lang':'en'}, headers={'Referer': _url}, timeout=_timeout).text, "html.parser")	
	for tag in content.find_all('a', class_='l_item'):
		item = {						
			'package': tag['href'].split('/')[-2],
			'title': tag.find('span', class_='name').get_text().lstrip().rstrip(),
			'thumb': tag.find('img')['data-src'],
			'author': tag.find('span', class_='author').get_text().lstrip().rstrip().split(' · ')[0],			
			'description': tag.find('span', class_='author').get_text().lstrip().rstrip().split(' · ')[1]
		}
		if item['thumb'].startswith('/static'):
			item['thumb'] = _url + item['thumb']
		else:
			item['thumb'] = item['thumb'].replace('=s75-rw','=s450-rw')
		result.append(item)
	return result
	


def get(app):
	result = []
	from helpers import http, storage
	session = http.initSession()
	r = session.get(_url + '/en/{}/'.format(app['package']), cookies={'__apkcombo_lang':'en'}, timeout=_timeout)
	content = BeautifulSoup(r.text, "html.parser")		
	info = content.find('div', class_='information-table').find_all('div',class_='value')
	try:
		item = {
			'title': content.find('div', class_="app_name").find('a').get_text().lstrip().rstrip(),
			'author': content.find('div', class_="author").find('a').get_text().lstrip().rstrip(),
			'thumb': content.find('div', class_='avatar').find('img')['data-src'],							
			'screenshot': content.find('div', {'id': 'gallery-screenshots'}).find_all('img')[0]['data-src'],
			'description': content.find('div', class_='text-description').get_text()
		}
		if item['thumb'].startswith('/static'):
			item['thumb'] = _url + item['thumb']
		else:
			item['thumb'] = item['thumb'].replace('=s75-rw','=s450-rw')
		if item['screenshot'].startswith('/static'):
			item['screenshot'] = _url + item['screenshot']				
		return item
	except:
		return None
		


def versions(app):
	result = []
	from helpers import http, storage
	session = http.initSession()
	r = session.get(_url + '/en/{}/old-versions/'.format(app['package']), cookies={'__apkcombo_lang':'en'}, timeout=_timeout)
	content = BeautifulSoup(r.text, "html.parser")		
	for tag in content.find_all('a', class_="ver-item"):
		result.append({
			'id': tag.find('span', class_="vername").get_text().replace(app['title'],'').lstrip().rstrip(),
			'date': tag.find('div', class_="description").get_text().split(' · ')[0],
			'size': '',
			'description': tag.find('div', class_="description").get_text().split(' · ')[1],
			'url': tag['href']
		})
	if len(result)==0:
		info = content.find('div', class_='information-table').find_all('div',class_='value')
		result.append({
			'id': info[0].get_text().lstrip().rstrip().split('(')[0].rstrip(),                                                          
			'date': info[1].get_text().lstrip().rstrip(),	
			'size': content.find('span', class_='fsize').find('span').get_text().lstrip().rstrip(),
			'description': '',
			'url': content.find('div', class_="download").find('a')['href']
		})
	return result
	

		
def download(app, version):			
	from helpers import http, storage		
	import re
	session = http.initSession()		
	text = session.get(_url + version['url'], timeout=_timeout).text
	content = BeautifulSoup(text, "html.parser")	
	tag = content.find('ul', class_='file-list')
	if tag is not None:
		url = tag.find('li').find('a')['href']	
	else:
		xid = re.search(r'var xid = "(.*?)"', text).group(1)		
		files = {
			'package_name': (None, app['package']),
			'version': (None, '')
		}
		text = session.post(_url + version['url'] + xid + '/dl', files=files, headers={'Referer': _url + version['url']}).text
		content = BeautifulSoup(text, "html.parser")	
		url = content.find('ul', class_='file-list').find('a')['href']	
	data = session.get(_url + '/checkin', headers={'Referer': _url + version['url']}, timeout=_timeout).text
	url = url + '&' + data		
	return url
