# coding: utf-8

import sys, xbmcaddon, xbmcvfs
import urllib.parse as parse

_baseUrl = sys.argv[0]
_handleId = int(sys.argv[1])
_params = dict(parse.parse_qsl(sys.argv[2][1:]))	

_addon = xbmcaddon.Addon()
_pathAddon = _addon.getAddonInfo('path')
_pathImg = _pathAddon + '/resources/img/'
_pathSettings = _pathAddon.replace('addons','userdata/addon_data')

_pathSearchHistory = _pathSettings + 'history'
_pathAccount = _pathSettings + 'accounts.json'
_tmdbLanguage = _addon.getSetting('tmdbLanguage')
_pathSources = _addon.getSetting('pathSources')
if len(_pathSources) == 0:
	_pathSources = _pathSettings
_pathSources = _pathSources + '/sources_/'

if not xbmcvfs.exists(_pathSettings):
	xbmcvfs.mkdir(_pathSettings)
if not xbmcvfs.exists(_pathSources):
	xbmcvfs.mkdirs(_pathSources)

if 'handler' not in list(_params.keys()):
	_params['handler'] = 'Root'
	
	
def listContent(contentlist, pageSize=None):	
	import xbmcplugin, xbmcgui, json
	from helpers import string, storage	
	xbmcplugin.setContent(_handleId, 'movies')		
#	if int(_params.get('page',1))>1:
#		item = buildListItem({'label': 'Previous Page', 'thumb': _pathImg + '/arrow-left.png'})
#		xbmcplugin.addDirectoryItem(_handleId, _baseUrl+'?' + parse.urlencode({'handler': 'SetPage', 'action': 'prev', 'params': string.b64encode(json.dumps(_params))}), item, False)
	for content in contentlist:		
		item = buildListItem({'label': content['title'], 'thumb': content.get('poster',''), 'fanart': content.get('poster','')})							
		if content['type']=='tv':
			item.setLabel('[COLOR FF2BB7FC]{}[/COLOR]'.format(item.getLabel()))
		info = item.getVideoInfoTag()
		info.setMediaType('movie' if content['type']=='movie' else 'tvshow')
		info.setOriginalTitle(content.get('titleOriginal',''))
		info.setPlot(content.get('plot',''))
		info.setPremiered(content.get('releaseDate',''))
		info.setStudios(content.get('studio',[]))
		info.setGenres(content.get('genre',[]))
		info.setCountries(content.get('country',[]))
		info.setRating(float(content.get('rating',None)))				
		info.setCast([xbmc.Actor(p['name'], p['role'], 0, p['thumbnail']) for p in content.get('cast',[])])
		if 'myrating' in list(content.keys()):
			info.setUserRating(int(content['myrating']))
			info.setPlaycount(1)				
		if content['type'] == 'tv':			
			info.setTvShowStatus(content.get('status',''))
			item.setProperty('TotalEpisodes',str(sum(season['episodes'] for season in content['seasons'])) )		
		contextMenuItems = []		
		cparams = { 'handler': 'Folder', 'content': string.b64encode(json.dumps(content)) }		
		contextMenuItems.append(('Folder', 'RunPlugin(' + _baseUrl+'?' + parse.urlencode(cparams) + ')'))		
		cparams = { 'handler': 'Source', 'content': string.b64encode(json.dumps(content)) }		
		contextMenuItems.append(('Source', 'RunPlugin(' + _baseUrl+'?' + parse.urlencode(cparams) + ')'))		
		cparams = { 'handler': 'Rate', 'content': string.b64encode(json.dumps(content)) }		
		contextMenuItems.append(('Rate', 'RunPlugin(' + _baseUrl+'?' + parse.urlencode(cparams) + ')'))		
		item.addContextMenuItems(contextMenuItems, replaceItems=True)										
		if content['type'] == 'tv':
			item.setIsFolder(True)	
			params = {'handler': 'Seasons', 'content': string.b64encode(json.dumps(content)) }
		else:
			item.setIsFolder(False)	
			item.setProperty("IsPlayable", 'true')		
			params = {'handler': 'Play', 'content': string.b64encode(json.dumps(content)) }		
		xbmcplugin.addDirectoryItem(_handleId, _baseUrl+'?' + parse.urlencode(params), item, item.isFolder())	
#	if pageSize is not None and len(contentlist)>=pageSize:
#		item = buildListItem({'label': 'Next Page', 'thumb': _pathImg + '/arrow-right.png'})
#		xbmcplugin.addDirectoryItem(_handleId, _baseUrl+'?' + parse.urlencode({'handler': 'SetPage', 'action': 'next', 'params': string.b64encode(json.dumps(_params))}), item, False)
	xbmcplugin.endOfDirectory(_handleId)


def handlerSetPage():
	from helpers import string, storage
	import json
	params = json.loads(string.b64decode(_params['params']))
	if _params['action'] == 'next':
		params['page'] = int(params.get('page', 1))+1
	elif _params['action'] == 'prev' and int(params.get('page', 1)) > 1:
		params['page'] = int(params.get('page', 1))-1
	else:
		return
	cmd = 'Container.Update({})'.format(_baseUrl+'?' + parse.urlencode(params))
	xbmc.executebuiltin(cmd)


def handlerSeasons():
	import xbmcplugin, xbmcgui, json
	from helpers import string, account, storage
	from tmdb import api, common
	xbmcplugin.setContent(_handleId, 'tvshows')	
	content = json.loads(string.b64decode(_params['content']))		
	api.getAccountContentStateSeasons(account.get(_pathAccount, common.validateAccount), content)		
	for season in content['seasons']:															
		item = buildListItem({'label': '{} - {}'.format(content['title'], season['title']), 'thumb': season.get('poster',''), 'fanart': season.get('poster','')})
		info = item.getVideoInfoTag()
		info.setMediaType('season')		
		info.setOriginalTitle(content.get('titleOriginal',''))
		info.setPlot(season.get('plot',''))
		info.setPremiered(season.get('releaseDate',''))
		info.setGenres(content.get('genre',[]))
		info.setCountries(content.get('country',[]))
		info.setRating(float(content.get('rating',None)))				
		info.setCast([xbmc.Actor(p['name'], p['role'], 0, p['thumbnail']) for p in content.get('cast',[])])							
		info.setSeason(int(season['id']))	
		info.setTvShowStatus(content.get('status',''))		
		item.setProperty('TotalEpisodes',str(season['episodes']) )
		item.setProperty('WatchedEpisodes',str(season['episodesWatched']) )
		if season['episodes']==0:
			item.setLabel('[COLOR grey]{}[/COLOR]'.format(item.getLabel()))
		if season['episodes']==season['episodesWatched'] and season['episodes']!=0:
			info.setPlaycount(1)				
		if 'myrating' in list(content.keys()):
			info.setUserRating(int(content['myrating']))		
		contextMenuItems = []
		cparams = { 'handler': 'Folder', 'content': string.b64encode(json.dumps(content)) }		
		contextMenuItems.append(('Folder', 'RunPlugin(' + _baseUrl+'?' + parse.urlencode(cparams) + ')'))		
		cparams = { 'handler': 'Source', 'content': string.b64encode(json.dumps(content)) }		
		contextMenuItems.append(('Source', 'RunPlugin(' + _baseUrl+'?' + parse.urlencode(cparams) + ')'))		
		cparams = { 'handler': 'Rate', 'content': string.b64encode(json.dumps(content)) }		
		contextMenuItems.append(('Rate', 'RunPlugin(' + _baseUrl+'?' + parse.urlencode(cparams) + ')'))	
		item.addContextMenuItems(contextMenuItems, replaceItems=True)			
		params = {'handler': 'Episodes', 'content': string.b64encode(json.dumps(content)), 'seasonId': season['id']} 
		xbmcplugin.addDirectoryItem(_handleId, _baseUrl+'?' + parse.urlencode(params), item, True)		
	xbmcplugin.endOfDirectory(_handleId, cacheToDisc=False)	


def handlerEpisodes():
	from helpers import string, account, storage
	from tmdb import api, common
	import json, xbmcplugin, xbmcgui
	content = json.loads(string.b64decode(_params['content']))		
	season = api.getSeason(account.get(_pathAccount, common.validateAccount), content['id'], _params['seasonId'], _tmdbLanguage)		
	xbmcplugin.setContent(_handleId, 'episodes')			
	for episode in season['episodes']:			
		item = buildListItem({'label': '{} - {} - {}'.format(content['title'], season['title'], episode['title']),'thumb': episode.get('poster',''), 'fanart': episode.get('poster','')})			
		info = item.getVideoInfoTag()
		info.setMediaType('episode')		
		info.setOriginalTitle(content.get('titleOriginal',''))
		info.setPlot(episode.get('plot',''))
		info.setPremiered(episode.get('releaseDate',''))		
		info.setGenres(content.get('genre',[]))
		info.setCountries(content.get('country',[]))
		info.setRating(float(episode.get('rating',None)))				
		info.setCast([xbmc.Actor(p['name'], p['role'], 0, p['thumbnail']) for p in content.get('cast',[])])							
		info.setSeason(int(_params['seasonId']))	
		info.setEpisode(int(episode['id']))	
		info.setDuration(episode.get('duration',0))			
		if 'myrating' in list(episode.keys()):			
			info.setPlaycount(1)		
		if 'myrating' in list(content.keys()):
			info.setUserRating(int(content['myrating']))
		contextMenuItems = []
		cparams = { 'handler': 'Folder', 'content': string.b64encode(json.dumps(content)) }		
		contextMenuItems.append(('Folder', 'RunPlugin(' + _baseUrl+'?' + parse.urlencode(cparams) + ')'))		
		cparams = { 'handler': 'Source', 'content': string.b64encode(json.dumps(content)) }		
		contextMenuItems.append(('Source', 'RunPlugin(' + _baseUrl+'?' + parse.urlencode(cparams) + ')'))		
		cparams = { 'handler': 'Rate', 'content': string.b64encode(json.dumps(content)) }		
		contextMenuItems.append(('Rate', 'RunPlugin(' + _baseUrl+'?' + parse.urlencode(cparams) + ')'))		
		item.addContextMenuItems(contextMenuItems, replaceItems=True)	
		params = {'handler': 'Play', 'content': string.b64encode(json.dumps(content)), 'seasonId': _params['seasonId'], 'episodeId': episode['id'] }				
		item.setProperty("IsPlayable", 'true')		
		xbmcplugin.addDirectoryItem(_handleId, _baseUrl+'?' + parse.urlencode(params), item, False)				
	xbmcplugin.endOfDirectory(_handleId)	


def handlerPlay():
	import xbmcplugin, xbmcgui, json
	from helpers import string, storage, player, account, locale
	from contextlib import closing
	from torrent import torrserve
	import vod
	from tmdb import common, api
	import copy
	content = json.loads(string.b64decode(_params['content']))	
	acc = account.get(_pathAccount, common.validateAccount)
	api.getAccountContentState(acc, content)	
	path = '{}/{}_{}.json'.format(_pathSources, content['type'], content['id'])
	if not xbmcvfs.exists(path):
		selectSource(content)
		if not xbmcvfs.exists(path):
			player.cancelResolvedUrl(_handleId) 				
			return
	with closing(xbmcvfs.File(path, 'r')) as f:
		source = json.load(f)
	sourceOriginal = copy.deepcopy(source)		

	try:
		lib = vod.getLib(source['source'])
		if 'episodeId' in _params.keys():
			season = [season for season in lib.seasons(source) if str(season['id'])==str(_params['seasonId'])]			
			if len(season)==0:
				raise Exception('Season not found')				
			season = season[0]
			episode = [episode for episode in lib.episodes(source, season) if str(episode['id'])==str(_params['episodeId'])]			
			if len(episode)==0:
				raise Exception('Episode not found')		
			episode = episode[0]
			stream = lib.stream(source,season,episode)					
		else:
			stream = lib.stream(source)			
	except Exception as e:
		import requests
		if isinstance(e, requests.ConnectionError):
			xbmcgui.Dialog().ok('TMDB', '{} not avaialble'.format(source['source']))
		elif 'cancelled' not in str(e):
			xbmcgui.Dialog().ok('TMDB', '{}: {}'.format(source['source'], str(e)))
		player.cancelResolvedUrl(_handleId) 
		return	
	
	item = xbmcgui.ListItem(offscreen=True)		
	
	torrentHash = None
	if 'torrent' in stream.keys():								
		try:
			torrentHash = torrserve.load(stream['torrent'])
			stream.update(torrserve.getStream(torrentHash, stream['torrent'].get('videoFileSeq',1)))
			item.setPath(stream['url'])
		except Exception as e:			
			try:
				torrserve.unload(torrentHash)
			except:			
				None
			import requests
			if isinstance(e, requests.ConnectionError):
				xbmcgui.Dialog().ok('TMDB', 'TorrServe not available')
			elif 'cancelled' not in str(e):
				xbmcgui.Dialog().ok('TMDB', 'TorrServe: ' + str(e))
			player.cancelResolvedUrl(_handleId) 
			return
					
	else:	
		item.setPath(player.buildUrl(stream))	

	if source != sourceOriginal:		
		with closing(xbmcvfs.File(path, 'w')) as f:
			json.dump(source, f, indent=4)					

	if locale.getLanguage(source['title'])=='en-US':
		import subs		
		data = {'tmdbId': content['id'], 'imdbId': content.get('imdbId',''), 'season': _params.get('seasonId',''), 'episode': _params.get('episodeId','')}
		api.delEmptyKeys(data)
		item.setSubtitles(subs.download(subs.filter(stream.get('subtitles',[]) + subs.get(data), stream.get('filename',stream.get('title','None')), languages=['Hebrew', 'English', 'Russian'])))
	xbmcplugin.setResolvedUrl(handle=_handleId, succeeded=True, listitem=item) 		
	
	progress = None
	try:
		progress = player.waitForCurrentItem()
	except:
		None

	if torrentHash is not None:
		try:
			torrserve.unload(torrentHash)
		except:			
			None
	
	if progress is None:
		return		
	if progress >= 90:		
		if content['type']=='movie':				
			if content['watchlist'] is True:
				api.setContentListState(acc, content, 'watchlist', False)
			if content['favorite'] is True:
				api.setContentListState(acc, content, 'favorite', False)
			if 'myrating' not in content.keys():
				handlerRate(content)
		else:
			api.setEpisodeRate(acc, content['id'], _params['seasonId'], _params['episodeId'], 8)		
	elif progress < 90 or content['type'] == 'tv': 
		if content['favorite'] is False:
			if xbmcgui.Dialog().yesno(content['title'],  'Move to Current folder ?') is True:				
				api.setContentListState(acc, content, 'favorite', True)
				if content['watchlist'] is True:
					api.setContentListState(acc, content, 'watchlist', False)
				xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%('TMDB', 'Folder updated', 1, _addon.getAddonInfo('icon')))


def buildSearchCriteria(content):
	from helpers import locale
	from tmdb import api			
	criteria = {'titles': {}, 'tmdbId': content['id'], 'type': content['type'], 'poster': content.get('poster',''), 'language': content['language']}	
	if 'releaseDate' in content.keys():
		criteria['year'] = content['releaseDate'].split('-')[0]
	if 'imdbId' in content.keys():
		criteria['imdbId'] = content['imdbId']		
	if content['type']=='tv':
		criteria['seasons']=[]
		for season in content['seasons']:
			item = {'id': season['id'], 'episodes': season['episodes']}
			if 'releaseDate' in season.keys():
				item['year'] = season['releaseDate'].split('-')[0]
			criteria['seasons'].append(item)
	for language in ['ru-RU','he-IL','en-US']:
		if language==_tmdbLanguage:
			title=content['title']
		else:
			item = {'id': content['id'], 'type': content['type']}
			api.getContentDetails(None, item, language)
			title = item['title']
		if locale.getLanguage(title)==language:
			criteria['titles'][language] = title
	return criteria
	

def selectSource(content, current=None):
	import json, xbmcgui
	import vod	
	from contextlib import closing		
	xbmc.executebuiltin('ActivateWindow(busydialognocancel)')
	try:		
		result = vod.search(buildSearchCriteria(content))
		xbmc.executebuiltin('Dialog.Close(busydialognocancel,true)') 
	except:
		xbmc.executebuiltin('Dialog.Close(busydialognocancel,true)') 
		raise			
	preselect = -1
	if current is not None:
		for i in range(len(result)):
			if result[i]['source'] == current.get('source','') and result[i]['id'] == current.get('id',''):
				preselect = i
	s = xbmcgui.Dialog().select('Sources', [buildListItem({'label': '{} [{}]'.format(item['source'], item['title']),'thumb': item['poster']}) for item in result], useDetails=True, preselect=preselect)
	if s == -1:
		return
	source = result[s]
	if current is None or source != current:
		path = '{}/{}_{}.json'.format(_pathSources, content['type'], content['id'])
		with closing(xbmcvfs.File(path, 'w')) as f:
			json.dump(source, f, indent=4)
		xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%('TMDB', 'Source updated', 1, _addon.getAddonInfo('icon')))	


def handlerSource():
	from helpers import string
	import json, xbmcgui
	from contextlib import closing	
	content = json.loads(string.b64decode(_params['content']))		
	path = '{}/{}_{}.json'.format(_pathSources, content['type'], content['id'])
	d = xbmcgui.Dialog()
	if not xbmcvfs.exists(path):
		selectSource(content)
	if not xbmcvfs.exists(path):
		return	
	with closing(xbmcvfs.File(path, 'r')) as f:
		source = json.load(f)	
	while True:
		items = [{'label': '[B]Source:[/B] {} [{}]'.format(source['source'],source['title']), 'thumb': source['poster']}]
		for key, value in source.get('defaults',{}).items():	
			items.append({'label': '[B]{}:[/B] {}'.format(key.capitalize(),value), 'thumb': _pathImg + '/{}.png'.format(key), 'key': key})		
		s = d.select('Source', [buildListItem(item) for item in items], useDetails=True)
		if s == -1:
			return
		if s == 0:
			selectSource(content, source)
			with closing(xbmcvfs.File(path, 'r')) as f:
				source = json.load(f)	
		else:
			key = items[s]['key']
			if d.yesno(content['title'],  'Reset {} ?'. format(key.capitalize()) ) is True:			
				del source['defaults'][key]
				if key=='torrent' and 'torrent' in source.keys():
					del source['torrent']
				with closing(xbmcvfs.File(path, 'w')) as f:
					json.dump(source, f, indent=4)
	
	
def handlerRate(content=None):
	from helpers import string, account
	import json, xbmcgui
	from tmdb import api, common
	if content is None:
		content = json.loads(string.b64decode(_params['content']))	
	acc = account.get(_pathAccount, common.validateAccount)
	api.getAccountContentState(acc, content)
	rates = ['10','9','8','7','6','5','4','3','2','1']
	preselect = -1
	if 'myrating' in content.keys():
		preselect=rates.index(str(int(content['myrating'])))
	s = xbmcgui.Dialog().select('Your rate for ' + content['title'], rates, preselect=preselect)
	if s == -1:
		return
	api.setContentRate(acc, content, rates[s])
	xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%('TMDB', 'Rate updated', 1, _addon.getAddonInfo('icon')))
	
	
def handlerFolder():
	from tmdb import api, common
	from helpers import account, string
	import json, xbmcgui
	content = json.loads(string.b64decode(_params['content']))	
	acc = account.get(_pathAccount, common.validateAccount)
	api.getAccountContentState(acc, content)	
	if content['watchlist'] is True:
		preselect=1
	elif content['favorite'] is True:
		preselect=0
	else:
		preselect=2
	folders = ['Current','Watchlist','None']
	s = xbmcgui.Dialog().select('Folders', folders, preselect=preselect)
	if s == -1:
		return
	if s == preselect:
		return
	folder = folders[s]
	if folder=='Current':
		if content['favorite'] is False:
			api.setContentListState(acc, content, 'favorite', True)
		if content['watchlist'] is True:
			api.setContentListState(acc, content, 'watchlist', False)
	elif folder=='Watchlist':
		if content['favorite'] is True:
			api.setContentListState(acc, content, 'favorite', False)
		if content['watchlist'] is False:
			api.setContentListState(acc, content, 'watchlist', True)
	elif folder=='None':
		if content['favorite'] is True:
			api.setContentListState(acc, content, 'favorite', False)
		if content['watchlist'] is True:
			api.setContentListState(acc, content, 'watchlist', False)
	xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%('TMDB', 'Folder updated', 1, _addon.getAddonInfo('icon')))
	if preselect==0 and 'myrating' not in content.keys():
		handlerRate(content)
		
		
def handlerSearch():
	from tmdb import common, api
	from helpers import string, storage, account
	import json	
	
	acc = account.get(_pathAccount, common.validateAccount)
	criteria = json.loads(string.b64decode(_params['criteria']))
#	page =  _params.get('page',1)
	
	if 'title' in criteria.keys():
		content = api.search(acc, criteria, _tmdbLanguage)	
	else: 
		content = api.discoverSpecial(acc, criteria, _tmdbLanguage)	
	listContent(content, pageSize=20)

		
def handlerSearchInput():
	import xbmcgui, json
	from helpers import gui, string
	from tmdb import web, common	
	d=xbmcgui.Dialog()
	criteria = {'sort_by': 'popularity'}
	while True:
		items = [
			{'label': 'Title: {}'.format(criteria.get('title', 'Any')), 'thumb': _pathImg + '/at.png'},
			{'label': 'Genre: {}'.format(str(criteria.get('genre', 'Any'))), 'thumb': _pathImg + '/shapes.png'},
			{'label': 'Language: {}'.format(str(criteria.get('language', 'Any'))), 'thumb': _pathImg + '/world.png'},
			{'label': 'Studio: {}'.format(str(criteria.get('studio', 'Any'))), 'thumb': _pathImg + '/location.png'},
			{'label': 'Sort By: {}'.format(criteria['sort_by']), 'thumb': _pathImg + '/bars.png'},
			{'label': '[B]Search Movies[/B]', 'thumb': _pathImg + '/video.png', 'type': 'movie'},
			{'label': '[B]Search Series[/B]', 'thumb': _pathImg + '/collection.png', 'type': 'tv'}
		]
		s = d.select('TMDB Search', [buildListItem(item) for item in items], useDetails=True)
		if s == -1:
			return
		item=items[s]
		if item['label'].startswith('Title'):
			text=gui.inputWithHistory('TMDB - Search Title', _pathSearchHistory, 10)	
			if text is not None and len(text)>0:
				criteria['title'] = text
				if 'genre' in criteria.keys():
					del criteria['genre']
				if 'language' in criteria.keys():
					del criteria['language']
		if item['label'].startswith('Genre'):
			genres = list(web._genres.keys())
			s = d.select('TMDB - Search Genre', genres)
			if s is not None:
				criteria['genre']= genres[s]
				if 'title' in criteria.keys():
					del criteria['title']
		if item['label'].startswith('Language'):
			languages = list(web._languages.keys())
			s = d.select('TMDB - Search Language', languages)
			if s >= 0:
				criteria['language']= languages[s]
				if 'title' in criteria.keys():
					del criteria['title']
		if item['label'].startswith('Studio'):
			studios = list(web._studios.keys())
			s = d.select('TMDB - Search Studio', studios)
			if s >= 0:
				criteria['studio']= studios[s]
				if 'title' in criteria.keys():
					del criteria['title']
		if item['label'].startswith('Sort By'):
			values = ['popularity','rating']
			s = d.select('TMDB - Search Sort By', values)
			if s >= 0:
				criteria['sort_by']= values[s]
		elif 'type' in item.keys():
			criteria['type']=item['type']
			break
	if 'genre' in criteria.keys():
		criteria['genre']=web._genres[criteria['genre']]
	if 'language' in criteria.keys():
		criteria['language']=web._languages[criteria['language']]
	if 'studio' in criteria.keys():
		criteria['studio']=web._studios[criteria['studio']]
	params = {
		'handler': 'Search',
		'criteria': string.b64encode(json.dumps(criteria))
	}	
	xbmc.executebuiltin("Dialog.Close(all, true) ")
	cmd = 'ActivateWindow(Videos, {}, return)'.format(_baseUrl+'?' + parse.urlencode(params))
	xbmc.executebuiltin(cmd)


def cleanSources(account):
	import time
	from helpers import storage
	import json
	pathCleanup = _pathSettings + '/cleanup'
	lastCleanupTime = 0	
	if xbmcvfs.exists(pathCleanup):
		with open(pathCleanup, 'r') as f:
			lastCleanupTime = int(f.read())			
	currentClenupTime = int(time.time())
	if currentClenupTime - lastCleanupTime < 60*60*24*7:  ## 7 days
		return				
	from tmdb import api
	keep = api.getAccountList(account, 'favorite',  _tmdbLanguage, details=False) + api.getAccountList(account, 'watchlist',  _tmdbLanguage, details=False)	
	remove = []
	for file in xbmcvfs.listdir(_pathSources)[1]:
		content = {'id': int(file.split('_')[1].split('.')[0]), 'type': file.split('_')[0]}
		if content not in keep:
			remove.append(file)
	for file in remove:
		xbmcvfs.delete(_pathSources + '/' + file)		
	with open(pathCleanup, 'w') as f:
		f.write(str(currentClenupTime))


def handlerAccountList():	
	from helpers import account, storage
	from tmdb import web, api, common
	acc = account.get(_pathAccount, common.validateAccount)
	if acc is None:
		xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%('TMDB', 'Account not set', 1, _addon.getAddonInfo('icon')))
		return
	try:
		cleanSources(acc)
	except Exception as e:
		storage.log('Failed to clean sources: ' + str(e))
	content = api.getAccountList(acc, _params['list'],  _tmdbLanguage)								
	listContent(content)


def handlerAccount():	
	from tmdb import common
	from helpers import account	
	account.open(_pathAccount, common.validateAccount, 'TMDB Accounts', 'credentials')

	
def buildListItem(data):	
	import xbmcgui
	li = xbmcgui.ListItem(data['label'])
	if 'thumb' in data.keys() and data['thumb'] is not None and len(data['thumb'])>0:
		li.setArt({'thumb': data['thumb']})
	if 'fanart' in data.keys() and data['fanart'] is not None and len(data['fanart'])>0:
		li.setArt({'fanart': data['fanart']})
	return li
	
	
def handlerRoot():	
	import xbmcplugin
	items=[		
		{'label': 'Current', 'path': _baseUrl +'?' + parse.urlencode({'handler': 'AccountList', 'list': 'favorite'}), 'thumb': _pathImg + '/eye.png', 'fanart': _pathAddon + '/fanart.jpg', 'folder': True},
		{'label': 'Watchlist', 'path': _baseUrl +'?' + parse.urlencode({'handler': 'AccountList', 'list': 'watchlist'}), 'thumb': _pathImg + '/list.png', 'fanart': _pathAddon + '/fanart.jpg', 'folder': True},		
		{'label': 'History', 'path': _baseUrl +'?' + parse.urlencode({'handler': 'AccountList', 'list': 'rated'}), 'thumb': _pathImg + '/recent.png', 'fanart': _pathAddon + '/fanart.jpg', 'folder': True},
		{'label': 'Search', 'path': _baseUrl +'?' + parse.urlencode({'handler': 'SearchInput'}), 'thumb': _pathImg + '/search.png', 'fanart': _pathAddon + '/fanart.jpg', 'folder': False},						
		{'label': 'Account', 'path': _baseUrl +'?' + parse.urlencode({'handler': 'Account'}), 'thumb': _pathImg + '/user.png', 'fanart': _pathAddon + '/fanart.jpg', 'folder': False}		
	]
	for item in items:
		xbmcplugin.addDirectoryItem(_handleId, item['path'], buildListItem(item), item['folder'])
	xbmcplugin.endOfDirectory(_handleId)	
	
		
globals()['handler' + _params['handler']]()
	
