_url = 'https://photos.google.com'


def getPageData(content, ds):
	import re,json
	matches=re.findall("AF_initDataCallback(.*?);</script>", content)	
	data = [match for match in matches if "'ds:{}'".format(ds) in match]
	if len(data)==0:
		return None
	data = data[0].split('data:')[1].split(', sideChannel:')[0]
	data = json.loads(data)		
	return data
	
	
	
def getAlbums(account):
	from helpers import http
	session = http.initSession()	
	content=session.get(_url + '/u/1/albums', cookies=account['cookies']).text			
	result = []
	for item in getPageData(content, '0')[0]:
		result.append({
			'id': item[0],
			'name': list(item[-1].values())[0][1],
			'thumb': '' if item[1] is None else item[1][0],
			'itemsCount': list(item[-1].values())[0][3]
		})
	return result
	


def getAlbumItems(account, id):
	from helpers import http
	import datetime
	session = http.initSession()
	content=session.get(_url + '/u/1/album/' + id, cookies=account['cookies']).text
	result = []
	maxWidth = 1980
	maxHeight = 1080
	for item in getPageData(content, '5')[1]:
		resultitem = {
			'id': item[0],
			'thumb': item[1][0],				
			'created': int(item[2] / 1000), 
			'height': item[1][2],
			'width': item[1][1],
			'type': 'image'  if len(list(item[-1].keys()))==1 else 'video'
		}		
		resultitem['orientation'] = 'landscape' if resultitem['width'] > resultitem['height'] else 'portrait'		
		if resultitem['type'] == 'image':
			scale = 0
			if resultitem['orientation'] == 'landscape' and resultitem['width'] > maxWidth:
				scale = maxWidth / resultitem['width']
			if resultitem['orientation'] == 'portrait' and resultitem['height'] > maxHeight:
				scale = maxHeight / resultitem['height']
			if scale != 0:
				resultitem['width'] = int(resultitem['width'] * scale)
				resultitem['height'] = int(resultitem['height'] * scale)				
			resultitem['url'] = resultitem['thumb'] + '=w{}-h{}-no'.format(resultitem['width'],resultitem['height'])
		else:						
			resultitem['url'] = resultitem['thumb'] + '=m22'
			if resultitem['width'] < 1280:
				resultitem['url'] = resultitem['thumb'] + '=m18'	
			resultitem['duration'] = int(list(item[-1].values())[0][0]/1000)		
		resultitem['title'] = '{} {}'.format(resultitem['type'].capitalize(), datetime.datetime.fromtimestamp(resultitem['created']).strftime('%d/%m/%Y %H:%M:%S'))							
		result.append(resultitem)					
	return(result)
				

def validateAccount(account):
	import re
	from helpers import http
	session = http.initSession()
	content=session.get('https://photos.google.com/u/1/?pageId=none', cookies=account['cookies']).text			
	account['title'] = re.search("Google Account: (.*?)&#10;", content).group(1).rstrip()	
	account['thumb'] = re.search("Google Account: (.*?)srcset", content).group(1).split('src="')[1].split('"')[0].replace('s32-c-mo','s720-c-mo')	

	