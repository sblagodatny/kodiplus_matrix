import sys, xbmc, xbmcgui, xbmcplugin, xbmcaddon
import urllib.parse as parse
import xbmcaddon
import json, os
from lib import gphotos
from helpers import string

_baseUrl = sys.argv[0]
_handleId = int(sys.argv[1])
_params = dict(parse.parse_qsl(sys.argv[2][1:]))	

_pathAddon = xbmcaddon.Addon().getAddonInfo('path')
_pathSettings = _pathAddon.replace('addons','userdata/addon_data')
_pathAccount = _pathSettings + '/account.json'


if not os.path.exists(_pathSettings):
	os.mkdir(_pathSettings)



def listContent(content):	
	xbmcplugin.setContent(_handleId, 'images')		
	videos = [i for i in content if i['type']=='video']
	photos = [i for i in content if i['type']!='video']
	items=[]
	if len(videos) > 0:
		params = {'handler': 'ListVideos', 'content': string.b64encode(json.dumps(videos))}		
		item = xbmcgui.ListItem('Videos', offscreen=True)
		item.setPath(_baseUrl+'?' + parse.urlencode(params))
		item.setIsFolder(True)				
		items.append((item.getPath(), item, item.isFolder(),))	
	for data in photos:
		item = xbmcgui.ListItem(data['title'] , offscreen=True)
		item.setArt({'thumb': data["thumb"]})				
		item.setMimeType('image/jpeg')				
		item.setPath(data['url'])
		item.setIsFolder(False)				
		items.append((item.getPath(), item, item.isFolder(),))		
	xbmcplugin.addDirectoryItems(handle=_handleId, items=items, totalItems=len(items))		
	xbmcplugin.endOfDirectory(_handleId)



def handlerListVideos():
	content = json.loads(string.b64decode(_params['content']))				
	xbmcplugin.setContent(_handleId, 'videos')	
	items=[]
	for data in content:
		item = xbmcgui.ListItem(data['title'] , offscreen=True)
		item.setArt({'thumb': data["thumb"]})				
		info = item.getVideoInfoTag()
		info.setDuration(data['duration'])		
		item.setProperty("IsPlayable", 'true')	
		item.setPath(data['url'])
		item.setIsFolder(False)				
		items.append((item.getPath(), item, item.isFolder(),))		
	xbmcplugin.addDirectoryItems(handle=_handleId, items=items, totalItems=len(items))		
	xbmcplugin.endOfDirectory(_handleId)

	
def handlerAlbums():
	from helpers import account
	xbmcplugin.setContent(_handleId, 'images')	
	acc = account.get(_pathAccount)
	if acc is None:
		xbmcgui.Dialog().ok('Google Photos', 'Account not set')
		xbmcplugin.endOfDirectory(_handleId)
		return
	albums = gphotos.getAlbums(acc)	
	items=[]
	for data in albums:
		item = xbmcgui.ListItem('{} ({})'.format(data['name'], data['itemsCount']),  offscreen=True)
		item.setArt({'thumb': data["thumb"]})			
		params = {'handler': 'ListAlbum', 'id': data["id"]	}						
		item.setPath(_baseUrl+'?' + parse.urlencode(params))
		item.setIsFolder(True)				
		items.append((item.getPath(), item, item.isFolder(),))		
	xbmcplugin.addDirectoryItems(handle=_handleId, items=items, totalItems=len(items))		
	xbmcplugin.endOfDirectory(_handleId)
	
		
def handlerListAlbum():						
	from helpers import account
	content = gphotos.getAlbumItems(account.get(_pathAccount), _params['id'])	
	listContent(content)			



def handlerManageAccounts():
	from helpers import account
	account.open(_pathAccount, gphotos.validateAccount, 'Google Photos Accounts')

	
def handlerRoot():		
	pathImg = _pathAddon + '/resources/img/'	
	rootLinks = [		
		{'name': 'My Albums', 'folder': True, 'thumb': pathImg+'collection.png', 'params': {'handler': 'Albums', 'shared': 'False'}},
		{'name': 'Account', 'folder': False, 'thumb': pathImg+'user.png', 'folder': False, 'params': {'handler': 'ManageAccounts'} }
	]
	items=[]
	for rootLink in rootLinks:		
		item=xbmcgui.ListItem(rootLink['name'], offscreen=True)		
		item.setArt({'thumb': rootLink['thumb'], 'fanart': _pathAddon + '/fanart.jpg'})			
		item.setPath(_baseUrl+'?' + parse.urlencode(rootLink['params']))
		item.setIsFolder(rootLink['folder'])				
		items.append((item.getPath(), item, item.isFolder(),))		
	xbmcplugin.addDirectoryItems(handle=_handleId, items=items, totalItems=len(items))		
	xbmcplugin.endOfDirectory(_handleId)	
	
	
	
if 'handler' in _params.keys():
	globals()['handler' + _params['handler']]()
else:
	handlerRoot()
