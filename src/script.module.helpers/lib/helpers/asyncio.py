def setupKodiAsyncIO():
	import sys
	sys.modules['_asyncio'] = None
	import asyncio
	if sys.platform == "win32" and (3, 8, 0) <= sys.version_info < (3, 9, 0):
		asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
