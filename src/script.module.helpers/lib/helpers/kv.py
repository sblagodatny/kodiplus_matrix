def valuesToKeys(values, d):
	result = []
	for key, value in d.items():
		if value in values:
			result.append(key)
	return result


def keysToValues(keys, d):
	result = []
	for key, value in d.items():
		if key in keys:
			result.append(value)
	return result
	