import xbmcgui, time, xbmcvfs, copy, json
from contextlib import closing
import copy


def buildListItem(data):	
	li = xbmcgui.ListItem(data['label'])
	li.setArt({'thumb': data['thumb']})
	return li
		

def get(path, fvalidate=None):		
	if not xbmcvfs.exists(path):								
		return None	
	with closing(xbmcvfs.File(path, 'r')) as f:
		data = json.load(f)
	current=None
	for i in range(len(data)):
		if data[i]['current'] is True:
			current=i
	if current is None:
		return None
	account = data[current]	
	if 'validDays' in account.keys() and fvalidate is not None:
		if time.time()-account['ts'] > float(account['validDays']*24*60*60):
			fvalidate(account)
			account['ts']=time.time()
			data[current]=account
			with closing(xbmcvfs.File(path, 'w')) as f:
				json.dump(data, f, indent=4)		
	return account


def parseCookies(content):				
	cookies = {}		 
	for line in content.split("\n"):
		if line.startswith('#'):
			continue
		if len(line.strip().lstrip())==0:
			continue
		data = line.split('	')
		cookies[data[5]]=data[6]												
	return cookies
	
	
## fvalidate has to populate at lest title and thumb, optionally validDays ##	
## mode: cookies, credentials, file
def open(path, fvalidate, title='Accounts', mode='cookies', usernameLabel=None, fileMask='*.txt'):
	if not xbmcvfs.exists(path):
		with closing(xbmcvfs.File(path, 'w')) as f:
			json.dump([], f)
			data=[]
	else:
		with closing(xbmcvfs.File(path, 'r')) as f:
			data = json.load(f)
	dataOriginal = copy.deepcopy(data)
	d = xbmcgui.Dialog()	
	while True:
		try:
			preselect = data.index([account for account in data if account['current'] is True][0])
		except:
			preselect = -1
		items = [{'label': item['title'], 'thumb': item['thumb'] } for item in data]
		items.append({'label': 'Add', 'thumb': 'special://skin/resources/icons/plus.png'})
		items.append({'label': 'Remove', 'thumb': 'special://skin/resources/icons/minus.png'})		
		s = d.select(title, [buildListItem(item) for item in items], useDetails=True, preselect=preselect)
		if s == -1:
			break
				
		elif items[s]['label'] == 'Add':
			account = {}
			if mode in ['cookies','file']:
				try:
					ipath = xbmcgui.Dialog().browse(type=1, heading='Choose file', shares='"', mask=fileMask, enableMultiple=False)	
					if len(ipath)<2:
						continue
					with closing(xbmcvfs.File(ipath, 'r')) as f:
						content = f.read()												
					if mode == 'file':
						account['content'] = content
					elif mode == 'cookies':
						account['cookies'] = parseCookies(content)						
				except Exception as e:
					xbmcgui.Dialog().ok('Error', format(str(e)))
					continue																	
			elif mode == 'credentials':
				from helpers import gui
				username=gui.input('Username' if usernameLabel is None else usernameLabel)	
				if username is None or len(username)==0: 
					continue
				password=gui.input('Password')	
				if password is None or len(password)==0:
					continue
				account.update({'username': username, 'password': password}) 
			account.update({'ts': time.time(), 'current': False})
			try:
				fvalidate(account)
			except Exception as e:
				xbmcgui.Dialog().ok('Error', str(e))
				continue
			exists = [a for a in data if a['title'] == account['title']]
			if len(exists) != 0:
				xbmcgui.Dialog().ok('Note', 'Account already exists ({})'.format(account['title']))
				continue
			data.append(account)
			if len(data)==1:
				data[0]['current'] = True
						
		elif items[s]['label'] == 'Remove':
			s = xbmcgui.Dialog().select('Remove account', [account['title'] for account in data])
			if s>=0:
				account = data[s]				
				data.remove(account)					
				if account['current'] is True and len(data)>0:
					data[0]['current'] = True
							
		else: 
			if s != preselect:
				data[s]['current'] = True
				data[preselect]['current'] = False
			break
		
	if json.dumps(data) != json.dumps(dataOriginal):
		with closing(xbmcvfs.File(path, 'w')) as f:
			json.dump(data, f, indent=4)
		return True
	else:
		return False
		
		

