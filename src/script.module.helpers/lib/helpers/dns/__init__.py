# Copyright (C) Dnspython Contributors, see LICENSE for text of ISC license

# Copyright (C) 2003-2007, 2009, 2011 Nominum, Inc.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose with or without fee is hereby granted,
# provided that the above copyright notice and this permission notice
# appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND NOMINUM DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL NOMINUM BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
# OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

"""dnspython DNS toolkit"""

__all__ = [
    "asyncbackend",
    "asyncquery",
    "asyncresolver",
    "dnssec",
    "dnssecalgs",
    "dnssectypes",
    "e164",
    "edns",
    "entropy",
    "exception",
    "flags",
    "immutable",
    "inet",
    "ipv4",
    "ipv6",
    "message",
    "name",
    "namedict",
    "node",
    "opcode",
    "query",
    "quic",
    "rcode",
    "rdata",
    "rdataclass",
    "rdataset",
    "rdatatype",
    "renderer",
    "resolver",
    "reversename",
    "rrset",
    "serial",
    "set",
    "tokenizer",
    "transaction",
    "tsig",
    "tsigkeyring",
    "ttl",
    "rdtypes",
    "update",
    "version",
    "versioned",
    "wire",
    "xfr",
    "zone",
    "zonetypes",
    "zonefile",
]




def get(domain, rr='a', endpoint = 'https://doh.opendns.com/dns-query'):
	from . import message
	import requests
	import base64
	message = dns.message.make_query(domain, rr)	
#	message.want_dnssec()
	dns_req = base64.urlsafe_b64encode(message.to_wire()).decode("UTF8").rstrip("=")
	r = requests.get(endpoint, params={"dns": dns_req}, headers={"Content-type": "application/dns-message"})
	r.raise_for_status()
	if "application/dns-message" not in r.headers["Content-Type"]:
		raise Exception ('Invalid DNS reply')
	response = dns.message.from_wire(r.content)
	answers = []
	for response in dns.message.from_wire(r.content).answer:
		for response_line in response.to_text().split("\n"):
			answers.append(response_line)
	return answers	