import requests
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import http.server
import json
import time

from helpers import storage, string

def initSession():
	s = requests.Session()
	s.verify = False
	s.headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'
	return s


def initSessionTLSv1_1():
	import ssl	
	from requests.adapters import HTTPAdapter
	from requests.packages.urllib3.poolmanager import PoolManager, PoolKey, _default_key_normalizer
	class MyAdapter(HTTPAdapter):
		def init_poolmanager(self, connections, maxsize, block=False):
			self.poolmanager = PoolManager(
				num_pools=connections,
				maxsize=maxsize,
				block=block,
				ssl_version=ssl.PROTOCOL_TLSv1_1
			)
	s = requests.Session()
	s.headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'
	s.mount('https://', MyAdapter())
	return s



	
class mpdProxyHandler (http.server.SimpleHTTPRequestHandler):
	def do_GET(self):
		try:			
			self.send_response(200)
			self.send_header("Content-type", 'application/octet-stream')
			self.send_header("Content-Length", len(self.server.manifest))
			self.end_headers()				
			self.wfile.write(self.server.manifest.encode())			
		except:				
			try:
				self.send_response(404)
				self.end_headers()
			except:
				None		
		
	def log_message(self, format, *args):
		return				
		
def setCookie(session, domain, name, value):
	args = {
		'domain': domain,
		'expires': None,
		'path': '/',
		'version':0
	}
	session.cookies.set(name=name, value=value, **args)
	
def resolveGoogleCaptcha(s, key, co, v):			
	import re, json
	params = {
		'ar': '1',
		'k': key,
		'co': co,
		'hl': 'en',
		'v': v,
		'size': 'invisible'
	}
	r = s.get('https://www.google.com/recaptcha/api2/anchor', params=params, timeout=3)
	recaptcha_regex = re.compile(r'<input type="hidden" id="recaptcha-token" value="([^"]+)">')   
	match = recaptcha_regex.search(r.text)
	if match is None:
		return None
	ctoken = match.group(1)		
	data = { "reason": "q", "c": ctoken }	
	r = s.post('https://www.google.com/recaptcha/api2/reload', params={'k': key}, data = data)
	text = r.text
	prefix = ")]}'"
	if text.startswith(prefix):
		text = text[len(prefix):]
	return json.loads(text)[1]	
	
	
	
class streamProxyHandler (http.server.BaseHTTPRequestHandler):
	timeout = 1
	
	def do_HEAD(self):			
		try:		
#			storage.log('streamProxyHandler object: ' + str(self))
#			storage.log('streamProxyHandler: HEAD Request headers: ' + str(self.headers))				
			headers = self.server.proxyTarget['headers']	
			s = initSession()
			r = s.head(self.server.proxyTarget['url'], cookies=self.server.proxyTarget['cookies'], headers=headers)
			self.send_response(200)
			self.send_header("Content-type", r.headers['Content-type'])											
			self.send_header('Accept-Ranges', 'bytes')
			self.send_header("Content-Length", str(r.headers['Content-Length']))
			self.end_headers()								
		except Exception as e:				
#			storage.log('streamProxyHandler: HEAD exception: ' + str(e))			
			try:
				self.send_response(404)
				self.end_headers()
			except:
				None	
		
		
	def do_GET(self):			
#		sid = string.randoms(filename=True)	
		try:							
#			storage.log('streamProxyHandler {}: GET Request headers: {}'.format(sid, str(self.headers)) )											
			import shutil									
			headers = self.server.proxyTarget['headers']
			headers['Range'] = self.headers['Range']
			s = initSession()
			r = s.get(self.server.proxyTarget['url'], stream=True, cookies=self.server.proxyTarget['cookies'], headers=headers, timeout=1)			
#			storage.log('streamProxyHandler: GET Received headers: ' + str(r.headers))				
			self.send_response(206)
			self.send_header("Content-type", r.headers['Content-type'])					
			self.send_header('Content-Range', r.headers['Content-Range'])				
			self.send_header("Content-Length", str(r.headers['Content-Length']))						
			self.end_headers()																											
			try:				
				for chunk in r.iter_content(chunk_size=1024):
					self.wfile.write(chunk)							
			except Exception as e:								
				if 'timed out' in str(e):
					try:
						self.send_response(408)						
					except:
						None
#				storage.log('streamProxyHandler: dump exception: ' + str(e))			
		except Exception as e:																
			try:
				self.send_response(404)
				self.end_headers()
			except:
				None	
#			storage.log('streamProxyHandler {}: GET Exception: {}'.format(sid, str(e)) )		

			
	def log_message(self, format, *args):
		return		
		
		
		
		
		
		
### Cloudflare passing request ###		

def get(url, headers={}, timeout=4):        	
	return send(url=url, headers=headers, timeout=timeout)

	
def post(url, headers={}, jsonData=None, formData=None, stringData='', timeout=4):        
	from six.moves import urllib_parse
	if jsonData is not None:
		stringData = json.dumps(jsonData)
		headers['Content-Type'] = 'application/json'
	elif formData is not None:
		stringData = urllib_parse.urlencode(formData, True)
	return send(url=url, headers=headers, timeout=timeout, data=stringData)

		
def send(url, headers={}, data=None, timeout=4):    
	import ssl	
	import gzip
	import six
	from six.moves import urllib_request
	import socket
	socket.setdefaulttimeout(timeout)			 				
	if data is not None:
		req = urllib_request.Request(url, data.encode('utf-8'))
	else:
		req = urllib_request.Request(url)       
	req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36')
	for key in headers:
		req.add_header(key, headers[key])	
	req.add_header('Accept-Encoding', 'gzip')      
	host = req.host
	req.add_unredirected_header('Host', host)	
	ctx = ssl.SSLContext(ssl.PROTOCOL_TLSv1_1) # ssl.PROTOCOL_TLSv1_2
	ctx.set_alpn_protocols(['http/1.1'])
	handlers = [urllib_request.HTTPSHandler(context=ctx)]
	opener = urllib_request.build_opener(*handlers)		  
	response = opener.open(req, timeout=timeout)	 
	return Response(response)

class Response:   
	def __init__(self, response):        
		self._response = response
		self.content = response.read()
		self.headers = response.headers
		if self.headers['content-encoding'].lower() == 'gzip':
			import gzip
			import six
			self.content = gzip.GzipFile(fileobj=six.BytesIO(self.content)).read()					
		try:
			self.encoding = self.headers['content-type'].split('charset=')[-1]
		except:
			self.encoding = 'utf8'
		self.text = self.content.decode(self.encoding, errors='ignore')
			
