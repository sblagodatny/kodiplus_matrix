import json
import xbmc


#favourite: {title, path, thumb, type, window}



def get():
	result = []
	cmd = {'jsonrpc': '2.0', 'method': 'Favourites.GetFavourites', 'params': {'properties': ['thumbnail', 'window', 'windowparameter', 'path']}, "id": 1}
	reply = json.loads(xbmc.executeJSONRPC(json.dumps(cmd)))	
	if 'result' not in reply.keys():
		return result
	if 'favourites' not in reply['result'].keys():
		return result
	if reply['result']['favourites'] is None:
		return result		
	for item in reply['result']['favourites']:
		resultitem = {
			'title': item['title'],
			'type': item['type'],
			'thumb': item['thumbnail']
		}
		if resultitem['type'] == 'window':
			resultitem['path'] = item['windowparameter']
			resultitem['window'] = item['window']
		elif resultitem['thumb'].startswith('androidapp'):
			resultitem['type'] = 'androidapp'
			resultitem['path'] = resultitem['thumb'].replace('.png','') + '/'
		else:
			resultitem['path'] = item['path']	
		result.append(resultitem)
	return result	
	

def find(path):	
	for f in get():
		if f['path'] == path:
			return f
	return None
	
	
def toggle(favourite):
	cmd = {'jsonrpc': '2.0', 'method': 'Favourites.AddFavourite', 'params': {'title': favourite['title'], 'type': favourite['type'], 'thumbnail': favourite['thumb']}}
	if favourite['type'] == 'window':
		cmd['params']['window'] = favourite['window']
		cmd['params']['windowparameter'] = favourite['path']
	else:
		cmd['params']['path'] = favourite['path']
	xbmc.executeJSONRPC(json.dumps(cmd))
	
	
def add(favourite):			
	if find(favourite['path']) is None:				
		toggle(favourite)
	
def remove(path):		
	f = find(path)
	if f is not None:		
		toggle(f)
	

def parse(cmd):
	if cmd.startswith('ActivateWindow'):
		return {'type': 'window', 'path': cmd.split(',')[1].replace('"',''), 'window': cmd.split(',')[0].split('(')[1]}		
	elif cmd.startswith('PlayMedia'):
		return {'type': 'media', 'path': cmd[11:][:-3]}		
	elif cmd.startswith('StartAndroidActivity'):	
		return {'type': 'androidapp', 'path': 'androidapp://sources/apps/' + cmd.replace('StartAndroidActivity(','').replace(')','').replace('"','')}
	else:
		raise Exception('Unable to parse favourites command: ' + cmd)