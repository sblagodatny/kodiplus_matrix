import xbmc, xbmcgui
from helpers import string
import json


def execPluginAPI(plugin, params, retry=10):
	params['sid'] = string.randoms()
	propertyName = plugin + '.' + params['handler']		
	cmd = 'RunPlugin({})'.format('plugin://' + plugin + '/' +'?' + urllib.parse.urlencode(params))
	xbmc.executebuiltin(cmd)		
	propertyValue = None	
	for r in range(retry):
		xbmc.sleep(500)				
		try:
			propertyValue = json.loads(string.b64decode(xbmcgui.Window(10000).getProperty(propertyName)))							
			if propertyValue['request']['sid'] == params['sid']:
				break
			else:			
				propertyValue = None
		except:
			None
	if propertyValue is not None:
		xbmcgui.Window(10000).clearProperty(propertyName)
		return propertyValue['response']
	else:
		return None
		
		
#def handlerSeasonAccountStateAPI():	
#	obj = {
#		'request': _params,
#		'response': tmdb.getAccountContentState(_params['id'], 'tv', _params['season'])['results']
#	}			
#	xbmcgui.Window(10000).setProperty('plugin.video.tmdb2.SeasonAccountStateAPI', string.b64encode(json.dumps(obj)))			