from helpers import storage



def parseM3U8(content, url):		
	import re
	result = []
	lines = content.replace("\r",'').replace(', mp4',' mp4').split("\n")	
	for i in range(len(lines)):		
		if lines[i].startswith('#EXT-X-STREAM-INF:'):							
			try:
				codecs = re.search(r'CODECS="(.*?)"', lines[i]).group(1)	
				lines[i]=lines[i].replace(codecs,'')
			except:
				None			
			data = {}
			for vp in lines[i].split(':')[1].split(','):				
				vp=vp.lstrip().rstrip().split('=')								
				data[vp[0].lower()]=vp[1]			
			surl = lines[i+1]	
			if not surl.startswith('http'):
				urlBase = url.replace(url.split('/')[-1],'')
				if surl.startswith('/'):
					surl = urlBase + surl[1:]
				elif surl.startswith('./'):
					surl = urlBase + surl[2:]
				elif surl.startswith('../'):					
					replaced = urlBase.split('/')[-2] + '/'										
					surl = urlBase.replace(replaced,'') + surl[3:]	
				else:
					surl = urlBase + surl
			data['url']=surl
			result.append(data)							
	return result

	