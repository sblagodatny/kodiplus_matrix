import xbmc
import xbmcgui
import xbmcaddon

from helpers import storage
	
	
def input(title='Input', default=''):
	result = None
	kb = xbmc.Keyboard(default, title, True)		
	kb.setHiddenInput(False)
	kb.doModal()
	if kb.isConfirmed():
		result = kb.getText()
	del kb
	return result		

def inputWithHistory(title, historyFile, historySize=10):		
	history = [xbmcgui.ListItem('New Input')]
	historyCache = []	
	try:
		historyCache = storage.fileToObj(historyFile)
		for s in historyCache:
			history.append(xbmcgui.ListItem(s))
	except:
		None	
	selected = xbmcgui.Dialog().select(title, history)
	if selected == -1:
		return None
	if selected == 0:	
		text = input(title)
		if text is None:
			return None
	else:
		text = historyCache[selected-1]
	if text in historyCache:
		historyCache.remove(text)
	historyCache.insert(0, text)
	historyCache = historyCache[:historySize]
	storage.objToFile(historyCache, historyFile)
	return text
	

def select(title, list, keyName, keyThumb=None):
	lov = []
	useDetails=False
	for item in list:
		li = xbmcgui.ListItem(item[keyName])
		if keyThumb is not None:
			li.setArt({'thumb': item[keyThumb]})
			useDetails=True
		lov.append(li)
	s = xbmcgui.Dialog().select(title, lov,  useDetails=useDetails)
	if s >= 0:			
		return s
	return None	

	
def color(str, color):
	return '[COLOR ' + color + ']' + str + '[/COLOR]'
	
def bold(str):
	return '[B]' + str + '[/B]'	
	

class GridDialog(xbmcgui.WindowDialog):

	events = {
			'on_press': [xbmcgui.ACTION_ENTER, xbmcgui.ACTION_SELECT_ITEM, xbmcgui.ACTION_TOUCH_TAP, xbmcgui.ACTION_MOUSE_LEFT_CLICK],
			'on_close': [xbmcgui.ACTION_PREVIOUS_MENU, xbmcgui.ACTION_NAV_BACK, xbmcgui.ACTION_BACKSPACE, xbmcgui.ACTION_MOUSE_RIGHT_CLICK]
		}

	def __init__(self, width, height, columns, rows, title = ''):
		self.pathImg = xbmcaddon.Addon('script.module.helpers').getAddonInfo('path') + '/resources/img/'		
		self.titleHeight = 50
		self.offsetX = int((self.getWidth() - int(self.getWidth() * width)) / 2)
		self.offsetY = int((self.getHeight() - int(self.getHeight() * height)) / 2) + self.titleHeight
		self.cellWidth = int((self.getWidth() - self.offsetX*2) / columns)
		self.cellHeight = int((self.getHeight() - self.offsetY*2 + self.titleHeight) / rows)
		self.controls = []	
		xbmcgui.WindowDialog.addControl(self, xbmcgui.ControlImage(x=self.offsetX, y=self.offsetY, width=self.getWidth()-self.offsetX*2, height=self.getHeight()-self.offsetY*2 + self.titleHeight, filename=self.pathImg + 'background.jpg' )	)	
		xbmcgui.WindowDialog.addControl(self, xbmcgui.ControlImage(x=self.offsetX, y=self.offsetY - self.titleHeight, width=self.getWidth()-self.offsetX*2, height=self.titleHeight, filename=self.pathImg + 'title.jpg') )
		xbmcgui.WindowDialog.addControl(self, xbmcgui.ControlLabel(x=self.offsetX, y=self.offsetY - self.titleHeight, width=self.getWidth()-self.offsetX*2, height=self.titleHeight, label=bold(title), font='font14', alignment=0x00000002|0x00000004 ) )
		self.paddingX=10
		self.paddingY=10
		
	def addControl(self, obj, name, row, column, rowSize, columnSize, **kvargs):        
		defaults = {
			xbmcgui.ControlLabel: {
				'font': 'font14',
				'alignment': 0x00000002|0x00000004,
				'textColor': '0xFFFFFFFF',
				'label': ''		
			},
			xbmcgui.ControlButton: {
				'font': 'font14',
				'alignment': 0x00000002|0x00000004,
				'textColor': '0xFFFFFFFF',
				'label': '',		
				'disabledColor': '0xFFFF3300',  
				'noFocusTexture': self.pathImg + 'buttonnofocus.jpg', 
				'focusTexture': self.pathImg + 'buttonfocus.jpg'
			}
		}
		for objd in defaults.keys():
			if obj==objd:
				kvargs = {**defaults[objd], **kvargs}		
		control = obj(
			x=self.offsetX+self.cellWidth*column + self.paddingX, 
			y=self.offsetY+self.cellHeight*row  + self.paddingY,
			width=self.cellWidth*columnSize - self.paddingX*2,
			height=self.cellHeight*rowSize  - self.paddingY*2,			
			**kvargs
		)		
		xbmcgui.WindowDialog.addControl(self, control)        
		self.controls.append({'object': control, 'id': control.getId(), 'name': name, 'actions': []})
		
	def addControlAction(self, name, event, action):
		if event not in self.events.keys():
			raise Exception('Invalid event')
		self.getControl('name', name)['actions'].append({'event': event, 'action': action})

	def getControl(self, searchKey, searchValue):
		for control in self.controls:			
			if control[searchKey]==searchValue:
				return control
		return None

	def onAction(self, action):			
		if action.getId() in self.events['on_close']:
			self.close()
			return
		control = self.getControl('id', self.getFocus().getId())
		if control is None:
			return		
		for controlAction in control['actions']:
			if action.getId() in self.events[controlAction['event']]:
				controlAction['action']()
					
                    
	def setNavigationSequence(self, controls):
		l = len(controls)
		for i in range(l):
			next = self.getControl('name', controls[i+1 if i<l-1 else 0])['object']
			prev = self.getControl('name', controls[i-1 if i>=1 else l-1])['object']
			self.getControl('name', controls[i])['object'].setNavigation(up=prev, down=next, left=prev, right=next) 				
		self.setFocus(self.getControl('name', controls[0])['object'])
		
		
		
def download(url, path, title='Download', message=''):
	from helpers import http
	import xbmcgui
	session = http.initSession()		
	response = session.head(url)
	headers = {k.lower(): v for k, v in response.headers.items()}
	while 'location' in headers.keys():	
		response = session.head(headers['location'])
		headers = {k.lower(): v for k, v in response.headers.items()}
	total = int(headers['content-length'])
	downloaded = 0	
	d = xbmcgui.DialogProgress()
	d.create(title, message + "\n\n" + 'Size: {}Mb'.format(round(total/(1024*1024),2) ) )
	try:
		with session.get(url, stream=True) as r:
			r.raise_for_status()
			with open(path, 'wb') as f:
				for chunk in r.iter_content(chunk_size=8192): 
					if d.iscanceled():
						raise ('cancelled')
					f.write(chunk)
					downloaded = downloaded + len(chunk)
					d.update(int(downloaded/total*100))				
		d.close()
		del d		
		return True
	except Exception as e:
		d.close()
		del d
		if str(e) != 'cancelled':
			xbmcgui.Dialog().ok(title, 'Download failed: ' + str(e))
		return False
