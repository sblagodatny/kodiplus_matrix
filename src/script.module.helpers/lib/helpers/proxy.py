import requests
import util
import datetime
import os
from bs4 import BeautifulSoup
import xbmcaddon
import concurrent.futures
import urllib

import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)



_addon = xbmcaddon.Addon()
_pathAddon = _addon.getAddonInfo('path')
_pathSettings = _pathAddon.replace('addons','userdata/addon_data')
_pathProxies = _pathSettings + '/proxies'

def initSession():
	session = requests.Session()
	session.verify = False
	util.setUserAgent(session, 'chrome')
	return session


def updateProxies(country='ru'):
	s = initSession()
	result = []
	data = BeautifulSoup(s.get('https://www.proxynova.com/proxy-server-list/country-' + country + '/').content, "html.parser")		
	for tag in data.find('table', {'id': 'tbl_proxy_list'}).find('tbody').find_all('tr'):
		try:
			host = tag.find('script').string.split("'")[1].split("'")[0]
			port = tag.find_all('td')[1].get_text().rstrip().lstrip()				
		except:
			continue
		result.append(host + ':' + port)				
	return result


def getProxies(country='ru'):
	data = None
	if os.path.isfile(_pathProxies):
		data = util.fileToObj(_pathProxies)
		if (datetime.datetime.now() - data['ts']).days >= 1 :
			data = None
	if data is None:		
		data = {'ts': datetime.datetime.now(), 'proxies': updateProxies(country)}				
		util.objToFile(data, _pathProxies)		
	return data['proxies']

	
def execFunction(func, args):
	result = None
	ex = concurrent.futures.ThreadPoolExecutor(max_workers=50)
	fs = []
	for p in getProxies():
		args['proxies'] = {'http': p, 'https': p}
		fs.append(ex.submit(func, **args))	
	for f in concurrent.futures.as_completed(fs):
		try:		
			result = f.result()
			break			
		except Exception:
			None
	ex.shutdown(wait=False)
	if result is None:
		raise Exception('Unable to execute function via proxy')	
	return result	