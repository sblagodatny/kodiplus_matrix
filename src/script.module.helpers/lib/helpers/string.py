import string
import random
import base64
import urllib
		
def randoms(length=10, filename=False):
	if filename is False:
		letters = string.printable
	else:
		letters = "%s%s" % (string.ascii_letters, string.digits)
	return ( ''.join(random.choice(letters) for i in range(length)) )		

	
def b64encode(str):
	return base64.b64encode(bytes(str,'utf-8')).decode('utf-8')
	
def b64decode(str):
	return base64.b64decode(str).decode("utf-8")	
	
def b32encode(str):
	return base64.b32encode(bytes(str,'utf-8'))
	
def b32decode(str):
	return base64.b32decode(str).decode("utf-8")	
	
def timeStrToSeconds (str):
	import time, datetime
	if str is None:
		return None
	format = '%H:%M:%S'
	if len(str.split(':')) == 2:
		format = '%M:%S'	
	x = time.strptime(str,format)
	return (datetime.timedelta(hours=x.tm_hour,minutes=x.tm_min,seconds=x.tm_sec).total_seconds())	
	

def escape(str):
	return str.replace("&", "&amp;").replace('"', "&quot;").replace("<", "&lt;").replace(">", "&gt;")

def unquote(str):
	return urllib.parse.unquote(str).replace('%2C',',').replace('%2F','/').replace('%3D','=')
	
	
def match(criteria, text):
	criteriaWords = len(criteria.split(' '))
	matchWords = 0
	for cword in criteria.split(' '):
		if cword.upper().replace('Ё','Е') in text.upper().replace('Ё','Е'):
			matchWords = matchWords + 1					
	return int(matchWords/criteriaWords*100)		