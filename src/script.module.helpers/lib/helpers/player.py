import xbmc, xbmcplugin, xbmcgui

from helpers import storage, string

class XBMCPlayer( xbmc.Player ):		
	def __init__( self, *args ):			
		super().__init__(*args)	
		self.label = None
#		self.sid = string.randoms(filename=True)	
	def onPlayBackEnded(self): 
		if self.label is None:
			return
#		storage.log('PlayBackEnded '  + self.sid)
		self._onStop()		
	def onPlayBackStopped(self):        
#		storage.log('PlayBackStopped '  + self.sid)
		self._onStop()			
	def onAVStarted(self):
#			storage.log('AVStarted '  + self.sid)
		if self.label is not None:
			return			
		try:
			self.label = self.getPlayingItem().getLabel()	
			self._onStart()
#				storage.log('waitForCurrentStream init label ' + self.sid + "\n" + self.label )
		except:
			None
	def onAVChange(self):			
#			storage.log('AVChanged '  + self.sid)
		if self.label is None:
			return			
		try:
			label = self.getPlayingItem().getLabel()	
			if len(label) !=0 and self.label != label:					
#				storage.log('waitForCurrentStream exit (label change) '  + self.sid + "\n" + 'Original' + self.label + "\n" + 'Current: ' + label)
				self._onStop()	
		except:
			return				
	
	
	def _onStart(self):		
		None
	def _onStop(self):		
		None
	
		
		
def waitForStream():
	class MXBMCPlayer( XBMCPlayer ):				
		def __init__( self, *args ):			
			super().__init__(*args)	
			self.wait = True
			self.error = False	
		def _onStart(self):			
			self.wait=False		
		def onPlayBackError(self):    
			self.wait=False	
			self.error = True		
	monitor=xbmc.Monitor()
	player = MXBMCPlayer()
	while player.wait is True and not monitor.abortRequested() and player.error is False:
		xbmc.sleep(100)	
	if monitor.abortRequested():
		raise Exception('Playback aborted')	
	if player.error is True:
		raise Exception('Playback error')	

			
def waitForCurrentItem():
	class MXBMCPlayer( XBMCPlayer ):
		def __init__( self, *args ):			
			super().__init__(*args)	
			self.wait=True	
			self.started=False
			self.error=False	
		def _onStop(self):			
			self.wait=False		
		def _onStart(self):
			self.started=True	
		def onPlayBackError(self): 
			self.wait=False	
			self.error = True	
	player = MXBMCPlayer()
	monitor=xbmc.Monitor()
	progress = 0	
	while player.wait is True and not monitor.abortRequested() and player.error is False:
		xbmc.sleep(500)									
		if player.started is False:
			continue
		try:
			progressUpdate = int((player.getTime() / player.getTotalTime()) * 100)			
			if progressUpdate > progress:
				progress = progressUpdate
		except:
			None										
	if monitor.abortRequested():
		raise Exception('Playback aborted')	
	if player.error is True:
		raise Exception('Playback error')	
	return progress	
				

def cancelResolvedUrl(handleId):
	xbmc.executebuiltin('PlayerControl(Stop)')
	xbmc.executebuiltin("Playlist.Clear")	
	xbmc.sleep(500)
	xbmcplugin.setResolvedUrl(handle=handleId, succeeded=True, listitem=xbmcgui.ListItem()		) 		
	xbmc.executebuiltin("Dialog.Close(all, true) ")
	
	
def waitForDelay(waitSeconds, msg='', icon=None, title=''):	
	monitor = xbmc.Monitor()
	seconds = 0
	while not monitor.abortRequested():	
		if icon is None:
			xbmc.executebuiltin('Notification(%s, %s, %d)'%(title, msg + ' (' + str(waitSeconds-seconds) + ' seconds)', 2000 ))
		else:
			xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%(title, msg + ' (' + str(waitSeconds-seconds) + ' seconds)', 2000, icon ))
		xbmc.sleep(2000)
		seconds = seconds + 2
		if seconds >= waitSeconds:
			break
	if seconds < waitSeconds:
		return False
	return True
	
	
def buildUrl(stream):
	import urllib.parse as parse
	url = stream['url']	
	if 'headers' in stream.keys() or 'cookies' in stream.keys():
		url = url + '|'
	if 'headers' in stream.keys():
		url = url + '|' + parse.urlencode(stream['headers'])
	if 'cookies' in stream.keys():
		if not url.endswith('|'):
			url = url + '&'
		url = url + 'Cookie=' + parse.urlencode(stream['cookies']).replace('&', '; ')	
	return url
	
	