def getLanguage(text):
	text = text.lower()
	languages = {
		'ru-RU': ['а','б','в','г','д','е','ё','ж','з','и','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ь','ы','э','ю','я'],
		'he-IL': ['א','ב','ג','ד','ה','ו','ז','ח','ט','י','כ','ך','ל','מ','ם','נ','ן','ס','ע','פ','ף','צ','ץ','ק','ר','ש','ת']
	}
	for language, chars in languages.items():
		for char in chars:
			if char in text:
				return language
	return 'en-US'
