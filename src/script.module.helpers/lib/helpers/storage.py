import xbmc
import xbmcaddon
import time, os


	
def log(msg, level=xbmc.LOGINFO):
	xbmc.log(xbmcaddon.Addon().getAddonInfo('name') + " : " + msg, level=level)
	
def objToFile(obj, path):
	import pickle, xbmcvfs
	from contextlib import closing
	with closing(xbmcvfs.File(path, 'wb')) as f:
		pickle.dump(obj, f)

def fileToObj(path):
	import pickle, xbmcvfs
	from contextlib import closing
	with closing(xbmcvfs.File(path, 'rb')) as f:
		obj = pickle.loads(f.readBytes())
	return obj
	

def setCache(id, data, path, maxAgeSeconds = 60*60*12):			
	import xbmcvfs, json, time
	from contextlib import closing
	if not xbmcvfs.exists(path):
		xbmcvfs.mkdirs(path)
	files = []
	now = time.time()
	for file in xbmcvfs.listdir(path)[1]:
		files.append( (path + '/' + file).replace('//','/') )
	for file in files:
		with closing(xbmcvfs.File(file, 'r')) as f:
			fdata = json.load(f)
		if now - fdata['ts'] > fdata['maxAgeSeconds']:
			xbmcvfs.delete(file)
	cachePath = path + '/' + str(id) + '.json'
	with closing(xbmcvfs.File(cachePath, 'w')) as f:
		json.dump({'id': id, 'data': data, 'ts': time.time(), 'maxAgeSeconds': maxAgeSeconds}, f, indent=4)
	
	
def getCache(id, path):
	import xbmcvfs, json, time
	from contextlib import closing
	cachePath = path + '/' + str(id) + '.json'
	if not xbmcvfs.exists(cachePath):
		return None
	with closing(xbmcvfs.File(cachePath, 'r')) as f:
		data = json.load(f)
	if time.time()-data['ts'] > data['maxAgeSeconds']:
		xbmcvfs.delete(cachePath)
		return None
	return data['data']
	
	
def readlines(f):
	data = f.read()				
	if "\n" in data:
		return data.split("\n")
	else:
		return [data]
		
def removeOldFiles(path, days=30):
	ts = time.time()
	for f in os.listdir(path):
		if (ts - os.stat(os.path.join(path,f)).st_atime)/(60*60*24) > days:						
			os.remove(os.path.join(path,f))		
