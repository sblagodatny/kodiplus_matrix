import sys, xbmcaddon, xbmcvfs
import urllib.parse as parse

_baseUrl = sys.argv[0]
_handleId = int(sys.argv[1])
_params = dict(parse.parse_qsl(sys.argv[2][1:]))	
if 'handler' not in _params.keys():
	_params = {'handler': 'Root'}

_addon = xbmcaddon.Addon()
_path = {'addon': _addon.getAddonInfo('path')}
_path.update({	
	'settings': _path['addon'].replace('addons','userdata/addon_data'),
	'img': _path['addon'] + '/resources/img/'
})

_pathAccount = _path['settings'] + 'account.json'

if not xbmcvfs.exists(_path['settings']):
	xbmcvfs.mkdir(_path['settings'])


def listContent(content):			
	import xbmcgui, xbmcplugin, json
	from helpers import account, string, gui
	acc = account.get(_pathAccount)
	xbmcplugin.setContent(_handleId, 'videos')
	items = []
	for media in content:	
		item = buildListItem({'label': media['title'], 'thumb': media['thumb']})
		info = item.getVideoInfoTag()
		if 'duration' in media.keys():
			info.setDuration(media['duration'])			
		info.setPlot("{}\n{}\n{}\n{}".format('' if 'channel' not in media.keys() else '[B]Channel:[/B] {}'.format(media['channel']['title']), '' if 'visibility' not in media.keys() else '[B]Visibility:[/B] {}'.format(media['visibility']), '' if 'age' not in media.keys() else '[B]Age:[/B] {}'.format(media['age']), '' if 'views' not in media.keys() else '[B]Views:[/B] {}'.format(media['views'])   ) )			
		item.setProperty("IsPlayable", 'true')		
		contextMenuItems=[]		
		if acc is not None:
			cparams = {'handler': 'ItemPlaylists', 'track': string.b64encode(json.dumps(media)), 'account': string.b64encode(json.dumps(acc))}
			contextMenuItems.append(('Playlists', 'RunPlugin(' + _baseUrl+'?' + parse.urlencode(cparams) + ')'))									
		if 'channel' in media.keys():
			cparams = {'handler': 'ChannelInput', 'id': media['channel']['id']}
			contextMenuItems.append(('Channel', 'RunPlugin(' + _baseUrl+'?' + parse.urlencode(cparams) + ')'))			
		cparams = {'handler': 'Download', 'id': media['id']}
		contextMenuItems.append(('Download', 'RunPlugin(' + _baseUrl+'?' + parse.urlencode(cparams) + ')'))
		item.addContextMenuItems(contextMenuItems, replaceItems=False)		
		params = {
			'handler': 'Play',
			'id': media['id'],
		}		
		item.setPath(_baseUrl+'?' + parse.urlencode(params))
		item.setIsFolder(False)
		items.append((item.getPath(), item, item.isFolder(),))	
	xbmcplugin.addDirectoryItems(handle=_handleId, items=items, totalItems=len(items))				
	xbmcplugin.endOfDirectory(_handleId)	
	

def listPlaylists(playlists):		
	import xbmcgui, xbmcplugin, json
	from helpers import account, string, storage	
	acc = account.get(_pathAccount)
	xbmcplugin.setContent(_handleId, 'videos')
	items = []
	for playlist in playlists:	
		item = buildListItem({'label': playlist['title'], 'thumb': playlist['thumb']})
		contextMenuItems=[]
		info = item.getVideoInfoTag()
		info.setPlot("{}\n{}".format('' if 'channel' not in playlist.keys() else '[B]Channel:[/B] {}'.format(playlist['channel']['title']), '' if 'visibility' not in playlist.keys() else '[B]Visibility:[/B] {}'.format(playlist['visibility'])  ) )								
		contextMenuItems=[]		
		if acc is not None:
			cparams = {'handler': 'ManagePlaylists', 'action': 'add', 'account': string.b64encode(json.dumps(acc)) }
			contextMenuItems.append(('Add Playlist', 'RunPlugin(' + _baseUrl+'?' + parse.urlencode(cparams) + ')'))		
			if 'visibility' in playlist.keys():
				cparams = {'handler': 'ManagePlaylists', 'action': 'remove', 'playlist': string.b64encode(json.dumps(playlist)), 'account': string.b64encode(json.dumps(acc)) }
				contextMenuItems.append(('Remove Playlist', 'RunPlugin(' + _baseUrl+'?' + parse.urlencode(cparams) + ')'))						
		if 'channel' in playlist.keys():
			cparams = {'handler': 'ChannelInput', 'id': playlist['channel']['id']}
			contextMenuItems.append(('Channel', 'RunPlugin(' + _baseUrl+'?' + parse.urlencode(cparams) + ')'))	
		item.addContextMenuItems(contextMenuItems, replaceItems=False)			
		params = {
			'handler': 'PlaylistItems',			
			'id': playlist['id']
		}			
		item.setPath(_baseUrl+'?' + parse.urlencode(params))
		item.setIsFolder(True)
		items.append((item.getPath(), item, item.isFolder(),))	
	xbmcplugin.addDirectoryItems(handle=_handleId, items=items, totalItems=len(items))			
	xbmcplugin.endOfDirectory(_handleId)
	

def listChannels(channels, nextPageUrl=None):	
	import xbmcgui, xbmcplugin
	items = []
	for channel in channels:	
		item = buildListItem({'label': channel['title'], 'thumb': channel['thumb']})		
		params = {'handler': 'ChannelInput', 'id': channel['id']}
		item.setPath(_baseUrl+'?' + parse.urlencode(params))
		item.setIsFolder(False)
		items.append((item.getPath(), item, item.isFolder(),))	
	xbmcplugin.addDirectoryItems(handle=_handleId, items=items, totalItems=len(items))		
	xbmcplugin.endOfDirectory(_handleId)	


def handlerItemPlaylists():	
	from youtube import web
	from helpers import string
	import json, xbmc, xbmcgui
	track = json.loads(string.b64decode(_params['track']))						
	acc = json.loads(string.b64decode(_params['account']))						
	
	
#	playlists = web.getItemPlaylist(acc, track['id'])
#	playlistsSelected = xbmcgui.Dialog().multiselect(heading='Playlists', options=[p['title'] for p in playlists], preselect=[playlists.index(p) for p in playlists if p['itemExists'] is True]  )
	
	playlists = web.getItemPlaylists(acc, track['id'])
	playlistsSelected = xbmcgui.Dialog().multiselect(
		heading='Playlists', 
		options=[buildListItem({'label': p['title'], 'thumb': p['thumb']}) for p in playlists], 
		preselect=[playlists.index(p) for p in playlists if p['itemExists'] is True] , 
		useDetails=True
	)
	
	
	if playlistsSelected is None:
		return
	changed = False                        
	for i in range(len(playlists)):
		pl=playlists[i]
		if pl['itemExists'] is True and i not in playlistsSelected:
			web.setItemPlaylist(acc, track['id'], pl['id'], 'remove')
			changed = True
		if pl['itemExists'] is False and i in playlistsSelected:
			web.setItemPlaylist(acc, track['id'], pl['id'], 'add')
			changed = True
	if changed is True:
		xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%('YouTube', 'Playlists updated', 1, _addon.getAddonInfo('icon')))
		

def handlerManagePlaylists():	
	from youtube import web
	from helpers import gui, string
	import xbmc, xbmcgui, json
	acc = json.loads(string.b64decode(_params['account']))	
	if _params['action'] == 'remove':		
		playlist = json.loads(string.b64decode(_params['playlist']))					
		if xbmcgui.Dialog().yesno('YouTube Music', "Remove playlist {} ?".format(playlist['title']) ) is True:		
			web.playlistRemove(acc, playlist['id'])		
		else:
			return
	elif _params['action'] == 'add':
		title = gui.input('Enter playlist title')
		if title is None:
			return
		web.playlistAdd(acc, title)		
	xbmc.sleep(1000)
	xbmc.executebuiltin('Container.Refresh')
		

def handlerPlaylistItems():
	from youtube import web
	data = web.getPlaylistItems(_params['id'])				
	listContent(data) 
		
		
def handlerPlay():		
	from youtube import web, common
	from helpers import account
	import xbmc, xbmcgui, xbmcplugin
	item = xbmcgui.ListItem()
	_account = account.get(_pathAccount)
#	video = web.getVideo(_params['id'], _account)
	video = web.getVideo(_params['id'])

	server = None
	if video['videoDetails']['isLive'] is True:
		item.setPath(video['streamingData']['hlsManifestUrl'])
	else:		
		server = common.startMPDProxyServer(common.getDashManifest(video, None, vp9=eval(_addon.getSetting('vp9').capitalize())), int(_addon.getSetting('dashProxyPort')))
		item.setProperty('inputstream', 'inputstream.adaptive')
		item.setProperty('inputstream.adaptive.manifest_type', 'mpd')				
#		if _account is not None:
#			headers = {
#				'Authorization': common.getAuthHash(_account['cookies'], 'https://www.youtube.com') ,
#				'User-Agent': 'com.google.android.youtube/19.17.34 (Linux; U; Android 14; US) gzip',
#				'X-YouTube-Client-Name': 'WEB_CREATOR', 
#				'X-YouTube-Client-Version': '1.20220918', 
#				'Origin': 'https://www.youtube.com', 
#				'Referer': 'https://www.youtube.com',
#				'Cookie': parse.urlencode(_account['cookies']).replace('&', '; ')	
#			}
#			item.setProperty('inputstream.adaptive.stream_headers', parse.urlencode(headers))		
		item.setMimeType('application/dash+xml')	
		item.setPath('http://{}:{}'.format('localhost', _addon.getSetting('dashProxyPort')))	

	xbmcplugin.setResolvedUrl(handle=_handleId, succeeded=True, listitem=item) 
	if server is not None:
		xbmc.sleep(1500)
		server.shutdown()
		xbmc.sleep(1000)


def handlerChannel():
	from youtube import web
	from helpers import string
	import json
	acc = json.loads(string.b64decode(_params['account']))	
	data = web.getChannel(acc, _params['id'], _params['category'], resultsCountLimit=60)	
	if _params['category'] == 'playlist':
		listPlaylists(data)
	elif _params['category'] == 'video':
		listContent(data) 	
	
	
def handlerChannelInput():
	import xbmc, xbmcgui, json
	from helpers import account, string		
	from youtube import web	
	acc = account.get(_pathAccount)
	channel = web.getChannel(acc, _params['id'])
	d = xbmcgui.Dialog()	
	items = [
		{'label': 'Videos', 'thumb': _path['img'] + '/video.png', 'category': 'video'},
		{'label': 'Playlists', 'thumb': _path['img'] + '/bars.png', 'category': 'playlist'},
		
	]
	if channel.get('subscribed', None) is True:
		items.append({'label': 'Remove Subscription', 'thumb': channel['thumb'], 'action':'unsubscribe'})
	elif channel.get('subscribed', None) is False:
		items.append({'label': 'Subscribe', 'thumb': channel['thumb'], 'action':'subscribe'})		
	s = d.select('Channel: {} ({})'.format(channel['title'], channel['details']), [buildListItem(item) for item in items], useDetails=True)
	if s == -1:
		return
	item = items[s]	
	if 'action' in item.keys():
		web.setSubscription(acc, _params['id'], item['action'])
		xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%('YouTube', 'Subscription updated', 1, _addon.getAddonInfo('icon')))
		return
	params = {
		'handler': 'Channel',		
		'id': _params['id'],
		'category': item['category'],
		'account': string.b64encode(json.dumps(acc))
	}	
	xbmc.executebuiltin("Dialog.Close(all, true) ")
	cmd = 'ActivateWindow(Videos, {}, return)'.format(_baseUrl+'?' + parse.urlencode(params))
	xbmc.executebuiltin(cmd)


def handlerLibrary():		
	from youtube import web
	import json
	from helpers import string
	acc = json.loads(string.b64decode(_params['account']))		
	data = web.getLibrary(acc, _params['category'])	
	if _params['category'] == 'playlist':
		listPlaylists(data)
	elif _params['category'] == 'video':
		listContent(data) 	
	if _params['category'] == 'channel':
		listChannels(data)	
	
		
def handlerLibraryInput():	
	import xbmc, xbmcgui, json
	from helpers import string
	from helpers import account
	acc = account.get(_pathAccount)
	if acc is None:
		xbmcgui.Dialog().ok('YouTube', 'Account not set')
		return		
	d = xbmcgui.Dialog()	
	items = [
		{'label': 'Videos', 'thumb': _path['img'] + '/video.png', 'category': 'video'},
		{'label': 'Playlists', 'thumb': _path['img'] + '/bars.png', 'category': 'playlist'},
		{'label': 'Channels', 'thumb': _path['img'] + '/user.png', 'category': 'channel'}		
	]	
	s = d.select('Library', [buildListItem(item) for item in items], useDetails=True)
	if s == -1:
		return
	item = items[s]	
	params = {
		'handler': 'Library',		
		'category': item['category'],
		'account': string.b64encode(json.dumps(acc))
	}	
	xbmc.executebuiltin("Dialog.Close(all, true) ")
	cmd = 'ActivateWindow(Videos, {}, return)'.format(_baseUrl+'?' + parse.urlencode(params))
	xbmc.executebuiltin(cmd)

	
def handlerFeeds():
	from helpers import account
	from youtube import web
	import xbmcgui
	acc = account.get(_pathAccount)
	if acc is None:
		xbmcgui.Dialog().ok('YouTube', 'Account not set')
		return		
	data = web.getFeeds(acc)		
	listContent(data)


def handlerSuggestions():
	from helpers import account
	from youtube import web
	data = web.getSuggestions(account.get(_pathAccount)	)		
	listContent(data)


def handlerDownload():	
	from youtube import download, android, common
	from helpers import account
	import xbmcgui
	acc = account.get(_pathAccount)
	downloadPath = _addon.getSetting('downloadPath')
	if not xbmcvfs.exists(downloadPath):
		xbmcgui.Dialog().ok('YouTube', 'Please set download folder via settings')
		return		
	video = android.getVideo(_params['id'], account.get(_pathAccount))		
	if video['videoDetails']['isLive'] is True:
		xbmcgui.Dialog().ok('YouTube', 'This is a live stream. Download not available')
		return
	streams = video['streamingData'].get('formats',[]) + video['streamingData'].get('adaptiveFormats',[])	
	s = xbmcgui.Dialog().select('Streams', [common.streamLabel(s) for s in streams])
	if s < 0:			
		return
	stream = streams[s]
	filename =  "".join(x for x in video['videoDetails']['title'] if x.isalnum() or x in '_- .()')
	filename = filename + '_' + str(stream['itag']) + '.' + stream['mimeType'].split(';')[0].split('/')[1]	
	download.downloadWithUI(stream['url'], downloadPath + filename, 'Video: ' + video['videoDetails']['title'] + "\n" + 'ITag: ' + str(stream['itag']))
	
	
def handlerSearch():
	from youtube import web
	data = web.search(_params['text'], _params['category'])	
	if _params['category'] == 'playlist':
		listPlaylists(data)
	elif _params['category'] == 'video':
		listContent(data) 	
	if _params['category'] == 'channel':
		listChannels(data)	


def handlerSearchInput():
	import xbmc, xbmcgui
	from helpers import gui
	d = xbmcgui.Dialog()	
	items = [
		{'label': 'Videos', 'thumb': _path['img'] + '/video.png', 'category': 'video'},
		{'label': 'Playlists', 'thumb': _path['img'] + '/bars.png', 'category': 'playlist'},
		{'label': 'Channels', 'thumb': _path['img'] + '/user.png', 'category': 'channel'}		
	]	
	s = d.select('YouTube Search', [buildListItem(item) for item in items], useDetails=True)
	if s == -1:
		return
	item = items[s]	
	text=gui.inputWithHistory('YouTube Search {}'.format(item['label']), _path['settings'] + 'history_search')	
	if text is None:
		return	
	params = {
		'handler': 'Search',		
		'category': item['category'],
		'text': text		
	}	
	xbmc.executebuiltin("Dialog.Close(all, true) ")
	cmd = 'ActivateWindow(Videos, {}, return)'.format(_baseUrl+'?' + parse.urlencode(params))
	xbmc.executebuiltin(cmd)


def handlerAccount():
	from youtube import web
	from helpers import account
	account.open(_pathAccount, web.validateAccount, 'YouTube Accounts')
	
	
def buildListItem(data):	
	import xbmcgui
	li = xbmcgui.ListItem(data['label'])
	li.setArt({'thumb': data['thumb']})
	if 'fanart' in data.keys():
		li.setArt({'fanart': data['fanart']})
	return li
	
	
def handlerRoot():	
	import xbmcplugin
	from helpers import account
	acc = account.get(_pathAccount)
	items=[		
		{'label': 'Search', 'path': _baseUrl +'?' + parse.urlencode({'handler': 'SearchInput'}), 'thumb': _path['img'] + '/search.png', 'fanart': _path['addon'] + '/fanart.jpg', 'folder': False, 'account': False},
		{'label': 'Library', 'path': _baseUrl +'?' + parse.urlencode({'handler': 'LibraryInput'}), 'thumb': _path['img'] + '/heart.png', 'fanart': _path['addon'] + '/fanart.jpg', 'folder': False, 'account': True},
		{'label': 'Feeds', 'path': _baseUrl +'?' + parse.urlencode({'handler': 'Feeds'}), 'thumb': _path['img'] + '/bars.png', 'fanart': _path['addon'] + '/fanart.jpg', 'folder': True, 'account': True},
		{'label': 'Suggestions', 'path': _baseUrl +'?' + parse.urlencode({'handler': 'Suggestions'}), 'thumb': _path['img'] + '/link.png', 'fanart': _path['addon'] + '/fanart.jpg', 'folder': True, 'account': True},
		{'label': 'Account', 'path': _baseUrl +'?' + parse.urlencode({'handler': 'Account'}), 'thumb': _path['img'] + '/user.png', 'fanart': _path['addon'] + '/fanart.jpg', 'folder': False, 'account': False}		
	]
	for item in items:
		if acc is not None or item['account'] is False:                
			xbmcplugin.addDirectoryItem(_handleId, item['path'], buildListItem(item), item['folder'])
	xbmcplugin.endOfDirectory(_handleId)

globals()['handler' + _params['handler']]()
