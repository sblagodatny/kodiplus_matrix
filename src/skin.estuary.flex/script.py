def scriptClearArtCache(args):
	import xbmc, xbmcvfs, shutil
	from lib import log
	path = 'special://profile/Thumbnails/'
	for folder in xbmcvfs.listdir(path)[0]:
		shutil.rmtree(xbmcvfs.translatePath(path + folder))
	xbmc.executebuiltin('Notification({}, {}, {}, {})'.format('Cleared Art Cache', 'Restarting Kodi to reload art', 3, 'special://skin/resources/kodi.png'))
	xbmc.sleep(1000)
	xbmc.executebuiltin('ShutDown')


def scriptSetPlaylistPosition(args):
	import xbmc
	try:
		requested = int(args[0])
		current = int(xbmc.getInfoLabel('Playlist.Position'))
		offset = requested - current
		if offset != 0:		
			xbmc.executebuiltin('Playlist.PlayOffset({})'.format(offset))
	except:
		None
		

###### Lyrics visualisation ###

def scriptVisKeyHandler(args):					
	from lib import log
	import xbmcgui	
	windowId = xbmcgui.getCurrentWindowId()
	w = xbmcgui.Window(windowId)
	controlLyricsUnSync = w.getControl(5001)
	controlLyricsSync = w.getControl(5002)			
	lyricsType=xbmc.getInfoLabel('Window({}).Property({})'.format(windowId, 'lyricsType'))
	if args[0] in ['left','right']:
		if lyricsType == 'synced':
			xbmc.executebuiltin('SetProperty({},{},{})'.format('lyricsType', 'unsynced', windowId))	
#		elif len(controlLyricsSync.getText()) > 1:
		if lyricsType == 'unsynced':
			xbmc.executebuiltin('SetProperty({},{},{})'.format('lyricsType', 'synced', windowId))		
	elif lyricsType == 'unsynced':
		line=int(xbmc.getInfoLabel('Window({}).Property({})'.format(windowId, 'lyricsUnSyncLine')))
		lines = len(controlLyricsUnSync.getText().split('[CR]'))			
		if args[0] == 'down' and line < lines-9:
			line = line + 1
		if args[0] == 'up' and line > 0:
			line = line - 1
		xbmc.executebuiltin('SetProperty({},{},{})'.format('lyricsUnSyncLine', line, windowId))	
		controlLyricsUnSync.scroll(line)		
	elif lyricsType == 'synced':		
		offset=float(xbmc.getInfoLabel('Window({}).Property(lyricsSyncOffset)'.format(windowId)))		
		if args[0] == 'down' and offset < 15.0:
			offset = offset + 0.5
		if args[0] == 'up' and offset > -15.0:
			offset = offset - 0.5					
		xbmc.executebuiltin('SetProperty(lyricsSyncOffset,{},{})'.format(offset, windowId))
		
	
def scriptVisJob(args):
	import xbmcgui, json
	from lib import util, log
	player = xbmc.Player()
	windowId = xbmcgui.getCurrentWindowId()
	w = xbmcgui.Window(windowId)
	controlLyricsUnSync = w.getControl(5001)
	controlLyricsSync = w.getControl(5002)
	lyrics = {'synced': None, 'unsynced': None}
	
#	w.clearProperty('lyrics')


	addon = xbmc.getInfoLabel('Container.PluginName')
	log.log(addon)
	
	source = ''
	  
	while xbmc.getCondVisibility('Window.IsActive({})'.format(windowId)):

		try:						
			
			_source = player.getPlayingFile()
			if _source != source:									
																			
				try:
#					lyrics = json.loads(xbmc.getInfoLabel('MusicPlayer.Lyrics'))	
					lyrics = json.loads(util.b64decode(w.getProperty('lyrics')))													
					if lyrics['label'] != player.getPlayingItem().getLabel():
						w.clearProperty('lyrics')
						raise
				except Exception as e:
					lyrics = {'synced': None, 'unsynced': None}													
		 		
				xbmc.executebuiltin('SetProperty(lyricsUnSyncLine,{},{})'.format(0, windowId))											
				xbmc.executebuiltin('SetProperty(lyricsSyncOffset,{},{})'.format(0.0, windowId))									
				
				controlLyricsSync.setText('')
				if lyrics['unsynced'] is not None:
					controlLyricsUnSync.setText(lyrics['unsynced'].replace("\n",'[CR]'))
				else:
					controlLyricsUnSync.setText('')
				
				if lyrics['synced'] is not None:
					xbmc.executebuiltin('SetProperty(lyricsType,{},{})'.format('synced', windowId))
				elif lyrics['unsynced'] is not None:
					xbmc.executebuiltin('SetProperty(lyricsType,{},{})'.format('unsynced', windowId))
				else:
					xbmc.executebuiltin('SetProperty(lyricsType,{},{})'.format('none', windowId))				
				
				source = _source		
			 					
			### Update unsynced ###
			if lyrics['synced'] is not None:
				offset=float(xbmc.getInfoLabel('Window({}).Property(lyricsSyncOffset)'.format(windowId)))		
				ts=round(xbmc.Player().getTime(),2)				
				lyricsCurrent = [item for item in lyrics['synced'] if item['start']+offset <= ts <= item['end']+offset ]
				lyricsNext = [item for item in lyrics['synced'] if item['start']+offset > ts]
				label = "{}[CR]{}".format('' if len(lyricsCurrent)==0 else lyricsCurrent[0]['text'], '' if len(lyricsNext)==0 else lyricsNext[0]['text'] )
				controlLyricsSync.setText(label)

			
		except Exception as e:
			None
			controlLyricsSync.setText('')
			controlLyricsUnSync.setText('')						
#			log.log(str(e))
									
		xbmc.sleep(500)

#	w.clearProperty('lyrics')
#	xbmc.executebuiltin('Notification({}, {}, {}, {})'.format('Cleared', 'Clear', 1, 'special://skin/resources/kodi.png'))


	
	
def scriptOverrideSetting(args):
	import xbmc, json
	from lib import settings
	xbmc.executebuiltin('ClearProperty({},10000)'.format(args[0]))
	value = settings.getSettingValue(args[0])	
	if value==args[1]:						
		return			
	settings.setSettingValue(args[0],args[1])	
	xbmc.sleep(100)
	xbmc.executebuiltin('Container.Refresh()')
	xbmc.executebuiltin('SetProperty({},{},10000)'.format(args[0],value))	


def scriptRestoreSetting(args):	
	import xbmc, json
	from lib import settings
	value = xbmc.getInfoLabel('Window(10000).Property({})'.format(args[0]))
	xbmc.executebuiltin('ClearProperty({},10000)'.format(args[0]))
	if len(value)==0:				
		return
	settings.setSettingValue(args[0], value)
	

def scriptSetLogLevel(args):	
	import xbmcgui	
	from lib import manage, advancedsettings, log
	d = xbmcgui.Dialog()				
	items = [
		{'label': 'Disabled', 'thumb': 'special://skin/resources/icons/minus.png', 'value': -1},
		{'label': 'Normal', 'thumb': 'special://skin/resources/icons/note.png', 'value': 0},
		{'label': 'Debug', 'thumb': 'special://skin/resources/icons/target.png', 'value': 1}
	]		
	settings = advancedsettings.getSettings()
	current = log.getLogLevel(settings)
	preselect = [items.index(item) for item in items if item['value']==current][0]	
	s = d.select('Set Log level', [manage.buildListItem(item) for item in items], useDetails=True, preselect=preselect)
	if s == -1:
		return
	log.setLogLevel(settings, items[s]['value'])	
	advancedsettings.setSettings(settings)
	

def scriptSetBackground(args):	
	import xbmc, xbmcgui, os, xbmcvfs
	from lib import manage
	d = xbmcgui.Dialog()		
	pathSkinBackgrounds = 'special://skin/resources/backgrounds'
	items = []
	for f in os.listdir(xbmcvfs.translatePath(pathSkinBackgrounds)):
		items.append({'label': f.replace('.jpg','').replace('.png', '').title(), 'thumb': pathSkinBackgrounds + '/' + f})
	items.append({'label': 'Custom', 'thumb': 'special://skin/resources/icons/folder.png'})
	s = d.select('Choose background', [manage.buildListItem(item) for item in items], useDetails=True )
	if s == -1:
		return
	item = items[s]
	if item['label'] == 'Custom':
		path = d.browse(2, 'Choose custom background','', defaultt=None)
		if path is None:
			return
		item['thumb'] = path
	xbmc.executebuiltin('Skin.SetString(background,{})'.format(item['thumb']))


	
	
def scriptUpdateCheck(args):	
	import xbmcvfs	
	from lib import manage, build, log
	
	config = manage.getConfig('profile')	
	pathSkinConfig = xbmcvfs.translatePath('special://skin/') + 'resources/templates/home.json'
	pathDataConfig = xbmcvfs.translatePath('special://profile/addon_data/skin.estuary.flex/') + 'home.json'					
	log.log('Estuary.Flex: Update check (Skin: {}, Profile: {})'.format(xbmcvfs.Stat(pathSkinConfig).st_mtime(),xbmcvfs.Stat(pathDataConfig).st_mtime()) )	
	if xbmcvfs.Stat(pathDataConfig).st_mtime() < xbmcvfs.Stat(pathSkinConfig).st_mtime():		
		log.log('Estuary.Flex: Re-Building home')
		manage.setConfig(config)
		build.buildHome()			
	else:
		log.log('Estuary.Flex: Home OK' )

	pathSkinKemap = xbmcvfs.translatePath('special://skin/') + 'keymap/keymap_estuaryflex.xml'
	pathProfileKeymap = xbmcvfs.translatePath('special://profile/keymaps/') + 'keymap_estuaryflex.xml'	
	if (not xbmcvfs.exists(pathProfileKeymap)) or (xbmcvfs.Stat(pathProfileKeymap).st_mtime() < xbmcvfs.Stat(pathSkinKemap).st_mtime()):		
		log.log('Estuary.Flex: Re-Building keymap')
		xbmcvfs.copy(pathSkinKemap, pathProfileKeymap)
		xbmc.executebuiltin('Action(reloadkeymaps)')
	else:
		log.log('Estuary.Flex: Keymap OK' )
	

def scriptManage(args):
	import xbmcgui, copy
	from lib import build, manage
	config=manage.getConfig('profile')
	configTemp = copy.deepcopy(config)
	d = xbmcgui.Dialog()
	manage.manageRoot(configTemp)	
	if config != configTemp:
		if d.yesno('Estuary Flex', 'Save changes ?') is True:
			manage.setConfig(configTemp)
			build.buildHome()
				
				
def scriptRefresh(args):				
	from lib import build
	build.buildHome()
	
	
def scriptViewLog(args):				
	import xbmcgui
	from lib import log
	data = log.get()
	data.reverse()
	message = ''	
	for item in data:	
		message = message + "[B]{}: {}[/B]\n{}\n\n".format(item['ts'], item['type'].capitalize(), item['message'].lstrip().rstrip())				
	xbmcgui.Dialog().textviewer('Kodi Log', message)


def scriptSeekBarApply(args):	
	import xbmc
	try:
		seekToPercent = int(args[0].replace('%',''))				
		player = xbmc.Player()		
		seekTo = int(player.getTotalTime()*seekToPercent/100)
		player.seekTime(seekTo)						
	except:
		None
	
	
def scriptSeekBarInit(args):	
	import xbmc	
	try:
		progress = int(xbmc.getInfoLabel('Player.Progress'))
		current = int(args[0].replace('%',''))	
		if progress > current:
			for i in range(progress-current):
				xbmc.executebuiltin('Action(Right)')
		elif progress < current:	
			for i in range(current-progress):
				xbmc.executebuiltin('Action(Left)')
	except:
		None


def getInfoControls(include):
	import xbmcvfs
	from bs4 import BeautifulSoup
	controls = []
	with open(xbmcvfs.translatePath('special://skin/xml/Includes_Flex_Info.xml'), 'r') as f:
		root = BeautifulSoup(f.read(), "html.parser")
	for tag in root.find('include', {'name': include}).find_all('include', {'content': 'ItemInfoLabel'}):
		controls.append({'id': int(tag.find('param', {'name': 'id'})['value']), 'value': tag.find('param', {'name': 'value'})['value'], 'type': 'label'})
	tag = root.find('include', {'name': 'ItemInfoPlot'}).find('control', {'type': 'textbox'})
	controls.append({'id': int(tag['id']), 'value': tag.find('label').get_text(), 'type': 'textbox'})
	return controls


def scriptGetAsyncInfo(args):	
	from lib import log
	import xbmc, xbmcgui, xbmcaddon
	addon = xbmc.getInfoLabel('ListItem.FolderPath').split('/')[2]		
	asyncInfo = xbmcaddon.Addon(addon).getSetting('asyncInfo')
	if asyncInfo != 'true':
		return	
	xbmc.executebuiltin('RunScript(' + addon + ',GetAsyncInfo,skin.estuary.flex,UpdateAsyncInfo)')


def scriptUpdateAsyncInfo(args):
	import xbmcgui, json
	import base64	
	info = json.loads(base64.b64decode(args[0]).decode("utf-8"))		
	w = xbmcgui.Window(12003)
	controls = getInfoControls('ItemInfoMovieLabels')	
	for control in controls:
		value = control['value']
		for key in info.keys():									
			value = value.replace('$INFO[{}]'.format(key), str(info[key]))															
		if control['type'] == 'label':
			w.getControl(control['id']).setLabel(value)
		elif control['type'] == 'textbox':
			w.getControl(control['id']).setText(value)
	if 'ListItem.CastAndRole' in info.keys():
		obj = w.getControl(50)
		for person in info['ListItem.CastAndRole']:
			li = xbmcgui.ListItem()
			li.setLabel(person['name'])
			li.setLabel2(person['role'])
			li.setArt({'thumb': person['thumbnail']})
			obj.addItem(li)


def scriptSetViewMode(args):				
	from lib import log, manage
	import xbmc, xbmcgui
	d = xbmcgui.Dialog()	
	viewModes = [
		{'id': 50, 'label': 'List', 'include': 'View_50_List', 'thumb': 'special://skin/resources/screenshots/List.jpg'},
		{'id': 53, 'label': 'Shift', 'include': 'View_53_Shift', 'thumb': 'special://skin/resources/screenshots/Shift.jpg'},
		{'id': 500, 'label': 'Wall', 'include': 'View_500_Wall', 'thumb': 'special://skin/resources/screenshots/Wall.jpg'}			
	]
	s = d.select('View Modes', [manage.buildListItem(item) for item in viewModes], useDetails=True)
	if s==-1:
		return
	viewModeTarget = viewModes[s]	
	viewModeCurrent = xbmc.getInfoLabel('Container.Viewmode')	
	if viewModeCurrent == viewModeTarget['label']:
		return	
	i = -1
	for viewMode in viewModes + viewModes:
		if i < 0:
			if viewMode['label'] == viewModeCurrent:
				i = 0
		else:
			i = i + 1
			if viewMode['label'] == viewModeTarget['label']:
				break
	for c in range(i):
		xbmc.executebuiltin('Container.NextViewMode')
		
		
	

globals()['script' + sys.argv[1]](sys.argv[2:])


