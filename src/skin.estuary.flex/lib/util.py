import base64


def b64encode(str):
	return base64.b64encode(bytes(str,'utf-8'))
	
def b64decode(str):
	return base64.b64decode(str).decode("utf-8")	