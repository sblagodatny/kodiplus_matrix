
def execute(method, params):
	import xbmc, json
	request = {'jsonrpc': '2.0', 'method': method, 'params': params, 'id': 1}
	response = xbmc.executeJSONRPC(json.dumps(request))	
	return json.loads(response)			