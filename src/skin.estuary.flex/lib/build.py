
def getDirectory(path, content):
	import xbmc, json
	try:
		params = {'properties': ['file','art'], 'directory': path, 'media': 'files'}
		request = {'jsonrpc': '2.0', 'method': 'Files.GetDirectory', 'params': params, 'id': 1}
		response = xbmc.executeJSONRPC(json.dumps(request))	
		response = json.loads(response)['result']['files']
		for item in response:
#			if 'art' not in item.keys():
#				item['art']={}
#			art = item.get('art',{})
#			item['thumb'] = art.get('thumb', art.get('poster', 'icons/sidemenu/addons.png' ) )			
			if item['filetype'] == 'directory':
				item['action'] = 'ActivateWindow({},{},return)'.format(content, item['file'])
			elif item['file'].startswith('plugin://'):
				item['action'] = 'RunPlugin({})'.format(item['file'])
			elif item['files'].startswith('androidapp://'):
				item['action'] = 'StartAndroidActivity({})'.format(item['file'].split('/')[-1])
			else:
				item['action'] = 'PlayMedia({})'.format(item['file'])
		return response
	except:
		return []


def buildHome():			
	import xbmc, xbmcvfs, json
	from bs4 import BeautifulSoup
	
	xbmc.executebuiltin('ActivateWindow(busydialognocancel)')
	
	try:
	
		pathSkin = xbmcvfs.translatePath('special://skin/') 
		pathData = xbmcvfs.translatePath('special://profile/addon_data/skin.estuary.flex/')
		
		with open(pathData + 'home.json', 'r') as f:
			data = json.load(f)		
		with open(pathSkin + 'resources/templates/home.xml', 'r') as f:
			home = BeautifulSoup(f.read(), "html.parser")
		with open(pathSkin + 'resources/templates/templates.xml', 'r') as f:
			templates = BeautifulSoup(f.read(), "html.parser")

		tagCategoriesBase = home.find('control', {'id': '2000'}).find('content')
		tagCategoriesWidgetsBase = home.find('control', {'id': '3000'})
		catId = 100000
		for item in data:
		
			tag = str(templates.find('categoryitem').find('item'))
			tag = tag.replace('@@LABEL@@', item['label'])
			tag = tag.replace('@@THUMB@@', item['thumb'])
			tag = tag.replace('@@ID@@', str(catId))
			tag = tag.replace('@@ACTION@@', item['action'])
			tag = BeautifulSoup(tag, "html.parser")
			tagCategoriesBase.append(tag)

			tag = str(templates.find('categorywidgetsbase').find('control'))
			tag = tag.replace('@@ID@@', str(catId))
			tag = tag.replace('@@SCROLL_ID@@', str(catId+1))
			tag = BeautifulSoup(tag, "html.parser")
			
			widgetId = catId + 2
			for widget in item['widgets']:		
				wtag = str(templates.find('widget').find('include'))		
				wtag = wtag.replace('@@ID@@', str(widgetId))
				wtag = wtag.replace('@@LABEL@@', widget['label'])				
				wtag = wtag.replace('@@CONTENT@@', widget.get('content',''))					
				wtag = wtag.replace('@@SHAPE@@', widget.get('shape',''))		
				if widget.get('source','').startswith('pvr'):
					wtag = wtag.replace('@@INFOUPDATE@@', '5000')
					wtag = wtag.replace('@@SORTBY@@', 'lastplayed')			# lastplayed / lastused	
					wtag = wtag.replace('@@SORTORDER@@', 'descending')	
				else:
					wtag = wtag.replace('@@INFOUPDATE@@', '')
					wtag = wtag.replace('@@SORTBY@@', '')				
					wtag = wtag.replace('@@SORTORDER@@', '')	
				if widget['type']=='dynamic':
					wtag = wtag.replace('@@SOURCE@@', widget.get('source',''))	
				else:
					wtag = wtag.replace('@@SOURCE@@', '')	
				wtag = BeautifulSoup(wtag, "html.parser")																
				if widget['type']=='static':																
					if 'items' in widget.keys():
						witems = widget['items']
					else:
						witems = getDirectory(widget['source'], widget['content'])													
					for witem in witems:
						witag = str(templates.find('widgetitem').find('item'))
						witag = witag.replace('@@LABEL@@', witem['label'])
						
						if 'art' not in witem.keys():
							witem['art']={}						
						if 'thumb' in witem.keys():
							witem['art']['thumb'] = witem['thumb']
						if 'thumb' not in witem['art'].keys():
							witem['art']['thumb'] = witem['art'].get('icon', witem['art'].get('poster',''))											
						witag = witag.replace('@@THUMB@@', witem['art']['thumb'])
						if 'fanart' in witem['art'].keys():
							witag = witag.replace('@@FANART@@', witem['art']['fanart'])
						else:
							witag = witag.replace('@@FANART@@', '')
						
						
						witag = witag.replace('@@ACTION@@', witem['action'])
						wtag.find('content').append(BeautifulSoup(witag, "html.parser"))
						
				tag.find('control', {'type': 'grouplist'}).append(wtag)
				widgetId = widgetId + 1					
			tagCategoriesWidgetsBase.append(tag)		
			catId = catId + 100
		
		with open(pathSkin + '/xml/Home.xml', 'w') as f:
			f.write(str(home))
						
		xbmc.executebuiltin('Dialog.Close(busydialognocancel,true)') 	
		
	except:
		xbmc.executebuiltin('Dialog.Close(busydialognocancel,true)') 
		raise
		
	xbmc.executebuiltin('ReloadSkin()')	
	xbmc.sleep(500)
	xbmc.executebuiltin('AlarmClock(dummy,Control.SetFocus(2000),0,silent)')		
	xbmc.executebuiltin('Notification({}, {}, {}, {})'.format('Estuary Flex', 'Updated Home Layout', 3, pathSkin + 'resources/icon.png'))
	