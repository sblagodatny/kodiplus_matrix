def get(app):
	import requests
	from bs4 import BeautifulSoup
	content = requests.Session().get('https://play.google.com/store/apps/details?id=' + app).text	
	content = BeautifulSoup(content, "html.parser")
	try:
		return {
			'thumb': content.find('img', {'alt': 'Icon image'})['srcset'].split(' ')[0],			
			'title': content.find('h1', {'itemprop': 'name'}).get_text(),
		}		
	except Exception as e:		
		return None	