	
def getConfig(source):
	import json, xbmcvfs
	pathSkinConfig = xbmcvfs.translatePath('special://skin/') + 'resources/templates/home.json'
	pathDataConfig = xbmcvfs.translatePath('special://profile/addon_data/skin.estuary.flex/') + 'home.json'	
	if source == 'skin':
		with open(pathSkinConfig, 'r') as f:
			return json.load(f)
	elif source == 'profile':
		if not xbmcvfs.exists(pathDataConfig):
			xbmcvfs.copy(pathSkinConfig, pathDataConfig)
		with open(pathDataConfig, 'r') as f:
			return json.load(f)	
	
	
def setConfig(config):
	import json, xbmcvfs
	pathDataConfig = xbmcvfs.translatePath('special://profile/addon_data/skin.estuary.flex/') + 'home.json'		
	with open(pathDataConfig, 'w') as f:
		json.dump(config, f, indent=4)	


def buildListItem(data):
	import xbmcgui
	li = xbmcgui.ListItem(data['label'])
	li.setArt({'thumb': data['thumb']})
	return li



def manageRoot(config):
	import xbmcgui, copy
	d = xbmcgui.Dialog()
	while True:
		
		items = [item for item in config]
		i = 0
		for item in items:
			item['index'] = i
			i = i + 1
		items.append({'label': 'Add Category', 'thumb': 'special://skin/resources/icons/plus.png'})
		items.append({'label': 'Add Category Default', 'thumb': 'special://skin/resources/icons/upload.png'})
		items.append({'label': 'Restore Defaults', 'thumb': 'special://skin/resources/icons/upload.png'})		
		s = d.select('Categories', [buildListItem(item) for item in items], useDetails=True)
		if s == -1:
			break					
		item = items[s]		
		if item['label'] == 'Restore Defaults':				
			if d.yesno('Estuary Flex', "Replace current settings with skin defaults ?") is True:		
				config.clear()
				for cat in getConfig('skin'):
					config.append(cat)
				continue
		elif item['label'] == 'Add Category':
			config.append({'label': 'New Category {}'.format(len(config)+1), 'thumb': 'special://skin/resources/icons/folder.png', 'widgets': [], 'action': ''})	
			action = manageCategory(config[-1])			
			item['index'] = len(config)-1
		elif item['label'] == 'Add Category Default':
			items = [cat for cat in getConfig('skin')]
			s = d.select('Default Categories', [buildListItem(item) for item in items], useDetails=True)
			if s == -1:
				continue			
			config.append(items[s])	
			continue		
		else:
			action = manageCategory(config[item['index']])
					
		if action == 'remove':
			config.pop(item['index'])
		elif action == 'moveup' and item['index']>0:			
			config[item['index']], config[item['index']-1] = config[item['index']-1], config[item['index']]
		elif action == 'movedown' and item['index']<len(config)-1:
			config[item['index']], config[item['index']+1] = config[item['index']+1], config[item['index']]
			
	
def manageItem(myitem):
	import json, xbmcgui
	d = xbmcgui.Dialog()		
	while True:	
		items = [		
			{'label': '[B]Action:[/B] {}'.format(myitem['action']), 'thumb': 'special://skin/resources/icons/action.png', 'function': chooseAction},
			{'label': '[B]Label:[/B] {}'.format(myitem['label']), 'thumb': 'special://skin/resources/icons/font.png', 'function': chooseLabel},
			{'label': '[B]Icon:[/B] {}'.format(myitem['thumb'].replace('special://skin/resources/icons/','').replace('.png','') ), 'thumb': myitem['thumb'], 'function': chooseIcon}						
		]
		items.append({'label': 'Remove Item', 'thumb': 'special://skin/resources/icons/minus.png', 'action': 'remove'})		
		items.append({'label': 'Move Item Up', 'thumb': 'special://skin/resources/icons/arrow-up.png', 'action': 'moveup'})		
		items.append({'label': 'Move Item Down', 'thumb': 'special://skin/resources/icons/arrow-down.png', 'action': 'movedown'})		
		s = d.select('Item', [buildListItem(item) for item in items], useDetails=True)
		if s == -1:
			break				
		item = items[s]
		if 'function' in item.keys():
			item['function'](myitem)		
		elif 'action' in item.keys():
			return item['action']


def manageWidget(widget):
	import json, xbmcgui	
	d = xbmcgui.Dialog()
	while True:	
		items = [		
			{'label': '[B]Source:[/B] {}'.format('Items List' if len(widget.get('source', ''))==0 else widget['source']), 'thumb': 'special://skin/resources/icons/playlist.png', 'function': chooseWidgetSource},
			{'label': '[B]Label:[/B] {}'.format(widget['label']), 'thumb': 'special://skin/resources/icons/font.png', 'function': chooseLabel},
			{'label': '[B]Type:[/B] {}'.format(widget['type'].capitalize()), 'thumb': 'special://skin/resources/icons/recent.png', 'function': chooseWidgetType},
#			{'label': '[B]Sort:[/B] {}'.format('None' if len(widget.get('sortby', ''))==0 else widget['sortby'] + ', ' + widget['sortorder']), 'thumb': 'special://skin/resources/icons/shuffle.png', 'function': chooseWidgetSort},
			{'label': '[B]Shape:[/B] {}'.format(widget.get('shape', 'square').capitalize()), 'thumb': 'special://skin/resources/icons/brush.png', 'function': chooseWidgetShape}	
		]
		if 'items' in widget.keys():
			i = 0
			for item in widget['items']:
				items.append({'label': '[B]Item:[/B] {}'.format(item['label']), 'thumb': item['thumb'], 'index': i})
				i = i + 1
			items.append({'label': 'Add Item', 'thumb': 'special://skin/resources/icons/plus.png'})
		items.append({'label': 'Remove Widget', 'thumb': 'special://skin/resources/icons/minus.png', 'action': 'remove'})
		items.append({'label': 'Move Widget Up', 'thumb': 'special://skin/resources/icons/arrow-up.png', 'action': 'moveup'})
		items.append({'label': 'Move Widget Down', 'thumb': 'special://skin/resources/icons/arrow-down.png', 'action': 'movedown'})
		s = d.select('Widget', [buildListItem(item) for item in items], useDetails=True)		
		if s == -1:
			break					
		item = items[s]
		if 'function' in item.keys():
			item['function'](widget)
			continue
		elif 'index' in item.keys():
			action = manageItem(widget['items'][item['index']])				
		elif item['label'] == 'Add Item':
			widget['items'].append({'label': 'New Item {}'.format(len(widget['items'])+1), 'action': '', 'thumb': 'special://skin/resources/icons/file.png'})
			action = manageItem(widget['items'][-1])
			item['index'] = len(widget['items'])-1
		elif 'action' in item.keys():
			return item['action']
			
		if action == 'remove':
			widget['items'].pop(item['index'])
		elif action == 'moveup' and item['index']>0:			
			widget['items'][item['index']], widget['items'][item['index']-1] = widget['items'][item['index']-1], widget['items'][item['index']]
		elif action == 'movedown' and item['index']<len(widget['items'])-1:
			widget['items'][item['index']], widget['items'][item['index']+1] = widget['items'][item['index']+1], widget['items'][item['index']]

		
def manageCategory(category):
	import json, xbmcgui	
	d = xbmcgui.Dialog()	
	while True:
		items = [
			{'label': '[B]Action:[/B] {}'.format(category['action']), 'thumb': 'special://skin/resources/icons/action.png', 'function': chooseAction},
			{'label': '[B]Label:[/B] {}'.format(category['label']), 'thumb': 'special://skin/resources/icons/font.png', 'function': chooseLabel},
			{'label': '[B]Icon:[/B] {}'.format(category['thumb'].replace('special://skin/resources/icons/','').replace('.png','') ), 'thumb': category['thumb'], 'function': chooseIcon}								
		]
		i = 0
		for widget in category['widgets']:
			items.append({'label': '[B]Widget:[/B] {}'.format(widget['label']), 'thumb': "special://skin/resources/icons/atom.png", 'index': i})
			i = i + 1
		items.append({'label': 'Add Widget', 'thumb': 'special://skin/resources/icons/plus.png'})
		items.append({'label': 'Remove Category', 'thumb': 'special://skin/resources/icons/minus.png', 'action': 'remove'})
		items.append({'label': 'Move Category Up', 'thumb': 'special://skin/resources/icons/arrow-up.png', 'action': 'moveup'})
		items.append({'label': 'Move Category Down', 'thumb': 'special://skin/resources/icons/arrow-down.png', 'action': 'movedown'})
		s = d.select('Category', [buildListItem(item) for item in items], useDetails=True)
		if s == -1:
			break		
		item = items[s]
		if 'function' in item.keys():
			item['function'](category)
			continue
		elif 'index' in item.keys():
			action = manageWidget(category['widgets'][item['index']])
		elif item['label'] == 'Add Widget':
			category['widgets'].append({'type': 'static', 'label': 'New Widget {}'.format(len(category['widgets'])+1), 'items': []})
			action = manageWidget(category['widgets'][-1])
			item['index'] = len(category['widgets'])-1				
		elif 'action' in item.keys():
			return item['action']
						
		if action == 'remove':
			category['widgets'].pop(item['index'])
		elif action == 'moveup' and item['index']>0:			
			category['widgets'][item['index']], category['widgets'][item['index']-1] = category['widgets'][item['index']-1], category['widgets'][item['index']]
		elif action == 'movedown' and item['index']<len(category['widgets'])-1:
			category['widgets'][item['index']], category['widgets'][item['index']+1] = category['widgets'][item['index']+1], category['widgets'][item['index']]





def chooseActionAndroidApp(obj):
	import xbmcgui, json
	from lib import log, googleplay	
	d = xbmcgui.Dialog()	
	path = d.browse(1, 'Choose App', shares='files',defaultt='androidapp://sources/apps/')					
	if path=='androidapp://sources/apps/':
		return None	
	package = path.split('/')[-1]	
	obj['label'] = package.split('.')[-1].capitalize()
	obj['thumb'] = 'androidapp://sources/apps/{}.png'.format(package)
	obj['action'] = 'StartAndroidActivity({})'.format(package)
		
	details = googleplay.get(package)
	if details is not None:
		obj['label'] = details['title']
		obj['thumb'] = details['thumb']




def chooseActionKodi(obj):
	import xbmcgui	
	d = xbmcgui.Dialog()	
	items = [
		{'label': 'Home Refresh', 'thumb': 'special://skin/resources/icons/dependencies.png', 'value': 'RunScript(skin.estuary.flex,Refresh)'},
		{'label': 'File Manager', 'thumb': 'special://skin/resources/icons/folder-open.png', 'value': 'ActivateWindow(filemanager)'},
		{'label': 'Log Viewer', 'thumb': 'special://skin/resources/icons/message.png', 'value': 'RunScript(skin.estuary.flex,ViewLog)'},		
		{'label': 'Kodi Settings', 'thumb': 'special://skin/resources/icons/settings.png', 'value': 'ActivateWindow(settings)'},		
		{'label': 'Addons Browser', 'thumb': 'special://skin/resources/icons/addon.png', 'value': 'ActivateWindow(addonbrowser,return)'},
		{'label': 'Addons Update', 'thumb': 'special://skin/resources/icons/update.png', 'value': 'UpdateAddonRepos'},				
		{'label': 'Videos', 'thumb': 'special://skin/resources/icons/video.png', 'value': 'ActivateWindow(Videos,root,return)'},
		{'label': 'Music', 'thumb': 'special://skin/resources/icons/music.png', 'value': 'ActivateWindow(Music,root,return)'},
		{'label': 'Pictures', 'thumb': 'special://skin/resources/icons/image.png', 'value': 'ActivateWindow(Pictures,root,return)'},
		{'label': 'PVR TV', 'thumb': 'special://skin/resources/icons/tv.png', 'value': 'ActivateWindow(TVChannels)'},
		{'label': 'PVR Radio', 'thumb': 'special://skin/resources/icons/radio.png', 'value': 'ActivateWindow(RadioChannels)'},
		{'label': 'Programs', 'thumb': 'special://skin/resources/icons/cubes.png', 'value': 'ActivateWindow(Programs)'},
		{'label': 'Games', 'thumb': 'special://skin/resources/icons/game.png', 'value': 'ActivateWindow(Games)'},		
		{'label': 'Exit Kodi', 'thumb': 'special://skin/resources/icons/power.png', 'value': 'ShutDown'}		
	]	
	s = d.select('Choose Kodi Action', [buildListItem(item) for item in items], useDetails=True )
	if s == -1:
		return None
	item = items[s]
	obj['label'] = item['label']
	obj['thumb'] = item['thumb']
	obj['action'] = item['value']



	
						
def chooseAction(obj):
	import xbmcgui, xbmc	
	d = xbmcgui.Dialog()	
	items = [{'label': 'Kodi Action', 'thumb': 'special://skin/resources/icons/settings.png', 'function': chooseActionKodi}]
	if xbmc.getCondVisibility('system.platform.android') is True:
		items.append({'label': 'Android App', 'thumb': 'special://skin/resources/icons/android.png', 'function': chooseActionAndroidApp})	
	items.append({'label': 'Custom', 'thumb': 'special://skin/resources/icons/at.png'})				
	items.append({'label': 'No Action', 'thumb': 'special://skin/resources/icons/note.png', 'value': ''})			
	s = d.select('Choose Action Type', [buildListItem(item) for item in items], useDetails=True )
	if s == -1:
		return None
	item = items[s]
	if 'function' in item.keys():
		item['function'](obj)
	elif 'value' in item.keys():		
		obj['action'] = item['value']
	else:
		action = d.input('Edit Action', defaultt=obj['action'])
		if len(action) > 0:
			obj['action'] = action
	
	
def chooseIcon(obj):
	import xbmcgui, xbmcvfs, os
	d = xbmcgui.Dialog()
	pathSkinIcons = 'special://skin/resources/icons/'
	items = []
#	if obj['action'].startswith('StartAndroidActivity'):
#		app = obj['action'].split('(')[1].split(')')[0]		
#		items.append({'label': 'App Icon', 'thumb': 'androidapp://sources/apps/{}.png'.format(app), 'value': 'androidapp://sources/apps/{}.png'.format(app) })
#		try:
#			thumb = getGooglePlayDetails(app)['thumb']
#			items.append({'label': 'App Icon from Google Play', 'thumb': thumb, 'value': thumb})
#		except:
#			None
	for icon in os.listdir(xbmcvfs.translatePath(pathSkinIcons)):
		items.append({'label': icon.replace('.png',''), 'thumb': pathSkinIcons + icon, 'value': pathSkinIcons + icon})
	s = d.select('Choose widget type', [buildListItem(item) for item in items], useDetails=True, preselect=getPreselect(items, obj['thumb']) )
	if s >= 0:
		obj['thumb'] = items[s]['value']
		
			
def chooseLabel(obj):	
	import xbmcgui
	d = xbmcgui.Dialog()
	label = d.input('Choose label', defaultt=obj['label'])
	if len(label) > 0:
		obj['label'] = label
	

#def chooseWidgetSort(widget):
#	None


def chooseWidgetShape(widget):
	import xbmcgui
	d = xbmcgui.Dialog()
	items = [
		{'label': 'Square', 'thumb': 'special://skin/resources/icons/shapes.png', 'value': 'square'},
		{'label': 'Poster', 'thumb': 'special://skin/resources/icons/shapes.png', 'value': 'poster'},
		{'label': 'Landscape', 'thumb': 'special://skin/resources/icons/shapes.png', 'value': 'landscape'}
	]
	s = d.select('Widget Shape', [buildListItem(item) for item in items], useDetails=True )
	if s == -1:
		return
	widget['shape'] = items[s]['value']
	

def chooseWidgetSourcePlaylist(widget):
	import xbmcgui, xbmcvfs, os
	d = xbmcgui.Dialog()
	pathSkinPlaylists = 'special://skin/playlists/'
	items = []
	for pl in os.listdir(xbmcvfs.translatePath(pathSkinPlaylists)):
		items.append({'label': pl.replace('.xsp','').replace('_', ' ').title(), 'thumb': 'special://skin/resources/icons/playlist.png', 'value': pathSkinPlaylists + pl})
	s = d.select('Choose Smart Playlist', [buildListItem(item) for item in items], useDetails=True )
	if s >= 0:
		widget['source']=items[s]['value']		
#		widget['sortby'] = ''
#		widget['sortorder'] = ''
		widget['type'] = 'dynamic'		
		widget['content'] = ''
		widget['label'] = items[s]['label']		
		if 'items' in widget.keys():
			del widget['items']
			

def chooseWidgetSourcePVR(widget):	
	import xbmcgui, xbmc, json
	from lib import log, jsonrpc
	response = jsonrpc.execute('PVR.GetClients', {})	
	clients = response['result']['clients']
	d = xbmcgui.Dialog()
	items = [
		{'label': 'TV', 'thumb': 'special://skin/resources/icons/tv.png', 'type': 'tv'},
		{'label': 'Radio', 'thumb': 'special://skin/resources/icons/radio.png', 'type': 'radio'}
	]
	s = d.select('PVR Criteria', [buildListItem(item) for item in items], useDetails=True )
	if s == -1:
		return
	channelsType = items[s]
	params = {'channeltype': channelsType['type']}	
	response = jsonrpc.execute('PVR.GetChannelGroups', params)		
	items = []
	for group in response['result']['channelgroups']:
		items.append({
			'label': group['label'], 
			'thumb': 'special://skin/resources/icons/bars.png', 
			'value': 'pvr://channels/{}/{}'.format(channelsType['type'], group['label'].replace('All channels','*') ),
			'id': group['channelgroupid']
		})			
	s = d.select('PVR Criteria', [buildListItem(item) for item in items], useDetails=True )
	if s >= 0:				
		if items[s]['value'].endswith('*'):
			clientId = -1
		else:
			params = {"channelgroupid" : items[s]['id'], 'properties': ['clientid'] }
			response = jsonrpc.execute('PVR.GetChannels', params)
			clientId = response['result']['channels'][0]['clientid']								
		widget['source'] = items[s]['value'] + '@' + str(clientId)
#		widget['sortby'] = 'lastplayed'
#		widget['sortorder'] = 'descending'
		widget['type'] = 'dynamic'		
		widget['content'] = ''
		widget['label'] = channelsType['label'] + ': ' + items[s]['label']
		if 'items' in widget.keys():
			del widget['items']
	


def chooseWidgetSourceMedia(widget):
	import xbmcgui, xbmc, json
	from lib import log, jsonrpc
	d = xbmcgui.Dialog()
	items = [
		{'label': 'Video', 'thumb': 'special://skin/resources/icons/video.png', 'type': 'video', 'content': 'videos'},
		{'label': 'Music', 'thumb': 'special://skin/resources/icons/music.png', 'type': 'music', 'content': 'music'},
		{'label': 'Picture', 'thumb': 'special://skin/resources/icons/image.png', 'type': 'pictures', 'content': 'pictures'}				
	]
	s = d.select('Choose Media type', [buildListItem(item) for item in items], useDetails=True )
	if s == -1:
		return None
	mediaType = items[s]
	params = {'media': mediaType['type']}
	response = jsonrpc.execute('Files.GetSources', params)
	items = [{'label': 'All {} Sources'.format(mediaType['label']), 'thumb': mediaType['thumb'], 'path': 'sources://{}/'.format(mediaType['type']) }]
	for r in response.get('result',{}).get('sources',[]):
		if r['label'].endswith('add-ons'):
			continue
		items.append({'label': r['label'], 'thumb': mediaType['thumb'], 'path': r['file']})						

	s = d.select('Choose Media Source', [buildListItem(item) for item in items], useDetails=True )
	if s == -1:
		return None	
	item = items[s]
	widget['label'] = item['label']
	widget['source'] = item['path']
	widget['content'] = mediaType['content']
#	widget['sortby'] = ''
#	widget['sortorder'] = ''
	widget['type'] = 'dynamic'
	if 'items' in widget.keys():
		del widget['items']		


def browseDirectory(paths, title='Choose directory'):	
	import xbmc, json, xbmcgui
	from lib import log, jsonrpc					
	d = xbmcgui.Dialog()	
	params = {'properties': ['file','art'], 'directory': paths[-1]['path'], 'media': 'files'}	
	response = jsonrpc.execute('Files.GetDirectory', params)	
	items = [{'label': '[B]Choose This Directory[/B] [{}]'.format(paths[-1]['label']), 'thumb': 'special://skin/resources/icons/check.png'}]
	if len(paths)>1:
		items.append({'label': '[B]Go Back[/B]', 'thumb': 'special://skin/resources/icons/rotate-left.png'})	
	for r in response.get('result',{}).get('files',[]):
		if r['filetype'] != 'directory':
			continue		
		art = r.get('art',{})
		items.append({'label': r['label'], 'thumb': art.get('thumb', art.get('poster', 'special://skin/resources/icons/addon.png')), 'path': r['file']})						
	s = d.select(title, [buildListItem(item) for item in items], useDetails=True )
	if s == -1:
		return None	
	item = items[s]
	if item['label'].startswith('[B]Choose This Directory[/B]'):				
		return paths[-1]
	elif item['label']=='[B]Go Back[/B]':
		return browseDirectory(paths[:-1])
	else:
		return browseDirectory(paths + [item])



def chooseWidgetSourceAddon(widget):
	import xbmcgui, xbmc, json
	from lib import log
	d = xbmcgui.Dialog()
	items = [
		{'label': 'Video', 'thumb': 'special://skin/resources/icons/video.png', 'type': 'video', 'content': 'videos'},
		{'label': 'Music', 'thumb': 'special://skin/resources/icons/music.png', 'type': 'audio', 'content': 'music'},
		{'label': 'Picture', 'thumb': 'special://skin/resources/icons/image.png', 'type': 'image', 'content': 'pictures'},	
		{'label': 'Game', 'thumb': 'special://skin/resources/icons/game.png', 'type': 'game', 'content': 'games'},			
		{'label': 'Program', 'thumb': 'special://skin/resources/icons/cubes.png', 'type': 'executable', 'content': 'programs'}	
	]
	s = d.select('Choose Addon type', [buildListItem(item) for item in items], useDetails=True )
	if s == -1:
		return None
	addonType = items[s]
	root = {'path': 'addons://sources/{}/'.format(addonType['type']), 'thumb': addonType['thumb'], 'label': '{} Addons'.format(addonType['label'])}
	directory = browseDirectory([root],'Choose Addon directory')
	if directory is None:
		return
	widget['label'] = directory['label']
	widget['source'] = directory['path']
	widget['content'] = addonType['content']
#	widget['sortby'] = ''
#	widget['sortorder'] = ''
	widget['type'] = 'dynamic'
	if 'items' in widget.keys():
		del widget['items']		
		
	
	
def chooseWidgetSource(widget):
	import xbmcgui	
	d = xbmcgui.Dialog()	
	items = [
		{'label': 'Addon', 'thumb': 'special://skin/resources/icons/addon.png', 'function': chooseWidgetSourceAddon},
		{'label': 'Media Source', 'thumb': 'special://skin/resources/icons/database.png', 'function': chooseWidgetSourceMedia},
		{'label': 'PVR', 'thumb': 'special://skin/resources/icons/antenna.png', 'function': chooseWidgetSourcePVR},		
		{'label': 'Smart Playlist', 'thumb': 'special://skin/resources/icons/playlist.png', 'function': chooseWidgetSourcePlaylist},		
		{'label': 'Favorites', 'thumb': 'special://skin/resources/icons/star.png', 'value': 'favourites://'},
		{'label': 'Android Apps', 'thumb': 'special://skin/resources/icons/android.png', 'value': 'androidapp://sources/apps/'},
		{'label': 'Items List', 'thumb': 'special://skin/resources/icons/bars.png', 'value': ''}		
	]	
	s = d.select('Choose Source Type', [buildListItem(item) for item in items], useDetails=True )
	if s == -1:
		return None
	item = items[s]
	if 'function' in item.keys():
		item['function'](widget)
	elif 'value' in item.keys():		
		widget['source'] = item['value']
		if len(widget['source'])==0:
			widget['type'] = 'static'
			if 'items' not in widget.keys():
				widget['items'] = []
		else:
			widget['type'] = 'dynamic'
			if 'items' in widget.keys():
				del widget['items']
#		widget['sortby'] = ''
#		widget['sortorder'] = ''
		widget['content'] = ''
		widget['label'] = item['label']
	

def chooseWidgetType(widget):
	if not widget.get('source','').startswith('plugin://'):
		return
	import xbmcgui	
	d = xbmcgui.Dialog()
	items = [
		{'label': 'Dynamic', 'thumb': '', 'value': 'dynamic'},
		{'label': 'Static', 'thumb': '', 'value': 'static'}		
	]	
	s = d.select('Choose Widget Type', [buildListItem(item) for item in items], useDetails=True, preselect=getPreselect(items, widget['type']) )
	if s > 0:
		widget['type'] = items[s]['value']				

		
def getPreselect(items, current):
	i=0
	for item in items:
		if item['value']==current:
			return i
		i=i+1	
	return -1	
	
	
import xbmcgui

class GUI(xbmcgui.WindowXMLDialog):
	def __init__(self, *args, **kwargs):        
		self.data = 'test'
	
def my_window():
	ui = GUI('FileBrowser.xml', 'bla', 'default', '1080i')
	ui.doModal()
	del ui	