def getSettings():
	import xbmcvfs
	from bs4 import BeautifulSoup
	path = xbmcvfs.translatePath('special://masterprofile/advancedsettings.xml')
	if not xbmcvfs.exists(path):
		return BeautifulSoup('<advancedsettings></advancedsettings>', "html.parser")
	else:
		with open(path, 'r') as f:
			return BeautifulSoup(f.read(), "html.parser")	
	
	
def setSettings(settings):
	import xbmcvfs, xbmc
	path = xbmcvfs.translatePath('special://masterprofile/advancedsettings.xml')
	with open(path, 'w') as f:
		f.write(str(settings))
#		f.write(settings.prettify())
	xbmc.executebuiltin('Notification({}, {}, {}, {})'.format('Updated Settings', 'Restarting Kodi to apply', 3, 'special://skin/resources/kodi.png'))
	xbmc.sleep(1000)
	xbmc.executebuiltin('ShutDown')