
def log(msg, level=None):
	import xbmc
	if level is None:
		level = xbmc.LOGINFO
	xbmc.log(msg, level=level)
	
def get():
	import xbmcvfs, json
	from datetime import datetime
	path = xbmcvfs.translatePath('special://logpath') + 'kodi.log'
	with open(xbmcvfs.translatePath('special://logpath') + 'kodi.log', 'r') as f:
		data = f.read()
	result = []
	pos = 0
	for line in data.split("\n"):
		try:
			if line.startswith(' '):
				raise
			sdate = line.split(' ')[0].replace('\u00ef\u00bb\u00bf','')
			stime = line.split(' ')[1].replace('\u00ef\u00bb\u00bf','')			
			pos = findPos(line, ':', 3) + 2
			message = line[pos:]			
			pos2 = findPos(line, ' ', 2) 			
			type = line[pos2:pos].lstrip().split(' ')[0]
			result.append({'ts': sdate + ' ' + stime, 'message': message, 'pos': pos, 'type': type})
		except:
			last = result[-1]
			last['message'] = last['message'] + "\n" + line[pos:]			
	return result


def findPos(haystack, needle, n):
	parts= haystack.split(needle, n+1)
	if len(parts)<=n+1:
		return -1
	return len(haystack)-len(parts[-1])-len(needle)
	
	
def getLogLevel(settings):
	tag = settings.find('loglevel')
	if tag is None:
		return 0
	try:
		level = int(tag.get_text())
		if level > 1:
			level = 1
		return level
	except:
		return 0
	
	
def setLogLevel(settings, level):
	from bs4 import BeautifulSoup
	tag = settings.find('loglevel')
	if tag is not None:
		tag.extract()
	data = '<loglevel hide="true">{}</loglevel>'.format(level)
	settings.find('advancedsettings').append(BeautifulSoup(data, "html.parser"))
