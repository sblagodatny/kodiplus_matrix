def getSettingValue(setting):
	import xbmc, json	
	request = {'jsonrpc': '2.0', 'method': 'Settings.GetSettingValue', 'params': {'setting': setting}, 'id': 1}
	response = xbmc.executeJSONRPC(json.dumps(request))	
	return str(json.loads(response)['result']['value']).lower()	
	
	
def setSettingValue(setting, value):
	import xbmc, json	
	request = {'jsonrpc': '2.0', 'method': 'Settings.SetSettingValue', 'params': {'setting': setting, 'value': eval(value.capitalize()) }, 'id': 1}
	xbmc.executeJSONRPC(json.dumps(request))	