# coding: utf-8

import sys, json
import xbmc, xbmcgui, xbmcplugin, xbmcaddon, xbmcvfs
import urllib.parse as parse
from helpers import storage, string, gui, player


_baseUrl = sys.argv[0]
_handleId = int(sys.argv[1])
_params = dict(parse.parse_qsl(sys.argv[2][1:]))	
if 'handler' not in _params.keys():
	_params = {'handler': 'Root'}
	
_addon = xbmcaddon.Addon()
_pathAddon = _addon.getAddonInfo('path')
_pathSettings = _pathAddon.replace('addons','userdata/addon_data')
_pathImg = _pathAddon + '/resources/img'

_karaokeString = {'en': 'Karaoke', 'ru': 'Караоке', 'iw': 'קריוקי'}	

if not xbmcvfs.exists(_pathSettings):
	xbmcvfs.mkdir(_pathSettings)

_pathAccount = _pathSettings.replace('plugin.audio.ytubemusic', 'plugin.video.ytube') + 'account.json'


def handlerPlay():
	from youtube import music, web, common
	from helpers import string, storage
	from lyrics import musixmatch
	track = json.loads(string.b64decode(_params['track']))						
	item = xbmcgui.ListItem()
	video = web.getVideo(track['id'])
	server = None
	
	stream = common.getAudioStream(video)	
	item.setArt({'fanart': track['thumb']})
	item.setPath(stream)		
	
	lyrics = {
		'unsynced': None, 
		'synced': None,
		'label': '{}'.format(track['title'])
	}
	if track['type']=='song':
		lyrics = {
			'unsynced': music.getLyrics(track), 
			'synced': None,
			'label': '{} - {}'.format(track['title'], track['artist']['name'])
		}
		_lyrics = musixmatch.get(track['title'], track['artist']['name'], synced=True, unsynced=lyrics['unsynced'] is None)
		lyrics['synced'] = _lyrics['synced']
		if lyrics['unsynced'] is None:
			lyrics['unsynced'] = _lyrics['unsynced']				
	
	value = string.b64encode(json.dumps(lyrics))
	xbmc.executebuiltin('SetProperty(lyrics,{},12006)'.format(value))


#	elif track['type']=='video':	
#		import http.server as http_server
#		from helpers import http
#		import threading
#		manifest = common.getDashManifest(video, None)					
#		server = http_server.HTTPServer(('localhost', int(_addon.getSetting('dashProxyPort'))) , http.mpdProxyHandler)											
#		server.manifest = manifest				
#		thread = threading.Thread(target = server.serve_forever)
#		thread.deamon = True
#		thread.start()	
#		item.setProperty('inputstream', 'inputstream.adaptive')
#		item.setProperty('inputstream.adaptive.manifest_type', 'mpd')
#		item.setMimeType('application/dash+xml')	
#		item.setPath('http://{}:{}'.format('localhost', _addon.getSetting('dashProxyPort')))	
		
		
	xbmcplugin.setResolvedUrl(handle=_handleId, succeeded=True, listitem=item) 

	
#	if server is not None:
#		xbmc.sleep(1500)
#		server.shutdown()
#		xbmc.sleep(1000)




def builtTrackListItem(t):
	from youtube import music
	if t['type'] == 'video':
		item=buildListItem({'label': t['title'], 'thumb': t['thumb']})			
	elif t['type'] == 'song':
		item=buildListItem({'label': '{} - {}'.format(t['title'], t['artist']['name']), 'thumb': t['thumb']})			
	item.setPath(_baseUrl+'?' + parse.urlencode({'handler': 'Play', 'track': string.b64encode(json.dumps(t)) }))		
	item.setIsFolder(False)				
	item.setProperty("IsPlayable", 'true')				
	if t['type']=='song':
		info = item.getMusicInfoTag()
		info.setMediaType('song')					
		info.setArtist(t['artist']['name'])	
	elif t['type']=='video':
		info = item.getVideoInfoTag()
		info.setMediaType('musicvideo')		
	info.setTitle(t['title'])
	info.setDuration(t['duration'])
	return item
	

def listTracks(tracks):	
	from helpers import account
	from youtube import common
	xbmcplugin.setContent(_handleId, 'songs')		
	items=[]	
	acc = account.get(_pathAccount)
		
	for t in tracks:						
		item = builtTrackListItem(t)
		contextMenuItems=[]
		
#		cparams = {'handler': 'AutoPlaylist', 'track': string.b64encode(json.dumps(t)) }
#		contextMenuItems.append(('Auto Playlist', 'RunPlugin(' + _baseUrl+'?' + parse.urlencode(cparams) + ')'))

		cparams = {'handler': 'Similar', 'track': string.b64encode(json.dumps(t)) }
		contextMenuItems.append(('Similar', 'ActivateWindow(Music, {}, return)'.format(_baseUrl+'?' + parse.urlencode(cparams))))


		
		if acc is not None:
			cparams = {'handler': 'ItemPlaylists', 'track': string.b64encode(json.dumps(t)), 'account': string.b64encode(json.dumps(acc))}
			contextMenuItems.append(('Playlists', 'RunPlugin(' + _baseUrl+'?' + parse.urlencode(cparams) + ')'))	
		
		cparams = {'handler': 'Search', 'category': 'video', 'text': '{} {} {}'.format(t['title'], '' if t['type']=='video' else t['artist']['name'], _karaokeString[common.getLanguage(t['title'])] )}
		contextMenuItems.append(('Karaoke YouTube', 'ActivateWindow(videos,plugin://plugin.video.ytube/?' + parse.urlencode(cparams) + ',return)'))
		
		cparams = {'handler': 'Search', 'text': '{} {}'.format(t['title'], '' if t['type']=='video' else t['artist']['name'] )}
		contextMenuItems.append(('Karaoke XMinus', 'ActivateWindow(music,plugin://plugin.audio.xminus/?' + parse.urlencode(cparams) + ',return)'))
		
		item.addContextMenuItems(contextMenuItems, replaceItems=False)		
		items.append((item.getPath(), item, item.isFolder(),))		
	xbmcplugin.addDirectoryItems(handle=_handleId, items=items, totalItems=len(items))	
	xbmcplugin.endOfDirectory(_handleId)
	

def listPlaylists(playlists):	
	items=[]	
	for p in playlists:						
		item = buildListItem({'label': p['title'], 'thumb': p['thumb']})
		item.setPath(_baseUrl+'?' + parse.urlencode({'handler': 'ListPlaylist', 'playlist': string.b64encode(json.dumps(p)) }))		
		item.setIsFolder(True)	
		info = item.getMusicInfoTag()		
		info.setComment('Created By: ' + ('Me' if 'channel' not in p.keys() else p['channel']['title']) )	
		items.append((item.getPath(), item, item.isFolder(),))		
	xbmcplugin.addDirectoryItems(handle=_handleId, items=items, totalItems=len(items))	
	xbmcplugin.endOfDirectory(_handleId)


def handlerListPlaylist():
	from youtube import music
	playlist = json.loads(string.b64decode(_params['playlist']))
	data = music.getPlaylistItems(playlist['id'])
	import random
	random.shuffle(data)
	listTracks(data)
	

def handlerAutoPlaylist():
	from youtube import music
	import xbmc
	track = json.loads(string.b64decode(_params['track']))				
	content = [track] + music.getNextPlayItems(track)
	playlist = xbmc.PlayList(xbmc.PLAYLIST_MUSIC)
	playlist.clear()
	for t in content:	
		item = builtTrackListItem(t)
		playlist.add(url=item.getPath(), listitem=item)
	xbmc.Player().play(playlist)



def handlerSimilar():
	from youtube import music
	import xbmc
	track = json.loads(string.b64decode(_params['track']))				
	content = [track] + music.getNextPlayItems(track)
	listTracks(content)


def handlerItemPlaylists():	
	from youtube import web, music
	from helpers import string
	import json, xbmc, xbmcgui
	track = json.loads(string.b64decode(_params['track']))						
	acc = json.loads(string.b64decode(_params['account']))						
	
	playlists = music.getItemPlaylists(acc, track['id'])
	playlistsSelected = xbmcgui.Dialog().multiselect(
		heading='Playlists', 
		options=[buildListItem({'label': p['title'], 'thumb': p['thumb']}) for p in playlists], 
		preselect=[playlists.index(p) for p in playlists if p['itemExists'] is True] , 
		useDetails=True
	)
	
	if playlistsSelected is None:
		return
	changed = False                        
	for i in range(len(playlists)):
		pl=playlists[i]
		if pl['itemExists'] is True and i not in playlistsSelected:
			web.setItemPlaylist(acc, track['id'], pl['id'], 'remove')
			changed = True
		if pl['itemExists'] is False and i in playlistsSelected:
			web.setItemPlaylist(acc, track['id'], pl['id'], 'add')
			changed = True
	if changed is True:
		xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%('YouTube Music', 'Playlists updated', 1, _addon.getAddonInfo('icon')))


def handlerLibrary():
	from youtube import music
	from helpers import account
	import xbmcgui, xbmcplugin
	acc = account.get(_pathAccount)
	if acc is None:
		xbmcgui.Dialog().ok('YouTube Music', 'Account not set')
		xbmcplugin.endOfDirectory(_handleId)
		return
	data = music.getAccountPlaylists(acc)	
	listPlaylists(data)




def handlerSearch():	
	from youtube import music
	data = music.search(_params['text'], _params['category'])	
	if _params['category'] in ['song','video']:
		listTracks(data)
	elif _params['category']=='playlist':
		listPlaylists(data)
	

def handlerSearchInput():
	d = xbmcgui.Dialog()	
	items = [
		{'label': 'Songs', 'thumb': _pathImg + '/song.png', 'category': 'song'},
		{'label': 'Videos', 'thumb': _pathImg + '/video.png', 'category': 'video'},
		{'label': 'Playlists', 'thumb': _pathImg + '/list.png', 'category': 'playlist'}
	]	
	s = d.select('YouTube Music Search', [buildListItem(item, False) for item in items], useDetails=True)
	if s == -1:
		return
	item = items[s]
	text=gui.inputWithHistory('YouTube Music - Search {}'.format(item['label']), _pathSettings + 'history_search_{}'.format(item['label']))	
	if text is None:
		return	
	params = {
		'handler': 'Search',		
		'category': item['category'],
		'text': text		
	}	
	xbmcplugin.endOfDirectory(_handleId)
	cmd = 'ActivateWindow(Music, {}, return)'.format(_baseUrl+'?' + parse.urlencode(params))
	xbmc.executebuiltin(cmd)


def buildListItem(data, addNewLine=False):	
	li = xbmcgui.ListItem(data['label'] if addNewLine is False else "\n" + data['label'])
	li.setArt({'thumb': data['thumb']})
	if 'fanart' in data.keys():
		li.setArt({'fanart': data['fanart']})
	return li

		
def handlerRoot():		
	items=[		
		{'label': 'Search', 'path': _baseUrl +'?' + parse.urlencode({'handler': 'SearchInput'}), 'thumb': _pathImg + '/search.png', 'fanart': _pathAddon + '/fanart.jpg', 'folder': False},
		{'label': 'Library', 'path': _baseUrl +'?' + parse.urlencode({'handler': 'Library'}), 'thumb': _pathImg + '/list.png', 'fanart': _pathAddon + '/fanart.jpg', 'folder': True},		
	]
	for item in items:
		xbmcplugin.addDirectoryItem(_handleId, item['path'], buildListItem(item), item['folder'])
	xbmcplugin.endOfDirectory(_handleId)

globals()['handler' + _params['handler']]()
