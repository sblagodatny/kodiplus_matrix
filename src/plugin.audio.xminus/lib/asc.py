# coding: utf-8


from bs4 import BeautifulSoup
import requests
import xbmcaddon
import time
import os
import xbmc


import json
from helpers import storage, http

_addon = xbmcaddon.Addon()
_pathAddon = _addon.getAddonInfo('path')
_pathSettings = _pathAddon.replace('addons','userdata/addon_data')	
_mp3File = _pathSettings + 'asc.mp3'

_pathCache = _pathSettings + '/cache.asc/'


_url = 'https://www.audiospeedchanger.com/'
_title = 'AudioSpeedChanger'

if not os.path.exists(_pathSettings):
	os.mkdir(_pathSettings)

	
def submit(url, pitch, cacheKey):
	data = storage.getCache(cacheKey, _pathCache)
	if data is not None:
		return data	
	session = http.initSession()
	if 'googlevideo.com/videoplayback' in url:
		from youtube import download
		download.download(url, _mp3File)
	else:	
		import shutil
		r = session.get(url, stream=True)
		with open(_mp3File, 'wb') as f:
			shutil.copyfileobj(r.raw, f)	
	with open(_mp3File, 'rb') as f:
		params = {'a': 'submit', 't': '0.6792909175692878'}
#		files = {'remotefile': (None,url), 'pitch': (None,pitch), 'tempo': (None,100), 'audioformat': (None,1), 'audiomethod': (None,1)}	
		files = {'localfile': ('asc.mp3',f), 'pitch': (None,pitch), 'tempo': (None,100), 'audioformat': (None,1), 'audiomethod': (None,1)}					
		reply = session.post(_url, params=params, files=files).json()		
	if 'error' in reply.keys():
		raise Exception (reply['error'])		
	storage.setCache(cacheKey, reply, _pathCache, maxAgeSeconds = 60*60*6)	
	return reply

	
def getState(job):
	session = http.initSession()
	params = {'a': 'state', 't': '0.14226431604274925'}
	files = {'job': (None,job['job_id'])}
	reply = session.post(_url, params=params, files=files).json()		
	if 'error' in reply.keys():
		raise Exception (reply['error'])
	return reply
	
	
def getUrl(job):
	session = http.initSession()
	params = {'job': job['job_id']}
	data = BeautifulSoup(session.get(_url, params=params).content, "html.parser").find('a', class_='dropbox-saver')
	if data is None:
		raise Exception ('Unable to get url')
	return data['href']
	

def transpose(url, pitch, cacheKey):
	job = submit(url, int(pitch), cacheKey + '.' + str(pitch))
	state = 1
	while state != 4:
		if xbmc.Monitor().abortRequested():
			return None
		state = getState(job)['job_status']
		xbmc.sleep(1000)
	return getUrl(job)
