# coding: utf-8


from bs4 import BeautifulSoup
import requests
import xbmcaddon
import os
import xbmcgui
import json
import urllib.parse as parse
import json, re
import time
import xbmc

from helpers import http, storage, gui


_addon = xbmcaddon.Addon('plugin.audio.xminus')
_pathAddon = _addon.getAddonInfo('path')
_pathSettings = _pathAddon.replace('addons','userdata/addon_data')
_account = _pathSettings + '/account.json'
_pathAICache = _pathSettings + '/cache.ai/'

_url = 'https://xminus.me'
_title = 'X-Minus'

_timeout=4


def parseTracks(data):
	result = []	
	tag = data.find('ol', class_='tracklist')
	if tag is None:
		return result
	for tag in tag.find_all('li'):		
		description = tag.find('b').get_text()
		if 'demo' in description or 'демо' in description:
			continue
		title = tag.find('a', class_="tracklist-tit").get_text()
		try:
			titleDigit = title.split(' ')[-1]
			i = int(titleDigit)
			title = title.replace(' ' + titleDigit,'')
		except:
			None	
		if '(' in title:
			description = description + "\n" + title.split('(')[1].split(')')[0].lstrip().rstrip()
			title = title.split('(')[0].rstrip()
		artist = tag.find('a', class_="tracklist-artist").get_text()
		artist = artist.replace('&',',')
		if artist.endswith(', The'):
			artist = 'The ' + artist.replace(', The','')
		if ',' in artist:
			description = description + "\n" + 'feat ' + artist.split(',')[1].lstrip()
			artist=artist.split(',')[0].rstrip()		
		result.append({
			'id': tag['id'],		
			'title': title,		
			'artist': artist,
			'description': description,
			'slug': tag.find('a')['href'].split('/')[-1],
			'source': 'xminus'
		})
	return result	

	
def search (text, account=None):					
	cookies = {} if account is None else account['cookies']	
	session = http.initSession()
	params = {'q': text, 'o': '0' }		
	cookies.update({'xm_locale': 'ru_RU','xm_vc_ai_visited': '1'})		
	data = BeautifulSoup(session.get(_url + '/search', params=params, cookies=cookies, timeout=_timeout).content, "html.parser")			
	return parseTracks(data)


def t668(trackId, trackK, playerK):	
	trackId = int(trackId[1:])
	c = 0;
	for e in range(0, len(playerK)):
		c = c + ord(playerK[e])	
	c = c + 5 + trackId + 999 
	return '{}zyxwz{}.9z{}z'.format(hex(c).replace('0x',''), trackId, trackK)


def getTrack(track, account=None):	
	cookies = None if account is None else account['cookies']	
	session = http.initSession()
	url = _url + '/track/' + track['id'].replace('m','') + '/' + track['slug']	
	data = BeautifulSoup(session.get(url, cookies=cookies, timeout=_timeout).content, "html.parser")	
	lyrics = data.find('div', class_='tab-lyrics').get_text()
	if 'Lyrics added' in lyrics:
		lyrics = lyrics.split('Lyrics added')[0]
	track['lyricsUnsynced'] = lyrics.lstrip().rstrip()
	trackK = data.find('div',class_='player')['data-k']
	playerK = data.find('span', {'id': 'player-data'})['data-k']
	params = {
		't668': t668(track['id'],trackK, playerK),
		'trackname': track['slug']
	}			
	track['stream'] = 'https://m9.xmst.cc/dl/minus/' + track['id'][1:] + '?' + parse.urlencode(params)
	try:
		tag = data.find('ol', {'id': 'plus-tracks'})
		params = {
			'mini': '1',
			'qhsec': tag['data-qhsec'],
			'lim': '2',
			'a': tag['data-ar'],
			't': tag['data-tit'],
			'_': int(time.time()*1000)
		}		
		dataPlus = session.get('https://p9.xmstatic.org/search_ajax3', params=params, timeout=_timeout).json()
		params = {
			'vid': dataPlus['r']['tracks']['1']['vid'],
			't': tag['data-t'],
			'qh': dataPlus['r']['qh']
		}
		track['streamPlus'] = 'https://p9.xmstatic.org/dl?' + parse.urlencode(params)
		track['idPlus'] = dataPlus['r']['tracks']['1']['vid']
	except:
		None
	try:	
		track['youtubeIdPlus'] = data.find('div', {'id': 'plus-video'}).find('iframe')['data-src'].split('/')[-1]
	except:
		None	

	

def getPlaylistItems(playlistId, account):	
	session = http.initSession()
	data = BeautifulSoup(session.get(_url + '/playlist/' + account['userId'] + '/' + playlistId, cookies=account['cookies'], timeout=_timeout).text, "html.parser")		
	return parseTracks(data)

	
def getPlaylists(account):	
	session = http.initSession()	
	data = {'action': 'GetUserPlaylists'}
	headers = {'X-Requested-With': 'XMLHttpRequest'}
	data = BeautifulSoup(session.post(_url + '/extra_html', data=data, headers=headers, cookies=account['cookies'], timeout=_timeout).text, "html.parser")
	result = []
	for tag in data.find('ul', class_='playlists-mini').find_all('li'):
		result.append({
			'title': tag.get_text().lstrip().rstrip(), 
			'id': tag['data-plid']
		})
	return result	
	

def playlistItemAdd(trackId, playlistId, account):	
	session = http.initSession()	
	data = {
		'action': 'AddTrackToUserPlaylist',
		'minus_id': trackId,
		'playlist_id': playlistId
	}
	headers = {'X-Requested-With': 'XMLHttpRequest' }
	data = session.post(_url + '/user-management', data=data, headers=headers, cookies=account['cookies'], timeout=_timeout).text


def playlistItemRemove(trackId, playlistId, account):	
	session = http.initSession()	
	data = {
		'action': 'RemoveTrackFromUserPlaylist',
		'minus_id': trackId,
		'playlist_id': playlistId
	}
	headers = {'X-Requested-With': 'XMLHttpRequest'}
	session.post(_url + '/user-management', data=data, headers=headers, cookies=account['cookies'], timeout=_timeout)
	

def validateAccount(account):	
	if 'cookies' in account.keys():
		try:
			session = http.initSession()
			data = BeautifulSoup(session.get(_url, cookies=account['cookies'], timeout=_timeout).content, "html.parser")	
			token = data.find('a', {'id': 'logout'})['data-tkn']			
			session.post(_url + '/auth/logout', data={'token': token}, headers={'X-Requested-With': 'XMLHttpRequest'}, cookies=account['cookies'], timeout=_timeout)
		except:
			None		
	session = http.initSession()	
	data = BeautifulSoup(session.get(_url + '/login', timeout=_timeout).content, "html.parser")	
	cookies = session.cookies.get_dict()
	token = data.find('div',{'data-url': '/auth/login'}).find('input', {'name': 'token'})['value']		
	data = {	
		'token': token,
		'login-form-host': _url.split('/')[-1],
		'email': account['username'],
		'password': account['password'],
		'remember_password': 'on'
	}	
	reply = session.post(_url + '/auth/login', data=data, headers={'X-Requested-With': 'XMLHttpRequest'}, cookies=cookies, timeout=_timeout).json()
	if reply['status'] == 'error':
		raise Exception(reply['message'])			
	cookies = session.cookies.get_dict()
	tag = BeautifulSoup(session.get(_url, cookies=cookies).content, "html.parser")
	account['title'] = tag.find('b', class_='drop-menu-title').get_text().lstrip().rstrip()
	account['userId'] = tag.find('a', class_='tmenu-link')['href'].split('/')[-1]		
	tag = BeautifulSoup(session.get(_url + '/user/' + account['userId'], cookies=cookies).content, "html.parser").find('img', {'id': 'user-avatar-upload'})
	if tag is not None:
		account['thumb'] = _url + tag['src']
	else:
		account['thumb'] = ''
	account['cookies'] = cookies
	account['validDays'] = 2

	
def addTrack(track, account, type=0):	
	session = http.initSession()
	path = _pathSettings + 'tmp.mp3'
	headers = {'X-Requested-With': 'XMLHttpRequest'}
	data = session.get(_url + '/add', cookies=account['cookies'], timeout=_timeout).text
	token = re.search(r'\/upload\/catchMinusTrack\?token=(.*?)"', data).group(1)		
	session2 = http.initSession()
	f = session2.get(track['stream'], stream=True).raw
	files = {'myfile': ('tmp.mp3', f, 'audio/mpeg')}	
	data = session.post(_url + '/upload/catchMinusTrack', params={'token':token}, files=files, cookies=account['cookies'], headers={'Referer': _url + '/add', 'Connection': 'keep-alive'}, timeout=30 ).text						
	try:
		fileId = re.search(r'name="tmp_file_id" value="(.*?)"', data).group(1)		
	except:
		storage.log(data)
		raise Exception ('Unable to upload file')		
	data = {
			'artist': track['artist'],
			'title': track['title'],
			'type': type,
			'type2': '0',
			'video': 'https://www.youtube.com/watch?v={}'.format(track['youtubeIdPlus']),
			'notice': '',
			'lyrics': track['lyricsUnsynced'],
			'lyrics_transcription': '',
			'lyrics_rus': '',
			'lang': '0', ## 19: Hebrew
			'genre': '255',
			'theme': '0',
			'key': '',
			'tmp_file_id': fileId,
	}
	data = session.post(_url + '/upload/submitMinusTrack', cookies=account['cookies'], data=data, headers=headers, timeout=_timeout).text	
	data = json.loads(data)
	if data['status']=='error':
		raise Exception(data['message'])
	return data['url']
	
	
def submitJobAIKaraoke(plusStreamUrl, cacheKey, account):	
	import json
	from helpers import storage
	import time
	import xbmc
	data = storage.getCache(cacheKey, _pathAICache)
	if data is not None:
		return data	
	session = http.initSession()	
	data = session.get(_url + '/ai', cookies=account['cookies'], timeout=_timeout).text
	authKey = BeautifulSoup(data, "html.parser").find('input', {'id': 'vocal-cut-auth-key'})['value']	
	version = re.search(r"version: '(.*?)'", data).group(1)	
	uploadUrl = 'https:' + re.search(r"upload_host: '(.*?)'", data).group(1)					
	path = _pathSettings + 'tmp.mp3'	
	from youtube import download				
	download.download(plusStreamUrl, path)
	with open(path, 'rb') as f:		
		files = {
			'auth_key': (None, authKey),
			'locale': (None, 'ru_RU'),
			'separation': (None, 'inst_vocal'), ## inst: only instrumental track
			'format': (None, 'mp3'),
			'version': (None, version),
			'model': (None, 'mb2bv'),           ## hdemucs_mmi: remove all vocal
			'aggressiveness': (None, '2'),
			'show_setting_format': (None, '0'),
			'hostname': (None, _url.split('/')[-1]),
			'client_fp': (None, '-'),									
			'myfile': ('tmp.mp3', f, 'audio/mpeg')
		}			
		data = session.post(uploadUrl + '/upload/vocalCutAi?catch-file', files=files, headers={'Referer': _url + '/', 'Connection': 'keep-alive', 'Origin': _url}, timeout=30 ).json()
	data['uploadUrl'] = uploadUrl
	data['authKey'] = authKey	
	data['version'] = version
	data['ts'] = time.time()
	storage.setCache(cacheKey, data, _pathAICache, maxAgeSeconds = 60*60*6)	
	return data


def getJobAIKaraoke(job):					
	session = http.initSession()	
	files = {
			'job_id': (None, job['job_id']),
			'auth_key': (None, job['authKey']),
			'locale': (None, 'ru_RU')
	}
	data = session.post(job['uploadUrl'] + '/upload/vocalCutAi?check-job-status', files=files, headers={'Referer': _url + '/', 'Connection': 'keep-alive', 'Origin': _url}, timeout=30 ).json()
	if data['status'] != 'done':
		return data
	params = {
		'job-id': job['job_id'],
		'quality': 'normal',
		'stem': 'inst',
		'fmt': 'mp3',
		'similar-job-id': job.get('similar_job_id',''),
		'trackname': 'tmp.mp3'
	}
	data['url'] = session.get(job['uploadUrl'] + '/dl/vocalCutAi', params=params, allow_redirects=False, headers={'Referer': _url + '/', 'Connection': 'keep-alive'}, timeout=_timeout).url
	return data
	
	
	