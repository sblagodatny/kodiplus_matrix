# coding: utf-8


from bs4 import BeautifulSoup
import requests
import json
from helpers import http, string, storage
from helpers.fuzzywuzzy import fuzz

_url = 'https://www.karaoke.co.il/'
_title = 'KaraokeIL'
_timeout=4


def parseSongs(data):
	result = []	
	for tag in data.find_all('div', class_='result_box'):
		artistName = tag.find('span', class_='pink').get_text()
		item = {
			'id': string.b32encode(tag.find_all('a')[1]['href'].replace(_url,'')).decode('utf-8'),
			'title': tag.find_all('a')[1].get_text().replace(artistName,''),			
			'artist': artistName,
			'source': 'karaokeil'
		}
		item['artist'] = item['artist'].replace('&',',').replace('עם',',').replace('מארח את',',').replace('מארחת את',',')
		if ',' in item['artist']:
			item['description'] = 'feat ' + item['artist'].split(',')[1].lstrip()	
			item['artist']=item['artist'].split(',')[0].rstrip()	
		result.append(item)
	return result	


	
def search (text, account):
	session = http.initSession()
	data = BeautifulSoup(session.get(_url + 'searchresults.php', params={'Sstr': text}, timeout=_timeout).content, "html.parser")		
	result = []
	return parseSongs(data)
	
	

def getTrack(track, account):
	session = http.initSession()
	data = BeautifulSoup(session.get(_url + string.b32decode(track['id']), timeout=_timeout).text, "html.parser")		
	url = data.find('a', class_='song_clip_play')['href']
	content = session.get(url, timeout=_timeout).text	
	
	
	url = format(content.split('src="')[1].split('"')[0].split('?')[0])

	raise Exception('Unable to play. IFrame for capture: ' + 'https://www.karaoke.co.il/develop/videoplaybackframe.php?{}'.format(url))


	
#	headers={
#		'Sec-Fetch-Dest': 'iframe',
#		'Sec-Fetch-Mode': 'navigate',
#		'Sec-Fetch-Site': 'cross-site',
#		'Upgrade-Insecure-Requests': '1',
#		'Referer': 'https://www.karaoke.co.il/'
#	}
#	content = session.get(url, headers=headers).text

	
		

#	track['stream']='https://www.video-cdn.com/upload/karaoke/2015/01-15/1421346126297/video_720p.m3u8'
#	track['stream'] = url.replace('embed/iframe','video/show') + '?type=mp3'	
#	track['stream'] = 'https://www.video-cdn.com/video/encrypt/2177ebbf1f3b83643914d2056b79d40b/2177ebbf1f3b83643914d2056b79d40b/video_720p.m3u8?token=R915dD-869424d7-06af-4746-a2b5-7929735e157e'
	
	
#	try:
#		url = data.find('a',class_='lyrics')['href']
#		content = BeautifulSoup(session.get(url, timeout=_timeout).text, "html.parser")
#		lyrics = content.find('div', class_='lyrics_text').get_text()
#		track['lyricsUnsynced'] = lyrics.replace("\r","\n").replace("\n\n","\n").lstrip().rstrip()
#	except:
#		None

	