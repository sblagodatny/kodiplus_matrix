import importlib
import xbmcaddon
import json

from helpers import storage

_sources = ['xminus','karaokeil','sttw']


def getLib(source):
	if source not in _sources:
		raise Exception('Invalid source')	
	return importlib.import_module('.' + source, 'lib.karaoke')



def search(criteria, account=None):
	import concurrent.futures
	from helpers.fuzzywuzzy import fuzz
	results = {}
	def task(source):														
		try:
			results[source] = getLib(source).search(criteria, account)						
		except Exception as e:
			storage.log('Search ' + source + ': ' + str(e))
	ex = concurrent.futures.ThreadPoolExecutor(max_workers=10)		
	for source in _sources:		
		ex.submit(task, source)		
	ex.shutdown(wait=True)	
	content = []
	for source, scontent in results.items():
		for item in scontent:
			item['description'] = item.get('description','') + "\n" + source								
			content.append(item)
	return content
	