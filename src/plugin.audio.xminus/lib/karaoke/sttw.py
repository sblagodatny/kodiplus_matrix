# coding: utf-8


from bs4 import BeautifulSoup
import requests
import xbmcaddon
import datetime
import os
import xbmcgui
import re
import json
import random

from helpers import http, string
from helpers.fuzzywuzzy import fuzz


_url = 'https://www.singtotheworld.com'
_title = 'Sing To The World'





def parseSongs(data):
	result = []	
	for tag in data.find_all('div', class_='track'):
		item = {
			'id': tag['data-t'],		
			'title': tag.find('span', class_='tracktitle').get_text(),
			'artist': tag.find('span', class_='trackartist').find('a').get_text(),
			'source': 'sttw'			
		}
		item['artist'] = item['artist'].replace('feat',',').replace('&',',')
		if ',' in item['artist']:
			item['description'] = 'feat ' + item['artist'].split(',')[1].lstrip()	
			item['artist']=item['artist'].split(',')[0].rstrip()	
		result.append(item)
	return result	
	

def search (text, account):	
	session = http.initSession()
	url = _url + '/custom-disc/search/' + text.replace(' ','-').replace("'",'')
	data = BeautifulSoup(session.get(url).content, "html.parser")
	return parseSongs(data)
	
	
	
def getTrack(track, account):
	session = http.initSession()		
	url = session.post(_url + '/api.aspx/GetSampleVideo', json = {'TrackGuid': track['id']}).json()
	track['stream'] = _url + url['d'].split('mp4')[0] + 'mp3'
	
