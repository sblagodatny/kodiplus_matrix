# coding: utf-8

import sys, json
import xbmc, xbmcgui, xbmcplugin, xbmcaddon, xbmcvfs
import urllib.parse as parse
from contextlib import closing
import importlib

from helpers import storage, string, gui, player
#from lib import karaoke, asc, musixmatch
#from lib.karaoke import xminus



_baseUrl = sys.argv[0]
_handleId = int(sys.argv[1])
_params = dict(parse.parse_qsl(sys.argv[2][1:]))	
if 'handler' not in _params.keys():
	_params = {'handler': 'Root'}
	
_addon = xbmcaddon.Addon()
_pathAddon = _addon.getAddonInfo('path')
_pathSettings = _pathAddon.replace('addons','userdata/addon_data')
_pathImg = _pathAddon + '/resources/img/'
_pathAccount = _pathSettings + 'accounts.json'


_title = 'X-Minus'

if not xbmcvfs.exists(_pathSettings):
	xbmcvfs.mkdir(_pathSettings)


def buildListItem(data):	
	li = xbmcgui.ListItem(data['label'])
	li.setArt({'thumb': data['thumb']})
	if 'fanart' in data.keys():
		li.setArt({'fanart': data['fanart']})
	return li
	

def setTranspose(track):
	keys = ['-3','-2','-1','0','+1','+2','+3']
	s = xbmcgui.Dialog().select('Set trasnpose key', keys, preselect=keys.index(track.get('transpose','0')) )
	if s < 0:	
		return		
	key = keys[s]	
	if key != track.get('transpose','0'):
		if key == '0':
			del track['transpose']			
		else:
			track['transpose']=key


def handlerPlay():
	import json	
	from youtube import common, web
	from lib import karaoke
	
	d = xbmcgui.Dialog()	
	track = json.loads(string.b64decode(_params['track']))		
	account = json.loads(string.b64decode(_params['account']))	

	### Play menu ###	
	while True:	
		items = [
			{'label': 'Karaoke', 'thumb': _pathImg + '/microphone.png'},
			{'label': 'Original', 'thumb': _pathImg + '/song.png'},
			{'label': 'Transpose{}'.format('' if 'transpose' not in track.keys() else ' [{}]'.format(track['transpose'])), 'thumb': _pathImg + '/arrows-rotate.png'},
		]
		s = d.select(track['title'] + ' - ' + track['artist'], [buildListItem(item) for item in items], useDetails=True)
		if s == -1:
			player.cancelResolvedUrl(_handleId)
			return
		if items[s]['label'].startswith('Transpose'):
			setTranspose(track)
		elif items[s]['label']=='Karaoke':
			streamType = 'karaoke'
			break
		elif items[s]['label']=='Original':
			streamType = 'original'
			break
	
	karaoke.getLib(track['source']).getTrack(track, account)
	getLyrics(track)
			
	### Set stream and apply transpose ###	
	if streamType=='original' and 'youtubeIdPlus' not in track.keys():			
		getYoutubeIdPlus(track)		
	stream = track['stream'] if streamType=='karaoke' else common.getAudioStream(web.getVideo(track['youtubeIdPlus']))
	if 'transpose' in track.keys():
		xbmc.executebuiltin('ActivateWindow(busydialognocancel)')
		try:
			id = track['id'] if streamType=='karaoke' else track['idPlus']									
			stream = asc.transpose(stream, track['transpose'], str(id))
			xbmc.executebuiltin('Dialog.Close(busydialognocancel,true)') 
		except Exception as e:						
			xbmc.executebuiltin('Dialog.Close(busydialognocancel,true)') 
			d.ok(_title, 'Unable to apply transpose: ' + str(e))
			player.cancelResolvedUrl(_handleId)
			return
	
	### Play ###
	item = xbmcgui.ListItem(offscreen=True)		
	item.setArt({'fanart': _pathAddon + '/fanart.jpg'})	
	item.setPath(stream)
	
	lyrics = {
		'unsynced': track.get('lyricsUnsynced',''), 
		'synced': track.get('lyricsSynced',None),
		'label': track['title'] + ' - ' + track['artist']
	}	
	value = string.b64encode(json.dumps(lyrics))
	xbmc.executebuiltin('SetProperty(lyrics,{},12006)'.format(value))	
	
	
	xbmcplugin.setResolvedUrl(handle=_handleId, succeeded=True, listitem=item) 
	xbmc.executebuiltin('ActivateWindow(visualisation)')
	xbmc.executebuiltin("Playlist.Clear")	
	


def getLyrics(track, unsyncedOnly=False):
	if track.get('lyricsUnsynced',None) is not None and track.get('lyricsSynced',None) is not None:
		return
	if track.get('lyricsUnsynced',None) is not None and unsyncedOnly is True:
		return	
	from lyrics import musixmatch
	lyrics = musixmatch.get(
		track['title'], track['artist'], 
		synced=track.get('lyricsSynced',None) is None and unsyncedOnly is False, 
		unsynced = track.get('lyricsUnsynced',None) is None
	)
	if lyrics['synced'] is not None:
		track['lyricsSynced'] = lyrics['synced']
	if lyrics['unsynced'] is not None:
		track['lyricsUnsynced'] = lyrics['unsynced']
					
	
	
			

def getYoutubeIdPlus(track):
	if 'youtubeIdPlus' in track.keys():
		return
	from youtube import music	
	track['youtubeIdPlus'] = music.search(track['title'] + ' ' + track['artist'])['id']	
	

def handlerPlaylistItemAdd(track=None, account=None):	
	from lib.karaoke import xminus
	from helpers import string
	import json, xbmcgui
	d = xbmcgui.Dialog()
	if track is None:
		track = json.loads(string.b64decode(_params['track']))	
	if account is None:
		account = json.loads(string.b64decode(_params['account']))	
	if track['source']=='ai':
		for key in ['title','artist']:
			if key not in track.keys() or track[key] is None or len(track[key])==0:
				d.ok(_title, 'Unable to add track: {} missing'.format(key.capitalize()))
				return False
		if 'stream' not in track.keys():
			d.ok(_title, 'Unable to add track: Karaoke stream missing')
			return False	
	elif track['source'] != 'xminus' and track['source'] != 'ai':		
		if not xbmcgui.Dialog().yesno(_title, 'This track does not exist in X-Minus yet.' + "\n" + 'Confirm upload to X-Minus and add to playlist ?'):
			return False		
	playlists = xminus.getPlaylists(account)
	s = d.select('Select playlist', [p['title'] for p in playlists])
	if s<0:
		return False
	if track['source']=='xminus':
		xminus.playlistItemAdd(track['id'], playlists[s]['id'], account)
		xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%('X-Minus', 'Added to playlist', 1, _addon.getAddonInfo('icon')))
		return True							
	xbmc.executebuiltin('ActivateWindow(busydialognocancel)')
	try:
		if 'stream' not in track.keys():
			karaoke.getLib(track['source']).getTrack(track, account)
		getLyrics(track, unsyncedOnly=True)
		getYoutubeIdPlus(track)
		url = xminus.addTrack(track, account, 21 if track['source']=='ai' else 0)	
		xminus.playlistItemAdd('m' + url.split('/')[2], playlists[s]['id'], account)
		xbmc.executebuiltin('Dialog.Close(busydialognocancel,true)') 
		xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%('X-Minus', 'Added to playlist', 1, _addon.getAddonInfo('icon')))
		return True
	except Exception as e:
		xbmc.executebuiltin('Dialog.Close(busydialognocancel,true)') 
		storage.log(str(e))
		xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%('X-Minus', 'Unable to add to playlist', 1, _addon.getAddonInfo('icon')))
		return False


def handlerPlaylistItemRemove():
	from lib.karaoke import xminus
	account = json.loads(string.b64decode(_params['account']))	
	xminus.playlistItemRemove(_params['trackId'], _params['playlistId'], account)
	xbmc.executebuiltin('Container.Refresh()')
	

def listTracks(tracks, account=None, playlistId=None):
	xbmcplugin.setContent(_handleId, 'songs')	
	items=[]	
	for t in tracks:				
		item=xbmcgui.ListItem(t['title'] + ' - ' + t['artist'], offscreen=True)						
		item.setPath(_baseUrl+'?' + parse.urlencode({'handler': 'Play', 'track': string.b64encode(json.dumps(t)), 'account': string.b64encode(json.dumps(account)) }))					
		item.setIsFolder(False)				
		item.setProperty("IsPlayable", 'true')				
		info = item.getMusicInfoTag()
		info.setMediaType('song')
		if 'description' in t.keys():
			info.setComment(t['description'])
		info.setTitle(t['title'])
		info.setArtist(t['artist'])	
		contextMenuItems=[]	
		if account is not None:
			cparams = {'handler': 'PlaylistItemAdd', 'track': string.b64encode(json.dumps(t)), 'account': string.b64encode(json.dumps(account)) }
			contextMenuItems.append(('Add to Playlist', 'RunPlugin(' + _baseUrl+'?' + parse.urlencode(cparams) + ')'))					
			if playlistId is not None:
				cparams = {'handler': 'PlaylistItemRemove', 'trackId': t['id'], 'playlistId': playlistId, 'account': string.b64encode(json.dumps(account))}
				contextMenuItems.append(('Remove from Playlist', 'RunPlugin(' + _baseUrl+'?' + parse.urlencode(cparams) + ')'))							
		item.addContextMenuItems(contextMenuItems, replaceItems=False)				
		items.append((item.getPath(), item, item.isFolder(),))		
	xbmcplugin.addDirectoryItems(handle=_handleId, items=items, totalItems=len(items))	
	xbmcplugin.endOfDirectory(_handleId)


def handlerListPlaylist():
	from lib.karaoke import xminus
	account = json.loads(string.b64decode(_params['account']))	
	listTracks(xminus.getPlaylistItems(_params['playlistId'], account), account, _params['playlistId'])


def handlerPlaylists():	
	from helpers import account
	from lib.karaoke import xminus
	acc = account.get(_pathAccount, xminus.validateAccount)
	if acc is None:
		xbmcgui.Dialog().ok(_title, 'Account not set')
		xbmcplugin.endOfDirectory(_handleId)
		return
	items=[]	
	for pl in xminus.getPlaylists(acc):				
		item=xbmcgui.ListItem(pl['title'], offscreen=True)		
		item.setPath(_baseUrl+'?' + parse.urlencode({'handler': 'ListPlaylist', 'playlistId': pl['id'], 'account': string.b64encode(json.dumps(acc))}))
		item.setIsFolder(True)				
		items.append((item.getPath(), item, item.isFolder(),))		
	xbmcplugin.addDirectoryItems(handle=_handleId, items=items, totalItems=len(items))	
	xbmcplugin.endOfDirectory(_handleId)


def handlerSearch():
	from lib.karaoke import xminus
	from lib import karaoke
	from helpers import account
	acc = account.get(_pathAccount, xminus.validateAccount)
	data = karaoke.search(_params['text'], acc)
	listTracks(data, acc)


def handlerSearchInput():
	text=gui.inputWithHistory('Search', _pathSettings + 'historySearch', 15)	
	if text is None:
		return			
	params = {
		'handler': 'Search',
		'text': text		
	}	
	xbmc.executebuiltin("Dialog.Close(all, true) ")
	cmd = 'ActivateWindow(Music, {}, return)'.format(_baseUrl+'?' + parse.urlencode(params))
	xbmc.executebuiltin(cmd)
	
	
		
def createPlayTrack(track, streamKey):
	import json
	getLyrics(track, unsyncedOnly=True)
	item = buildListItem(track)
	item.setPath(track[streamKey])
	item.setProperty("IsPlayable", 'true')	
	item.setArt({'fanart': _pathAddon + '/fanart.jpg'})	
	info = item.getMusicInfoTag()
	if track.get('lyricsUnsynced',None) is not None:
		info.setLyrics(track['lyricsUnsynced'].replace("\n",'[CR]') )
	if track.get('lyricsSynced',None) is not None:		
		info.setComment(json.dumps(track['lyricsSynced']))
	xbmc.Player().play(item.getPath(), listitem=item)
	xbmc.executebuiltin('ActivateWindow(visualisation)')
	try:
		player.waitForCurrentItem()
	except:
		None


def createTrackMenu(track, account):
	import xbmcgui, xbmc, time, json
	from helpers import gui, player, string
	from youtube import music, common, android
	d=xbmcgui.Dialog()
	while True:
		items = [
			{'label': 'Original', 'thumb': _pathImg + '/song.png'},
			{'label': 'Karaoke', 'thumb': _pathImg + '/microphone.png'},
			{'label': 'Title: {}'.format(track['title']), 'thumb': _pathImg + '/edit.png'},
			{'label': 'Artist: {}'.format(track['artist']), 'thumb': _pathImg + '/user.png'},
			{'label': 'Save to Playlist', 'thumb': _pathImg + '/plus.png'}
		]
		s = d.select('YouTube: {}'.format(track['label']), [buildListItem(item) for item in items], useDetails=True)
		if s == -1:
			return False
		action = items[s]
		if action['label'].startswith('Title'):
			text = gui.input(title='Title', default=track['title'])
			if text is not None and len(text)>0:
				track['title']=text
		elif action['label'].startswith('Artist'):
			text = gui.input(title='Artist', default=track['artist'])
			if text is not None and len(text)>0:
				track['artist']=text						
		elif action['label'] == 'Original':
			if 'streamPlus' not in track.keys():
				track['streamPlus'] = common.getAudioStream(android.getVideo(track['id']))
			createPlayTrack(track, 'streamPlus')			
		elif action['label'] == 'Karaoke':
			if 'stream' not in track.keys():
				if 'streamPlus' not in track.keys():
					track['streamPlus'] = common.getAudioStream(android.getVideo(track['id']))
				job = xminus.submitJobAIKaraoke(track['streamPlus'], track['id'], account)				
				if job['status']=='rejected':
					d.ok(_title, 'Unable to process Karaoke' + "\n" + job['message'])
					continue
				wait = int(job['ts'] + job['remaining_time'] - time.time())
				if wait > 0:
					d.ok(_title, 'Processing Karaoke. Please wait {} seconds'. format(wait))
					continue														
				data = xminus.getJobAIKaraoke(job)								
				track['stream'] = data['url']
			createPlayTrack(track, 'stream')
		elif action['label'] == 'Save to Playlist':			
			track['youtubeIdPlus'] = track['id']			
			track['source'] = 'ai'
			if handlerPlaylistItemAdd(track, account) is True:
				return True


def handlerCreate():
	import xbmcgui, xbmc
	from helpers import gui, player, account
	from youtube import music, common, android	
	acc = account.get(_pathAccount, xminus.validateAccount)
	if acc is None:
		xbmcgui.Dialog().ok(_title, 'Account not set')
		xbmcplugin.endOfDirectory(_handleId)
		return	
	d=xbmcgui.Dialog()				
	items = [
		{'label': 'Songs', 'thumb': _pathImg + '/music.png', 'category': 'song'},
		{'label': 'Videos', 'thumb': _pathImg + '/video.png', 'category': 'video'}
	]
	s = d.select('YouTube Category', [buildListItem(item) for item in items], useDetails=True)
	if s == -1:
		return
	category = items[s]
	text=gui.inputWithHistory('YouTube Search', _pathSettings + 'historySearch', 15)	
	if text is None:
		return		
	data = music.search(text, category['category'], resultsCountLimit=15)
	for t in data:
		if category['category']=='song':
			t['label'] = '{} - {}'.format(t['title'],t['artist']['name'])
			t['artist'] = t['artist']['name']
		else:
			t['label'] = t['title']
			t['artist'] = ''
	while True:
		s = d.select('YouTube {}'.format(category['label']), [buildListItem(t) for t in data], useDetails=True)
		if s == -1:
			return
		track = data[s]
		track['lyricsUnsynced'] = music.getLyrics(track)
		if createTrackMenu(track, acc) is True:
			break
	

def handlerAccount():	
	from helpers import account
	account.open(_pathAccount, xminus.validateAccount, 'X-Minus Accounts', 'credentials', usernameLabel='Email')

		
def handlerRoot():	
	items=[]	
	for rootLink in [		
		{'name': 'Search', 'urlParams': {'handler': 'SearchInput'}, 'thumb': 'search.png', 'folder': False},	
		{'name': 'Playlists', 'urlParams': {'handler': 'Playlists'}, 'thumb': 'list.png', 'folder': True},
		{'name': 'Create', 'urlParams': {'handler': 'Create'}, 'thumb': 'cubes.png', 'folder': False},
		{'name': 'Account', 'urlParams': {'handler': 'Account'}, 'thumb': '/user.png', 'folder': False}				
	]:
		item=xbmcgui.ListItem(rootLink['name'], offscreen=True)		
		item.setArt({'thumb': _pathImg + rootLink["thumb"], 'fanart': _pathAddon + '/fanart.jpg'})		
		item.setPath(_baseUrl+'?' + parse.urlencode(rootLink['urlParams']))
		item.setIsFolder(rootLink['folder'])				
		items.append((item.getPath(), item, item.isFolder(),))		
	xbmcplugin.addDirectoryItems(handle=_handleId, items=items, totalItems=len(items))	
	xbmcplugin.endOfDirectory(_handleId)

globals()['handler' + _params['handler']]()
