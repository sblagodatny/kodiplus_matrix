# -*- coding: utf-8 -*-

from . import common


import requests
import json
import urllib
from bs4 import BeautifulSoup
import datetime
import xbmcaddon
import re

from helpers import http, media, storage

_url = 'https://www.xoox.co.il'

_timeout = 4


import xbmcaddon
_addon = xbmcaddon.Addon('script.module.vod')
_pathAddon = _addon.getAddonInfo('path')
_pathSettings = _pathAddon.replace('addons','userdata/addon_data')
_pathAccount = _pathSettings + 'accounts/xoox.json'


def getVideos(cat):
	s = http.initSession()		
	soup = BeautifulSoup(s.get(_url + '/clip/ccat.php', params={'ccat': cat}, timeout=_timeout).text, "html.parser")							
	videos = []
	for tag in soup.find_all('table'):	
		try:		
			item = {							
				'media_id': tag.find('a')['href'].split('=')[-1],
				'title': tag.find('img')['title'],
				'poster': 'https://www.xoox.co.il/clip/img/big_' + tag.find('img')['src'].split('_')[1],
				'description': tag.find('td').get_text()			
			}			
			if not item['title'].startswith(cat):
				continue										
			item['episode'] = int(item['title'].split('פרק')[1].split(' ')[1])												
			try:
				item['season'] = int(item['title'].split('עונה')[-1].split(' ')[1])
			except:
				item['season'] = 1							
			videos.append(item)			
		except:
			None
	return videos
	

def search(criteria):	
	if criteria['type'] != 'tv':
		return[]
	if 'he-IL' not in criteria['titles'].keys():
		return []
	if 'עִבְרִית' not in criteria.get('language',['עִבְרִית']):
		return []				
	result = []		
	videos = getVideos(criteria['titles']['he-IL'])		
	if len(videos) > 1:
		result.append({
			'id': criteria['titles']['he-IL'],
			'title': criteria['titles']['he-IL'],
			'poster': videos[-1]['poster'],
			'plot': videos[-1]['description'].split('פרק' + ' ' + str(videos[-1]['episode']))[1],
			'type': 'tv'
		})	
	return result

	
def seasons(content):
	result = []
	videos = getVideos(content['id'])		
	for video in videos:
		season = {			
			'id': video['season'],
			'title': 'עונה' + ' ' + str(video['season'])			
		}	
		if season not in result:
			result.append(season)
	result.reverse()
	return result


def episodes(content, season):
	result = []
	videos = getVideos(content['id'])		
	for video in videos:
		if video['season']==season['id']:
			episode = {
				'id': video['episode'],
				'title': 'פרק' + ' ' + str(video['episode']),			
				'thumb': '', 
				'plot': '',
				'media_id': video['media_id']
			}
			result.append(episode)	
	result.reverse()
	return result

	
def stream(content, season=None, episode=None):		
	account = common.getAccount(_pathAccount)
	if account is None:
		raise Exception ('Please login to XooX')
	s = http.initSession()		
	soup = BeautifulSoup(s.get(_url + '/clip/show_media.php', params={'id': episode['media_id']}, cookies=account['cookies'], timeout=_timeout).text, "html.parser")
	tag = soup.find('div', {'id': 'DivVideo'})	
	
	
	if tag.find('source') is not None:
		return {'url': tag.find('source')['src']}		
	if tag.find('iframe') is not None:	
		url = tag.find('iframe')['src']
		if 'youtube' in url:
			return {'youtube': url.split('/')[-1]}		
	raise Exception ('Stream not available')



def login(username, password):	
	s = http.initSession()		
	data = {'UserNameV': username, 'MyPass': password}	
	headers = { 'Content-Type': 'application/x-www-form-urlencoded', 'Origin': _url, 'Referer': _url + '/MembersX/LogIn'}
	result = s.post(_url + '/MembersX/LogIn', data=data, timeout=_timeout, headers=headers).cookies.get_dict()
	if 'Member_ID' not in result.keys():
		raise Exception('Unable to login')	
	return result