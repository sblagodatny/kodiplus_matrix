# -*- coding: utf-8 -*-


import requests
import json
import urllib
from bs4 import BeautifulSoup
import datetime
import xbmcaddon
import re

from helpers import http, media, storage

_url = 'https://vod.walla.co.il'

_timeout = 4




def search(criteria):	
	if criteria['type'] != 'tv':
		return[]
	if 'he-IL' not in criteria['titles'].keys():
		return []
	if 'עִבְרִית' not in criteria.get('language',['עִבְרִית']):
		return []
	
	s = http.initSession()		
	soup = BeautifulSoup(s.get(_url + '/search', params={'q': criteria['titles']['he-IL']}, timeout=_timeout).text, "html.parser")	
	result = []
	for tag in soup.find_all('li', class_="event"):			

		resultitem = {
			'id': tag.find('a')['href'],
			'title': tag.find('h3').get_text(),
			'poster': '',
			'plot': '',
			'type': 'tv',
			'url': tag.find('a')['href']
		}				
		result.append(resultitem)		
	return result

	
def seasons(content):
	result = []
	s = http.initSession()
	soup = BeautifulSoup(s.get(content['url'], timeout=_timeout).text, "html.parser")		
	for tag in soup.find('ul', class_="tvshow-seasons").find_all('li'):
		resultitem = {			
			'id': int(tag.get_text().split(' ')[1]),
			'title': tag.get_text(),
			'url': content['url'] if tag.find('a') is None else _url + tag.find('a')['href']
		}	
		result.append(resultitem)	
	return result


def episodes(content, season):
	result = []
	s = http.initSession()
	soup = BeautifulSoup(s.get(season['url'], timeout=_timeout).text, "html.parser")	
	for tag in soup.find('ul', class_="sequence").find_all('li'):
		item = {			
			'id': int(tag.find('h3').get_text().split(' ')[1]),
			'title': tag.find('h3').get_text(),			
			'thumb': '', 
			'plot': '',
			'url': tag.find('a')['href']
		}				
		result.append(item)	
	result.reverse()
	return result

	
def stream(content, season=None, episode=None):		
	s = http.initSession()			
	data = s.get(episode['url'], timeout=_timeout).text
	url = re.search(r'"contentUrl": "(.*?)"', data).group(1)	
	return {'url': url}
