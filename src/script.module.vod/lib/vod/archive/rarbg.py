# coding: utf-8


from bs4 import BeautifulSoup
import requests
import xbmcaddon
import datetime
import os
import xbmcgui
import re
import json

from lib import util

import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

_pathAddon = xbmcaddon.Addon().getAddonInfo('path')
_pathSettings = _pathAddon.replace('addons','userdata/addon_data')
_pathTorrents = _pathSettings + '/torrents'
_pathCookies = _pathSettings + '/cookies_rarbg'

_url = 'https://proxyrarbg.org'
_title = 'RARBG'
_timeout = 4





def initSession():
	session = requests.Session()
	session.verify = False
	util.setUserAgent(session, 'chrome')
	try:
		cookies = util.fileToObj(_pathCookies)
		session.cookies=cookies['cookies']
		
		util.log('hi')
		
		if (datetime.datetime.now() - cookies['ts']).days > 2:
			raise	
		
		util.log('hi2')
		
	except Exception as e:		
		if len(session.cookies.get_dict().keys()) == 0:
			session.cookies.update({
				'skt': '57IS9ezbyu',
				'__cf_bm': 'YVhmDpUSrIfyH15CKn3uEbssNQYcLjscMHUtJJI_9LY-1660487283-0-AbzVKr3QV73PUlzDSYWeE0NYBGlvnXPsY5PKfBQ+j9O69x0rop0tD8jAObLaLUqvNApWPprdNplNUudJnKw/mX+34M5l044E/S9k5y0XJ3IgXQlKyithfNlCy0r0LtI7gw=='
			})
		session.get(_url + '/torrents.php')
		util.objToFile ({'ts': datetime.datetime.now(), 'cookies': session.cookies}, _pathCookies)		
	return session

		
def search (text):
	return[]


def searchTMDB (tmdb):
	if tmdb['imdbId'] is None:
		return[]
	session = initSession()
	
	
	util.log(json.dumps(session.cookies.get_dict()))
	
	result = []			
	if tmdb['type'] == 'movie':
		data = BeautifulSoup(session.get (_url + '/torrents.php', params={'imdb': tmdb['imdbId']}, timeout=_timeout).content, "html.parser")		
	else:		
		data = BeautifulSoup(session.get (_url + '/tv/' + tmdb['imdbId'] + '/', timeout=_timeout).content, "html.parser")				
		
		util.log(data.prettify())
		
		seasons = {}
		for tag in data.find_all('div', class_="tvcontent"):
			season = tag.find('div')['id'].split('_')[1]
			try:
				episode = tag.find('script').string.split('"#episode_')[1].split('").insertAfter')[0]
			except:
				continue
			seasons[season] = episode						
		if tmdb['season'] not in seasons.keys():
			return []
		data = BeautifulSoup(session.get (_url + '/tv.php', params={'ajax':1, 'tvepisode': seasons[tmdb['season']]}, timeout=_timeout).content, "html.parser")		
	for tag in data.find_all('tr', class_="lista2"):
		torrent = {
			'id': tag.find_all('td')[1].find('a')['href'].split('/')[-1],
			'name': tag.find_all('td')[1].find('a')['title'],
			'size': tag.find_all('td')[3].get_text(),					
			'description': '',
			'poster': '',
			'date': tag.find_all('td')[2].get_text().split(' ')[0],
			'quality': '',
			'seeds': int(tag.find_all('td')[5].get_text())
		}				
		result.append(torrent)			
	return result		
	
	
	
def getTorrent(torrent):
	session = initSession()
	page = session.get(_url + '/torrent/' + torrent['id'], timeout=_timeout).text
	torrent['magnet'] = 'magnet:' + page.split('<a href="magnet:')[1].split('"')[0]
	torrent['hash'] = torrent['magnet'].split('btih:')[1].split('&')[0].upper()







