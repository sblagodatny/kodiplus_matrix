# -*- coding: utf-8 -*-

import json
import urllib
from bs4 import BeautifulSoup
import xbmc
import os
import xbmcgui
import datetime
import xbmcaddon
import xbmcvfs
import concurrent.futures


from helpers import http, gui, storage, player, dns



_addon = xbmcaddon.Addon('plugin.video.vod')

_host = 'sdarot.buzz'
#_url = 'https://' + dns.get(_host)[0].split(' ')[-1]	
_url = 'https://37.221.65.66'

 
_urlAPI = _url.replace('https://', 'https://api.') 
_timeout = 4

_pathAddon = _addon.getAddonInfo('path')
_pathSettings = _pathAddon.replace('addons','userdata/addon_data')
_account = _pathSettings + '/accountSdarot'
_title = 'Sdarot'





def categories():
	s = http.initSession()				
	soup = BeautifulSoup(s.get(_url + '/series', timeout=_timeout, headers={'Host': _host}).text, "html.parser")
	result = []
	for item in soup.find('select', {'name': 'genre[]'}).find_all('option'):
		result.append({
			'id': item['value'], 
			'title': item.get_text().lstrip().rstrip()
		})
	return result


def catalog(category, page):
	result = []	
	s = http.initSession()			
	perPage = 24
	params = {
		'loadMore': perPage,
		'start': int(page)*perPage,
		'search[genre][]': category,
		'search[from]': '',
		'search[to]': '',
		'search[order]': '',
		'search[dir]': '',
	}
	for tag in BeautifulSoup(s.get(_url + '/ajax/series', params=params, headers={'Host': _host, 'Referer': 'https://' + _host}, timeout=_timeout).text, "html.parser").find_all('div', class_="col-lg-2"):
		result.append({
			'id': tag.find('a')['href'].replace('/watch/','').split('-')[0],
			'title': tag.find('h4').get_text().rstrip().lstrip() + ' / ' + tag.find('h5').get_text().rstrip().lstrip(),
#			'titleOriginal': tag.find('h5').get_text().rstrip().lstrip(),
			'poster': 'https:' + tag.find('img')['src'],			
			'year': tag.find('p').get_text().split(':')[1],
			'episodes': True
		})			
	pages = 30
	if len(result)<perPage:
		pages = page
	return {'content': result, 'pages':pages}
	

def get(id):
	s = http.initSession()	
	data = BeautifulSoup(s.get(_url + '/watch/' + id, headers={'Host': _host}, timeout=_timeout).text, "html.parser")		
	poster = 'https:' + data.find('meta', {'property': "og:image"})['content']
	year = data.find('div', {'id':"year"}).find('span').get_text()
	description = data.find('strong', text='תקציר הסדרה').parent.parent.find('p').get_text()	
	seasons = []	
	for tag in data.find('ul',{'id':'season'}).find_all('li'):
		item = {			
			'id': tag['data-season'],
			'title': 'עונה' + ' ' + tag.find('a').get_text()
		}					
		try:
			item['description'] = data.find('strong', text='תקציר הסדרה').parent.parent.find('p').get_text()
		except:
			None
		seasons.append(item)	
	return {'seasons': seasons, 'poster': poster, 'year': year, 'description': description}
	


def poster(id):
	url = 'https://static.sdarot.buzz/series/{}.png'.format(id)
	s = http.initSession()	
	reply = s.head(url)
	if reply.status_code != 200:
		url = url.replace('.png','.jpg')
	return url

def search(text, type):
	if type != 'tv':
		return[]
	result = []
	s = http.initSession()
	data = json.loads(s.get(_url + '/ajax/index', params={'search':text}, allow_redirects=False, timeout=_timeout, headers={'Host': _host, 'Referer': 'https://' + _host}).text)	
	for item in data:
		result.append({
			'id': item['id'],
			'title': item['name'],
			'poster': poster(item['id']),
			'episodes': True
		})	
#	def task(i):
#		result[i].update(get(result[i]['id']))			
#	ex = concurrent.futures.ThreadPoolExecutor(max_workers=10)		
#	for i in range(len(result)):
#		ex.submit(task, i)	
#	ex.shutdown(wait=True)		
	return result
	
	

def seasons(content):	
	return get(content['id'])['seasons']
	
	
def episodes(content, season):
	result = []
	s = http.initSession()
	data = BeautifulSoup(s.get(_url + '/watch/' + content['id'] + '/season/' + season['id'], timeout=_timeout, headers={'Host': _host}).text, "html.parser")
	for tag in data.find('ul',{'id':'episode'}).find_all('li'):
		result.append({			
			'id': tag['data-episode'],
			'title': 'פרק' + ' ' + tag.find('a').get_text()			
		})									
	return result
	
	
	
def stream(content, season, episode):	
	account = getAccount()
	if account is None:
		raise Exception('Please authorize to Sdarot')
	s = http.initSession()	
	headers = {	
		'Host': _host,
		"content-type": "application/x-www-form-urlencoded",
		'X-Requested-With': 'XMLHttpRequest',
		'Referer': 'https://' + _host
	}
	cookies = account['cookies'].get_dict()	
	data = {
		'preWatch': 'true',		
		'SID': content['id'],
		'season': season['id'],
		'ep': episode['id']					
	}		
	token = s.post(_url + '/ajax/watch', data=data, headers=headers, cookies=cookies , timeout=_timeout ).text					
	if not player.waitForDelay(30, 'Waiting for stream', _addon.getAddonInfo('icon')):
		raise Exception('Stream wait aborted')			
	data = {
		'watch': 'true',
		'token': token,
		'serie': content['id'],
		'season': season['id'],
		'episode': episode['id'],
		'type': 'episode'				
	}	
	data = s.post(_url + '/ajax/watch', data=data, headers=headers, cookies=cookies , timeout=_timeout ).json()		
	error = None
	if 'error' in list(data.keys()):
		error = data['error']
	if 'errors' in list(data.keys()):
		error = data['errors'][0]
	if error is not None:
		if '.' in error:
			error = error.split('.')[0]
		raise Exception(error)
	
	resolution = sorted(list(data['watch'].keys()))[-1]		
	url = 'https:' + data['watch'][resolution]
	
	
	host = url.split('/')[2]
	ip = dns.get(host)[0].split(' ')[-1]		
	url = url.replace(host,ip)
	
	return {
		'quality': '480p',
#		'url': url + '|' + urllib.parse.urlencode(s.headers) + '&Cookie=' + urllib.parse.urlencode(cookies).replace('&', '; ')
		'url': url,
		'cookies': cookies,
		'headers': {'Host': _host},		
	}
	
		
	
def addAccount():
	account = getAccount()	
	if account is not None:
		if not xbmcgui.Dialog().yesno(_title, 'Authorization was already done for this device' + "\n" + 'User: ' + account['username'] + "\n" + 'Press Yes to Re-Authorize'):
			return
	user=gui.input(_title + ' Username')	
	if user is None: 
		return
	password=gui.input(_title + ' Password')	
	if password is None:
		return			
	session = http.initSession()
	session.get(_url, headers={'Host': _host})
	data = {
		'location': '/',
		'username': user,
		'password': password,
		'submit_login': ''
	}
	reply = session.post(_url + '/login', data=data, headers={'Referer': 'https://' + _host, 'Host': _host}, allow_redirects=False).text
	if len(reply) != 0:
		xbmcgui.Dialog().ok(_title, 'Authorization failed')
		return	
	account = {
		'username': user,
		'password': password,
		'ts': datetime.datetime.now(),
		'cookies': session.cookies,
	}	
	storage.objToFile(account, _account)	
	xbmcgui.Dialog().ok(_title, 'Authorization completed')
	
	
def getAccount():
	if not os.path.isfile(_account):
		return None	
	account = storage.fileToObj(_account)	
	session = http.initSession()
	session.get(_url, headers={'Host': _host})
	data = {
		'location': '/',
		'username': account['username'],
		'password': account['password'],
		'submit_login': ''
	}
	session.post(_url + '/login', data=data, headers={'Referer': 'https://' + _host, 'Host': _host}, allow_redirects=False)	
	account['cookies'] = session.cookies			
	return account
