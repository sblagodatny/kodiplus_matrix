# coding: utf-8

from bs4 import BeautifulSoup
import requests
import datetime
import xbmcaddon
import os
import xbmcgui
import re
import xbmc

from helpers import http, storage, gui



_addon = xbmcaddon.Addon('plugin.video.torrents')
_pathAddon = _addon.getAddonInfo('path')
_pathSettings = _pathAddon.replace('addons','userdata/addon_data')
_pathTorrents = _pathSettings + '/torrents'
_account = _pathSettings + '/accountKinozal'


_url = 'https://kinozal.tv'
_title = 'Kinozal'
_timeout = 4


	
def search (text, type='movie'):
	account = getAccount()
	if account is None:
		xbmcgui.Dialog().ok(_title, 'Please authorize via settings')
		return []
	session = http.initSession()
	data = None
	for retry in range(2):
		try:
			data = BeautifulSoup(session.get (_url + '/browse.php', params={'s': text.encode('cp1251', 'xmlcharrefreplace')}, cookies=account['cookies'] , timeout=2).content, "html.parser")
			break
		except:
			xbmc.sleep(500)
	if data is None:
		xbmcgui.Dialog().ok(_title, _url + ' is not available, please retry later')
		return []
	result = []	
	for tag in data.find_all('tr', class_='bg'):
		torrent = {
			'id': tag.find('td', class_="nam").find('a')['href'].split('id=')[-1],
			'name': tag.find('td', class_="nam").find('a').get_text(),
			'size': tag.find_all('td', class_='s')[1].get_text(),
			'description': '',
			'poster': '',
			'date': '',
			'quality': ''			
		}
		try:
			torrent['seeds'] = int(tag.find('td', class_='sl_s').get_text())
		except:
			continue
		if torrent['seeds'] == 0:
			continue
		result.append(torrent)
	return result

	

def getTorrent(torrent):
	account = getAccount()
	if account is None:
		xbmcgui.Dialog().ok(_title, 'Please authorize via settings')
		raise Exception('Not authorized')	
	session = http.initSession()	
	
	path = _pathTorrents + '/kinozal_' + torrent['id'] + '.torrent'	
	if os.path.exists(path):	
		torrent['file'] = path	
		return
	
	### Try to download torrent file (up to 5 downloads per day) ###
	try:
		r = session.get(_url + '/download.php', params={'id':torrent['id']}, cookies=account['cookies'], timeout=_timeout, headers={'Referer': _url})		
		if 'text/html' in r.headers['Content-Type']:		
			raise
		storage.removeOldFiles(_pathTorrents)			
		with open(path, 'wb') as f:
			f.write(r.content)		
		torrent['file'] = path	
		return
	except:
		None
			
	
	### Get magnet ###									
	content = None
	for retry in range(3):
		try:
			content = session.get(_url + '/get_srv_details.php', params={'id': torrent['id'],'action':2}, cookies=account['cookies'], timeout=1).text					
			break
		except:
			xbmc.sleep(500)		
	if content is None:
		xbmcgui.Dialog().ok(_title, _url + ' not available, please retry later')
		raise Exception('Site not available')		
	torrent['hash'] = content.split('<li>Инфо хеш: ')[1].split('</li>')[0].upper()
	torrent['magnet'] = 'magnet:?xt=urn:btih:' + torrent['hash']
	for i in range(6):
		torrent['magnet'] += '&tr=http://tr{}.tor4me.info/ann?uk=GR4o4Fx1U1'.format(i)		
	for i in range(6):
		torrent['magnet'] += '&tr=http://tr{}.tor2me.info/ann?uk=GR4o4Fx1U1'.format(i)		
	for i in range(6):
		torrent['magnet'] += '&tr=http://tr{}.torrent4me.info/ann?uk=GR4o4Fx1U1'.format(i)				
		
			

def addAccount():
	account = getAccount()
	if account is not None:
		if not xbmcgui.Dialog().yesno(_title, 'Authorization was already done for this device' + "\n" + 'User: ' + account['username'] + "\n" + 'Press Yes to Re-Authorize'):
			return
	user=gui.input(_title + ' Username')	
	if user is None: 
		return
	password=gui.input(_title + ' Password')	
	if password is None:
		return
	session = http.initSession()
	headers = {
		'Referer': _url,
		'Origin': _url
	}	
	data = {
		'username': user,
		'password': password,
		'returnto': ''
	}	
	try:
		session.post(_url + '/takelogin.php', data=data, headers=headers, timeout=_timeout)
	except:
		xbmcgui.Dialog().ok(_title, _url + ' not available, retry later')
		return
	if 'uid' not in str(session.cookies):
		xbmcgui.Dialog().ok(_title, 'Authorization failed')
		return	
		
	session.get(_url)	
		
	account = {
		'username': user,
		'password': password,
		'ts': datetime.datetime.now(),
		'cookies': session.cookies,
		'url': _url
	}	
	storage.objToFile(account, _account)	
	xbmcgui.Dialog().ok(_title, 'Authorization completed')
	
	

	
	
def getAccount():
	if not os.path.isfile(_account):
		return None
	data = storage.fileToObj(_account)	
	if data.get('url','') != _url:
		return None
	return data
