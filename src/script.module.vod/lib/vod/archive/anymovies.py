# -*- coding: utf-8 -*-

import requests
from bs4 import BeautifulSoup
from helpers import http, gui, storage

import importlib
cdn = importlib.import_module('.' , 'cdn')



_url = 'https://www.downloads-anymovies.co'

_timeout = 4

_headers = {
	'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'
}

_cookies = {
	'cf_clearance': 'zz9TDL9USDYTdej.q30ZGwyGvEBuL9Tn4AS4iXehuMk-1690645505-0-160.0.0'
}

def categories():
	return []
		

	
def catalog(category, page):	
	return {'content': [], 'pages': 1}
	
	
def search(text, type='movie'):	
	if type != 'movie':
		return[]
	s = http.initSession()
	content = BeautifulSoup(s.get(_url + '/search.php', params={'zoom_query': text.replace(' ','+')}, timeout=_timeout, headers=_headers, cookies=_cookies).text, "html.parser")		
	result = []
	for tag in content.find_all('div', class_="result_block") + content.find_all('div', class_="result_altblock"):		
		item = {
			'title': tag.find('div', class_="result_title").get_text().lstrip().rstrip(),
			'poster': tag.find('img')['src'],			
			'url': tag.find('div', class_="result_title").find('a')['href'],			
			'episodes': False	
		}
		if '(' in item['title']:
			item['year'] = item['title'].split('(')[1].split(')')[0]
			item['title'] = item['title'].split('(')[0].rstrip()					
		if item['title'].startswith('Watch '):
			item['title'] = item['title'].replace('Watch ','')
		result.append(item)	
	return result



	
		
def stream(content, season=None, episode=None):
	s = http.initSession()
	soup = BeautifulSoup(s.get(content['url'], timeout=_timeout, headers=_headers, cookies=_cookies).text, "html.parser")		
	embedUrls = []	
	for tag in soup.find_all('a', {'target': "_blank"}):
		embedUrls.append(tag['href'])			
	return cdn.parse(embedUrls)	
