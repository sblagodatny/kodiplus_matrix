# coding: utf-8

import util
from bs4 import BeautifulSoup
import requests
import xbmcaddon
import datetime
import os
import xbmcgui
import re

import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

_pathAddon = xbmcaddon.Addon().getAddonInfo('path')
_pathSettings = _pathAddon.replace('addons','userdata/addon_data')
_pathTorrents = _pathSettings + '/torrents'
_account = _pathSettings + '/accountHebits'

_url = 'https://hebits.net'
_title = 'HeBits'



def initSession():
	session = requests.Session()
	session.verify = False
	util.setUserAgent(session, 'chrome')
	return session

		
def search (text):
	account = getAccount()
	if account is None:
		xbmcgui.Dialog().ok(_title, 'Please authorize via settings')
		return []
	session = initSession()
	params = {
		'search': text.encode('cp1255', 'xmlcharrefreplace'), 
		'cata': '0'
	}
	headers = {'Referer': _url + '/browse.php'}
	data = BeautifulSoup(session.post (_url + '/browse.php', params=params, headers=headers, cookies=account['cookies']).content, "html.parser")
	result = []	
	if data.find('div', class_="browse") is None:
		return []
#	for color in ["lineBrown", lineBlue, "lineGreen", "lineGray"]:
	for color in ["lineBlue", "lineBrown"]:
		for tag in data.find('div', class_="browse").find_all('div', class_=color):	
			torrent = {
				'id': tag.find('div', class_="bTitle").find('a')['href'].split('=')[-1], 	
				'name': tag.find('div', class_="bTitle").find('img')['title'].replace('[Hebits.net]','').replace('.torrent',''),
				'size': tag.find('div', class_="bSize").get_text().split(':')[-1],
				'seeds': int(tag.find('div', class_="bUping").get_text()),
			}
			result.append(torrent)
	return result


	
#https://hebits.net/torrents.php?imdbgt=0&imdblt=10&searchstr=%D7%94%D7%9E%D7%A4%D7%A7%D7%93%D7%AA&order_by=time&order_way=desc&action=basic&searchsubmit=1	
	
	
	
def searchTMDB (tmdb):
	text = tmdb['titleOriginal']	
	result = []
	for torrent in search(text):
		if re.search("S(\d\d?)E(\d\d?)",torrent['name']):
			continue			
		if 'season' in tmdb.keys():	
			if len(tmdb['seasons']) > 1:
				found = False
				for pattern in ['S' + tmdb['season']	, 'S0' + tmdb['season']	]:
					if pattern in torrent['name']:
						found = True
						break
				if not found:
					continue
		result.append(torrent)	
	return result			
		
		
	

def getTorrent(id):
	account = getAccount()
	if account is None:
		raise Exception('Not authorized')	
	session = initSession()
	r=session.get(_url + '/download.php', params={'id': id}, cookies=account['cookies'])
	with open(_pathTorrents + '/hebits' + id + '.torrent', 'wb') as f:
		f.write(r.content)		
	return { 'file': 'hebits' + id + '.torrent' }
	

		
def addAccount():
	account = getAccount()
	if account is not None:
		if not xbmcgui.Dialog().yesno(_title, 'Authorization was already done for this device' + "\n" + 'User: ' + account['username'] + "\n" + 'Press Yes to Re-Authorize'):
			return
	user=util.input(_title + ' Username')	
	if user is None: 
		return
	password=util.input(_title + ' Password')	
	if password is None:
		return
	session = initSession()	
	data = BeautifulSoup(session.get(_url + '/login.php').content, "html.parser")
	img = data.find('img')['src']
	answers = []
	for tag in data.find('ul', {'id': 'captcha_answer'}).find_all('li'):
		answers.append({'id': tag.find('input')['value'], 'value': tag.find('label').get_text(), 'thumb': img})
	s = util.select('Captcha', answers, 'value', 'thumb')
	if s is None:
		return
	headers = {
		'Referer': _url + '/login.php',		
		'Origin': _url,
		'Content-Type': 'application/x-www-form-urlencoded'
	}
	data = {				
		'username': user,
		'password': password,
		'twofa': '',
		'image': data.find('input', {'name': 'image'})['value'],
		'captcha': answers[s]['id'],
		'login': 'התחבר'		
	}
	session.post(_url + '/login.php', data=data, headers=headers)
	if 'userid' not in str(session.cookies):
		xbmcgui.Dialog().ok(_title, 'Authorization failed')
		return	
	account = {
		'username': user,
		'password': password,
		'ts': datetime.datetime.now(),
		'cookies': session.cookies
	}	
	util.objToFile(account, _account)	
	xbmcgui.Dialog().ok(_title, 'Authorization completed')
	
	
	
def getAccount():
	if not os.path.isfile(_account):
		return None
	return util.fileToObj(_account)	



