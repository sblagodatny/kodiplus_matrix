# -*- coding: utf-8 -*-

import requests
import json
import urllib
from bs4 import BeautifulSoup
import base64
import random
import time
import re


from helpers import http, gui, storage
#from lib import cdn
import importlib
cdn = importlib.import_module('.' , 'cdn')

import xbmc

import concurrent.futures



_url = 'https://bstsrs.one'
_timeout = 4



def categories():
	return []
		

	
def catalog(category, page):	
	return {'content': [], 'pages': 1}
	
def search(text, type='tv'):
	if type != 'tv':
		return[]
	s = http.initSession()	
	result = []
	data = {
		'q': text,
		'limit': 8,
		'timestamp': int(time.time() * 1000),
		'verifiedCheck': ''
	}
	data = s.post(_url + '/ajax/search.php', data=data, timeout=_timeout)	
	data = data.json()
	for item in data:
		result.append({
			'url': item['permalink'], 
			'title': item['title'],			
			'poster': item['image'].replace('w=80&h=80','w=240&h=360'),			
			'year': item['year'],
			'episodes': True
		})	
	return result


	


def seasons(content):
	s = http.initSession()					
	soup = BeautifulSoup(s.get(content['url'], timeout=_timeout).text, "html.parser")
	result = []
	for tag in soup.find('span', class_="spn-numbers").find_all('a', class_="page-numbers"):		
		if tag.get_text().lstrip().rstrip() == 'All seasons':
			continue
		result.append({			
			'id': tag['href'].split('/')[-1],
			'title': tag.get_text().lstrip().rstrip(),			
			'url': tag['href']
		})		
	return result
	

def episodes(content, season):
	s = http.initSession()			
	soup = BeautifulSoup(s.get(season['url'], timeout=_timeout).text, "html.parser")	
	result = []
	for tag in soup.find_all('div', class_="hgrid"):				
		result.append({			
			'id': tag.find('a', class_="hb-image")['href'].split('/')[-1],
			'title': tag.find('span', class_="episode")['title'],			
			'url': tag.find('a', class_="hb-image")['href'],
			'thumb': tag.find('a', class_="hb-image")['data-original'].replace('&amp;','&').replace('w=170&h=115', 'w=400&h=230')
		})		
	result.reverse()
	return result
	




def decodeUrl(url):
	result = url
	ALPHABET = {
		'47ab07f9': 'A', '47ab07fa': 'B', '47ab07fb': 'C', '47ab07fc': 'D', '47ab07fd': 'E',
		'47ab07fe': 'F', '47ab07ff': 'G', '47ab0800': 'H', '47ab0801': 'I', '47ab0802': 'J',
		'47ab0803': 'K', '47ab0804': 'L', '47ab0805': 'M', '47ab0806': 'N', '47ab0807': 'O',
		'47ab0808': 'P', '47ab0809': 'Q', '47ab080a': 'R', '47ab080b': 'S', '47ab080c': 'T',
		'47ab080d': 'U', '47ab080e': 'V', '47ab080f': 'W', '47ab0810': 'X', '47ab0811': 'Y',
		'47ab0812': 'Z',
		'47ab0819': 'a', '47ab081a': 'b', '47ab081b': 'c', '47ab081c': 'd', '47ab081d': 'e',
		'47ab081e': 'f', '47ab081f': 'g', '47ab0820': 'h', '47ab0821': 'i', '47ab0822': 'j',
		'47ab0823': 'k', '47ab0824': 'l', '47ab0825': 'm', '47ab0826': 'n', '47ab0827': 'o',
		'47ab0828': 'p', '47ab0829': 'q', '47ab082a': 'r', '47ab082b': 's', '47ab082c': 't',
		'47ab082d': 'u', '47ab082e': 'v', '47ab082f': 'w', '47ab0830': 'x', '47ab0831': 'y',
		'47ab0832': 'z',
		'47ab07e8': '0', '47ab07e9': '1', '47ab07ea': '2', '47ab07eb': '3', '47ab07ec': '4',
		'47ab07ed': '5', '47ab07ee': '6', '47ab07ef': '7', '47ab07f0': '8', '47ab07f1': '9',
		'47ab07f2': ':', '47ab07e7': '/', '47ab07e6': '.', '47ab0817': '_', '47ab07e5': '-'
	}
	for key, c in ALPHABET.items():
		result = result.replace(key,c)
	result = result.replace('-','')
	return result


		
def stream(content, season=None, episode=None):
	s = http.initSession()
	soup = BeautifulSoup(s.get(episode['url'], timeout=_timeout).text, "html.parser")		
	embedUrls = []		
	for tag in soup.find_all('a', {'target': "_blank"}):
		try:
			embedUrls.append(decodeUrl(re.search(r"dbneg\('(.*?)'", tag['onclick']).group(1))	)			
		except:
			None
	return cdn.parse(embedUrls)	
