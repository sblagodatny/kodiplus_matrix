# -*- coding: utf-8 -*-

import requests
import json
import urllib
from bs4 import BeautifulSoup
import xbmc
import xbmcaddon
import xbmcvfs
from helpers import http, player, storage

_addon = xbmcaddon.Addon('plugin.video.vod')
#_url = _addon.getSetting('sratimUrl')
_url = 'https://104.21.235.62'

_urlAPI = _url.replace('https://', 'https://api.') 
_urlImg = _url.replace('https://', 'https://static.')

_timeout = 4
	

def categories():
	return [
		{'id': '38', 'title': 'ילדים'},
		{'id': '3', 'title': 'פעולה'},
		{'id': '1', 'title': 'קומדיה'},
		{'id': '19', 'title': 'פשע'},
		{'id': '5', 'title': 'מדע בדיוני'}
	]

	
	
def catalog(category, page):
	s = http.initSession()
	perPage = 50
	params = {
		'loadMore': perPage,
		'start': ((page-1)*perPage)+1,
		'search[genre][]': category,
		'search[from]': '',
		'search[to]': '',
		'search[order]': '',
		'search[dir]': ''
	}
	soup = BeautifulSoup(s.get(_url + '/ajax/movies', params=params, headers={'Referer': _url, 'Host': 'sratim.tv'}, timeout=_timeout).text, "html.parser")
	
	result = []
	for tag in soup.find_all('div', class_='block-wrapper'):
		item = {
			'id': tag.find('div', class_="hover-wrapper").find('a')['href'].split('/')[-1],
			'title': tag.find('div', class_="block-title").find('h3').get_text().lstrip().rstrip(),	
			'poster': 'https:' + tag.find('img')['src'],						
			'episodes': False
		}
		try:
			item['title'] = item['title'] + ' / ' + tag.find('div', class_="block-title").find('small').get_text().lstrip().rstrip()
		except:
			None		
		try:
			item['year'] = tag.find('div', class_='rating-wrapper').find('span').get_text() 
		except:
			None
		try:
			item['description'] = tag.find('div', class_="body-hover").find('p').get_text().replace('קרא עוד','').lstrip().rstrip()
		except:
			None	
		result.append(item)
	
	pages = 10
	if len(result) < perPage:
		pages=page
	return {'content': result, 'pages': pages}
	
	
	
	
	
def search(text):
	s = http.initSession()
	reply = s.post(_urlAPI + '/movie/search', data={'term': text}, headers={'Referer': _url}, timeout=_timeout).json()	
	if 'results' not in reply.keys():
		return []
	result = []
	for r in reply['results']:
		item = {
			'id': r['id'],
			'title': r['name'],	
			'poster': _urlImg + '/movies/' + r['id'] + '.jpg',						
			'episodes': False
		}
		result.append(item)
	return result

	
	
	
def stream(content):
	s = http.initSession()	
	s.get(_url, timeout=_timeout)
	token = s.get(_urlAPI + '/movie/preWatch', headers={'Referer': _url+ '/'}, timeout=_timeout ).text	
	
	if not player.waitForDelay(30, 'Waiting for stream', _addon.getAddonInfo('icon')):
		raise Exception('Stream wait aborted')		
	
	data = s.get(_urlAPI + '/movie/watch/id/' + content['id'] + '/token/' + token, headers={'Referer': _url+ '/'}, timeout=_timeout ).json()	
	error = None
	if 'error' in list(data.keys()):
		error = data['error']
	if 'errors' in list(data.keys()):
		error = data['errors'][0]
	if error is not None:
		if '.' in error:
			error = error.split('.')[0]
		raise Exception(error)
	resolution = sorted(list(data['watch'].keys()))[-1]			
	url = 'https:' + data['watch'][resolution]
	
#	url = 'https:{}|Cookie=Sratim={}&Referer={}'.format(data['watch'][resolution], urllib.parse.quote(s.cookies.get_dict()['Sratim'], safe=''), _url + '/')		
	return {
		'quality': '480p',
#		'url': url + '|' + urllib.parse.urlencode({'Referer': _url}) + '&Cookie=' + urllib.parse.urlencode(s.cookies).replace('&', '; ')
		'url': url,
		'cookies': s.cookies.get_dict(),
		'headers': {'Referer': _url}
	}
	
	



	
	

	
	

