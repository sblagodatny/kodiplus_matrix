# -*- coding: utf-8 -*-

import requests
from bs4 import BeautifulSoup
from helpers import http, gui, storage
import json

import importlib
cdn = importlib.import_module('.' , 'cdn')



_url = 'https://sflix.is'

_timeout = 4


	
	
def search(criteria):		
	if 'English' not in criteria['title'].keys():
		return []
	s = http.initSession()
	result = []	
	content = BeautifulSoup(s.get(_url + '/search/' + criteria['title']['English'].replace(' ','-'), headers={'Referer': _url + '/'}, timeout=_timeout).text,  "html.parser")
	for tag in content.find_all('div', class_='flw-item'):	
		try:
			item = {
				'title': tag.find('h2', class_="film-name").find('a')['title'],				
				'poster': tag.find('img')['data-src'],
				'url': tag.find('h2', class_="film-name").find('a')['href']
			}			
		except:
			continue
		item['episodes'] = '/tv/' in item['url']		
		try:
			year = tag.find('div', class_="fd-infor").find_all('span', class_='fdi-item')[0].get_text().lstrip().rstrip()			
			year = int(year)
			item['year'] = str(year)
		except:
			None
		result.append(item)					
	return [item for item in result if item['episodes'] is (criteria['type']=='tv')]
	

def seasons(content):
	s = http.initSession()
	data = s.get(_url + '/ajax/season/list/' + content['url'].split('-')[-1], headers = {'Referer': content['url']}, timeout=_timeout).text			
	data = BeautifulSoup(data,  "html.parser")			
	result = []	
	for tag in data.find_all('a', class_='ss-item'):					
		result.append({
			'id': tag.get_text().rstrip().lstrip().split(' ')[-1],
			'title': tag.get_text().rstrip().lstrip(),
			'data-id': tag['data-id']
		})
	return result
	
	
def episodes(content, season):	
	s = http.initSession()	
	data = s.get(_url + '/ajax/season/episodes/' + season['data-id'], headers = {'Referer': content['url']}, timeout=_timeout).text
	data = BeautifulSoup(data,  "html.parser")	
	result = []	
	for tag in data.find_all('div', class_="episode-item"):		
		result.append({
			'id': tag.find('div', class_="episode-number").get_text().lstrip().rstrip().split(' ')[1].split(':')[0],
			'title': tag.find('h3', class_="film-name").find('a')['title'],
			'data-id': tag['data-id'],
			'thumb': tag.find('img')['src']
		})	
	return result

		
def stream(content, season=None, episode=None):	
	s = http.initSession()	
	
	### Get servers ###
	servers = []
	if episode is None:		
		data = s.get(_url + '/ajax/episode/list/' + content['url'].split('-')[-1], headers = {'Referer': content['url']}, timeout=_timeout).text
	else:
		data = s.get(_url + '/ajax/episode/servers/' + episode['data-id'], headers = {'Referer': content['url']}, timeout=_timeout).text	
	data = BeautifulSoup(data, "html.parser")	
	for tag in data.find_all('li'):
		server = {'title': tag.find('span').get_text(), 'id': tag.find('a')['data-id']}
		servers.append(server)	
	
	### Get embedUrl for each server ###
	import concurrent.futures	
	def task(server):					
		st = http.initSession()	
#		data = st.get(_url + '/ajax/sources/' + server['id'], headers = {'Referer': content['url'] + '.' + server['id']}, timeout=_timeout).text
		data = st.get(_url + '/ajax/episode/sources/' + server['id'], headers = {'Referer': content['url'] + '.' + server['id']}, timeout=_timeout).text
		server['embedUrl'] = json.loads(data)['link']	
	ex = concurrent.futures.ThreadPoolExecutor(max_workers=10)		
	for server in servers:			
		ex.submit(task, server)		
	ex.shutdown(wait=True)		
	
	
	
	### Get streams for each embedUrl ###
	streams = cdn.parse([server['embedUrl'] for server in servers], referer=_url +'/')
	
	### Build result ###
	result = []
	for server in servers:
		if server['embedUrl'] in streams.keys():
			streamsForEmbedUrl = streams[server['embedUrl']]
			if not isinstance(streamsForEmbedUrl, list):	
				streamsForEmbedUrl['title'] = server['title']
				result.append(streamsForEmbedUrl)
			else:
				for stream in streamsForEmbedUrl:
					stream['title'] = server['title'] + ' ' + stream['title']
				result = result + streamsForEmbedUrl		
	
	return result		

