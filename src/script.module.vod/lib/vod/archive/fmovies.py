# -*- coding: utf-8 -*-

import requests
import json
import urllib
import base64
import time
from bs4 import BeautifulSoup
import xbmcgui
import re
import xbmc
import xbmcaddon
import sys
from lib import jsunpack


from helpers import http, media, storage

_url = 'https://fmovies.to'

_timeout = 4


	

def categories():
	return []
	
	
def catalog(category, page):	
	return {'content': [], 'pages': 1}


def search(text, type):		
	s = http.initSession()
	result = []	
	data = s.get(_url + '/ajax/film/search', params={'keyword': text}, headers={'Referer': _url}).json()
	content = BeautifulSoup(data['result']['html'], "html.parser")	
	for tag in content.find_all('a', class_="item"):				
		meta = tag.find('div', class_='meta').find_all('span', class_='dot')                                                        
		result.append({
			'id': tag['href'].split('-')[-1],
			'url': tag['href'],
			'title': tag.find('div', class_='name').get_text().lstrip().rstrip(),
			'poster': tag.find('img')['src'].replace('-w100',''),							
			'year': meta[1].get_text().lstrip().rstrip(),
			'episodes': meta[0].get_text().lstrip().rstrip() != 'Movie'	
		})
	return [item for item in result if item['episodes'] is (type=='tv')]



def getEpisodesSoup(url):
	s = http.initSession()		
	linkId = BeautifulSoup(s.get(_url + url, timeout=_timeout).text, "html.parser").find('div', class_='watch')['data-id']
	
#	storage.log(linkId)
#	storage.log(getVerid(linkId))

	vrf = 'QA+~PRjFSB,;'
	
#	data = s.get(_url + '/ajax/episode/list/' + linkId, headers={'Referer': _url + url}, params={'vrf': getVerid(linkId)} ).text
	
	data = s.get(_url + '/ajax/episode/list/' + linkId, headers={'Referer': _url + url}, params={'vrf': vrf} ).text
	
	storage.log(data)
	
	data = json.loads(data)
	
	return BeautifulSoup(data['result'], "html.parser")


		
def seasons(content):
	soup = getEpisodesSoup(content['url'])	
	result = []
	for tag in soup.find_all('a', class_='season-item'):
		result.append({
			'id': tag['data-season'],					
			'title': tag.get_text().lstrip().rstrip()			
		})
	return result


	

def episodes(content, season):
	soup = getEpisodesSoup(content['url'])	
	result = []
	for tag in soup.find('ul', {'data-season': season['id']}, class_='episodes').find_all('a'):
		result.append({
			'id': tag['data-num'],
			'title': tag.find_all('span')[-1].get_text().lstrip().rstrip(),
			'linkId': tag['data-id']	
		})
	return result





try:
	import string
	STANDARD_ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='
	CUSTOM_ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='#'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+/='
	ENCODE_TRANS = string.maketrans(STANDARD_ALPHABET, CUSTOM_ALPHABET)
	DECODE_TRANS = string.maketrans(CUSTOM_ALPHABET, STANDARD_ALPHABET)
except:
	STANDARD_ALPHABET = b'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='
	CUSTOM_ALPHABET = b'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='#'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+/='		
	ENCODE_TRANS = bytes.maketrans(STANDARD_ALPHABET, CUSTOM_ALPHABET)
	DECODE_TRANS = bytes.maketrans(CUSTOM_ALPHABET, STANDARD_ALPHABET)

def encode2(input):
	return base64.b64encode(input).translate(ENCODE_TRANS)

def decode2(input):
	try:	
		xx= input.translate(DECODE_TRANS)
	except:
		xx= str(input).translate(DECODE_TRANS)
	return base64.b64decode(xx)

def endEN(t, n) :
    return t + n;

def rLMxL(t, n):
    return t < n;

def VHtgA (t, n) :
    return t % n;

def DxlFU(t, n) :
    return rLMxL(t, n);

def dec2(t, n) :
    o=[]
    s=[]
    u=0
    h=''
    for e in range(256):
        s.append(e)

    for e in range(256):
        u = endEN(u + s[e],ord(t[e % len(t)])) % 256
        o = s[e];
        s[e] = s[u];
        s[u] = o;
    e=0
    u=0
    c=0
    for c in range(len(n)):
        e = (e + 1) % 256
        o = s[e]
        u = VHtgA(u + s[e], 256)
        s[e] = s[u];
        s[u] = o;
        try:
            h += chr((n[c]) ^ s[(s[e] + s[u]) % 256]);
        except:
            h += chr(ord(n[c]) ^ s[(s[e] + s[u]) % 256]);    
    return h

def getVerid(id):
    def convert_func(matchobj):
        m =  matchobj.group(0)
        if m <= 'Z':
            mx = 90
        else:
            mx = 122
        mx2 = ord( m)+ 13  
        if mx>=mx2:
            mx = mx2
        else:
            mx = mx2-26
        gg = chr(mx)
        return gg
    def but(t):
        o=''
        for s in range(len(t)):
            u = ord(t[s]) 
            if u==0:
                u=0
            else:
                if (s % 6 == 1):
                    u += 5
                else:
                    if (s % 6 == 5):
                        u -= 6
                    else:
                        if (s % 6 == 0 or s % 6 == 4):
                            u += 6
                        else:
                            if not (s % 6 != 3 and s % 6 != 2):
                                u -= 5
            o += chr(u) 
        if sys.version_info >= (3,0,0):
            o=o.encode('Latin_1')        
        o = encode2(o)  
        if sys.version_info >= (3,0,0):
            o=(o.decode('utf-8'))
        o = re.sub("[a-zA-Z]", convert_func, o)  
        if sys.version_info >= (3,0,0):
            o=o.encode('Latin_1')        
        o = encode2(o)  
        if sys.version_info >= (3,0,0):
            o=(o.decode('utf-8'))
        return o
    ab = 'DZmuZuXqa9O0z3b7' #####stare
    ab = 'MPPBJLgFwShfqIBx'
    ab = 'rzyKmquwICPaYFkU'
    ac = id
    hj = dec2(ab,ac) #
    if sys.version_info >= (3,0,0):
        hj=hj.encode('Latin_1')
    hj2 = encode2(hj)   
    hj2 = encode2(hj2)   
    if sys.version_info >= (3,0,0):
        hj2=(hj2.decode('utf-8'))
    xc= but(hj2) 
    return xc




def dekoduj(r,o):
	t = []
	e = []
	n = 0
	a = ""
	for f in range(256): 
		e.append(f)
	for f in range(256):
		n = (n + e[f] + ord(r[f % len(r)])) % 256
		t = e[f]
		e[f] = e[n]
		e[n] = t
	f = 0
	n = 0
	for h in range(len(o)):
		f = f + 1
		n = (n + e[f % 256]) % 256
		if not f in e:
			f = 0
			t = e[f]
			e[f] = e[n]
			e[n] = t

			a += chr(ord(o[h]) ^ e[(e[f] + e[n]) % 256])
		else:
			t = e[f]
			e[f] = e[n]
			e[n] = t
			if sys.version_info >= (3,0,0):
				#a += chr((o[h]) ^ e[(e[f] + e[n]) % 256])				
				try:
					a += chr((o[h]) ^ e[(e[f] + e[n]) % 256])#x += chr((n[e])^ i[(i[o] + i[u]) % c] )
				except:
					a += chr(ord(o[h]) ^ e[(e[f] + e[n]) % 256])#x += chr(ord(n[e])^ i[(i[o] + i[u]) % c] )			
			else:
				a += chr(ord(o[h]) ^ e[(e[f] + e[n]) % 256])
	return a


def DecodeLink(mainurl):
	ab = '8z5Ag5wgagfsOuhz'
	ac = decode2(mainurl)
	link = dekoduj(ab,ac)
	link = urllib.parse.unquote(link)
	return link

	
	

def stream(content, season=None, episode=None):			
	if episode is None:
		soup = getEpisodesSoup(content['url'])	
		linkId = soup.find_all('li')[0].find('a')['data-id']
	else:
		linkId = episode['linkId']
	s = http.initSession()	
	servers = []
	data = s.get(_url + '/ajax/server/list/' + linkId, headers={'Referer': _url + content['url']}, params={'vrf': getVerid(linkId)} ).json()	
	for tag in BeautifulSoup(data['result'], "html.parser").find('ul', class_='servers').find_all('li'):
		servers.append({'title': tag.find('span').get_text().lstrip().rstrip(), 'linkId': tag['data-link-id']})
		
	
	server = servers[0]					
	url = s.get(_url + '/ajax/server/' + server['linkId'], headers={'Referer': _url + content['url']}, params={'vrf': getVerid(server['linkId'])} ).json()['result']['url']
	url = DecodeLink(url)			
#	return {'url': server['parser'](url)}
	
	storage.log(url)
	
	raise Exception ('bla bla')


