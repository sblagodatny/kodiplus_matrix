# -*- coding: utf-8 -*-

import json
import urllib
import concurrent.futures
from helpers import storage, http, media
from bs4 import BeautifulSoup


_timeout = 4



def getPlayerUrl(url):
	s = http.initSession()		
	content = BeautifulSoup(s.get(url, timeout=_timeout).text, "html.parser")	
	for tag in content.find_all('a', class_='server'):		
		if tag.get_text().replace(' ','').lower()=='playerf':
			if tag.find('span', class_="text-success") is not None:
				return tag['data-url']
	return None


def getEmbedUrl(tmdbId, seasonId=None, episodeId=None, validate=False):
	params = {'tmdb': tmdbId}
	if seasonId is not None:
		params['season']=seasonId		
		params['episode']=episodeId					
	return 'https://embed.smashystream.com/playere.php?' + urllib.parse.urlencode(params)	
	

def getPlayerData(playerUrl, embedUrl):
	s = http.initSession()		
	data = s.get(playerUrl, headers={'Referer': embedUrl}, timeout=_timeout).text			
	data = json.loads(data)
	if data['sourceUrls'] is None or len(data['sourceUrls']) == 0:
		return None
	return data

def parseSubtitles(subtitleUrls):
	subtitles = []
	if subtitleUrls is None or len(subtitleUrls)==0:
		return subtitles
	if ',' not in subtitleUrls:
		subtitleUrls = [subtitleUrls]
	else:
		subtitleUrls = subtitleUrls.split(',')
	for sub in subtitleUrls:
		if len(sub) == 0:
			continue
		subtitle = {
			'language': sub.split(']')[0].split('[')[1],
			'url': sub.split(']')[1],
			'name': 'Original', 
			'format': 'vtt', 
			'zip': False
		}
		for delimiter in ('-','('):
			if delimiter in subtitle['language']:
				subtitle['name'] = subtitle['name'] + ' ' + subtitle['language'].split(delimiter)[1].lstrip()
				subtitle['language'] = subtitle['language'].split(delimiter)[0].rstrip()
		subtitles.append(subtitle)	
	return subtitles
	
	
def search(criteria):			
	if criteria['type'] == 'tv':
		return[]
	if 'English' not in criteria['title'].keys():
		return[]	
	if 'tmdbId' in criteria.keys():
		result = [{'id': criteria['tmdbId'], 'type': criteria['type'], 'title': criteria['title']['English'], 'poster': criteria['poster']}]
	else:
		from tmdb import api
		result = api.search(None, {'title': criteria['title']['English'], 'type': criteria['type']}, 'en-US')		
	for item in result:
		item['episodes'] = item['type']=='tv'	
	resultfinal = []
	def task(item):							
		item['embedUrl'] = getEmbedUrl(item['id'], 1 if item['type']=='tv' else None, 1 if item['type']=='tv' else None)
		item['playerUrl'] = getPlayerUrl(item['embedUrl'])
		if item['playerUrl'] is not None:			
			item['playerData'] = getPlayerData(item['playerUrl'], item['embedUrl'])
			if not item['playerData'] is None:
				resultfinal.append(item)										
	ex = concurrent.futures.ThreadPoolExecutor(max_workers=10)		
	for item in result:	
		ex.submit(task, item)		
	ex.shutdown(wait=True)	
	return resultfinal


#def seasons(content):
#	result = tmdb.seasons(content['id'])
#	resultfinal = []
#	def task(item):					
#		try:
#			embedUrl = getEmbedUrl(content['id'], item['id'], 1)
#			playerUrl = getPlayerUrl(embedUrl)								
#			resultfinal.append(item)									
#		except:
#			None
#	ex = concurrent.futures.ThreadPoolExecutor(max_workers=10)		
#	for item in result:	
#		ex.submit(task, item)		
#	ex.shutdown(wait=True)	
#	resultfinal = sorted(resultfinal, key=lambda d: d['id']) 		
#	return resultfinal


#def episodes(content, season):		
#	return tmdb.episodes(content['id'], season['id'])	


def stream(content, season=None, episode=None):
#	if episode is None:
	playerUrl = content['playerUrl']
	embedUrl = content['embedUrl']
	data = content['playerData']
#	else:
#		embedUrl = getEmbedUrl(content['id'], season['id'], episode['id'])
#		playerUrl = getPlayerUrl(embedUrl)
#		if playerUrl is None:
#			return None
#		data = getPlayerData(playerUrl, embedUrl)
#		if data is None:
#			return None
	subtitles = parseSubtitles(data.get('subtitleUrls',''))
	s = http.initSession()		
	result = media.parseM3U8(s, data['sourceUrls'][0])	
	result = sorted(result, key=lambda item: int(item['bandwidth']))
#	result = [{'title': item.get('resolution', item['bandwidth']), 'url': item['url'], 'subtitles': subtitles, 'headers': {'Referer': 'https://embed.smashystream.com/','Origin': 'https://embed.smashystream.com/','User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36'}} for item in result]		
	result = [{'title': item.get('resolution', item['bandwidth']), 'url': item['url'], 'headers': {'Referer': 'https://embed.smashystream.com/','Origin': 'https://embed.smashystream.com/','User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36'}} for item in result]		
	from . import common
	return common.select(content, result, 'Streams', 'stream')

	
