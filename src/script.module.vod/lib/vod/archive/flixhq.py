# -*- coding: utf-8 -*-

import requests
from bs4 import BeautifulSoup
from helpers import http, gui, storage
import json




_url = 'https://flixhq.to'

_timeout = 4


	
	
def search(criteria):		
	if 'English' not in criteria['title'].keys():
		return []
	s = http.initSession()
	result = []	
	content = BeautifulSoup(s.get(_url + '/search/' + criteria['title']['English'].replace(' ','-'), headers={'Referer': _url + '/'}, timeout=_timeout).text,  "html.parser")
	for tag in content.find_all('div', class_='flw-item'):	
		try:
			item = {
				'title': tag.find('h2', class_="film-name").find('a')['title'],				
				'poster': tag.find('img')['data-src'],
				'url': tag.find('h2', class_="film-name").find('a')['href']
			}			
		except:
			continue
		item['episodes'] = '/tv/' in item['url']		
		try:
			year = tag.find('div', class_="fd-infor").find_all('span', class_='fdi-item')[0].get_text().lstrip().rstrip()			
			year = int(year)
			item['year'] = str(year)
		except:
			None
		result.append(item)		
	return [item for item in result if item['episodes'] is (criteria['type']=='tv')]
	

def seasons(content):
	s = http.initSession()
	referer = _url + '/watch-' + content['url'][1:]
	data = s.get(_url + '/ajax/v2/tv/seasons/' + content['url'].split('-')[-1], headers = {'Referer': referer}, timeout=_timeout).text
	data = BeautifulSoup(data,  "html.parser")
	result = []	
	for tag in data.find_all('a', class_='ss-item'):			
		result.append({
			'id':  tag.get_text().split(' ')[-1],
			'title': tag.get_text(),
			'data-id': tag['data-id']
		})			
	return result
	
	
def episodes(content, season):	
	s = http.initSession()	
	referer = _url + '/watch-' + content['url'][1:]
	data = s.get(_url + '/ajax/v2/season/episodes/' + season['data-id'], headers = {'Referer': referer}, timeout=_timeout).text
	data = BeautifulSoup(data,  "html.parser")
	result = []	
	for tag in data.find_all('li', class_="nav-item"):		
		result.append({
			'id': tag.find('strong').get_text().split(' ')[1].split(':')[0],
			'title': tag.find('a')['title'],
			'data-id': tag.find('a')['data-id']
		})	
	return result

		
def stream(content, season=None, episode=None):	
	s = http.initSession()	
	servers = []
	referer = _url + '/watch-' + content['url'][1:]
	if episode is None:		
		data = s.get(_url + '/ajax/movie/episodes/' + content['url'].split('-')[-1], headers = {'Referer': referer}, timeout=_timeout).text
		data = BeautifulSoup(data, "html.parser")	
		for tag in data.find_all('li'):
			servers.append({'title': tag.find('a')['title'], 'id': tag.find('a')['data-linkid']})	
	else:
		data = s.get(_url + '/ajax/v2/episode/servers/' + episode['data-id'], headers = {'Referer': referer}, timeout=_timeout).text				
		data = BeautifulSoup(data, "html.parser")	
		for tag in data.find_all('li'):
			servers.append({'title': tag.find('a')['title'].replace('Server ',''), 'id': tag.find('a')['data-id']})	
	
	servers_=[]
	for server in servers:
		if server['title'].lower() in ['mixdrop','vidcloud']:
			servers_.append(server)
	
	from . import common
	server = common.select(content, servers_, 'Servers', 'cdn')
	if server is None:
		return None
	data = s.get(_url + '/ajax/sources/' + server['id'], headers = {'Referer': referer + '.' + server['id']}, timeout=_timeout).text
	embedUrl = json.loads(data)['link']	


	data = s.get('https://api.consumet.org/meta/tmdb/watch/' + content['url'].split('-')[-1], params={'id': content['url'][1:]}).text			
	storage.log(data)

	
#	from . import cdn
#	parser = cdn.get(server['title'].lower())
#	data = parser.parse(embedUrl, _url)
#	storage.log(json.dumps(data))
	
#	server = servers[2]	
#	data = s.get(_url + '/ajax/sources/' + server['id'], headers = {'Referer': referer + '.' + server['id']}, timeout=_timeout).text
#	embedUrl = json.loads(data)['link']		
#	return cdn.get('vidcloud').parse(embedUrl, None)
#	return cdn.get(cdn.match(embedUrl)).parse(embedUrl, None)
	
#	result = []
#	for server in servers:
#		data = s.get(_url + '/ajax/sources/' + server['id'], headers = {'Referer': referer + '.' + server['id']}, timeout=_timeout).text
#		embedUrl = json.loads(data)['link']	
#		streams = cdn.get(cdn.match(embedUrl)).parse(embedUrl, None)
#		if not isinstance(streams, list):	
#			streams['title'] = server['title']
#			result.append(streams)
#		else:
#			for stream in streams:
#				stream['title'] = server['title'] + ' ' + stream['title']
#			result = result + streams						
#	return result		
	raise Exception('bla bla')
	


### Use ConsumeT API ###

#	data = s.get('https://api.consumet.org/meta/tmdb/watch/' + content['url'].split('-')[-1], params={'id': content['url'][1:]}).text			
#	data = json.loads(data)
#	streams = []
#	for item in data['sources']:
#		if item['quality'] == 'auto':	
#			continue
#		streams.append({'title': item['quality'], 'url': item['url']})
#	subtitles = [sb for sb in data['subtitles'] if 'english' in sb['lang'].lower() or 'russian' in sb['lang'].lower() or 'hebrew' in sb['lang'].lower()]
#	for stream in streams:
#		stream['subtitles']=subtitles
#	streams.reverse()
#	return streams

