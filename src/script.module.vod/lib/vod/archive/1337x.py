# coding: utf-8


from bs4 import BeautifulSoup
import requests
import xbmcaddon
import datetime
import os
import xbmcgui
import re
import json

from helpers import http, storage

_addon = xbmcaddon.Addon('plugin.video.torrents')
_pathAddon = _addon.getAddonInfo('path')
_pathSettings = _pathAddon.replace('addons','userdata/addon_data')
_pathTorrents = _pathSettings + '/torrents'


_url = 'https://1337x.to'
_title = '1337x'
_timeout = 4

		
def search (text, type='movie'):
	session = http.initSession()	
	stype = 'Movies' if type=='movie' else 'TV'
	data = BeautifulSoup(session.get (_url + '/category-search/{}/{}/1/'.format(text.replace(' ','+'), stype), timeout=_timeout).content, "html.parser")				
	result = []	
	for tag in data.find('tbody').find_all('tr'):		
		try:			
			torrent = {
				'id': tag.find('td', class_="name").find_all('a')[1]['href'].split('/')[2],
				'url': tag.find('td', class_="name").find_all('a')[1]['href'],
				'name': tag.find('td', class_="name").get_text(),
				'size': tag.find('td', class_="size").get_text(),
				'seeds': int(tag.find('td', class_="seeds").get_text()),
				'description': '',
				'poster': '',
				'date': '',
				'quality': ''									
			}
			torrent['size'] = torrent['size'][:-1*len(str(torrent['seeds']))]
		except Exception as e:
			storage.log(str(e))
			continue
		if torrent['seeds'] == 0:
			continue	
		result.append(torrent)		
	return result

	
	
def getTorrent(torrent):
	session = http.initSession()
	magnet = 'magnet:' + re.search(r'"magnet:(.*?)"', session.get(_url + torrent['url']).text).group(1)		
	torrent['hash'] = magnet.split('btih:')[1].split('&')[0]	
	torrent['file'] = _pathTorrents + '/' + torrent['hash'] + '.torrent'		
	if not os.path.exists(torrent['file']):			
		storage.removeOldFiles(_pathTorrents)
		r = session.get('http://itorrents.org/torrent/' + torrent['hash'] + '.torrent', timeout=_timeout)	
		with open(torrent['file'], 'wb') as f:
			f.write(r.content)				
	return







