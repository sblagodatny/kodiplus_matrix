# -*- coding: utf-8 -*-

import requests
import json
import urllib
import base64
import time
from bs4 import BeautifulSoup
import xbmcgui
import re
import xbmc
import xbmcaddon

from helpers import http, media, storage

_addon = xbmcaddon.Addon('plugin.video.vod')
#_url = _addon.getSetting('moviesjoyUrl')
_url = 'https://moviesjoy.is'

_timeout = 4


	

def categories():
	return []
	
	
def catalog(category, page):	
	return {'content': [], 'pages': 1}


def search(text):	
	s = http.initSession()
	result = []
	content = BeautifulSoup(s.post(_url + '/ajax/search', data={'keyword':text}, headers={'Referer':_url + '/search/' + text.replace(' ','-')} , timeout=_timeout).text, "html.parser")
	for tag in content.find_all('a', class_="nav-item"):		
		try:
			item = {			
				'id': tag['href'].split('-')[-1],
				'url': tag['href'],
				'title': tag.find('h3', class_="film-name").get_text(),
				'poster': tag.find('img')['src'].replace('188x288','376x576'),							
				'year': tag.find('div', class_="film-infor").find_all('span')[0].get_text(),
				'episodes': tag.find('div', class_="film-infor").find_all('span')[2].get_text()	!= 'Movie'				
			}
			if item['episodes'] is True:
				item['year'] = ''
			result.append(item)
		except:
			None
	return result
	
	

		
def seasons(content):
	s = http.initSession()	
	id = 1
	result = []
	data = BeautifulSoup(s.get(_url + '/ajax/season/list/' + content['id'], headers={'Referer': content['url']}, timeout=_timeout).text, "html.parser")
	for item in data.find_all('a', class_='ss-item'):
		result.append({
			'id': id,					
			'title': item.get_text(),			
			'dataId': item['data-id']
		})
		id = id + 1
	return result

	

def episodes(content, season):
	s = http.initSession()	
	data = BeautifulSoup(s.get(_url + '/ajax/season/episodes/' + season['dataId'], timeout=_timeout).text, "html.parser")	
	id = 1
	result = []
	for item in data.find_all('li', class_="nav-item"):
		result.append({
			'id': id,
			'title': item.find('a')['title'],			
			'dataId': item.find('a')['data-id']
		})
		id = id + 1
	return result
	
#def token(s):
#	return http.resolveGoogleCaptcha(s, '6LegO6AaAAAAAGzQq4XIIS-HChM4preVV0kH4PDB', 'aHR0cHM6Ly9tb3ZpZXNqb3kudG86NDQz', 'wxAi4AKLXL2kBAvXqI4XLSWS')	
	
def stream(content, season=None, episode=None):			
	s = http.initSession()	
	if content['episodes'] is True:
		servers = BeautifulSoup(s.get(_url + '/ajax/v2/episode/servers/' + episode['dataId'], timeout=_timeout).text, "html.parser")
	else:
		servers = BeautifulSoup(s.get(_url + '/ajax/movie/episodes/' + content['id'], timeout=_timeout).text, "html.parser")


	### Streamlare ###
	id = None
	for tag in servers.find_all('li'):	
		if 'Streamlare' in tag.find('span').get_text():
			try:
				id = tag.find('a')['data-linkid']
			except:
				id = tag.find('a')['data-id']
	if id is None:
		raise Exception ('Streamlare server not available')		
	data = s.get(_url + '/ajax/sources/' + id).json()		
	url = 'https://sltube.org' ## Take from redirect s.get(data['link'])
	id = data['link'].split('/')[-1]	
	data = s.post(url + '/api/video/stream/get', json={'id': id}).json()
	if data['status'] != 'success':
		raise Exception ('Stream not available')
	file = list(data['result'].keys())[0] ## Fix: multi files ??
	headers = {
		'Referer': url + '/e/' + id,
		'User-Agent': s.headers['User-Agent']
	}
	return {
		'quality': data['result'][file]['label'],
		'url': data['result'][file]['file'] + '|' + urllib.parse.urlencode(headers)
	}
		
		
		
		
		
		
		
		
#	https://moviesjoy.to/ajax/sources/9224434	
		

#	storage.log(json.dumps(data))

#	raise Exception ('bla bla')

#	url = s.get(_url + '/ajax/get_link/' + id, params={'_token': token(s)}, timeout=_timeout).json()['link']	
#	id = url.split('/')[-1].split('?')[0]
#	url = 'https://' + url.split('/')[2] + '/ajax/embed-4/getSources'	
#	params = {'id': id,	'_token': token(s)}


#	data = s.get(url, params=params, headers={'X-Requested-With': 'XMLHttpRequest'}, timeout=_timeout).json()
	
	
	
#	sources = json.loads(util.b64decode(data['sources']))
#	storage.log(json.dumps(data))
	
	

#	raise Exception('Stream not available')

#	urlFinal = None
#	for i in range(3):
#		try:
#			urlFinal = s.get(url, params=params, headers={'X-Requested-With': 'XMLHttpRequest'}, timeout=_timeout).json()['sources'][0]['file']
#			urlFinal = s.get(url, params=params, headers={'X-Requested-With': 'XMLHttpRequest'}, timeout=_timeout).json()
#			
#			util.log(json.dumps(urlFinal))
#			
#			urlFinal = urlFinal['sources'][0]['file']
#			
#			
#			break
#		except:
#			xbmc.sleep(500)
#	if urlFinal is None:
#		raise Exception('Stream not available, please retry')
	
#	return media.parseM3U8(s, urlFinal)
	
	
	

	

def info(content):
	return {		
		'title': '',
		'titleOriginal': '',		
		'poster': '',
		'plot': '',
		'year': '',
		'rating': '',
		'country': '',
		'genre': ''
	}


	
	

	
	

