# -*- coding: utf-8 -*-

import requests
import json
import urllib
from bs4 import BeautifulSoup
import random
import re
import concurrent.futures

from helpers import storage, http, gui, media


_url = 'https://kinokong.pro'

_timeout = 4
	
	
def search(criteria):		
	if 'Russian' not in criteria['title'].keys():
		return []
	result = []
	s = http.initSession()
	try:
		data = {'subaction': 'search', 'story': criteria['title']['Russian'].replace(' ','%20').replace('ё','е').encode('cp1251')	} 
	except Exception as e:
		return result
	soup = BeautifulSoup(s.post(_url + '/index.php', params={'do': 'search'}, data=data, timeout=_timeout).text, "html.parser")			
	for tag in soup.find_all('div', class_='item'):
		try:
			item={
				'id': tag.find('div', class_='main-sliders-title').find('a')['href'].split('/')[-1].split('-')[0],
				'url': tag.find('div', class_='main-sliders-title').find('a')['href'], 
				'title': tag.find('div', class_='main-sliders-title').find('a').get_text().split('(')[0].rstrip(),
				'poster': tag.find('img')['src'],			
				'year': tag.find('div', class_='main-sliders-title').find('a').get_text().split('(')[1].split(')')[0],
				'episodes': tag.find('div', class_='main-sliders-season') is not None
			}
			if '(' in item['title']:
				item['title'] = item['title'].split('(')[0].rstrip()				
			if item['poster'].startswith('/'):
				item['poster'] = _url + item['poster']	
			try:
				if '-' in item['year']:
					item['year'] = item['year'].split('-')[0]
				item['year']=str(int(item['year']))				
				if len(item['year'])!=4:
					del item['year']
			except:					
				del item['year']		
		except:
			continue
		
		result.append(item)	
	return [item for item in result if item['episodes'] is (criteria['type']=='tv')]
		
	
def getSourceData_cdn(content):
	url = getContentSource(content, 'cdn')
	s = http.initSession()	
	soup = BeautifulSoup(s.get(url, headers={'Referer': _url +'/'}, timeout=_timeout).text, "html.parser")			
	files = json.loads(soup.find('input', {'id':'fs'})['value'])
	translations = []
	ttag = soup.find('div', class_="translations")
	if ttag is not None:	
		for tag in ttag.find_all('option'):
			translations.append({'id': tag['value'], 'title': tag.get_text().lstrip().rstrip()})
	if len(translations)==0:
		translations.append({'id': list(files.keys())[0], 'title': 'Original'})	
	from . import common
	translation = common.select(content, translations, 'Translations', 'translation')
	if translation is None:
		return None	
	data = files[translation['id']]
	if isinstance(data, str): # movie
		streams = []
		for sdata in data.split(','):
			stream = {'title': sdata.split(']')[0].split('[')[1], 'url': 'https:' + sdata.split(']')[1] + ':hls:manifest.m3u8' }
			streams.append(stream)
		return streams			
	else: ## series			
		seasons = []
		for sndata in data:				
			season = {'id': sndata['id'], 'title': sndata['comment'].lstrip().rstrip(), 'episodes': []}
			for edata in sndata['folder']:
				episode = {'id': edata['id'], 'title': edata['comment'].lstrip().rstrip(), 'thumb': 'https:' + edata['poster'], 'streams': []}
				if '_' in episode['id']:
					episode['id'] = episode['id'].split('_')[1]
				for sdata in edata['file'].split(','):
					stream = {'title': sdata.split(']')[0].split('[')[1], 'url': 'https:' + sdata.split(']')[1] + ':hls:manifest.m3u8' }
					episode['streams'].append(stream)
				season['episodes'].append(episode)
			seasons.append(season)
		return seasons	


# player: cdn, HDVB, Collaps
def getContentSource(content, player):
	s = http.initSession()	
	soup = BeautifulSoup(s.get(content['url'], timeout=_timeout).text, "html.parser")		
	for tag in soup.find('ul', class_='newTabs').find_all('li'):
		if tag.get_text() == player:
			url = tag['data-iframe']	
			if not url.startswith('https:'):
				url = 'https:' + url
			return url
	return None
	

	
def seasons(content):
	data = getSourceData_cdn(content)
	if data is None:
		return []
	return data
		
	
def episodes(content, season):	
	return season['episodes']

	
def stream(content, season=None, episode=None):		
	from helpers import media
	if episode is not None:						
		streams = episode['streams']
	else:
		streams = getSourceData_cdn(content)
	if streams is None:
		return None
	from . import common
	return common.select(content, streams, 'Streams', 'stream')



	
	

