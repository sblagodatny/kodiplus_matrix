# -*- coding: utf-8 -*-

import json
import urllib
import concurrent.futures

from helpers import storage, http

import importlib
tmdb = importlib.import_module('.' , 'tmdb')
cdn = importlib.import_module('.' , 'cdn')

_timeout = 4


def getEmbedUrl(tmdbId, seasonId=None, episodeId=None, validate=False):
	url = 'https://vidsrc.me/embed/{}'.format(tmdbId)
	if seasonId is not None:
		url = url + '/{}-{}'.format(seasonId, episodeId)
	if validate is False:
		return url		
	s = http.initSession()	
	content = s.get(url).text		
	if len(content) < 100 or '404 Not Found' in content:
		return None
	return url
		
	
	
def search(criteria):	
	if 'tmdbId' in criteria.keys():
		result = [tmdb.get(criteria['tmdbId'], criteria['type'])]
	elif 'English' in criteria['title'].keys():
		result = tmdb.search(criteria['title']['English'], criteria['type'])		
	else:
		return []
	resultfinal = []
	def task(item):					
		season = None
		if item['episodes'] is True:
			if 'season' in criteria.keys():
				season = criteria['season']
			else:
				season = 1
		if getEmbedUrl(item['id'], season, 1 if item['episodes'] is True else None, validate=True) is not None:
			resultfinal.append(item)						
	ex = concurrent.futures.ThreadPoolExecutor(max_workers=10)		
	for item in result:	
		ex.submit(task, item)		
	ex.shutdown(wait=True)	
	return resultfinal


def seasons(content):
	result = tmdb.seasons(content['id'])
	resultfinal = []
	def task(item):									
		if getEmbedUrl(content['id'], item['id'], 1, validate=True) is not None:						
			resultfinal.append(item)									
	ex = concurrent.futures.ThreadPoolExecutor(max_workers=10)		
	for item in result:	
		ex.submit(task, item)		
	ex.shutdown(wait=True)	
	resultfinal = sorted(resultfinal, key=lambda d: d['id']) 		
	return resultfinal


def episodes(content, season):		
	return tmdb.episodes(content['id'], season['id'])	


def stream(content, season=None, episode=None):
	parser = cdn.get('vidsrc')
	if episode is not None:
		return parser.parse(getEmbedUrl(content['id'], season['id'], episode['id']), None)			
	else:
		return parser.parse(getEmbedUrl(content['id']))			
	
	
