# -*- coding: utf-8 -*-

from . import common

import requests
import json
import urllib
from bs4 import BeautifulSoup
import random
import re
import concurrent.futures

from helpers import storage, http, gui, media


_url = 'https://kinokong.sk'

_timeout = 4


#def getYear(url):
#	s = http.initSession()
#	content = s.get(url, timeout=_timeout).text	
#	try:
#		year = str(int(re.search(r'Год:</span> <b>(.*?)</b>', content).group(1)))
#		return year
#	except Exception as e:		
#		return None
	
	
def search(criteria):		
	if 'ru-RU' not in criteria['titles'].keys():
		return []
	result = []
	s = http.initSession()
	try:
		data = {'subaction': 'search', 'story': criteria['titles']['ru-RU'].replace(' ','%20').replace('ё','е').encode('cp1251')	} 
	except Exception as e:
		return result
	soup = BeautifulSoup(s.post(_url + '/index.php', params={'do': 'search'}, data=data, timeout=_timeout).text, "html.parser")			
	for tag in soup.find_all('div', class_='item'):		
		
		try:
			item={
				'id': tag.find('div', class_='main-sliders-title').find('a')['href'].split('/')[-1].split('-')[0],
				'url': tag.find('div', class_='main-sliders-title').find('a')['href'], 
				'title': tag.find('div', class_='main-sliders-title').find('a').get_text(),
				'poster': tag.find('img')['src'],			
				'type': 'tv' if tag.find('div', class_='main-sliders-season') is not None else 'movie'
			}
		except:
			continue

		if '(' in item['title']:			
			try:
				item['year'] = str(int(item['title'].split('(')[1].split(')')[0].lstrip().rstrip()))
			except:
				None			
			item['title'] = item['title'].split('(')[0].rstrip()
		if item['poster'].startswith('/'):
			item['poster'] = _url + item['poster']				
		result.append(item)	
		
	return [item for item in result if item['type'] == criteria['type']]
		



def getStream_hdvb(data):	
	s = http.initSession()		
	data = s.get(data['url'], headers=data['headers'], timeout=_timeout).text
	return data

	
def getSourceData_hdvb(content):
	url = getContentSource(content)	
	s = http.initSession()		
	data = s.get(url, headers={'Referer': _url +'/'}, timeout=_timeout).text	
	data = re.search(r'playerConfigs = (.*?);', data).group(1)			
	data = json.loads(data)	
	urlBase = 'https://' + url.split('/')[2]
	headers = {
		'origin': '[url]' + urlBase + '[/url]',
		'referer': '[url=' + url + ',]' + url + ',[/url]',
		'x-csrf-token': data['key']
	}
	if '/playlist/' not in data['file']:
		url=urlBase + '/playlist/' + data['file'] + '.txt'		
		data = s.get(url, headers=headers, timeout=_timeout).text
		return {'url': data}
		
	else:		
		url = urlBase + data['file']
		headers = {
			'origin': '[url]' + urlBase + '[/url]',
			'referer': '[url=' + url + ',]' + url + ',[/url]',
			'x-csrf-token': data['key']
		}
		data = json.loads(s.get(url, headers=headers, timeout=_timeout).text)			
		
		translations = []
		for sndata in data:	
			for edata in sndata['folder']:
				for sdata in edata['folder']:
					try:
						translator = {'id': sdata['translator'], 'title': sdata['title']}
						if translator not in translations:
							translations.append(translator)
					except:
						None								
		translation = common.select(content, translations, 'Translations', 'translation')		
		
		seasons = []
		for sndata in data:				
			season = {'id': sndata['id'], 'title': sndata['title'], 'episodes': []}
			for edata in sndata['folder']:											
				streams = [stream for stream in edata['folder'] if isinstance(stream, dict) and stream.get('translator',-1)==translation['id'] ]
				if len(streams) == 0:
					continue	
				episode = {
					'id': edata['id'], 
					'title': edata['title'], 					
					'streamsData': {'url': urlBase + '/playlist/' + streams[0]['file'] + '.txt', 'headers': headers}					
				}
				if '-' in episode['id']:
					episode['id'] = episode['id'].split('-')[1]
				season['episodes'].append(episode)
			if len(season['episodes'])==0:
				continue
			seasons.append(season)	
		return seasons


def getContentSource(content):
	s = http.initSession()	
	soup = BeautifulSoup(s.get(content['url'], timeout=_timeout).text, "html.parser")			
	for tag in soup.find('ul', class_='newTabs').find_all('li'):
		if tag.get_text() == 'HDVB':
			return tag['data-iframe']	
	raise Exception('No available options found for Servers')
	
	
def seasons(content):
	return getSourceData_hdvb(content)
		
	
def episodes(content, season):	
	return season['episodes']

	
def stream(content, season=None, episode=None):		
	from helpers import media
	if episode is not None:						
		url = getStream_hdvb(episode['streamsData'])		
	else:
		url = getSourceData_hdvb(content)['url']		
	s = http.initSession()	
	result = media.parseM3U8(s.get(url, timeout=_timeout).text, url)			
	result = sorted(result, key=lambda item: int(item['bandwidth']))
	result = [{'title': item.get('resolution', item['bandwidth']), 'url': item['url']} for item in result]
	result.reverse()
	from . import common
	return common.select(content, result, 'Streams', 'stream')


	
	

