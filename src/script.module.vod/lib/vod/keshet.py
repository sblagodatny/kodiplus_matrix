# -*- coding: utf-8 -*-



import requests
import json
import xbmcaddon

_url = 'https://www.mako.co.il'
_buildId = '5.16.0'
_timeout = 4

from helpers import http, media, storage	


	
def search(criteria):
	if criteria['type'] != 'tv':
		return[]
	if 'he-IL' not in criteria['titles'].keys():
		return []
	if 'עִבְרִית' not in criteria.get('language',['עִבְרִית']):
		return []
	result = []
	s = http.initSession()
	params={
		'jspName': 'vodSolrSearch.jsp',
		'platform': 'responsive',
		'q': criteria['titles']['he-IL'],
	}
	try:
		data = json.loads(s.get('{}/AjaxPage'.format(_url), params=params, timeout=_timeout).text)		
		if data['result'][0]['makoRefComp'] != 'Program_Slider':
			raise
	except:
		return result			
	for item in data['result'][0]['items']:			
#		id = item['pageUrl'].split('/')[-1]
#		if '-' in id:
#			id = id.split('-')[0]		
		result.append({			
			'id': item['pageUrl'].split('/')[-1],
			'title': item['title'],
			'poster': item['pics'][0]['picUrl'],
			'plot': '',		
			'url': item['pageUrl'],
			'type': 'tv'
		})
	return result



	
def seasons(content):
	import json
	result = []
	s = http.initSession()			
	params = {
		'mako_vod_channel': 'mako-vod-keshet',
		'program': content['id']
	}	
	data = json.loads(s.get('{}/_next/data/{}/mako-vod-keshet/{}.json'.format(_url, _buildId, content['id']), params=params, timeout=_timeout).text)				
	for item in data['pageProps']['data']['seasons']:			
		try:
			id = int(item['pageUrl'].split('-')[-1][1:])
		except:
			id = -1
		
		resultitem = {
			'id': id,
			'title': item['seasonTitle'],
			'slug': item['pageUrl'].split('/')[-1]
		}				
		result.append(resultitem)		
	return result	
		

def episodes(content, season):
	import json
	result = []
	s = http.initSession()
	params = {
		'mako_vod_channel': 'mako-vod-keshet',
		'program': season['slug']
	}	
	data = json.loads(s.get('{}/_next/data/{}/mako-vod-keshet/{}.json'.format(_url,_buildId,season['slug']), params=params, timeout=_timeout).text)					
	for item in data['pageProps']['data']['menu'][0]['vods']:
		
		try:
			id = int(item['extraInfo'].split('@')[0].split('פרק')[1].lstrip().rstrip())
		except:
			id = -1
		
		if 'title' not in item.keys():
			continue
		
		result.append({
			'id': id,
			'title': item['extraInfo'].replace('@',' : ') + ' : ' + item['title'],
			'thumb': item['pics'][0]['picUrl'],
			'plot': '',
			'vcmid': item['itemVcmId'],
			'videoChannelId': item['channelId'],
			'galleryChannelId': item['galleryChannelId']
		})			
		
	if result[0]['id'] > result[-1]['id']:
		result.reverse()			
	return result	
	
	
def stream(content, season, episode):	
	from helpers import media
	s = http.initSession()
	params = {
		'jspName': 'playlist.jsp',
		'vcmid': episode['vcmid'],
		'videoChannelId': episode['videoChannelId'],
		'consumer': 'web',
		'encryption': 'no'
	}	
	data = json.loads(s.get('{}/AjaxPage'.format(_url), params=params, timeout=_timeout).text)
	data = data['media'][0]
	lp = data['url'].split(data['url'].split('/')[2])[1]
	params = {
		'et': 'gt',
		'rv': data['cdn'].lower(),
		'lp': lp
	}
	data2 = json.loads(s.get('http://mass.mako.co.il/ClicksStatistics/entitlementsServicesV2.jsp', params=params, timeout=_timeout).text)		
	url = data['url'] + '?' + data2['tickets'][0]['ticket'] 	
	result = media.parseM3U8(s.get(url, timeout=_timeout).text, url)		
	result = sorted(result, key=lambda item: int(item['bandwidth']), reverse=True)
	result = [{'title': item.get('resolution', item['bandwidth']), 'url': item['url'] + '|User-Agent=' + s.headers['User-Agent'] } for item in result]		
	from . import common
	return common.select(content, result, 'Streams', 'stream')
	