from . import common

_url = 'https://eztvx.to/api'
_timeout = 2
		

def getTorrents(criteria, maxPage=1):
	from bs4 import BeautifulSoup
	from helpers import http
			
	
	session = http.initSession()	
	params = {'imdb_id': criteria['imdbId'].replace('t',''), 'limit': 100}
	torrents = []
	for page in range(maxPage):	
		params['page']=page+1		
		ttorrents = session.get (_url + '/get-torrents', params=params, timeout=_timeout).json().get('torrents',[])				
		if len(ttorrents)>0 and ttorrents[0]['imdb_id']!=criteria['imdbId'].replace('t',''):
			return torrents
		torrents = torrents + [t for t in ttorrents if t['imdb_id']==criteria['imdbId'].replace('t','')]
		if len(ttorrents)<100:
			break
	torrents = [{'hash': t['hash'], 'title': t['filename'], 'size': common.fileSizeToStr(int(t['size_bytes'])), 'seeds': t['seeds'], 'season': int(t['season']) } for t in torrents if t['seeds']>0 and t['season'] != '0' and t['episode']=='0']
	for torrent in torrents:
		common.updateTorrentData(torrent)
	
	
	
	
	
	return torrents
	
	
def search(criteria):		
	import copy
	if criteria['type']!='tv':
		return []
	if 'imdbId' not in criteria.keys():
		return []
	if 'en-US' not in criteria['titles'].keys():
		return []		
	if len(getTorrents(criteria, 1))==0:
		return []	
	result = copy.deepcopy(criteria)
	result['id'] = result['type'] + str(result['tmdbId'])
	result['title'] = result['titles']['en-US']		
	return [result]
	

def seasons(content):
	return content['seasons']


def episodes(content, season):
	return [{'id': i+1} for i in range(season['episodes'])]
	

def stream(content, season=None, episode=None):	
	import copy
	if (content.get('torrent',{}).get('title','x') == content.get('defaults',{}).get('torrent','y')) and (season is None or content.get('torrent',{}).get('season',0)==season['id']):
		torrent = copy.deepcopy(content['torrent'])
	else:			
		torrents = getTorrents(content, 10)		
		torrents = [torrent for torrent in torrents if torrent.get('season',0)==season['id']]
		torrents = sorted(torrents, key=lambda k: k['seeds'], reverse=True)
		for torrent in torrents:
			torrent['details'] = '[B][{}][/B] '.format(common.torrentDetailsStr(torrent))
		torrent = common.select(content, torrents, 'Torrents', 'torrent')
		content['torrent']=copy.deepcopy(torrent)
	torrent['videoFileSeq']=int(episode['id'])			
	return {'torrent': torrent}	

