



def getLib(source):
	import importlib	
	return importlib.import_module('.' + source, 'vod')


def search(criteria):
	import concurrent.futures
	import xbmcaddon
	settings = xbmcaddon.Addon('script.module.vod').getSettings()		
	results = {}
	def task(source):														
		try:
			data = getLib(source).search(criteria)			
			results[source] = data					
		except Exception as e:
			from helpers import storage
			storage.log('Search ' + source + ": " + str(e))
	ex = concurrent.futures.ThreadPoolExecutor(max_workers=10)		
	for source in ['hdrezka','filmix','kinokong','keshet','reshet','kan','kankids','watchserieshd','yify','eztv','kinozal','walla','xoox']:		
		if settings.getBool(source + 'Enabled') is True:
			ex.submit(task, source)		
	ex.shutdown(wait=True)
	content = []
	for source, scontent in results.items():
		for item in scontent:
			item['source'] = source				
			if 'year' not in criteria.keys() or 'year' not in item.keys():
				content.append(item)
			else:
				try:
					match = abs(int(item['year']) - int(criteria['year']))
					if match <= 1:
						content.append(item)
				except:
					None	
	return content
