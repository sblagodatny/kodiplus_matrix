# -*- coding: utf-8 -*-

from . import common
import xbmcaddon
_addon = xbmcaddon.Addon('script.module.vod')
_pathAddon = _addon.getAddonInfo('path')
_pathSettings = _pathAddon.replace('addons','userdata/addon_data')
_pathAccount = _pathSettings + 'accounts/kinozal.json'

_url = 'https://kinozal.tv'
_timeout = 3
	

def parse(soup):
	result = []	
	for tag in soup.find_all('tr', class_='bg'):
		torrent = {
			'id': tag.find('td', class_="nam").find('a')['href'].split('id=')[-1],
			'title': tag.find('td', class_="nam").find('a').get_text(),
			'size': tag.find_all('td', class_='s')[1].get_text().replace(' МБ',' Mb').replace(' ГБ',' Gb'),			
		}
		try:
			torrent['seeds'] = int(tag.find('td', class_='sl_s').get_text())
		except:
			torrent['seeds'] = 0		
		result.append(torrent)
	return result


def getTorrents(criteria, account, maxPage=1):
	from bs4 import BeautifulSoup
	from helpers import http
	session = http.initSession()	

	

#	text = '^' + criteria['titles']['ru-RU']
#	if 'en-US' in criteria['titles'].keys():
#		text = text + '+' + criteria['titles']['en-US']

	if 'ru-RU' not in criteria['titles'].keys():
		return []	
	text = criteria['titles']['ru-RU']	
	params = {'s': text.encode('cp1251', 'xmlcharrefreplace'),'c': 1001 if criteria['type']=='tv' else 1002, 'g': '3', 'v': 0, 'd': 0, 'w': 0, 't': 1, 'f': 0 }

	
	torrents = []
	for page in range(maxPage):	
		params['page']=page
		soup = BeautifulSoup(session.get (_url + '/browse.php', params=params, cookies=account['cookies'] , timeout=_timeout).text, "html.parser")
		ttorrents = parse(soup)								
		ttorrents = [t for t in ttorrents if t['seeds']>0]
		torrents = torrents + ttorrents
		if len(ttorrents)<50:
			break	
	torrents = [t for t in torrents if t['title'].lower().startswith(criteria['titles']['ru-RU'].lower() + ' (') or t['title'].lower().startswith(criteria['titles']['ru-RU'].lower() + ' /') ]		
	for torrent in torrents:
		common.updateTorrentData(torrent)
	return torrents


def search(criteria):
	import copy
	if 'ru-RU' not in criteria['titles'].keys():
		return []
	if 'tmdbId' not in criteria.keys():
		return []
	account = common.getAccount(_pathAccount)
	if account is None:
		return []	
	if len(getTorrents(criteria, account, 1))==0:
		return []
	result = copy.deepcopy(criteria)
	result['id'] = result['type'] + str(result['tmdbId'])
	result['title'] = result['titles']['ru-RU']		
	return [result]


		
def seasons(content):
	return content['seasons']


def episodes(content, season):
	return [{'id': i+1} for i in range(season['episodes'])]

	
def stream(content, season=None, episode=None):				
	import copy
	if (content.get('torrent',{}).get('title','x') == content.get('defaults',{}).get('torrent','y')) and (season is None or content.get('torrent',{}).get('season','0')==season['id']):
		torrent = copy.deepcopy(content['torrent'])		
	else:
		account = common.getAccount(_pathAccount)
		torrents = getTorrents(content, account, 10)
		if season is not None and len(content['seasons'])>1:
			torrents = [torrent for torrent in torrents if torrent.get('season',0)==season['id']]
		year = None
		if season is not None and 'year' in season.keys():
			year = season['year']
		elif season is None and 'year' in content.keys():
			year = content['year']
		else:
			year = None
		if year is not None:
			torrents = [t for t in torrents if abs(int(t.get('year','0'))-int(year))<=2]				
		torrents = sorted(torrents, key=lambda k: k['seeds'], reverse=True)		
		for torrent in torrents:
			torrent['details'] = '[B][{}][/B] '.format(common.torrentDetailsStr(torrent))	
		torrent = common.select(content, torrents, 'Torrents', 'torrent')				
		torrent['hash'] = getHash(torrent['id'], account)
		content['torrent']=copy.deepcopy(torrent)		
	if episode is not None:
		torrent['videoFileSeq']=int(episode['id'])			
	return {'torrent': torrent}	
	

def login(username, password):	
	from helpers import http
	session = http.initSession()
	headers = {'Referer': _url, 'Origin': _url}	
	data = {'username': username, 'password': password, 'returnto': ''}		
	session.post(_url + '/takelogin.php', data=data, headers=headers, timeout=_timeout)	
	if 'uid' not in str(session.cookies):
		raise Exception('Invalid credentials')			
	session.get(_url)	
	return session.cookies.get_dict()

	
def getHash(torrentId, account):
	from helpers import http
	session = http.initSession()
	try:
		content = session.get(_url + '/get_srv_details.php', params={'id': torrentId,'action':2}, cookies=account['cookies'], timeout=_timeout).text								
		return content.split('<li>Инфо хеш: ')[1].split('</li>')[0].upper()
	except:
		return None

