## References: 
## https://github.com/consumet/consumet.ts/tree/master/src/extractors
## https://github.com/jewbmx/repo/tree/master/zips/plugin.video.scrubsv2


import os
from helpers import storage
import json

def match(url):
	parsers = {
#		'vidsrc': ['vidsrc.me'],
		'mixdrop': ['mixdrop.co', 'mixdrop.to', 'mixdrop.sx', 'mixdrop.bz', 'mixdrop.ch', 'mixdrp.co', 'mixdrp.to', 'mixdrop.gl', 'mixdrop.club', 'mixdroop.bz'],
		'vidcloud': ['rabbitstream.net', 'dokicloud.one','megacloud.tv'],
		'smashystream': ['smashystream.com']
	}
	for parser, hosts in parsers.items():
		for host in hosts:
			if host in url:
				return parser
	return None


def get(parser):	
	import importlib
	return importlib.import_module('.' + parser, 'vod.cdn')
		
		
def parse(embedUrls, referer=None):
	import concurrent.futures
	result={}
	def task(embedUrl):					
		parser = match(embedUrl)		
		if parser is None:
			return		
		parser = get(parser)							
		try:
			result[embedUrl] = parser.parse(embedUrl, referer)		
		except Exception as e:			
			return		
	ex = concurrent.futures.ThreadPoolExecutor(max_workers=10)		
	for embedUrl in embedUrls:			
		ex.submit(task, embedUrl)		
	ex.shutdown(wait=True)		
	return result