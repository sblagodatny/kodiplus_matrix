# -*- coding: utf-8 -*-


import re
from helpers import storage, http, jsunpack
from bs4 import BeautifulSoup


_timeout = 2

def parse(url, referer=None):	
	s = http.initSession()
	content = s.get(url, headers=None if referer is None else {'Referer': referer}, timeout=_timeout).text		
#	title = re.search(r'\<title\>(.*?)\<\/title\>', content).group(1).replace('MixDrop - Watch ','')		
	script = re.search(r'eval\((.*?)split', content)
	if script is None:
		turl = 'https://' + url.split('/')[2] + BeautifulSoup(content, "html.parser").find('iframe')['src'] 
		content = s.get(turl, headers=None if referer is None else {'Referer': referer}, timeout=_timeout).text			
		script = re.search(r'eval\((.*?)split', content)	
	script = script.group(1) + "split('|'))"	
	data = jsunpack.unpack(script)				
	stream = 'https:' + re.search(r'MDCore.wurl="(.*?)"', data).group(1) 
	
	
	return {'url': stream}
	


	
