# -*- coding: utf-8 -*-

import re
from helpers import storage, http

_timeout = 2

def parse(url, referer=None):	
	s = http.initSession()
	content = s.get(url, headers=None if referer is None else {'Referer': referer}, timeout=_timeout).text	
	stream = re.search(r'source src="(.*?)"', content).group(1)
	title = stream.split('/')[-1]	
	headers = {
		'Referer': 'https://eplayvid.net/',
		'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'
	}
	return {'url': stream, 'headers': headers, 'title': title}
	
