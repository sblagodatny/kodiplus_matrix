# -*- coding: utf-8 -*-

import re
from helpers import storage, http
import time
import urllib

_timeout = 2



def parse(url, referer=None):	
#	storage.log('Parsing DoodStream: ' + url)		
	s = http.initSession()
	r = s.get(url, headers={'Referer': referer}, timeout=_timeout)
	html = r.text		
	headers = {'Referer': r.url}
	try:
		urlt = 'https://{}/pass_md5/{}'.format(r.url.split('/')[2], re.search(r"/pass_md5/(.*?)'", html).group(1))
	except:
		raise Exception('Video not available')
	stream = s.get(urlt, headers=headers, timeout=_timeout).text
	params = {
		'token': urlt.split('/')[-1], 
		'expiry': int(time.time() * 1000)
	}	
	headers.update({'User-Agent': s.headers['User-Agent']})
	return ({'url': stream + 'aaaaaaaaaa?' + urllib.parse.urlencode(params), 'headers': headers})



	
