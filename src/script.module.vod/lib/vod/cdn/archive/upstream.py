# -*- coding: utf-8 -*-


import re
from helpers import storage, http

import importlib
jsunpack = importlib.import_module('.' , 'jsunpack')

_timeout = 2

	
def parse(url, referer=None):
#	storage.log('Parsing UpStream: ' + url)	
	s = http.initSession()
	content = s.get(url, headers=None if referer is None else {'Referer': referer}, timeout=_timeout).text
	title = re.search(r'\<title\>(.*?)\<\/title\>', content).group(1).replace('Watching ','')	
	script = re.search(r'eval\((.*?)split', content).group(1) + "split('|'))"	
	data = jsunpack.unpack(script)	
	stream = re.search(r'file\:"(.*?)"', data).group(1)
	return {'url': stream, 'title': title}


	
