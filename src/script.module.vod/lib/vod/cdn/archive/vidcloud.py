# -*- coding: utf-8 -*-


import re, json
from helpers import storage, http, aes
import base64, hashlib
import six


_timeout = 2
_keyUrl='https://raw.githubusercontent.com/theonlymo/keys/e4/key'



def derive_key_and_iv(password, salt, key_length, iv_length):
	d = d_i = six.ensure_binary('')
	password = six.ensure_binary(password)
	while len(d) < key_length + iv_length:
		d_i = hashlib.md5(d_i + password + salt).digest()
		d += d_i
	return d[:key_length], d[key_length:key_length + iv_length]

def decrypt(data, password, key_length=32):
	data = base64.b64decode(data)
	bs = 16
	salt = data[len("Salted__"): 16]
	data = data[len("Salted__") + len(salt):]
	key, iv = derive_key_and_iv(password, salt, key_length, bs)
	cipher = aes.AESModeOfOperationCBC(key, iv)
	decryptedBytes = six.ensure_binary('')
	for x in range(len(data) // 16):
		blockBytes = data[x * 16: (x * 16) + 16]
		decryptedBytes += cipher.decrypt(blockBytes)
	padding = decryptedBytes[-1]
	padding_length = ord(padding) if isinstance(padding, six.string_types) else padding
	return six.ensure_str(decryptedBytes[:-padding_length])

	
def parse(url, referer=None):
#	storage.log('Parsing vidcloud: ' + url + ', referer: ' + referer)	
#	from helpers import media
	id = url.split('/')[-1].split('?')[0]	
	baseUrl = url.split('/' + id)[0]
	baseUrlLast = baseUrl.split('/')[-1]	
	baseUrl = baseUrl.replace(baseUrlLast, 'ajax/' + baseUrlLast)	
	s=http.initSession()					
	data = s.get(baseUrl + '/getSources', params={'id': id}, headers={'Referer': url, 'X-Requested-With': 'XMLHttpRequest'}, timeout=_timeout).json()
	
	storage.log(data['sources'])
	
	key = eval(s.get(_keyUrl, timeout=_timeout).text)	

	storage.log(str(key))
	
	sourcesArray = list(data['sources'])
	extractedKey = ''
	currentIndex = 0
	for index in key:
		start = index[0] + currentIndex
		end = start + index[1]
		for i in range(start, end):
			extractedKey += data['sources'][i]
			sourcesArray[i] = ''
		currentIndex += index[1]
		
	key = extractedKey				
	sources = ''.join(sourcesArray)
	
	storage.log(key)
	storage.log(sources)
	
	
	sources = decrypt(sources, key)	
	
	storage.log(sources)
	raise Exception('bla bla bla')
	
#	sources = ''.join(sources)	
#	sources = decrypt(sources, key)		
#	stream = json.loads(sources)[0]['file']		
#	
#	return stream
	
	
#	subtitles = [{'language': sb['label'], 'url': sb['file'], 'name': 'Original', 'format': 'vtt', 'zip': False} for sb in data['tracks']]			
#	for subtitle in subtitles:
#		for delimiter in ('-','('):
#			if delimiter in subtitle['language']:
#				subtitle['name'] = subtitle['name'] + ' ' + subtitle['language'].split(delimiter)[1].lstrip()
#				subtitle['language'] = subtitle['language'].split(delimiter)[0].rstrip()
#		
#	result = media.parseM3U8(s, stream)		
#	result = sorted(result, key=lambda item: int(item['bandwidth']))
#	result = [{'title': item.get('resolution', item['bandwidth']), 'url': item['url'], 'subtitles': subtitles} for item in result]
#	return result




