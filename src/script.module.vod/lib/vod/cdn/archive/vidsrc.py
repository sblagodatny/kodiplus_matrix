# -*- coding: utf-8 -*-


import re
from helpers import storage, http
from bs4 import BeautifulSoup
import json
import importlib


import xbmc

_timeout = 2


def deobfstr(data_h, data_i):
    data_i = str(data_i)
    result = ''
    for i in range(0, len(data_h), 2):
        temp = data_h[i:i+2]
        result += chr(int(temp, 16) ^ ord(data_i[i // 2 % len(data_i)]))
    return result
    

def getDataTMDB(url):
	season = None
	episode = None
	tmdbId = url.split('/')[-1]
	if '-' in tmdbId:
		season = tmdbId.split('-')[0]
		episode = tmdbId.split('-')[1]
		tmdbId = url.split('/')[-2]
	return tmdbId, season, episode


def getSubtitles(url):	
	try:
		s = http.initSession()		
		tmdbId, season, episode = getDataTMDB(url)
		if season is None:
			content = s.get('https://vidsrc.to/embed/movie/{}'.format(tmdbId), timeout=_timeout ).text
		else:
			tmdb = importlib.import_module('.' , 'tmdb')
			content = s.get('https://vidsrc.to/embed/tv/{}/{}/{}'.format(tmdb.imdbId('tv',tmdbId), season, episode), timeout=_timeout ).text
		dataId = re.search(r'data-id="(.*?)"', content).group(1)	
		subs = s.get('https://vidsrc.to/ajax/embed/episode/{}/subtitles'.format(dataId), headers={'Referer': 'https://vidstream.pro/'} ).json()	
		return [{'language': sub['label'], 'url': sub['file'], 'format': 'vtt', 'name': 'Original', 'zip': False} for sub in subs]
	except:
		return []
	

	
	
def parse(url, referer=None):
	storage.log('Parsing VidSrc: ' + url)	
	s = http.initSessionTLSv1_1()	
#	s = http.initSession()					
	host = url.split('/')[2]
	content = s.get(url, headers=None if referer is None else {'Referer': referer}, timeout=_timeout).text	
	serverHash =  BeautifulSoup(content, "html.parser").find_all('div', class_='source')[0]['data-hash']
	rcpUrl = 'https://rcp.{}/rcp/{}'.format(host, serverHash)		

	storage.log(rcpUrl)
	
#	content = s.get(rcpUrl, headers={'Referer':url}, timeout=_timeout ).text	
	content = http.get(rcpUrl, headers={'Referer':url}, timeout=_timeout ).text	
	
	storage.log(content)
	
	data_i = re.search(r'data-i="(.*?)"', content).group(1)	
	data_h = re.search(r'data-h="(.*?)"', content).group(1)	
	srcrcpUrl = 'https:' + deobfstr(data_h, data_i)
	
	storage.log(srcrcpUrl)
	
	response = s.get(srcrcpUrl, headers={'Referer':rcpUrl}, timeout=_timeout ) 
	content = response.text			
	hlsUrl = re.search(r'hls_url = "(.*?)"', content).group(1)	
	passUrl = 'https:' + re.findall(r'var path = "(.*?)"', content)[1]			
	headers = {'Referer': response.url}
	s.get(passUrl, headers=headers)	
	return {'url': hlsUrl, 'headers': headers, 'subtitles': getSubtitles(url)}

	