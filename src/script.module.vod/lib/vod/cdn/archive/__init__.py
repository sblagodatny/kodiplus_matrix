import importlib
import os

from helpers import storage
import json

def match(url):
	parsers = {
		'mixdrop': ['mixdrop.co', 'mixdrop.to', 'mixdrop.sx', 'mixdrop.bz', 'mixdrop.ch', 'mixdrp.co', 'mixdrp.to', 'mixdrop.gl', 'mixdrop.club', 'mixdroop.bz'],
		'upstream': ['upstream.to'],
##		'streamtape': ['streamtape.com', 'strtape.cloud', 'streamtape.net', 'streamta.pe', 'streamtape.site', 'strcloud.link', 'strtpe.link', 'streamtape.cc', 'scloud.online', 'stape.fun', 'streamadblockplus.com', 'shavetape.cash', 'streamtape.to', 'streamta.site', 'streamadblocker.xyz', 'tapewithadblock.org', 'adblocktape.wiki'],
		'filemoon': ['filemoon.sx', 'filemoon.to', 'filemoon.in', 'filemoon.link', 'filemoon.nl', 'filemoon.wf', 'cinegrab.com', 'filemoon.eu', 'filemoon.art', 'moonmov.pro'],
		'eplayvid': ['eplayvid.net'],
		'doodstream': ['dood.watch', 'doodstream.com', 'dood.to', 'dood.so', 'dood.cx', 'dood.la', 'dood.ws', 'dood.sh', 'doodstream.co', 'dood.pm', 'dood.wf', 'dood.re', 'dood.yt', 'dooood.com', 'dood.stream'],
		'vidsrc': ['vidsrc.me'],
		'vidcloud': ['rabbitstream.net', 'dokicloud.one'],
		'smashystream': ['smashystream.com']
	}
	for parser, hosts in parsers.items():
		for host in hosts:
			if host in url:
				return parser
	return None


def get(parser):	
	return importlib.import_module('.' + parser, 'cdn')
		

def quality(title):
	delimiters = [' ','.','-']
	qualities = [
		{'quality': 1080, 'patterns': ['1080', '1080p']},
		{'quality': 720, 'patterns': ['720', '720p']}				
	]
	for quality in qualities:
		for pattern in quality['patterns']:
			for delimiter in delimiters:
				if delimiter + pattern in title:
					return quality['quality']			
	return 480
	
	
	
def parse(embedUrls, referer=None):
	import concurrent.futures
	streams=[]
	def task(embedUrl):					
		parser = match(embedUrl)		
		if parser is None:
			return		
		parser = get(parser)							
		stream = parser.parse(embedUrl, referer)				
		stream['quality'] = quality(stream['title'])						
		streams.append(stream)		
	ex = concurrent.futures.ThreadPoolExecutor(max_workers=10)		
	for embedUrl in embedUrls:			
		ex.submit(task, embedUrl)		
	ex.shutdown(wait=True)		
	streamsfinal = []
	for stream in streams:
		found = [st for st in streamsfinal if st['url']==stream['url']]
		if len(found)==0:
			streamsfinal.append(stream)	
	streamsfinal = sorted(streamsfinal, key=lambda k: k['quality'])	
	return streamsfinal	
