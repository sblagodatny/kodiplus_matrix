# -*- coding: utf-8 -*-

import re
#from helpers import storage

_timeout = 2

def parse(s, url, referer=None):
#	storage.log('Parsing StreamTape: ' + url)	
	content = s.get(url, headers=None if referer is None else {'Referer': referer}, timeout=_timeout).text	
	title = re.search(r'\<title\>(.*?)\<\/title\>', content).group(1).replace(' at Streamtape.com','')	
	data = re.search(r'''ById\('.+?=\s*(["']//[^;<]+)''', content).group(1)
	stream = 'https://streamtape.com/get_video?id=' + data.split('id=')[1].split("')")[0]
	headers = {
		'Referer': url,
		'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'
	}
	return {'url': stream, 'headers': headers, 'title': title}
	
	