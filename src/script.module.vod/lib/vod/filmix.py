# -*- coding: utf-8 -*-

from . import common

import requests
import json
import urllib
import base64
import time
from bs4 import BeautifulSoup
import xbmcgui
from helpers import http, storage

import xbmcaddon
_addon = xbmcaddon.Addon('script.module.vod')
_pathAddon = _addon.getAddonInfo('path')
_pathSettings = _pathAddon.replace('addons','userdata/addon_data')
_pathAccount = _pathSettings + 'accounts/filmix.json'

_url = 'https://filmix.my'
_timeout = 4
	

def parse(content):
	result = []
	for tag in content.find_all('article'):					
		try:
			item = {			
				'id': tag['data-id'],
				'title': tag.find('h2', class_='name')['content'],
				'poster': tag.find('img')['src'].replace('thumbs/w220','orig'),			
				'plot': tag.find('p', {'itemprop': 'description'}).get_text(),
				'year': tag.find('div', class_='year').find('a').get_text(),
				'type': 'tv' if tag.find('li', {'data-id': 'episode'}) is not None else 'movie'
			}	
			item['title'] = " ".join(item['title'].split())
			result.append(item)
		except:
			None
	return result
	


def search(criteria):
	import xbmcvfs
	if 'ru-RU' not in criteria['titles'].keys():
		return []
	account = common.getAccount(_pathAccount)
	if account is None:
		return []
	s = http.initSession()
	headers = {	'X-Requested-With': 'XMLHttpRequest', 'Referer': _url }
	data = {'story': criteria['titles']['ru-RU'], 'do': 'search', 'subaction': 'search'}
	content = BeautifulSoup(s.post(_url + '/engine/ajax/sphinx_search.php', data=data, headers=headers, cookies=account['cookies'], timeout=_timeout).text, "html.parser")				
	return [item for item in parse(content) if item['type']==criteria['type']]
	
	
def decode(str):
	s = str[2:].replace("\/","/")
	tokens = [':<:Mm93S0RVb0d6c3VMTkV5aE54',':<:bzl3UHQwaWk0MkdXZVM3TDdB',':<:bE5qSTlWNVUxZ01uc3h0NFFy',':<:SURhQnQwOEM5V2Y3bFlyMGVI',':<:MTluMWlLQnI4OXVic2tTNXpU']
	for r in range(3):
		for token in tokens:
			s = s.replace(token, '')
	return base64.b64decode(s).decode("utf-8")


def getPlayerData(content, translation=None):
	account = common.getAccount(_pathAccount)
	s = http.initSession()	
	data = {'post_id': content['id'], 'showfull': 'false'}	
	d = s.post(_url + '/api/movies/player-data', params={'t':  int(time.time() * 1000)}, headers={ 'X-Requested-With': 'XMLHttpRequest', 'Referer': _url }, data=data, cookies=account['cookies'], timeout=_timeout).json()					
	if translation is None:	
		translations = [{'title': t} for t in d['message']['translations']['video'].keys()]				
		translation = common.select(content, translations, 'Translations', 'translation')		
		translation = translation['title']				
	return { 
		'url': decode(d['message']['translations']['video'][translation]),
		'translation': translation
	}

def getEpisodesData(url):
	s = http.initSession()	
	data = json.loads(decode(s.get(url, timeout=_timeout).text))	
	if 'folder' not in data[0].keys():
		data[0]['folder'] = data
	return data	
	
		
def seasons(content):
	s = http.initSession()				
	playerData = getPlayerData(content)							
	data = getEpisodesData(playerData['url'])
	id = 1
	result = []
	for item in data:
		result.append({
			'id': id,					
			'title': item['title'],			
			'translation': playerData['translation']
		})
		id = id + 1				
	return result
	

def episodes(content, season):
	s = http.initSession()			
	playerData = getPlayerData(content, season['translation'])		
	data = getEpisodesData(playerData['url'])
	result = []
	id = 1	
	for item in data[season['id']-1]['folder']:
		result.append({	
			'id': id,
			'title': item['title']			
		})					
		id = id + 1
	return result
	
	
def stream(content, season=None, episode=None):						
	if episode is not None:
		playerData = getPlayerData(content, season['translation'])			
		data = getEpisodesData(playerData['url'])										
		urls = data[season['id']-1]['folder'][episode['id']-1]['file']			
	else:		
		playerData = getPlayerData(content)['url']		
		urls = playerData			
	streams = []
	for q in urls.split(','):
		quality = {
			'title': q.split(']')[0].split('[')[-1],
			'url': q.split(']')[-1]
		}		
		if quality['title'] in ["480p","720p"]: ## Higher qualities are paid
			streams.append(quality)										
	streams.reverse()
	return common.select(content, streams, 'Streams', 'stream')
	

def login(username, password):	
	s = http.initSession()		
	s.get(_url)
	data = {
		'login_name': username,
		'login_password': password,
		'login_not_save': '1',
		'login': 'submit'
	}
	result = s.post(_url + '/engine/ajax/user_auth.php', data=data, timeout=_timeout, headers={ 'X-Requested-With': 'XMLHttpRequest', 'Origin': _url, 'Referer': _url + '/'}).text
	if result != 'AUTHORIZED':
		raise Exception(result)
	return s.cookies.get_dict()

	
	

