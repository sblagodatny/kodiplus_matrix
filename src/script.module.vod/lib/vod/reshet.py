# -*- coding: utf-8 -*-



import requests
import json
import urllib
import re
from bs4 import BeautifulSoup
import xbmcaddon
import time, datetime

_addon = xbmcaddon.Addon('plugin.video.vod')
_url = 'https://13tv.co.il'

_timeout = 4

from helpers import http, media, storage


_seasonsShift = {
	'הישרדות': 6
}
	

def getAll():	
	result = []	
	s = http.initSession()	
	buildId = re.search(r'"buildId":"(.*?)"', s.get(_url + '/all-shows', timeout=_timeout).text).group(1)	
	data = s.get(_url + '/_next/data/'+buildId+'/he/all-shows/all-shows-list.json?all=all-shows&all=all-shows-list', timeout=_timeout).json()
	for show in data['pageProps']['page']['Content']['PageGrid'][0]['shows']:
		result.append({
			'id': show['id'],
			'title': show['title'],
			'poster': show['poster'],			
			'url': show['url'],			
			'type': 'tv'	
		})
	return result
	

def search(criteria):
	if criteria['type'] != 'tv':
		return[]
	if 'he-IL' not in criteria['titles'].keys():
		return []
	if 'עִבְרִית' not in criteria.get('language',['עִבְרִית']):
		return []
	result = []
	for item in getAll():
		if criteria['titles']['he-IL'] in item['title']:
			result.append(item)
	return result

	
def seasons(content):
	s = http.initSession()	
	result = []
	buildId = re.search(r'"buildId":"(.*?)"', s.get(_url + '/all-shows', timeout=_timeout).text).group(1)	
	data = s.get(_url + '/_next/data/'+buildId+'/he' + content['url'][:-1] + '.json?all=all-shows&all=' + content['url'].split('/')[-2]).json()
	for id, sdata in data['pageProps']['page']['Content']['PageGrid'][0]['episodesSeasonsMap'].items():
		try:
			id = int(sdata['name'].split("עונה")[1].lstrip().rstrip())
		except:
			id = -1
		result.append({
			'id': id,
			'title': sdata['name'],
			'posts': sdata['episodes']
		})
	result.reverse()
	return result
	
	
def episodes(content, season):
	result = []	
	for post in sorted(season['posts'], key=lambda p: int(p['id'])):
		
		
		try:
			id = post['title'].split("פרק")[1]
			if ':' in id:
					id = id.split(':')[0]
			id = int(id.lstrip().rstrip())
		except:
			id = -1
		
		item = {
			'kalturaId': post['video']['kalturaId'],
			'title': post['title'],
			'thumb': post['imageObj']['m'], 
			'plot': post['secondaryTitle'],
			'duration': post['video']['duration'],
			'id': id,
			'releaseDate': post['air_date']	
		}
		item['releaseDate'] = datetime.datetime(*(time.strptime(item['releaseDate'], '%d/%m/%Y')[0:6]))	
		item['releaseDate'] = item['releaseDate'].strftime('%Y-%m-%d')			
		if 'פרק' in item['title']:
			item['title'] = re.search(r"פרק.*?(?=/|$)", item['title']).group(0)	
		if ' | ' in item['plot']:
			item['plot'] = item['plot'].split(' | ')[0].lstrip().rstrip()
		result.append(item)					
	
	return result

	
def stream(content, season, episode):	
	s = http.initSession()
	data = {
		"1":{"service":"session","action":"startWidgetSession","widgetId":"_2748741"},
		"2":{
			"service":"baseEntry","action":"list","ks":"{1:result:ks}",
			"filter":{"redirectFromEntryId":episode['kalturaId']},
			"responseProfile":{"type":1,"fields":"id,referenceId,name,description,thumbnailUrl,dataUrl,duration,msDuration,flavorParamsIds,mediaType,type,tags,dvrStatus,externalSourceType,status"}
		},
		"3":{"service":"baseEntry","action":"getPlaybackContext","entryId":"{2:result:objects:0:id}","ks":"{1:result:ks}","contextDataParams":{"objectType":"KalturaContextDataParams","flavorTags":"all"}},
		"4":{"service":"metadata_metadata","action":"list","filter":{"objectType":"KalturaMetadataFilter","objectIdEqual":"{2:result:objects:0:id}","metadataObjectTypeEqual":"1"},"ks":"{1:result:ks}"},
		"apiVersion":"3.3.0","format":1,"ks":"","clientTag":"html5:v1.3.0","partnerId":"2748741"
	}
	sources = s.post('https://cdnapisec.kaltura.com/api_v3/service/multirequest', json=data, timeout=_timeout).json()[2]['sources']
	for source in sources:
		if source['format']=='applehttp':			
			result = media.parseM3U8(s.get(source['url'], timeout=_timeout).text, source['url'])		
			result = sorted(result, key=lambda item: int(item['bandwidth']),reverse=True)
			result = [{'title': item.get('resolution', item['bandwidth']), 'url': item['url']} for item in result]
			from . import common
			return common.select(content, result, 'Streams', 'stream')		
			
	raise Exception('No available options for Streams')

	
