def select(content, items, title, defaults_key):
	import xbmcgui					
	item = None
	if len(items)==0:
		raise Exception('No available options for {}'.format(title))
	if len(items)==1:
		item = items[0]
	elif content.get('defaults',{}).get(defaults_key,None) is not None:
		for s in items:
			if s['title']==content['defaults'][defaults_key]:
				item=s
	if item is None:	
		s = xbmcgui.Dialog().select(title, [t.get('details','') + t['title'] for t in items])	
		if s==-1:
			raise Exception('cancelled')
		item = items[s]
	if 'defaults' not in content.keys():
		content['defaults']={}
	content['defaults'][defaults_key]=item['title']
	return item	
	
	
def getAccount(pathAccount):
	from contextlib import closing	
	import json
	import xbmcvfs
	if not xbmcvfs.exists(pathAccount):
		return None
	with closing(xbmcvfs.File(pathAccount, 'r')) as f:
		account = json.load(f)
	return account	
	
	
def updateTorrentData(torrent):	
	if 'year' not in torrent.keys():
		import datetime
		yearCurrent = datetime.date.today().year
		for i in range(50):		
			if str(yearCurrent-i) in torrent['title']:
				torrent['year']=str(yearCurrent-i)
				break
	if 'quality' not in torrent.keys():
		for quality in ['2160p', '1080p', '720p', '576p', '480p', '400p']:
			if quality in torrent['title']:
				torrent['quality']=quality
				break
	if 'origin' not in torrent.keys():
		for origin in ['TVRip', 'SATRip', 'DVDRip', 'BDRip', 'WEBRip', 'HDRip', 'BluRay']:
			if origin.lower() in torrent['title'].lower():
				torrent['origin']=origin
				break
		if 'origin' not in torrent.keys() and 'WEB' in torrent['title']:
			torrent['origin']='WEB'
		if 'origin' not in torrent.keys() and 'TS' in torrent['title']:
			torrent['origin']='TS'
	if 'season' not in torrent.keys():
		patterns = ['({} сезон', '[{} сезон', ' {} сезон', 'сезон {})','сезон {}]','сезон {} ', 'сезон: {})','сезон: {}]','сезон: {} ']
		for i in range(1,30):
			for pattern in patterns:
				if pattern.format(i) in torrent['title'].lower():
					torrent['season']=i


def fileSizeToStr(size_bytes):
	import math
	if size_bytes == 0:
		return "0B"
	size_name = ("B", "Kb", "Mb", "Gb", "Tb")
	i = int(math.floor(math.log(size_bytes, 1024)))
	p = math.pow(1024, i)
	s = round(size_bytes / p, 2)
	return "%s %s" % (s, size_name[i])
	
	
def torrentDetailsStr(torrent):
	keys = ['quality','origin','seeds','size']	
	values = [{'value': value if key != 'seeds' else str(value) + ' seeds', 'order': keys.index(key) } for key, value in torrent.items() if key in keys]
	values = sorted(values, key=lambda k: k['order'])
	return ', '.join([v['value'] for v in values])
