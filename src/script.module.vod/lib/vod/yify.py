from . import common

_url = 'https://yts.mx/api/v2'
_timeout = 3
		


def getTorrents(imdbId):	
	from helpers import http
	session = http.initSession()	
	data=session.get(_url + '/movie_details.json', params = {'imdb_id': imdbId.replace('t','')}, timeout=_timeout).json()	
	torrents = data.get('data',{}).get('movie',{}).get('torrents',[])
	return [{'title': '{}.{}.{}.YIFY'.format(data['data']['movie']['title_long'], t['quality'],t['type']), 'hash': t['hash'], 'quality': t['quality'], 'origin': t['type'], 'seeds': t['seeds'], 'size': t['size'].replace('GB','Gb').replace('MB','Mb')} for t in torrents]

	
def search(criteria):
	import copy
	if 'en-US' not in criteria['titles'].keys():
		return []
	if criteria['type']=='tv':
		return []
	if 'imdbId' not in criteria.keys():
		return []
	torrents = getTorrents(criteria['imdbId'])	
	if len(torrents)==0:
		return []
	result = copy.deepcopy(criteria)
	result['id'] = result['type'] + str(result['tmdbId'])
	result['title'] = result['titles']['en-US']		
	return [result]
		

def stream(content, season=None, episode=None):
	import copy
	if content.get('torrent',{}).get('title','x') == content.get('defaults',{}).get('torrent','y'):
		torrent = copy.deepcopy(content['torrent'])
	else:
		torrents = getTorrents(content['imdbId'])
		torrents = sorted(torrents, key=lambda k: k['seeds'], reverse=True)
		for torrent in torrents:
			torrent['details'] = '[B][{}][/B] '.format(common.torrentDetailsStr(torrent))	
		torrent = common.select(content, torrents, 'Torrents', 'torrent')
		content['torrent']=copy.deepcopy(torrent)
	return {'torrent': torrent}

