# -*- coding: utf-8 -*-

from . import common

import requests
import json
import urllib
from bs4 import BeautifulSoup
import base64
import random

from helpers import http, gui, storage



_url = 'https://rezka.ag'

_timeout = 4

	
	
def parse(content):
	result = []	
	for tag in content.find_all('div', class_="b-content__inline_item"):				
		item = {
			'id': tag['data-url'].split('/')[-1].split('-')[0],			
			'title': tag.find('div', class_="b-content__inline_item-link").find('a').get_text(),
			'poster': tag.find('img')['src'],			
			'year': tag.find('div', class_="b-content__inline_item-link").find('div').get_text().split(',')[0],
			'type': 'tv' if tag.find('span', class_="info") is not None else 'movie',
			'url': tag['data-url'],
		}
		if '-' in item['year']:
			item['year'] = item['year'].split('-')[0].lstrip().rstrip()
		result.append(item)	
	return result

	
def search(criteria):
	if 'ru-RU' not in criteria['titles'].keys():
		return []
	s = http.initSession()
	content = BeautifulSoup(s.get(_url + '/search/', params={'do': 'search', 'subaction': 'search', 'q': criteria['titles']['ru-RU']}, timeout=_timeout).text, "html.parser")			
	return [item for item in parse(content) if item['type'] == criteria['type']]

	
def getDefaultStreams(url):
	s = http.initSession()	
	content = s.get(url, timeout=_timeout).text
	return content.split('"streams":"')[1].split('",')[0].replace('\/','/')
	
	
def getTranslator(content):
	s = http.initSession()	
	translations = []
	contentt = s.get(content['url'], timeout=_timeout).text
	soup = BeautifulSoup(contentt, "html.parser")
	for tag in soup.find_all('li',class_='b-translator__item'):
		translations.append({'id': tag['data-translator_id'], 'title': tag.get_text()})			
	if len(translations) == 0:
		translations.append({'id': contentt.split('initCDN')[1].split(',')[1].rstrip().lstrip(), 'title': 'Default'})		
	from . import common	
	return common.select(content, translations, 'Translations', 'translation')
		

def seasons(content):
	s = http.initSession()				
	translator = getTranslator(content)		
	params = {
		'id': content['id'],
		'action': 'get_episodes',
		'translator_id': translator['id']		
	}
	data=s.post(_url + '/ajax/get_cdn_series/', data=params, timeout=_timeout).json()	
	if 'seasons' not in data.keys():
		return []
	result = []	
	soup = BeautifulSoup(data['seasons'], "html.parser")
	for tag in soup.find_all('li',class_='b-simple_season__item'):		
		result.append({			
			'id': tag['data-tab_id'],
			'title': tag.get_text(),			
			'translator': translator['id']	
		})		
	return result
	

def episodes(content, season):
	s = http.initSession()		
	params = {
		'id': content['id'],
		'action': 'get_episodes',
		'season': season['id'],
		'translator_id': season['translator']
	}
	data=s.post(_url + '/ajax/get_cdn_series/', data=params, timeout=_timeout).json()		
	result = []
	soup = BeautifulSoup(data['episodes'], "html.parser")
	id = 1		
	for tag in soup.find_all('li',class_='b-simple_episode__item'):
		if tag['data-season_id'] != season['id']:
			continue
		result.append({
#			'id': tag['data-episode_id'],
			'id': id,
			'title': tag.get_text()					
		})
		id = id + 1
	return result
		
		
def decode(str):
	s = str[2:].replace("\/","/")
	tokens = ['//_//JCQhIUAkJEBeIUAjJCRA','//_//QEBAQEAhIyMhXl5e','//_//IyMjI14hISMjIUBA','//_//Xl5eIUAjIyEhIyM=','//_//JCQjISFAIyFAIyM=']
	for r in range(3):
		for token in tokens:
			s = s.replace(token, '')
	return base64.b64decode(s).decode("utf-8")		
		
		
def stream(content, season=None, episode=None):
	s = http.initSession()
	if episode is not None:														
		params = {
			'id': content['id'],
			'translator_id': season['translator'],
			'season': season['id'],
			'episode': episode['id'],
			'action': 'get_stream'
		}		
	else:	
		translator = getTranslator(content)		
		params = {
			'id': content['id'],
			'translator_id': translator['id'],			
			'is_camrip':'0',
			'is_ads':'0',
			'is_director':'1',
			'action': 'get_movie'
		}
	data=json.loads(s.post(_url + '/ajax/get_cdn_series/', data=params, timeout=_timeout).text)		
	if 'url' not in data.keys():
		data['url'] = getDefaultStreams(content['url'])
	if data['url'] is False:
		raise Exception('No available options found for Streams')					
	data['url'] = decode(data['url'])						
	streams = []
	for q in data['url'].split(','):
		source = {
			'title': q.split(']')[0].split('[')[-1],
			'url': q.split(']')[-1]
		}				
		if ' or ' in source['url']:
			source['url'] = source['url'].split(' or ')[0]	
		if source['title'] in ['360p','480p','720p']:
			streams.append(source)													
	streams.reverse()
	return common.select(content, streams, 'Streams', 'stream')
	
	
	
	
