# -*- coding: utf-8 -*-



import requests
import json
import urllib
from bs4 import BeautifulSoup
import datetime
import xbmcaddon
import re

from helpers import http, media, storage

_url = 'https://www.kankids.org.il'

_timeout = 4




def search(criteria):
	from helpers.fuzzywuzzy import fuzz
	if criteria['type'] != 'tv':
		return[]
	if 'he-IL' not in criteria['titles'].keys():
		return []
	if 'עִבְרִית' not in criteria.get('language',['עִבְרִית']):
		return []
	import concurrent.futures	
	results = {}
	def task(page):
		s = http.initSession()
		data = {
			"filters":{"categories":[["video",True]]},
			"q": criteria['titles']['he-IL'],
			"h": "www.kankids.org.il",
			"p": page
		}	
		reply = s.post('https://heyday.io/search/s/', json=data, timeout=_timeout, headers={'Referer': 'https://heyday.io/kan11Box.html?type=holder'}).json()['r']
		results[page]=reply							
	ex = concurrent.futures.ThreadPoolExecutor(max_workers=10)		
	for page in range(10):			
		ex.submit(task, page + 1)		
	ex.shutdown(wait=True)	
	result = []
	for data in results.values():
		for item in data:
			if len(item['pd']['url'].split('/'))>8:
				continue
			resultitem = {
				'id': item['pd']['url'],
				'title': item['pd']['title'],
				'poster': item['pd'].get('img',''),
				'plot': item['pd'].get('description',''),
				'type': 'tv',
				'url': item['pd']['url']
			}
			if ' - ' in resultitem['title']:
				resultitem['title'] = resultitem['title'].split(' - ')[0]
			
			if fuzz.token_set_ratio(resultitem['title'], criteria['titles']['he-IL']) >= 70:			
				result.append(resultitem)	
	return result

	
def seasons(content):	
	result = []
	s = http.initSession()		
	soup = BeautifulSoup(s.get(content['url'], timeout=_timeout).text, "html.parser")	
	base = soup.find('ul', {'aria-labelledby':"dropdownMenuButtonKids"})
	if base is not None:
		for tag in base.find_all('li'):	
			resultitem = {			
				'title': tag.find('a').get_text(),
				'url': _url + tag.find('a')['href']
			}
			if resultitem['title'] == 'כל הפרקים':
				continue
			try:
				resultitem['id'] = int(resultitem['title'].split(' ')[-1])
			except:			
				continue			
			result.append(resultitem)
		result.reverse()
		return result
	else:
		return [{'id': 1, 'title': 'עונה 1', 'url': content['url']}]
	
	

def episodes(content, season):	
	result = []
	s = http.initSession()	
	soup = BeautifulSoup(s.get(season['url'], timeout=_timeout).text, "html.parser")
	for tag in soup.find('div', class_="seasons-item").find_all('li'):								
		item = {			
			'title': tag.find('div', class_='card-title').get_text().lstrip().rstrip(),
			'thumb': tag.find('img')['src'], 
			'plot': tag.find('div', class_='card-text').get_text().lstrip().rstrip(),
			'url': _url + tag.find('a')['href']
		}				
		try:
			item['id'] = item['title'].split('פרק ')[1]
			if ' ' in item['id']:
				item['id'] = item['id'].split(' ')[0]
			item['id'] = int(item['id'])
		except:
			item['id'] = 0
#			continue
		result.append(item)		
	return result
	
	
def urlencode(url):	
	return 'https://' + urllib.parse.quote(url.replace('https://',''))
	
	
def stream(content, season=None, episode=None):		
	s = http.initSession()			
	tag = BeautifulSoup(s.get(urlencode(episode['url']), timeout=_timeout).text, "html.parser").find('div', class_="data-kaltura-player")
	data = {
		"1":{"service":"session","action":"startWidgetSession","widgetId":"_2717431"},
		"2":{"service":"baseEntry","action":"list","ks":"{1:result:ks}","filter":{"redirectFromEntryId":tag['data-entryid']},"responseProfile":{"type":1,"fields":"id,referenceId,name,description,thumbnailUrl,dataUrl,duration,msDuration,flavorParamsIds,mediaType,type,tags,dvrStatus,externalSourceType,status,createdAt,endDate,plays,views"}},
		"3":{"service":"baseEntry","action":"getPlaybackContext","entryId":"{2:result:objects:0:id}","ks":"{1:result:ks}","contextDataParams":{"objectType":"KalturaContextDataParams","flavorTags":"all"}},
		"4":{"service":"metadata_metadata","action":"list","filter":{"objectType":"KalturaMetadataFilter","objectIdEqual":"{2:result:objects:0:id}","metadataObjectTypeEqual":"1"},"ks":"{1:result:ks}"},
		"apiVersion":"3.3.0",
		"format":1,
		"ks":"",
		"clientTag":"html5:v3.14.8",
		"partnerId": tag['data-partnerid']
	}
	data = s.post('https://cdnapisec.kaltura.com/api_v3/service/multirequest', json=data, headers = {'Referer': _url + '/'}, timeout=_timeout).json()	
	url = [source['url'] for source in data[2]['sources'] if source['format']=='applehttp'][0]
	result = media.parseM3U8(s.get(url, timeout=_timeout).text, url)		
	result = sorted(result, key=lambda item: int(item['bandwidth']), reverse=True)
	result = [{'title': item.get('resolution', item['bandwidth']), 'url': item['url'] } for item in result]
	from . import common
	return common.select(content, result, 'Streams', 'stream')
	