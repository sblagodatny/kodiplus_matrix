# -*- coding: utf-8 -*-

import requests
from bs4 import BeautifulSoup
from helpers import http, gui, storage, media
import json




_url = 'https://www2.watchserieshd.stream'

_timeout = 4

	
def search(criteria):		
	if 'en-US' not in criteria['titles'].keys():
		return []
	s = http.initSession()
	result = []	
	content = BeautifulSoup(s.get(_url, params={'s': criteria['titles']['en-US'].replace(' ','+')}, headers={'Referer': _url + '/'}, timeout=_timeout).text,  "html.parser")
	
	for tag in content.find_all('div', class_='item'):		
		item = {
			'id': tag.find('a', class_='title')['href'].split('/')[-2],
			'title': tag.find('a', class_='title')['title'],								
			'url': tag.find('a', class_='title')['href'],
			'type': 'tv' if tag.find('i', class_="type").get_text()!='Movie' else 'movie',			
			'poster': 'https:' + tag.find('a', class_="poster").find('img')['src']
		}
		if ' (' in item['title']:
			try:
				item['year']=str(int(item['title'].split(' (')[1].split(')')[0]))
				item['title']=item['title'].replace(' ({})'.format(item['year']),'')		
			except:
				None
		result.append(item)		
	return [item for item in result if item['type']==criteria['type']]
	

def seasons(content):
	s = http.initSession()
	soap = BeautifulSoup(s.get(content['url'], timeout=_timeout).text,  "html.parser")
	result = []
	for tag in soap.find_all('div', class_='season-single'):		
		season = {			
			'title': tag.find('span', class_="title").get_text(),
			'date': tag.find('span', class_="date-span").get_text(),
			'episodes': []
		}
		season['title'] = season['title'].split(season['date'])[0].rstrip()
		for etag in tag.find('div', class_="episodes-list").find_all('li'):
			episode = {
				'url': etag.find('a')['href'],
				'title': etag.find('a').get_text()
			}
			episode['id'] = episode['url'].split('-')[-1].replace('/','')
			season['id'] = episode['url'].split('-')[-3]
			season['episodes'].append(episode)				
		result.append(season)				
	return result
	
	
	
def episodes(content, season):	
	return season['episodes']
	
		
def stream(content, season=None, episode=None):	
	import re
	from . import common
	s = http.initSession()	
	if episode is None:
		html = s.get(content['url'], timeout=_timeout).text
		tags = BeautifulSoup(html,  "html.parser").find_all('div', class_='button-play')
	else:
		html = s.get(episode['url'], timeout=_timeout).text
		tags = BeautifulSoup(html,  "html.parser").find('ul', class_="servers").find_all('li')						
	servers = []
	url = None
	for tag in tags:
		if tag.get_text().lstrip().rstrip().lower() in ['doodstream','filemoon']:
			servers.append({'title': tag.get_text().lstrip().rstrip().lower(), 'url': tag['data-vs']})			
	if len(servers)==0:
		raise Exception('No available options found for Servers')
	server=common.select(content, servers, 'Servers', 'server')	
	from . import cdn
	parser = cdn.get(server['title'])	
	stream = parser.parse(server['url'], content['url'])			
	if 'm3u8' not in stream['url']:	
		stream['title']='dummy'
		return stream				
	result = media.parseM3U8(s.get(stream['url'], headers=stream.get('headers',None), timeout=_timeout).text, stream['url'])			
	result = sorted(result, key=lambda item: int(item['bandwidth']), reverse=True)
	result = [{'title': item.get('resolution', item['bandwidth']), 'url': item['url'] } for item in result]	
	result = common.select(content, result, 'Streams', 'stream')	
	if 'headers' in stream.keys():
		result['headers']=stream['headers']
	return result
	
#	try:
#		subs = 'https://cloudvid' + re.search(r"'https://cloudvid(.*?)'", html).group(1)
#		subs = s.get(subs, timeout=_timeout).json()						
#		subs = [{'label': sb['label'], 'url': sb['file'], 'source': 'original', 'format': 'vtt', 'zip': False} for sb in subs]
#		for sub in subs:
#			sub['language']=sub['label']
#			for meta in ['-','(']:
#				if meta in sub['language']:
#					sub['language']=sub['language'].split(meta)[0].rstrip()				
#		result['subtitles'] = subs
#	except Exception as e:				
#		None				
	
	

