def scriptLogin(args):	
	import xbmcaddon, xbmcvfs, xbmcgui	
	from contextlib import closing	
	import time, json
	import sys
	pathAddon = xbmcaddon.Addon('script.module.vod').getAddonInfo('path')
	pathSettings = pathAddon.replace('addons','userdata/addon_data')
	pathAccounts = pathSettings + '/accounts/'
	if not xbmcvfs.exists(pathAccounts):
		xbmcvfs.mkdirs(pathAccounts)
	pathAccount = pathAccounts + args[0] + '.json'
	sys.path.insert(1, pathAddon + '/lib')
	
	if xbmcvfs.exists(pathAccount):
		with closing(xbmcvfs.File(pathAccount, 'r')) as f:
			account = json.load(f)
		if not xbmcgui.Dialog().yesno(args[0], 'Authorization was already done as {}'.format(account['username']) + "\n" 'Press Yes to Re-Authorize'):
			return			
	username=xbmcgui.Dialog().input(args[0] + ' username')	
	if len(username)==0:
		return
	password=xbmcgui.Dialog().input(args[0] + ' password')	
	if len(password)==0:
		return			
	try:
		import vod
		cookies = vod.getLib(args[0]).login(username, password)
	except Exception as e:
		xbmcgui.Dialog().ok(args[0], 'Unable to login' + "\n" + str(e))
		return			
	with closing(xbmcvfs.File(pathAccount, 'w')) as f:
		json.dump({'username': username, 'password': password, 'cookies': cookies, 'ts': time.time()}, f, indent=4)
	xbmcgui.Dialog().ok(args[0], 'Logged in successfully as {}'.format(username))



globals()['script' + sys.argv[1]](sys.argv[2:])
