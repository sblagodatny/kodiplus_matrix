# coding: utf-8

import sys
import xbmc
import xbmcgui
import xbmcplugin
import xbmcaddon
import urllib.parse as parse

_baseUrl = sys.argv[0]
_handleId = int(sys.argv[1])
_params = dict(parse.parse_qsl(sys.argv[2][1:]))	

_addon = xbmcaddon.Addon()
_pathAddon = _addon.getAddonInfo('path')
_pathSettings = _pathAddon.replace('addons','userdata/addon_data')
_pathImg = _pathAddon + '/resources/img/'

if 'handler' not in list(_params.keys()):
	_params['handler'] = 'Root'


def listContent(contentlist):
	import xbmcplugin, xbmcgui, json
	from helpers import string, storage	
	xbmcplugin.setContent(_handleId, 'movies')		
	for content in contentlist:		
		item = buildListItem({'label': content['title'], 'thumb': content.get('poster',''), 'fanart': content.get('poster','')})							
		if content['type']=='tv':
			item.setLabel('[COLOR FF2BB7FC]{}[/COLOR]'.format(item.getLabel()))
		info = item.getVideoInfoTag()
		info.setMediaType('movie' if content['type']=='movie' else 'tvshow')
		info.setOriginalTitle(content.get('titleOriginal',''))
		info.setPlot("[B]Source: {}[/B]\n".format(content['source']) + content.get('plot',''))
#		info.setPremiered(content.get('releaseDate',''))
#		info.setYear(int(data['year']))	
		info.setGenres(content.get('genre',[]))
		info.setCountries(content.get('country',[]))
		if content['type'] == 'tv':
			item.setIsFolder(True)	
			params = {'handler': 'Seasons', 'content': string.b64encode(json.dumps(content)) }
		else:
			item.setIsFolder(False)	
			item.setProperty("IsPlayable", 'true')		
			params = {'handler': 'Play', 'content': string.b64encode(json.dumps(content)) }		
		xbmcplugin.addDirectoryItem(_handleId, _baseUrl+'?' + parse.urlencode(params), item, item.isFolder())		
	xbmcplugin.endOfDirectory(_handleId)


def handlerSeasons():
	from helpers import string
	import json
	import vod
	content = json.loads(string.b64decode(_params['content']))		
	
	try:	
		seasons = vod.getLib(content['source']).seasons(content)
	except Exception as e:
		import requests
		if isinstance(e, requests.ConnectionError):
			xbmcgui.Dialog().ok('VOD', '{} not avaialble'.format(content['source']))
		elif 'cancelled' not in str(e):
			xbmcgui.Dialog().ok('VOD', '{}: {}'.format(content['source'], str(e)))
		xbmcplugin.endOfDirectory(_handleId)	
		return
		
	xbmcplugin.setContent(_handleId, 'tvshows')
	for season in seasons:															
		item = buildListItem({'label': '{} - {}'.format(content['title'], season['title']), 'thumb': content.get('poster',''), 'fanart': content.get('poster','')})
		info = item.getVideoInfoTag()
		info.setMediaType('season')		
		info.setOriginalTitle(content.get('titleOriginal',''))
		info.setPlot(content.get('plot',''))						
		info.setSeason(int(season['id']))	
#		item.setProperty('TotalEpisodes',str(season['episodes']) )
#		if season['episodes']==0:
#			item.setLabel('[COLOR grey]{}[/COLOR]'.format(item.getLabel()))				
		params = {'handler': 'Episodes', 'content': string.b64encode(json.dumps(content)), 'season': string.b64encode(json.dumps(season))} 
		xbmcplugin.addDirectoryItem(_handleId, _baseUrl+'?' + parse.urlencode(params), item, True)		
	xbmcplugin.endOfDirectory(_handleId, cacheToDisc=False)	
	
	
def handlerEpisodes():
	from helpers import string
	import json
	import vod
	content = json.loads(string.b64decode(_params['content']))									
	season = json.loads(string.b64decode(_params['season']))
	
	try:	
		episodes = vod.getLib(content['source']).episodes(content, season )			
	except Exception as e:
		import requests
		if isinstance(e, requests.ConnectionError):
			xbmcgui.Dialog().ok('VOD', '{} not avaialble'.format(content['source']))
		elif 'cancelled' not in str(e):
			xbmcgui.Dialog().ok('VOD', '{}: {}'.format(content['source'], str(e)))
		xbmcplugin.endOfDirectory(_handleId)	
		return	
	
	xbmcplugin.setContent(_handleId, 'episodes')		
	for episode in episodes:			
		item = buildListItem({'label': '{} - {} - {}'.format(content['title'], season['title'], episode['title']),'thumb': episode.get('thumb','')})			
		info = item.getVideoInfoTag()
		info.setMediaType('episode')		
#		info.setOriginalTitle(content.get('titleOriginal',''))
		info.setPlot(episode.get('plot',''))
		info.setPremiered(episode.get('releaseDate',''))		
#		info.setGenres(content.get('genre',[]))
#		info.setCountries(content.get('country',[]))					
		info.setSeason(int(season['id']))	
		info.setEpisode(int(episode['id']))	
		info.setDuration(episode.get('duration',0))							
		params = {'handler': 'Play', 'content': string.b64encode(json.dumps(content)), 'season': string.b64encode(json.dumps(season)), 'episode': string.b64encode(json.dumps(episode)) }				
		item.setProperty("IsPlayable", 'true')		
		xbmcplugin.addDirectoryItem(_handleId, _baseUrl+'?' + parse.urlencode(params), item, False)				
	xbmcplugin.endOfDirectory(_handleId)	
	
	
def handlerSearch():	
	from helpers import string, locale
	import json
	import vod
	criteria = json.loads(string.b64decode(_params['criteria']))
	criteria['titles']={locale.getLanguage(criteria['title']): criteria['title']}
	del criteria['title']
	try:		
		result = vod.search(criteria)
		xbmc.executebuiltin('Dialog.Close(busydialognocancel,true)') 
		listContent(result)
	except:
		xbmc.executebuiltin('Dialog.Close(busydialognocancel,true)') 
		raise		
	

def handlerSearchInput():	
	from helpers import gui, string
	import json
#	text=gui.inputWithHistory('Search', _pathSettings + 'historySearch_' + _params['type'], int(_addon.getSetting('historySize')))	
	text=gui.inputWithHistory('Search', _pathSettings + 'historySearch_' + _params['type'], 15)	
	if text is None or len(text) < 1:
		return	
	params = {
		'handler': 'Search',
		'criteria': string.b64encode(json.dumps({'type': _params['type'], 'title': text}))		
	}	
	xbmc.executebuiltin("Dialog.Close(all, true) ")
	cmd = 'ActivateWindow(Videos, {}, return)'.format(_baseUrl+'?' + parse.urlencode(params))
	xbmc.executebuiltin(cmd)
		

def handlerPlay():
	from helpers import player, string, storage
	import json
	import vod
	content = json.loads(string.b64decode(_params['content']))		
	try:
		if 'episode' in _params.keys():
			season = json.loads(string.b64decode(_params['season']))			
			episode = json.loads(string.b64decode(_params['episode']))			
			stream = vod.getLib(content['source']).stream(content, season, episode)			
		else:
			stream = vod.getLib(content['source']).stream(content)			
	except Exception as e:
		import requests
		if isinstance(e, requests.ConnectionError):
			xbmcgui.Dialog().ok('VOD', '{} not avaialble'.format(content['source']))
		elif 'cancelled' not in str(e):
			xbmcgui.Dialog().ok('VOD', '{}: {}'.format(content['source'], str(e)))
		player.cancelResolvedUrl(_handleId) 
		return							
				
	if 'url' in stream.keys():		
		item = xbmcgui.ListItem(offscreen=True)	
		item.setPath(player.buildUrl(stream))	
		xbmcplugin.setResolvedUrl(handle=_handleId, succeeded=True, listitem=item) 	
	
	elif 'youtube' in stream.keys():
		from youtube import web, common
		item = xbmcgui.ListItem()
		video = web.getVideo(stream['youtube'], None)				
		server = common.startMPDProxyServer(common.getDashManifest(video, None, vp9=eval(_addon.getSetting('vp9').capitalize())), int(_addon.getSetting('dashProxyPort')))
		item.setProperty('inputstream', 'inputstream.adaptive')
		item.setProperty('inputstream.adaptive.manifest_type', 'mpd')
		item.setMimeType('application/dash+xml')	
		item.setPath('http://{}:{}'.format('localhost', _addon.getSetting('dashProxyPort')))	
		xbmcplugin.setResolvedUrl(handle=_handleId, succeeded=True, listitem=item) 	
		xbmc.sleep(1500)
		server.shutdown()
		xbmc.sleep(1000)
		
		
def handlerAuth():	
	from helpers import gui
	import time
	import xbmcvfs
	import xbmcgui
	from contextlib import closing	
	path = _pathAccounts + '/filmix.json'	
	if xbmcvfs.exists(path):
		with closing(xbmcvfs.File(path, 'r')) as f:
			account = json.load(f)
		if not xbmcgui.Dialog().yesno(_params['source'], 'Authorization was already done as {}'.format(account['username']) + "\n" 'Press Yes to Re-Authorize'):
			return	
	username=gui.input(_params['source'] + ' Username')	
	if username is None  or len(username)==0:
		return
	password=gui.input(_params['source'] + ' Password')	
	if password is None or len(password)==0:
		return			
	try:
		cookies = vod.getLib(_params['source']).login(username, password)
	except Exception as e:
		xbmcgui.Dialog().ok(_params['source'], 'Unable to login' + "\n" + str(e))
		return			
	with closing(xbmcvfs.File(path, 'w')) as f:
		json.dump({'username': username, 'password': password, 'cookies': cookies, 'ts': time.time()}, f, indent=4)
	xbmcgui.Dialog().ok(_params['source'], 'Logged in successfully as {}'.format(username))
	

def buildListItem(data):	
	import xbmcgui
	li = xbmcgui.ListItem(data['label'])
	if 'thumb' in data.keys() and data['thumb'] is not None and len(data['thumb'])>0:
		li.setArt({'thumb': data['thumb']})
	if 'fanart' in data.keys() and data['fanart'] is not None and len(data['fanart'])>0:
		li.setArt({'fanart': data['fanart']})
	return li


def handlerRoot():				
	item=buildListItem({'label': 'Search Series', 'thumb': _pathImg + 'collection.png', 'fanart': _pathAddon + 'fanart.jpg'})
	xbmcplugin.addDirectoryItem(handle=_handleId, url=_baseUrl+'?'+parse.urlencode({'handler': 'SearchInput', 'type': 'tv'}), isFolder=False, listitem=item)
	item=buildListItem({'label': 'Search Movies', 'thumb': _pathImg + 'video.png', 'fanart': _pathAddon + 'fanart.jpg'})
	xbmcplugin.addDirectoryItem(handle=_handleId, url=_baseUrl+'?'+parse.urlencode({'handler': 'SearchInput', 'type': 'movie'}), isFolder=False, listitem=item)
	xbmcplugin.endOfDirectory(_handleId)


globals()['handler' + _params['handler']]()
