from . import common

import xbmcaddon, xbmcvfs

_pathSettings = xbmcaddon.Addon('script.module.lyrics').getAddonInfo('path').replace('addons','userdata/addon_data')
if not xbmcvfs.exists(_pathSettings):
	xbmcvfs.mkdir(_pathSettings)
_pathToken = _pathSettings + 'musixmatch_token.json'

_urlSearch = 'https://apic-desktop.musixmatch.com/ws/1.1'
_headers = {
	"authority": "apic-desktop.musixmatch.com",
	"cookie": "AWSELBCORS=0; AWSELB=0",
	"User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0",
}




def getToken():
	import time, json	
	if xbmcvfs.exists(_pathToken):
		f = xbmcvfs.File(_pathToken, 'r')				 
		data = json.load(f)
		f.close()
		if data['expiration_time'] > int(time.time()):
			return data['token']			           					
	session = common.initSession()	
	params = {
		'user_language': 'en',
		'app_id': 'web-desktop-app-v1.0',
		't': int(time.time())	
	}
	result = session.get(url=_urlSearch + '/token.get', params=params, headers=_headers, timeout=5).json()		
	if 'message' in result and 'body' in result["message"] and 'user_token' in result["message"]["body"]:
		data = {'token': result["message"]["body"]["user_token"], 'expiration_time': params['t'] + 600}
		f = xbmcvfs.File(_pathToken, 'w')	              			
		json.dump(data, f)
		f.close()
		return data['token']		
	else:
		raise Exception('Unable to get token')		
		
		
def get(title,artist,synced=True,unsynced=True):		
	import time, json	
	result = {'synced': None, 'unsynced': None}
	
	
	try:		
		session = common.initSession()		
		token = getToken()
		
		if synced:
			
			try:
				params = {
					'q_track': title,
					'q_artist': artist,			
					'app_id': 'web-desktop-app-v1.0',
					'usertoken': token,
					't': int(time.time())	
				}	
				data = session.get(url=_urlSearch + '/matcher.subtitle.get', params=params, headers=_headers, timeout=5).json()		
				result['synced'] = common.parseLRC(data['message']['body']['subtitle']['subtitle_body'])
			except:
				None
		
		if unsynced:	
			try:
				params = {
					'q_track': title,
					'q_artist': artist,			
					'app_id': 'web-desktop-app-v1.0',
					'usertoken': token,
					't': int(time.time())	
				}	
				data = session.get(url=_urlSearch + '/matcher.lyrics.get', params=params, headers=_headers, timeout=5).json()		
				result['unsynced'] = data['message']['body']['lyrics']['lyrics_body']
			except:
				None
		
		
		return result
		
	except:
		return result