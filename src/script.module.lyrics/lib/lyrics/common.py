
def log(msg, level=None):
	import xbmc
	if level is None:
		level = xbmc.LOGINFO
	xbmc.log(msg, level=level)


def initSession():
	import requests
	s = requests.Session()
	s.verify = False
	s.headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'
	return s


def getLanguage(text):
	languages = [
		{'chars': 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя', "hl":"ru"},
		{'chars': 'אבגדהוזחטיכלמנסעפצקרשת', "hl":"iw"}
	]
	for language in languages:
		for i in range(len(language['chars'])):
			if language['chars'][i] in text:
				return language['hl']
	return 'en'
	
def parseLRC(lrc):
	result = []
	for line in lrc.split("\n"):
		item = {'start': line[1:9].split(':'),	'text': line[11:]}
		item['start'] = round ( float(item['start'][-1]) + float(item['start'][-2])*60 , 2)
		result.append(item)
	for i in range(len(result)-1):
		result[i]['end']=result[i+1]['start']
	result[-1]['end'] = result[-1]['start']+10
	return result	
	