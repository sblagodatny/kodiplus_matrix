import xbmcaddon
from helpers import http, storage
import json

_url = 'http://127.0.0.1:{}'.format(xbmcaddon.Addon('script.module.torrent').getSetting('torrServePort')) 
_timeout = 2


def load(torrent):		
	import os, xbmc
	from . import common
	session = http.initSession()	
	if 'file' in torrent.keys():	
		if torrent['file'].startswith('http'):
			data = session.get(torrent['file'], headers=torrent.get('headers',None), cookies=torrent.get('cookies',None), timeout=_timeout).content
		else:	
			with open(torrent['file'], 'rb') as f:
				data = f.read()
		files = {'file': ('dummy', data, 'application/x-bittorrent')}
		reply=session.post(_url + '/torrent/upload', files=files, timeout=_timeout).json()
	elif 'magnet' in torrent.keys():	
		reply = session.post(_url + '/torrents', json={"action": "add", "link": torrent['magnet'], "title":"", "poster":"", "save_to_db":False}, timeout=_timeout).json()
	elif 'hash' in torrent.keys():
		reply = session.post(_url + '/torrents', json={"action": "add", "link": common.hash2magnet(torrent['hash']), "title":"", "poster":"", "save_to_db":False}, timeout=_timeout).json()			
	return reply['hash']
	

def unload(hash):		
	session = http.initSession()
	session.post(_url + '/torrents', json={"action": "rem", "hash": hash}, timeout=_timeout)
	
def preload(hash, fileId):
	session = http.initSession()
	try:
		session.get(_url + '/stream?link={}&index={}&preload'.format(hash, fileId), timeout=1)
	except:
		None
	
def getTorrent(hash):
	session = http.initSession()
	return session.post(_url + '/torrents', json={'action': 'get', 'hash': hash}, timeout=_timeout).json()
	
def getCache(hash):
	session = http.initSession()
	return session.post(_url + '/cache', json={'action': 'get', 'hash': hash}, timeout=_timeout).json()
	

def getSettings():
	session = http.initSession()
	return session.post(_url + '/settings', json={"action":"get"}, timeout=_timeout).json()

	
def getStream(hash, videoFileSeq=1):
	import xbmcgui, xbmc
	d = xbmcgui.DialogProgress()
	d.create('TorrServe', 'Waiting for stream')	
	try:				
		file = None
		while True:	
			if d.iscanceled() or xbmc.Monitor().abortRequested():
				raise Exception('Wait for stream was cancelled')			
			if file is None:
				torrent = getTorrent(hash)
				seq = 0
				if 'file_stats' in torrent.keys():
					for f in torrent['file_stats']:
						if f['path'].split('.')[-1].lower() in ['avi','mkv','mp4','m4v','ts','webm']:
							seq = seq + 1
							if seq == videoFileSeq:
								file = f
								break
					if file is None:
						raise Exception('No video file found')		
					preload(hash, file['id'])						
			else:				
				cache = getCache(hash)
				progress = int(cache['Torrent'].get('preloaded_bytes',0)/cache['Torrent'].get('preload_size',1)*100)
				if progress > 99:
					progress = 99
				d.update(progress, 'Waiting for stream' + "\n\n" + 'Download Speed: {} Kbps'.format(int(cache['Torrent'].get('download_speed',0)/1024)) )
				if cache['Torrent']['stat']==3:
					break			
			xbmc.sleep(1000)		
		d.close()
		del d
		filename = file['path']
		if '/' in filename:
			filename = filename.split('/')[-1]
		return {'url': _url + '/stream?link={}&index={}&play'.format(hash, file['id']), 'filename': filename}	
	except:
		d.close()
		del d
		raise	
