

def parse_blist(bdata):
    blist = []
    if bdata[0:1] == b'l':
        bdata = bdata[1:]
    while bdata[0:1] != b'' and bdata[0:1] != b'e':     
        parse_func = btype_dict.get(bdata[0:1], parse_bstring)
        elem, bdata = parse_func(bdata) 
        blist.append(elem)
    return blist, bdata[1:]


def parse_bdict(bdata):
    bdict = {}
    bdata = bdata[1:]
    while bdata[0:1] != b'' and bdata[0:1] != b'e':
        key, bdata = parse_bstring(bdata)
        parse_func = btype_dict.get(bdata[0:1], parse_bstring)
        value, bdata = parse_func(bdata)		
        bdict[key] = value
    return bdict, bdata[1:]


def parse_bint(bdata):
    end_pos = bdata.index(b'e')
    num_str = bdata[1:end_pos]
    num_str = bdata[1:end_pos]
    bdata = bdata[end_pos + 1:]
    return int(num_str), bdata


def parse_bstring(bdata):
    delim_pos = bdata.index(b':')
    length = bdata[0:delim_pos]
    length = int(length) 
    delim_pos += 1
    bstring = bdata[delim_pos:delim_pos + length]
    bdata = bdata[delim_pos + length:]
    return bstring, bdata


def decode(bdata):
    return parse_blist(bdata)
    

btype_dict = {
    b'd' : parse_bdict,
    b'l' : parse_blist,
    b'i' : parse_bint
}

