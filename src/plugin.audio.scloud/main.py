# coding: utf-8

import sys, xbmcaddon, xbmcvfs
import urllib.parse as parse


_baseUrl = sys.argv[0]
_handleId = int(sys.argv[1])
_params = dict(parse.parse_qsl(sys.argv[2][1:]))	
if 'handler' not in _params.keys():
	_params = {'handler': 'Root'}
	
_addon = xbmcaddon.Addon()
_pathAddon = _addon.getAddonInfo('path')
_pathSettings = _pathAddon.replace('addons','userdata/addon_data')
_pathImg = _pathAddon + '/resources/img'
_pathAccount = _pathSettings + 'accounts.json'
	

if not xbmcvfs.exists(_pathSettings):
	xbmcvfs.mkdir(_pathSettings)


def handlerPlay():
	from lib import soundcloud
	from helpers import string, account, storage
	import xbmc, xbmcgui, json, xbmcplugin
	track = json.loads(string.b64decode(_params['track']))
	item = xbmcgui.ListItem()
	url = soundcloud.resolveTrackUrl(track['url'], account.get(_pathAccount))
	item.setPath(url)
	xbmcplugin.setResolvedUrl(handle=_handleId, succeeded=True, listitem=item) 



def builtTrackListItem(t):
	from helpers import string
	import json
	item=buildListItem({'label': t['title'], 'thumb': t['thumb']})			
	item.setPath(_baseUrl+'?' + parse.urlencode({'handler': 'Play', 'track': string.b64encode(json.dumps(t)) }))		
	item.setIsFolder(False)				
	item.setProperty("IsPlayable", 'true')				
	info = item.getMusicInfoTag()
	info.setMediaType('song')					
	info.setTitle(t['title'])
	info.setDuration(t['duration'])
	info.setComment('User: ' + t['user']['name'])
	return item
	

def listTracks(tracks):	
	import xbmcplugin, json
	xbmcplugin.setContent(_handleId, 'songs')		
	items=[]	
	for t in tracks:						
		item = builtTrackListItem(t)
		items.append((item.getPath(), item, item.isFolder(),))		
	xbmcplugin.addDirectoryItems(handle=_handleId, items=items, totalItems=len(items))	
	xbmcplugin.endOfDirectory(_handleId)



def listPlaylists(playlists):	
	from helpers import string, account
	import xbmcplugin, json
	items=[]	
	xbmcplugin.setContent(_handleId, 'albums')
	for p in playlists:						
		item = buildListItem({'label': p['title'], 'thumb': p['thumb']})
		item.setPath(_baseUrl+'?' + parse.urlencode({'handler': 'ListPlaylist', 'playlist': string.b64encode(json.dumps(p)) }))		
		item.setIsFolder(True)	
		info = item.getMusicInfoTag()
		info.setComment("Tracks: {}\nUser: {}".format(p['items'], p['user']['name']))	
		items.append((item.getPath(), item, item.isFolder(),))		
	xbmcplugin.addDirectoryItems(handle=_handleId, items=items, totalItems=len(items))	
	xbmcplugin.endOfDirectory(_handleId)


def handlerListPlaylist():
	from lib import soundcloud
	from helpers import string
	import json
	from helpers import account		
	playlist = json.loads(string.b64decode(_params['playlist']))
	data = soundcloud.getPlaylistItems(playlist['id'], account.get(_pathAccount))
	listTracks(data)
	



def handlerSearch():
	from lib import soundcloud
	from helpers import account	
	data = soundcloud.search(_params['text'], _params['category'], account.get(_pathAccount))	
	if _params['category'] == 'tracks':
		listTracks(data)
	elif _params['category']=='playlists_without_albums':
		listPlaylists(data)
	

def handlerSearchInput():
	import xbmc, xbmcgui
	d = xbmcgui.Dialog()	
	items = [
		{'label': 'Tracks', 'thumb': _pathImg + '/song.png', 'category': 'tracks'},
		{'label': 'Playlists', 'thumb': _pathImg + '/list.png', 'category': 'playlists_without_albums'}
	]	
	s = d.select('SoundCloud Search', [buildListItem(item) for item in items], useDetails=True)
	if s == -1:
		return
	item = items[s]
	from helpers import gui
	text=gui.inputWithHistory('SoundCloud {}'.format(item['label']), _pathSettings + 'history_search_{}'.format(item['label']))	
	if text is None:
		return	
	params = {
		'handler': 'Search',		
		'category': item['category'],
		'text': text		
	}	
	xbmc.executebuiltin("Dialog.Close(all, true) ")
	cmd = 'ActivateWindow(Music, {}, return)'.format(_baseUrl+'?' + parse.urlencode(params))
	xbmc.executebuiltin(cmd)


def buildListItem(data):	
	import xbmcgui
	li = xbmcgui.ListItem(data['label'])
	li.setArt({'thumb': data['thumb']})
	if 'fanart' in data.keys():
		li.setArt({'fanart': data['fanart']})
	return li


def handlerLibrary():
	from lib import soundcloud
	from helpers import account
	data = soundcloud.getLibrary(account.get(_pathAccount))
	listPlaylists(data)
	

def handlerAccount():
	from lib import soundcloud
	from helpers import account
	account.open(_pathAccount, soundcloud.validateAccount, 'SoundCloud Accounts', mode='file', fileMask='*.json')

		
def handlerRoot():	
	import xbmcplugin
	items=[		
		{'label': 'Search', 'path': _baseUrl +'?' + parse.urlencode({'handler': 'SearchInput'}), 'thumb': _pathImg + '/search.png', 'fanart': _pathAddon + '/fanart.jpg', 'folder': False},
		{'label': 'Playlists', 'path': _baseUrl +'?' + parse.urlencode({'handler': 'Library'}), 'thumb': _pathImg + '/list.png', 'fanart': _pathAddon + '/fanart.jpg', 'folder': True},
		{'label': 'Account', 'path': _baseUrl +'?' + parse.urlencode({'handler': 'Account'}), 'thumb': _pathImg + '/user.png', 'fanart': _pathAddon + '/fanart.jpg', 'folder': False}		
	]
	for item in items:
		xbmcplugin.addDirectoryItem(_handleId, item['path'], buildListItem(item), item['folder'])
	xbmcplugin.endOfDirectory(_handleId)

globals()['handler' + _params['handler']]()
