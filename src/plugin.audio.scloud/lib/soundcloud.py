def get(route, params={}, account=None):
	headers = {'Authorization': account['Authorization'], 'Referer': 'https://soundcloud.com/'}
	cparams = {'client_id': 'vFpIrv8xcvgvWbpDKER2JNBXZTTR4jw2', 'app_version': '1712059351', 'app_locale': 'en'}
	cparams.update(params)		
	from helpers import http
	session = http.initSession()	
	return session.get('https://api-v2.soundcloud.com/' + route, params=cparams, headers=headers).json()
		
		
def validateAccount(account):	
	import json
	try:	
		account.update(json.loads(account['content']))						
		del account['content']				
		data = get('me', account=account)			
		account['title'] = data['username']
		account['thumb'] = data['avatar_url']
	except:
		raise Exception('Invalid OAUTH file')			
	
		
def getLibrary(account):
	result = []
	data = get('me/library/all', {'limit': '20', 'offset': '0', 'linked_partitioning': '1'}, account)					
	for item in data['collection']:
		if not item['type']=='playlist':
			continue
		resitem = {
			'id': item['playlist']['id'],
			'title': item['playlist']['title'],
			'thumb': resizeImage(item['playlist']['artwork_url']),
			'items': item['playlist']['track_count'],
			'user': {'id': item['playlist']['user']['id'], 'name': item['playlist']['user']['full_name']}
		}
		if len(resitem['user']['name'])==0:
			resitem['user']['name']=item['playlist']['user']['username']
		result.append(resitem)
	return result
	

def resizeImage(url):
	if url is None:
		return None
	else:
		return url.replace('-large','-t500x500')
		

def parseTrack(item):
	result = {
		'id': item['id'],
		'thumb': resizeImage(item['artwork_url']) if item['artwork_url'] is not None else resizeImage(item['user']['avatar_url']),					
		'title': item['title'],
		'duration': int(item['duration']/1000),
		'url': item['media']['transcodings'][1]['url'],
		'user': {'id': item['user']['id'], 'name': item['user']['full_name']}
	}
	if len(result['user']['name'])==0:
		result['user']['name']=item['user']['username']
	return result

def parsePlaylist(item):
	result = {
		'id': item['id'],
		'title': item['title'],
		'thumb': resizeImage(item['artwork_url']),
		'items': item['track_count'],
		'user': {'id': item['user']['id'], 'name': item['user']['full_name']}		
	}
	artworks = [t['artwork_url'] for t in item['tracks'] if t.get('artwork_url',None) is not None]
	if result['thumb'] is None:
		if len(artworks)>0:
			result['thumb']=resizeImage(artworks[0])
		elif len(item['tracks'])>0:
			result['thumb']=resizeImage(item['user']['avatar_url'])
	if len(result['user']['name'])==0:
		result['user']['name']=item['user']['username']
	return result

	
def getPlaylistItems(playlistId, account):
	result = []
	data = get('playlists/{}'.format(playlistId), account=account)
	ids = [str(track['id']) for track in data['tracks']]
	data = get('tracks', {'ids': ','.join(ids)}, account=account)
	for item in data:
		result.append(parseTrack(item))
	return result


# entity: tracks, playlists_without_albums, albums
def search(text, entity, account):
	result = []
	data = get('search/{}'.format(entity), params={'q': text, 'limit': '20', 'offset': '0'}, account=account)	
	for item in data['collection']:
		if entity=='tracks':
			result.append(parseTrack(item))
		elif entity=='playlists_without_albums':
			result.append(parsePlaylist(item))
	return result
	
	
def resolveTrackUrl(url, account):
	headers = {'Authorization': account['Authorization'], 'Referer': 'https://soundcloud.com/'}
	from helpers import http
	session = http.initSession()		
	data = session.get(url, headers=headers).json()
	return data['url']